#!/bin/bash

export PWD=`pwd`
export DOCKER_FILES_PATH=$PWD/../docker-files/ponto/

TARGET=dev

if [[ $# > 0 && $1 != "" ]]; then
	TARGET=$1
fi;

docker-compose -f docker-compose-base.yml -f docker-compose-$TARGET.yml -p ponto stop
docker-compose -f docker-compose-base.yml -f docker-compose-$TARGET.yml -p ponto rm
docker-compose -f docker-compose-base.yml -f docker-compose-$TARGET.yml -p ponto up --build -d