FROM bylexus/apache-php55
ENV PHP_ERROR_REPORTING='E_ALL & ~E_STRICT & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED'
RUN apt-get update && apt-get install -y mysql-client git
#COPY ./ponto/ /var/www/html/ponto/
WORKDIR /var/www/html/ponto