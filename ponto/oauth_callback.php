<?php
/**
 * Created by PhpStorm.
 * User: TMP-mmendonca
 * Date: 09/02/2015
 * Time: 10:34
 */

require_once "./php-skydrive/src/functions.inc.php";
$response = skydrive_auth::get_oauth_token($_GET['code']);
if (skydrive_tokenstore::save_tokens_to_store($response)) {
    header("Location: index.php");
} else {
    echo "error";
}
?>