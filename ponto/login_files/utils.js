/**
 * Função para substiruir ponto por vazio e vírgula por ponto
 * 
 * @param numero
 * @returns
 */

function substituirPontoEVirgula(numero) {
	return numero.replace(/\./g, "").replace(",", ".");
};

/**
 * Função para truncar números decimais
 * ex: 2700.4586, 2
 * retorna 2700.45
 * @param numberString
 * @param trunk
 * @returns
 */
function truncate(numberString, trunk) {
	var onpoint = numberString.split(".", 2);
	var numberStringTruncated = numberString;
	if (onpoint.length > 1) {
		numberStringTruncated = onpoint[0] + '.'
				+ onpoint[1].substring(0, trunk);
	}
	return numberStringTruncated;
};

// Coloca o foco no primeiro controle habilitado
function focusFirst() {
	var oField;
	if (document.forms.length > 0) {
		for ( var i = 0; i < document.forms[0].elements.length; ++i) {
			oField = document.forms[0].elements[i];
			if ((oField.type != "hidden") && (oField.type != "submit")) {
				if (oField.id != "") {
					if (SetFocus(oField.id)) break;
				}
			}
		}
	}
}

// Coloca o foco em um controle da tela
function SetFocus(_sControlID) {
	if (_sControlID == "") {
		return false;
	}
	var oControl = document.getElementById(_sControlID);
	try {
		var bPodeSetarFoco = oControl != null && !oControl.disabled && oControl.className != "naoEditavel";
		if (bPodeSetarFoco) {
			var sStyle = oControl.getAttribute("style");
			if (sStyle && sStyle != "") {
				var a = sStyle.split(";") // Delimiter is a string
				for (var i = 0; i < a.length; i++) {
					var sSemEspaco = a[i].replace(/\s/g, '');
					if (sSemEspaco == "display:none") {
						return false;
					}
				}

			}
		}
		if (bPodeSetarFoco) {
			oControl.focus();
			return true;
		} else {
			return false;
		}
	} catch (e) {
		return false;
	}
}

// Apresenta mensagem Sim/Não
function msgBoxYesNo(_sMensagem) {
	execScript('n = msgbox("' + _sMensagem + '","4132","Responda")', "vbscript");
	return (n == 6);
}

function dispareClickBtn(sBotao) {
	var oBtn = document.getElementById(sBotao);
	if (oBtn == null) {
		return false;
	}
	if (oBtn.enabled == false) {
		return false;
	}
	if (oBtn.visible == false) {
		return false;
	}
	// oBtn.fireEvent('onClick');
	oBtn.click();
	return true;
}

function keyEnterClickBtn(e, sBtnID) {
	if (!e)
		e = window.event;
	var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;

	if (keyCode == 13) {
		dispareClickBtn(sBtnID);
		return false;
	}
	return true;
}

function keyEnterExecFuncao(e, sFuncao) {
	if (!e)
		e = window.event;
	var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;

	if (keyCode == 13) {
		window.setTimeout(sFuncao, 0);
		return false;
	}
	return true;
}

// Atualiza a página atual
function fun_atu_pagina() {
	location.reload(true);
}

// Abre uma Janela em Modal
function fun_modal(sUrl, sTitle) {
	var oWin = window;
	if (parent.GB_showPage) {
		oWin = parent
	}
	;
	if (!sTitle) {
		sTitle = oWin.document.title
	}
	;
	if (sTitle == "") {
		sTitle = oWin.document.title
	}
	;
	oWin.GB_showPage(sTitle, sUrl);
	return false;
}

// Abre uma Janela em Modal e atualiza a página atual quando a janela for
// fechada
function fun_modal_atualizar(sUrl, sTitle) {
	var oWin = window;
	if (parent.GB_showPage) {
		oWin = parent
	}
	;
	if (!sTitle) {
		sTitle = oWin.document.title
	}
	;
	if (sTitle == "") {
		sTitle = oWin.document.title
	}
	;
	oWin.GB_showPage(sTitle, sUrl, fun_atu_pagina);
	return false;
}

function abrirHtmlEmNovaJanela(_sElementoID, _sTituloJanela, _bImprimir) {
	var myWin = window.open("", "myWin",
			"menubar,scrollbars,left=30px,top=40px,height=400px,width=600px");
	var sConteudo = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'
			+ '<html><head>'
			+ '<link rel="stylesheet" href="../arq_table_sorter/css/blue/style.css" type="text/css" media="print, projection, screen" />'
			+ '<title>' + _sTituloJanela + '</title>' + '</head>';
	if (_bImprimir) {
		sConteudo = sConteudo + '<body onload="window.print();">';
	} else {
		sConteudo = sConteudo + '<body>';
	}
	sConteudo = sConteudo + '<div id="dest_div">' + '</div>' + '</body></html>';
	myWin.document.write(sConteudo);
	myWin.document.close();

	var oElemento = document.getElementById(_sElementoID);
	if (oElemento) {
		myWin.document.getElementById("dest_div").innerHTML += oElemento.outerHTML;
		return true;
	} else {
		myWin.document.getElementById("dest_div").innerHTML += "N?o foi encontrado o elemento para ser aprsentado: '"
				+ _sElementoID + "'";
		return false;
	}
}

function gerarNumeroAleatorio(inferior, superior) {
	numPossibilidades = superior - inferior;
	aleat = Math.random() * numPossibilidades;
	aleat = Math.floor(aleat);
	return parseInt(inferior) + aleat;
}

function gerarNumeroAleatorioTop(p_iTopNumber) {
	return gerarNumeroAleatorio(1, p_iTopNumber);
}

function getQuerystring(key, _sDefault) {
	return getQueryStringParameter(window.location.href, key, _sDefault);
}

function getQueryStringParameter(uri, key, _sDefault) {
	if (_sDefault == null)
		_sDefault = "";
	key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
	var qs = regex.exec(uri);
	if (qs == null) {
		return _sDefault;
	} else {
		return qs[1];
	}
}

function setQueryStringParameter(uri, key, value) {
	var re = new RegExp("([?|&])" + key + "=.*?(&|$)", "i");
	separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	} else {
		return uri + separator + key + "=" + value;
	}
}

function pintarComponentes(_sContainerName) {
	var i = 0;
	$("#" + _sContainerName).find("tr[id$='.linha']").each(function() {
		$(this).find('*').each(function() {			
			// pinta:
			if (i % 2 == 0) {
				this.className = 'odd';
			} else {
				this.className = 'even';
			}			
		});
		if (this.id.indexOf(".linha") > 0) {
			// apenas aumenta i se estivermos tratando uma linha
			i++;	
		}
	});
	return i--; // é necessário subtrair 1 pois o valor que i contém não representa o último objeto tratado
}

function salveRegistro(_sAcao) {
	dispareClickBtn('_action_save_and_' + _sAcao);
	
	return false;
}
function redimensioneIFrame(_sIFrameId) {
    var oFra = document.getElementById(_sIFrameId);
    var iHeight = 0;
    if (oFra) {
        if (isDocumentPermissionDenied(oFra.contentWindow) == false) {
            var oDocumentBody = oFra.contentWindow.document.body;
            if (oDocumentBody != null) {
                iHeight = oDocumentBody.scrollHeight;
                /*
                 if (mi_heightInicial == 0) {
                 mi_heightInicial = iHeight;
                 } else if (iHeight < mi_heightInicial) {
                 //iHeight = mi_heightInicial;
                 }
                 */
                oFra.height = iHeight;
                var oParent = oFra.parent;
                if (oParent) {
                    oFra.width = oParent.width;
                }
            }
        }
    }
}

function voltar() {
	dispareClickBtn('_action_voltar');
	return false;
}


function htmlExtrairHeadBody(data) {
	var sDataHead = "<style>" + htmlExtrairStyle(data) + "</style>";//htmlExtrairHead(data);
	var sDataBody = htmlExtrairBody(data);
	return sDataHead + sDataBody;
}
function htmlExtrairStyle(data) {
	var pattern = /<style[^>]*>((.|[\n\r])*)<\/style>/im;
	var matches = pattern.exec(data);
	if (matches){
		return matches[1];	// only extract body content
	} else {
		return '';
	}
}
function htmlExtrairScript(data) {
	var pattern = /<script[^>]*>((.|[\n\r])*)<\/script>/im;
	var matches = pattern.exec(data);
	if (matches){
		return matches[1];	// only extract body content
	} else {
		return '';
	}
}
function htmlExtrairHead(data) {
	var pattern = /<head[^>]*>((.|[\n\r])*)<\/head>/im;
	var matches = pattern.exec(data);
	if (matches){
		return matches[1];	// only extract body content
	} else {
		return data;
	}
}
function htmlExtrairBody(data) {
	var pattern = /<body[^>]*>((.|[\n\r])*)<\/body>/im;
	var matches = pattern.exec(data);
	if (matches){
		return matches[1];	// only extract body content
	} else {
		return data;
	}
}

function truncate(n) {
	return Math[n > 0 ? "floor" : "ceil"](n);
}

function navigatorIsMozilla() {
	return jQuery.browser.mozilla;
}
function navigatorIsIE() {
	return jQuery.browser.msie;
}
function navigatorIsSafari() {
	return jQuery.browser.safari;
}
function navigatorIsChrome() {
	return jQuery.browser.safari;
}
function navigatorIsOpera() {
	return jQuery.browser.opera;
}

function desabilitePostComEnter() {
	/* Desabilitando envio de formulário com enter */ 
	$(document).ready(function() {
		$(this).keydown(function(event){
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

	    	if(keyCode == 13) {
	    		event.preventDefault();
	    	}
		});
	});
}

function setMaskInputCPF_CNPJ(_oInput) {
    var campo = $(_oInput);
    if (campo.val().length <= 14) {
        campo.setMask("999.999.999-999");
    }
    else if (campo.val().length > 14) {
        campo.setMask("99.999.999/9999-99");
    }
}

/**
 * Generates a GUID string.
 * @returns {String} The generated GUID.
 * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
 * @author Slavik Meltser (slavik@meltser.info).
 * @link http://slavik.meltser.info/?p=142
 */
function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16)+"000000000").substr(2,8);
        return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

/**
 * Retorna os n Caracteres da esquerda de uma string informada
 * @param str string com os caracteres a serem retornados
 * @param n quantidade de caracteres a esquerda a serem retornados
 * @returns {string} n caracteres da esquerda de str
 */
function strLeft(str, n){
    if (n <= 0)
        return "";
    else if (n > String(str).length)
        return str;
    else
        return String(str).substring(0,n);
}
/**
 * Retorna os n Caracteres da direita de uma string informada 
 * @param str string com os caracteres a serem retornados
 * @param n quantidade de caracteres a direita a serem retornados
 * @returns {string} n caracteres da direita de str
 */
function strRight(str, n){
    if (n <= 0)
        return "";
    else if (n > String(str).length)
        return str;
    else {
        var iLen = String(str).length;
        return String(str).substring(iLen, iLen - n);
    }
}

/**
 * Recebe uma data em uma string no formato dd/MM/aaaa e retorna um objeto Date, considerando uma data válida e desconsiderando os valores inválidos para o dia e o mês
 * @param sData_dd_MM_AAAA data no formato dd/mm/aaaa
 * @returns {date} objeto do tipo Date com a data informada por parâmetro
 */
function parseStrToDate_ValidOrTrunc(sData_dd_MM_AAAA){// dd/mm/aaaa
	// Valores padrão: agora
	var oAgora = new Date();
	var year = oAgora.getFullYear();
	var month = strRight( '00' + (oAgora.getMonth()+1), 2);
	var day = strRight( '00' + oAgora.getDate(), 2);

	var arrData = sData_dd_MM_AAAA.split("/");
	if (arrData.length >= 1) {day = arrData[0];}
	if (arrData.length >= 2) {month = arrData[1];}
	if (arrData.length >= 3) {year = arrData[2];}

	// DIA
	if( (month==01) || (month==03) || (month==05) || (month==07) || (month==08) || (month==10) || (month==12) )    {//mes com 31 dias
		if(day < 01) day = '01';
		if(day > 31) day = '31';
	} else	if( (month==04) || (month==06) || (month==09) || (month==11) ){//mes com 30 dias
		if(day < 01) day = '01';
		if(day > 30) day = '30';
	} else if( (month==02) ){//February and leap year
		if( (year % 4 == 0) && ( (year % 100 != 0) || (year % 400 == 0) ) ){
			if(day < 01) day = '01';
			if(day > 29) day = '29';
		} else {
			if(day < 01) day = '01';
			if(day > 28) day = '28';
		}
	}
	// MÊS
	if (parseInt(month)< 1) month = 1;
	if (parseInt(month)> 12) month = 12;

	var oData = Date.parse(month + '/' + day + '/' + year);
	return oData;
}

/**
 * Recebe uma data em uma string no formato dd/MM/aaaa e retorna um objeto Date
 * @param sData_dd_MM_AAAA - data no formato dd/MM/aaaa
 * @returns {date} objeto do tipo Date com a data informada por parâmetro
 */
function parseStrToDate(sData_dd_MM_AAAA) {
    var asData = sData_dd_MM_AAAA.split('/');
    var t = Date.parse(asData[1] + '/' + asData[0] + '/' + asData[2]);
    return t;
}

function parseDateToStr(date){
    var y = date.getFullYear();
    var m = strRight( '00' + (date.getMonth()+1), 2);
    var d = strRight( '00' + date.getDate(), 2);
    return d+'/'+m+'/'+y;
}

function jQueryPrepareId(_sId) {
	var sReturn = _sId;
	//var sReturn = _sId.replace(".", "\\.").replace("[", "\\[").replace("]", "\\]");
	return sReturn;
}

function isParentPermissionDenied() {
    try {
        if(window.parent.qualquercoisa) {
            return false;
        }
        return false;
    }
    catch(err) {
        return true;
    }
}

function isDocumentPermissionDenied(_oWindow) {
    try {
        if(_oWindow.document) {
            return false;
        }
        return false;
    } catch(err) {
        return true;
    }
}

/**
 * Executa uma função fn apenas quando o seletor em _sSelector informado retornar algum elemento
 * @param _sSelector : string - expressão para ser usada como seletor para encontrar elementos
 * @param fn : function - função java script a ser executada
 * @param _nTotalTentativas : number - número de tentativas que deve serem feitas para encontrar os elementos que permitirão executar a função
 * @param _tempoDeEsperaParaNovaTentativa : number - tempo de espera em milisegundos para fazer cada consulta aos elementos
 * @returns {*} - retorna false, caso não seja executada ou o mesmo retorno da função executada
 */
function executarQuandoSeletorEncontrarElemento(_sSelector, fn, _nTotalTentativas, _tempoDeEsperaParaNovaTentativa) {
    var tick = _nTotalTentativas || 30;
    var nTempo = _tempoDeEsperaParaNovaTentativa || 100;
    if($(_sSelector).length == 0){
        if (tick > 1) {
            setTimeout(function() {
                executarQuandoSeletorEncontrarElemento(_sSelector, fn, tick - 1, _tempoDeEsperaParaNovaTentativa);
            }, nTempo)
        }
    } else {
        return fn();
    }
    return false;
}

/**
 * Executa uma função fn apenas quando todos os elementos de uma lista de seletores forem encontrados
 * @param _selectorsList : array - lista de expressões para serem usadas como seletor para encontrar elementos
 * @param fn : function - função java script a ser executada
 * @param _nTotalTentativas : number - número de tentativas que deve serem feitas para encontrar os elementos que permitirão executar a função
 * @param _tempoDeEsperaParaNovaTentativa : number - tempo de espera em milisegundos para fazer cada consulta aos elementos
 * @returns {*} - retorna false, caso não seja executada ou o mesmo retorno da função executada
 */
function executarQuandoEncontrarTodosElementos(_selectorsList, fn, _nTotalTentativas, _tempoDeEsperaParaNovaTentativa) {
	var tick = _nTotalTentativas || 30;
	var nTempo = _tempoDeEsperaParaNovaTentativa || 100;
	var nResultadosPositivos = 0;
	$(_selectorsList).each(function (index, element) {
		if($(element).length > 0){
			nResultadosPositivos++;
		}
	});

	if(nResultadosPositivos != $(_selectorsList).length){
		if (tick > 1) {
			setTimeout(function() {
				executarQuandoEncontrarTodosElementos(_selectorsList, fn, tick - 1, _tempoDeEsperaParaNovaTentativa);
			}, nTempo)
		}
	} else {
		return fn();
	}
	return false;
}