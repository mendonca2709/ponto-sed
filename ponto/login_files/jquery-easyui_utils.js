var sTabPrincipalId = 'tabConteudoPrincipal'; // determina o nome da tab principal da aplicação

/*
 * configurações para o formato de data do componente datebox
 */
$.fn.datebox.defaults.formatter = function(date){
    return parseDateToStr(date);
}
$.fn.datebox.defaults.parser = function(s){
    var t = parseStrToDate_ValidOrTrunc(s);
    if (!isNaN(t)){
        return new Date(t);
    } else {
        return new Date();
    }
}

$(function() {
    aplicarMascarasPadrao();
});

$(document).ajaxSuccess(function(e, xhr, settings) {
    aplicarMascarasPadrao();
});

function aplicarMascarasPadrao(){
    $(':input[mask]').each(function( index ) {
        var maskApplied = $(this).attr('maskApplied');
        var mask = $(this).attr('mask');
        if(maskApplied ==  null || maskApplied ==  undefined || maskApplied != mask ){
            if ($(this).attr('maskReverse') == "true") {
                $(this).mask(mask, {reverse: true})
	            if (!isNaN(mask)) {
		            $(this).css("text-align", "right");
	            }
            } else {
                $(this).mask(mask)
            }
            $(this).attr('maskApplied', mask);
        }
    });
}
/*
 * FUNÇÕES DIVERSAS
 */
function gerarHtmlIFrame(p_sId, p_sUrl) {
	var sHtml = '<iframe id="frm_' + p_sId + '"' +
		            ' name="ifTab' + p_sId + '"';
    if (p_sUrl != null && p_sUrl != '') {
        sHtml = sHtml + ' src="' + p_sUrl + '"';
    }
    sHtml = sHtml + ' marginwidth="1" marginheight="0" frameborder="0" vspace="0" hspace="0"' +
        			' onload="redimensioneIFrame(\'#frm_' + p_sId + '\');"' +
            	    ' style="width: 100%; height:100%;">' +
            	    'Seu navegador não oferece suporte para quadros embutidos ou está configurado para não exibí-los.' +
        		'</iframe>';
	return sHtml;
}

function abrirLinkInTabPanel(p_sTabPanelId, p_sPanelId, p_sTitulo, p_sUrl, p_bIFrame, p_bClosabe, p_UtilizarAbas) {
	var sUrl = setQueryStringParameter(p_sUrl, 'abaId', p_sPanelId);
	if (window.exibaMensagem) window.exibaMensagem(null);
	if (window.parent.exibaMensagem) window.parent.exibaMensagem(null);
	var oTabReturn;

	var oFncOnDestroy = function(){return true};
	var bAtualizarAbaPai = getQueryStringParameter(p_sUrl, 'atualizarAbaPai', 'false');
	if (bAtualizarAbaPai == 'true') {
		var abaPaiId = getQueryStringParameter(p_sUrl, 'abaPaiId', '');
		oFncOnDestroy = function(){
			window.parent.tabActivarInMain(abaPaiId, true, null);
		}
	}


	if (p_UtilizarAbas) {
		if (p_bIFrame) {
		    var content = gerarHtmlIFrame(p_sPanelId, sUrl);  // '<iframe scrolling="auto" frameborder="0"  src="'+p_sUrl+'" style="width:100%;height:100%;"></iframe>';
			oTabReturn = $('#' + p_sTabPanelId).tabs('add',{
				id: p_sPanelId,
				title: p_sTitulo,
				content: content,
				closable: p_bClosabe,
				onDestroy: oFncOnDestroy
			});
		} else {
			oTabReturn = $('#' + p_sTabPanelId).tabs('add',{
				id: p_sPanelId,
				title: p_sTitulo,
				href: sUrl,
				closable: p_bClosabe,
				onDestroy: oFncOnDestroy
			});
		}
	} else {
		var sTabId = sTabPrincipalId;
		if (p_bIFrame) {
		    var content = gerarHtmlIFrame(p_sPanelId, sUrl);  // '<iframe scrolling="auto" frameborder="0"  src="'+p_sUrl+'" style="width:100%;height:100%;"></iframe>';
		    oTabReturn = tabAtualizarFromContent(p_sTabPanelId, sTabId, p_sTitulo, content);
		    
		} else {
			oTabReturn = tabAtualizarFromUrl(p_sTabPanelId, sTabId, p_sTitulo, sUrl);
		}
	}
	return oTabReturn;
}

function tabBusqueIndex(_sTabPanelId, _sTabId) {
	var nTabIndexReturn = -1;
	var oTabs = $('#' + _sTabPanelId).tabs('tabs');
	$.each(oTabs, function(indexTab, oTab) {
		if (oTab[0].id ==  _sTabId) {
			nTabIndexReturn = indexTab;
			return nTabIndexReturn;
		}
	});
	return nTabIndexReturn;
}

function tabBusqueTab(_sTabPanelId, _sTabId) {
	var oTabReturn;
	var oTabs = $('#' + _sTabPanelId).tabs('tabs');
	$.each(oTabs, function(indexTab, oTab) {
		if (oTab[0].id ==  _sTabId) {
			oTabReturn = oTab;
			return oTabReturn;
		}
	});
	return oTabReturn;
}

function tabAtualizarFromUrl(_sTabPanelId, _sTabId, _sTitle, _sUrl) {
	var oTab = tabBusqueTab(_sTabPanelId, _sTabId);
	if (oTab != null) {
		 $('#' + _sTabPanelId).tabs('update', {
			tab: oTab,
			options: {
				title: _sTitle,
				href: _sUrl
			}
		});
	}
	return oTab;
}

function tabAtualizarFromContent(_sTabPanelId, _sTabId, _sTitle, _sContent) {
	var oTab = tabBusqueTab(_sTabPanelId, _sTabId);
	if (oTab != null) {
		$('#' + _sTabPanelId).tabs('update', {
			tab: oTab,
			options: {
				title: _sTitle,
				content: _sContent
			}
		});
	}
	return oTab;
}

function fecharAba(_sAbaPaiId, _bAtualizar) {
	_sAbaPaiId = _sAbaPaiId ? _sAbaPaiId : '';
    if (_sAbaPaiId=='self') {
		history.go(-1);
	} else if (_sAbaPaiId=='') {
		window.close();
	} else {
		window.parent.tabRemoverAtivaInMain(_sAbaPaiId, _bAtualizar);
	}
	return false;
}
function fecharAbaSemAtualizar(_sAbaPaiId) {
	return fecharAba(_sAbaPaiId, false);
}

function exibirMensagem(_sMsgTagParent, _sMsg, _sMsgTitle, _nTimeOfMessageShow, _sPosition) {
    var oMessageShow = $('#' + _sMsgTagParent);
    if (_sMsg == null) {
        oMessageShow.tooltip("hide");
        oMessageShow.hide();
        return;
    }
    if( _sPosition == null || _sPosition == "") {
        _sPosition = "bottom";
    }
    oMessageShow.tooltip({
        position: _sPosition,
        content: $('<div style="padding:5px"></div>'),
        onUpdate: function(content){
            content.panel({
                title: _sMsgTitle,
                content: _sMsg,
                width: 403,
                headerCls:'panelMsgHeader'
            });
        },
        onShow: function(){
            var t = $(this);
            t.tooltip('tip').unbind().bind('mouseenter', function(){
                t.tooltip('show');
            }).bind('mouseleave', function(){
                t.tooltip('hide');
            });
        }
    });
    oMessageShow.show();
    oMessageShow.tooltip("show");
    if(_nTimeOfMessageShow != null && _nTimeOfMessageShow > 0) {
        setTimeout("$('#" + _sMsgTagParent + "').tooltip('hide')", _nTimeOfMessageShow);
    }
}

function panelWidthExpand(_sSelector) {
	var sSelector = '.panel,.panel-header,.easyui-panel';
	if (_sSelector != undefined && _sSelector != null) {
		sSelector = _sSelector;
	}

	$(window).load(function() {
		executarQuandoSeletorEncontrarElemento(sSelector, function () {
			$(sSelector).each(function () {
				$(this).css('width', 'auto');
			});
		}, 1000, 100);
	});
}