﻿<?php session_start();
require_once 'funcoes/conexao.php';
require_once 'funcoes/funcoes.php';
$foto = $_POST["tph"];
$id_usr = $_POST["id_usr"];
$registro = getUltimoRegistroDoDia($id_usr);
enviarEmailForaDaGrade($registro);
ob_start();
?>
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/estilos.css">

    <script language="JavaScript" type="text/javascript">
        setTimeout("fncChama()", 5000);
        function fncChama() {
            window.location.href = 'index.php';
        }
        function fncEnter() {
            window.location.href = 'index.php';
        }
    </script>
    <div class="alert alert-success">
        <h1 class="text-center">Ponto registrado com sucesso!</h1>
    </div>
    <div class="alert alert-info">
        <h2 class="text-center">Último Registro: <?php echo mudaData($registro->data_registro); ?></h2>
    </div>
<?php
$html = ob_get_clean();
include 'index.php';