<?php
/**
 * Created by PhpStorm.
 * User: TMP-mmendonca
 * Date: 02/03/2015
 * Time: 08:31
 */
ob_start();
require_once 'funcoes/conexao.php';
require_once 'funcoes/funcoes.php';
?>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $('#matricula').mask('999.999.999-99');
            $('#matricula').keyup(function (e) {
                if ($('#matricula').val().length == 14) {
                    $('#registrar-ponto').attr('disabled', false);
                }
                if (e.which == 13 || e.keyCode == 13) {
                    if ($('#matricula').val().length < 14) {
                        $('.informe-cpf').parent().effect('highlight', 1000);
                        $(this).val('');
                        $(this).focus();
                        return false;
                    }
                    confirmaFoto();
                }
            });
            $('#matricula').focus();
            $('#matricula').keydown(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    e.preventDefault();
                }
            });
            $('body').keyup(function (e) {
                if (e.which == 27 || e.keyCode == 27) {
                    window.location.href = 'index.php';
                }
            });
            Webcam.set({
                // live preview size
                width: 640,
                height: 480,
                dest_width: 640,
                dest_height: 480,
                crop_width: 300,
                crop_height: 400,
                image_format: 'jpeg',
                jpeg_quality: 90
            });
            Webcam.attach('#my_camera');
        });

        function confirmaFoto() {
            if ($('#matricula').val().length < 14) {
                alert('Informe seu CPF e clique novamente!');
                $('#matricula').focus();
                return;
            }
            $.post(
                'verifica_foto.php',
                {cpf: $('#matricula').val()},
                function (r) {
                    if (r.usuario.nome == null) {
                        mostraNaoEncontrado();
                    } else if (r.existe == false) {
                        mostrarFormulario();
                    } else {
                        mostraTemFoto();
                    }
                }, "json"
            );

        }
        var fotoTirada;
        function sorria_2() {
            Webcam.upload(fotoTirada, 'salvafotousuario.php', function (code, text) {
                $.post(
                    'salvafotousuario.php',
                    {
                        matricula: $('#matricula').val(),
                        nome_arquivo: text
                    },
                    function (r) {
                        window.location = 'salvoufoto.php'
                    }
                );
            });
        }
        function mostrarFormulario() {
            Webcam.snap(function (data_uri) {
                fotoTirada = data_uri;
                $('#my_camera').html('<img src="' + data_uri + '"/>');
            });

            $('#formulario-foto').addClass('hide');
            $('#formulario-ponto').addClass('hide');
            $('#formulario-confirma-foto').removeClass('hide');

            $('body').bind('keydown', function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    e.preventDefault();
                    sorria_2();
                }
            });
        }
        function mostraTemFoto() {
            $('#formulario-foto').addClass('hide');
            $('#tem-foto').removeClass('hide');
            setTimeout('inicio()', 5000);
        }
        function mostraNaoEncontrado() {
            $('#formulario-foto').addClass('hide');
            $('#nao-encontrado').removeClass('hide');
            setTimeout('inicio()', 5000);
        }

        function click(event) {
            if (event.button == 2 || event.button == 3) {
                oncontextmenu = 'return false';
                alert('Botão do mouse incorreto!\n\nEssa ação foi registrada para este usuário.');
            }
        }

        function setFocus() {
            document.registro_ponto.matricula.focus();

        }
        function inicio() {
            window.location.href = 'index.php';
        }

    </script>
    <script src="./js/deployJava.js"></script>
    <style>
        #loginForm {
            -webkit-box-shadow: 0px 0px 20px -2px rgba(191, 191, 191, 1);
            -moz-box-shadow: 0px 0px 20px -2px rgba(191, 191, 191, 1);
            box-shadow: 0px 0px 20px -2px rgba(191, 191, 191, 1);
        }
    </style>
<?php if (isVPN() || $_REQUEST['vpn']) { ?>
    <script>
        function write(i) {
            $('#dadosConexao').text(i);
        }
    </script>
    <applet code="MyApplet.class" height="1" width="1" archive="meuapplet.jar"></applet>
<?php } ?>

    <form action="frm_confirma_ponto.php" method="post" name="registro_ponto" id="loginForm">
        <input type="hidden" id="gravaFoto" name="gravaFoto"/>
        <textarea name="dadosConexao" id="dadosConexao" style="display:none;visibility: hidden;"></textarea>
        <table class="table">
            <tr>
                <th colspan="2">
                    <h2 class="text-center">
                        <?php echo getDiaSemana(); ?>, <?php echo date('d'); ?>
                        de <?php echo getMes(); ?>
                        de <?php echo date('Y'); ?>
                    </h2>
                </th>
            </tr>
            <tr>
                <td class="col-lg-6">
                    <div id="my_camera" style="margin-left:80px;"></div>
                </td>
                <td class="col-lg-6">
                    <div id="formulario-ponto" class="row">
                        <div class="col-lg-12">
                            <h3 class="informe-cpf">Informe seu CPF</h3>
                        </div>
                        <div class="col-lg-12">
                            <input name="matricula"
                                   id="matricula"
                                   type="text"
                                   class="form-control input-lg"
                                   autocomplete="off"
                                />
                        </div>
                        <div class="col-lg-12"><br/></div>
                    </div>
                    <div id="tem-foto" class="alert alert-success hide">
                        <h1 class="text-center">Você já tem foto cadastrada!</h1>
                    </div>
                    <div id="nao-encontrado" class="alert alert-danger hide">
                        <h1 class="text-center">Cadastro não encontrado!</h1>
                    </div>
                    <div id="formulario-foto" class="row">
                        <div class="col-lg-12">
                            <h3 class="text-justify">
                                Por favor, posicione-se em frente a câmera e pressione
                                <a href="javascript:void(0);" class="btn btn-success">ENTER</a>
                                para capturar a foto.
                                Esta foto ficará no seu cadastro no sistema de ponto.
                            </h3>

                            <p>Esta foto só poderá ser alterada com solicitação formal à Gerência de Gestão de
                                Pessoas</p>
                        </div>
                    </div>
                    <div id="formulario-confirma-foto" class="hide">
                        <div class="col-lg-12">
                            <h3>Confirmar atualização da foto?</h3>

                            <div class="row">
                                <div class="col-lg-6">
                                    <a href="javascript:void(0);" class="btn btn-block btn-success"
                                       onclick="javascript:sorria_2();">SIM</a>
                                </div>
                                <div class="col-lg-6">
                                    <a href="javascript:void(0);" class="btn btn-block btn-danger"
                                       onclick="javascript:window.location='index.php';">NÃO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2"><h2 class="text-center">&nbsp;</h2></td>
            </tr>
        </table>
    </form>
<?php
$html = ob_get_clean();
include 'index.php';
?>