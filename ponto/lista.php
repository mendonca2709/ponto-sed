﻿<?php session_start(); ?>
<head><title>Teste do sistema - Login</title>
    <?php

    $ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
    $iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
    $android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
    $palmpre = strpos($_SERVER['HTTP_USER_AGENT'], "webOS");
    $berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
    $ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");
    $ops = 0;
    $texto = "";
    if ($ipad == true) {
        $ops = 1;
        $texto = " para iPad";
    }
    if ($iphone == true) {
        $texto = " para iPhone";
        $ops = 1;
    }
    if ($android == true) {
        $texto = " para Android";
        $ops = 1;
    }
    if ($palmpre == true) {
        $texto = " para Palmpre";
        $ops = 1;
    }
    if ($berry == true) {
        $texto = " para Berry";
        $ops = 1;
    }
    if ($ipod == true) {
        $texto = " para iPod";
        $ops = 1;
    }

    $numr_ip = $_SERVER[HTTP_X_FORWARDED_FOR] . "-" . $_SERVER[REMOTE_ADDR];
    $data_registro = date('Y') . "/" . date('m') . "/" . date('d') . " " . date("H") . ":" . date("i") . ":" . date("s");

    $dia = date("d");
    $mes = date("m");
    $ano = date("Y");
    $hora = date("H");

    $sql_consult = "insert into acessos (id_usr,data_acesso,entrada,numr_ip) values (4000,'" . date("Y/m/d") . "','" . date("H:i:s") . "','" . $numr_ip . "')";
    $dados_consult = mysql_query($sql_consult, $conexao);
    ?>


    <style>
        .botao {
            background: rgb(79, 149, 255);

            padding: 1px 1px 1px;
            border-radius: 34px;
            border: 1px solid rgb(186, 186, 186);
            box-shadow: 0px 3px 7px #333;
            cursor: pointer;
        }

        .botao2 {
            background: rgb(0, 165, 0);

            padding: 1px 1px 1px;
            border-radius: 34px;
            border: 1px solid rgb(186, 186, 186);
            box-shadow: 0px 3px 7px #333;
            cursor: pointer;
        }

        .botao4 {
            background: rgb(255, 36, 36);

            padding: 1px 1px 1px;
            border-radius: 34px;
            border: 1px solid rgb(186, 186, 186);
            box-shadow: 0px 3px 7px #333;
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">
        <!--document.getElementById('var_login_name').focus();-->
    </script>


    <script language="javascript" type="text/javascript">
        function fncEnter() {
            if (window.event.keyCode == 13) {
                fncValida();
            }
        }
        function setFocus() {
            document.loginForm.Usuario.select();
            document.loginForm.Usuario.focus();
        }

        function fncValida() {
            if (document.loginForm.Usuario.value == '') {
                alert("Informe o seu login de acesso!");
                document.loginForm.Usuario.focus();
                return false;
            }
            if (document.loginForm.Senha.value == '') {
                alert("Informe sua senha!");
                document.loginForm.Senha.focus();
                return false;
            }
            document.loginForm.submit();
        }
        function fncReenvia() {
            if (document.loginForm.Usuario.value == '') {
                alert("Para reenviarmos sua senha para seu email, informe o seu login de acesso!");
                document.loginForm.Usuario.focus();
                return false;
            }
            document.formulario_envia.Usuario.value = document.loginForm.Usuario.value;
            document.formulario_envia.submit()
        }
    </script>
</head>


<body onload="javascript:setFocus()">


<BR> <BR> <BR>
</p>
<table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="93%"><strong><font style="font-size:40px;" face="Arial, Helvetica, sans-serif">
                    Login - Sistema de teste</font></strong></td>
        <td width="7%" rowspan="2" valign="bottom">&nbsp;</td>
    </tr>
    <tr>
        <td valign="bottom"><strong><font style="font-size:20px;" face="Arial, Helvetica, sans-serif">
                    <?php

                    $hoje = getdate();

                    switch ($hoje['wday']) {
                        case 0:
                            echo "Domingo, ";
                            break;
                        case 1:
                            echo "Segunda-Feira, ";
                            break;
                        case 2:
                            echo "Terça-Feira, ";
                            break;
                        case 3:
                            echo "Quarta-Feira, ";
                            break;
                        case 4:
                            echo "Quinta-Feira, ";
                            break;
                        case 5:
                            echo "Sexta-Feira, ";
                            break;
                        case 6:
                            echo "Sábado, ";
                            break;
                    }

                    echo $hoje['mday'];
                    switch ($hoje['mon']) {
                        case 1:
                            echo " de Janeiro de ";
                            break;
                        case 2:
                            echo " de Fevereiro de ";
                            break;
                        case 3:
                            echo " de Março de ";
                            break;
                        case 4:
                            echo " de Abril de ";
                            break;
                        case 5:
                            echo " de Maio de ";
                            break;
                        case 6:
                            echo " de Junho de ";
                            break;
                        case 7:
                            echo " de Julho de ";
                            break;
                        case 8:
                            echo " de Agosto de ";
                            break;
                        case 9:
                            echo " de Setembro de ";
                            break;
                        case 10:
                            echo " de Outubro de ";
                            break;
                        case 11:
                            echo " de Novembro de ";
                            break;
                        case 12:
                            echo " de Dezembro de ";
                            break;
                    }

                    echo $hoje['year'] . ".";
                    ?>
                </font></strong></td>
    </tr>
</table>
<BR>
<?php if ($ops == 1) { ?>
    <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
        <tr valign="bottom">
            <td height="25" class="Titulo_rel">&nbsp; </td>
        </tr>
        <tr class="Itens_rel_s_traco">
            <td height="2">
                <form action="frm_Verifica_usuario2.php" method="post" name="loginForm" id="loginForm" target="Verifica"
                      onsubmit="return fncValida();">
                    <br>
                    <table width="40%" border="0" align="center" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="20" rowspan="4" valign="middle"><strong><font size="2"><font
                                            color="#333333"></font></font></strong></td>
                            <td width="126">&nbsp;</td>
                            <td width="1044">&nbsp;</td>
                        </tr>
                        <tr>
                            <td><font style="font-size:40px;" face="Arial, Helvetica, sans-serif">
                                    Usu&aacute;rio:</font></td>
                            <td><input name="Usuario" type="text" id="Usuario" onKeyPress="fncEnter();" value="admdemo"
                                       size="15" width="120" style="font-size:30px">
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0px 0px 3px 0px;">&nbsp;</td>
                            <td style="padding: 0px 0px 3px 0px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="padding: 0px 0px 3px 0px;"><font style="font-size:40px;"
                                                                        face="Arial, Helvetica, sans-serif">
                                    Senha: </font></td>
                            <td style="padding: 0px 0px 3px 0px;"><input name="Senha" id="Senha" size="15"
                                                                         type="password" onKeyPress="fncEnter();"
                                                                         width="120" style="font-size:30px">
                            </td>
                        </tr>
                    </table>
                    <p>
                        <input type="text" name="numr_ip" value="<?php echo $numr_ip; ?>" style="display:none">
                    </p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p>&nbsp; </p>
                    <table width="42%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="71%" height="130" align="center" valign="middle" class="botao"
                                onClick="fncValida();">
                                <font color="#FFFF99" style="font-size:80px;"
                                      face="Arial, Helvetica, sans-serif"><strong>Entrar</strong></font></td>
                        </tr>
                    </table>
                    <p><br>
                        <br>
                    </p>
                </form>
            </td>
        </tr>
        <tr class="Sub_titulo_rel">
            <td height="15">
                <form name="formulario_envia" method="post" action="adm/reenvia_senha.php" target="Verifica">
                    <input type="text" name="Usuario2" style="display:none">
                </form>
            </td>
        </tr>
    </table>
<?php } ?>
<iframe width="801" height="801" name="Verifica" style="display:none">
</iframe>
</body>

</html>