<?php
/**
 * Created by PhpStorm.
 * User: marcelo-tm
 * Date: 06/04/2015
 * Time: 07:34
 */
ini_set('error_reporting', E_ERROR);

if ($argv[1] == 'path') {
    //$_SERVER['DOCUMENT_ROOT'] = 'C:/wamp/www/desenvolvimento/fontes/gpPonto/';
    $_SERVER['DOCUMENT_ROOT'] = $argv[2];
}

require_once 'conexao.php';
require_once 'funcoes.php';

$fechamento = new FechamentoDiario();

//$fechamento->processarFechamentoDoDia();
$fechamento->enviarEmailGerentes();

class FechamentoDiario
{
    public function processarFechamentoDoDia()
    {
        $hoje = new DateTime();

        foreach (R::findAll(BaseObject::USUARIO, 'id_usr = 75493870100') as $usuario) {
            $pontoDeHoje = getUltimoRegistroDoDia($usuario->id_usr, $hoje->format('d/m/Y'));
            if (!$pontoDeHoje->id_registro) {
                $ocorrencia = R::dispense(BaseObject::OCORRENCIA);
                $ocorrencia->id_usr = $usuario->id_usr;
                $ocorrencia->data_inclusao = date('H:i');
                $ocorrencia->mensagem = 'Falta';
                $ocorrencia->descricao = "Não foi registrado nenhum ponto no dia.";
                R::store($ocorrencia);
            }
        }
    }

    public function enviarEmailGerentes()
    {
        foreach (getGerentes() as $usuario) {
            $servidores = implode(",", array_keys($usuario->getMeusServidoresArray(null, true)));
            if ($servidores) {
                $ocorrencias = R::findAll(BaseObject::OCORRENCIA, "id_usr in ({$servidores}) and emailEnviado is null");
                //$ocorrencias = R::findAll(BaseObject::OCORRENCIA, "id_usr in ({$servidores})");
                if ($ocorrencias) {
                    $__c = 0;
                    $html = "<table style=\"width:100%;border:1px solid #ccc;\">";
                    foreach ($ocorrencias as $ocorrencia) {
                        $html .= "<tr style='" . ($__c++ % 2 == 1 ? 'background-color: #efefef' : '') . "'><td>{$ocorrencia->descricao}</td></tr>";
                        $_ocorrenciasEmail[] = $ocorrencia;
                    }
                    $html .= "</table>";
                    ob_start();
                    include './emailOcorrencia.php';
                    $tpl = ob_get_clean();
                    $tpl = str_replace('{ocorrencias}', $html, $tpl);
                    if (enviarEmail($usuario->email, utf8_decode('Ocorrências de Ponto do dia ' . date('d/m/Y')), $tpl)) {
                        foreach ($_ocorrenciasEmail as $_ocorrenciaEnviada) {
                            $_ocorrenciaEnviada->emailEnviado = date('Y-m-d H:i:s');
                            R::store($_ocorrenciaEnviada);
                        }
                    }
                    exit;
                }
            }
        }
    }
}