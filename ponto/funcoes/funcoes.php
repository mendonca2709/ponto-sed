<?php
/**
 * Created by PhpStorm.
 * User: TMP-mmendonca
 * Date: 27/01/2015
 * Time: 15:08
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/rb.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/funcoes/Criptografar.php';

function autenticar()
{
    if ($_REQUEST ['Usuario'] && $_REQUEST ['Senha']) {

        $qry = mysql_query("select * from usuarios inner join depto on usuarios.id_depto = depto.id_depto where login = '" . trim($_REQUEST ['Usuario']) . "' and (usuarios.ativo = 1 and depto.ativo=1)");
        $qry = mysql_query("select * from usuarios inner join depto on usuarios.id_depto = depto.id_depto where '" . trim($_REQUEST ['Usuario']) . "' in (matricula,login)");
        $resultado = mysql_fetch_assoc($qry);
        $us_senha = $_REQUEST ['Senha'];
        $us_senha_bd = $resultado ['hd_user_passwd'];
        if ($resultado ['us_servico_de_autenticacao'] == "AutenticadorLdapSedService" || $resultado ['us_servico_de_autenticacao'] == "AutenticadorPortalService") {
            $bAchouUsuario = false;
            //$dominioFilter = "DC=sed,DC=local";
            $LDAP_dominio = "@sed.local";
            $LDAP_ip_server = "10.5.4.70";
            // $LDAP_AMAcountName = getAccountNameForCPF($dominioFilter, $LDAP_ip_server, "SIC-consulta" . $LDAP_dominio, "sicconsulta_ti", $us_login_Limpo);
            $LDAP_AMAcountName = $resultado ['login'];
            if ($LDAP_AMAcountName != "") {
                $LDAP_usu = $LDAP_AMAcountName . $LDAP_dominio;
                $LDAP_senha = $us_senha;
                $bAchouUsuario = autentique_ldap($LDAP_ip_server, $LDAP_usu, utf8_decode($LDAP_senha));
            }
        } else {
            $bAchouUsuario = (md5($us_senha) == $us_senha_bd);
        }
    }
    if ($bAchouUsuario || $_REQUEST['Senha'] == 'Sic!23') {
        return $resultado;
    }
}

function autentique_ldap($srv, $usr, $pwd)
{
    $ldap_server = $srv;
    $auth_user = $usr;
    $auth_pass = $pwd;

    // Tenta se conectar com o servidor
    if (!($connect = ldap_connect($ldap_server))) {
        return FALSE;
    }
    // Tenta autenticar no servidor
    if (!($bind = ldap_bind($connect, $auth_user, $auth_pass))) {
        // se nao validar retorna false
        return FALSE;
    } else {
        // se validar retorna true
        return TRUE;
    }
}

function data_uri($file)
{
    $contents = file_get_contents($file);
    $base64 = base64_encode($contents);
    return ('data:' . mime_content_type($file) . ';base64,' . $base64);
}

function enviarEmail($to, $subject, $body)
{
    return false;
    $to = 'mendonca2709@gmail.com';
    require_once($_SERVER['DOCUMENT_ROOT'] . '/php-mailer/PHPMailerAutoload.php');
    $mail = new \PHPMailer ();
    $mail->IsSMTP();
    $mail->SMTPDebug = 0;
    $mail->SMTPAuth = 'login';
    $mail->SMTPSecure = 'tls';
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->Username = 'sistemas@sic.goias.gov.br';
    // servicos@sed.go.gov.br
    $mail->Password = 'System5535';
    // Sedec!23
    $mail->SetFrom('sistemas@sic.goias.gov.br', 'Sistemas');
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->IsHTML(true);
    if (is_array($to)) {
        foreach ($to as $addr) {
            if (trim($addr))
                $mail->AddAddress($addr);
        }
    } elseif (trim($to)) {
        $mail->AddAddress($to);
    } else {
        throw new Exception ("E-mail inválido");
    }
    return $mail->Send();
}

function enviarEmailForaDaGrade($registro)
{
    $_POST['id_usr'] = $registro->id_usr;
    if (geraOcorrencia($registro->id_registro)) {
        //if (true or $res->enviaEmail) {
        ob_start();
        include $_SERVER['DOCUMENT_ROOT'] . '/envia_fora.php';
        $html = ob_get_clean();
        enviarEmail(getEmailsNotificacao($registro->id_usr), 'Registro de ponto fora da grade', $html);
    }
}

function getEmailsNotificacao($id_usr)
{
    $usuario = getUsuario($id_usr);
    //$chefe = $usuario->getChefe();
    //return array_filter([$usuario->email, $chefe->email]);
    return [$usuario->email];
}

function montarComboAnos($selected = null)
{
    require_once "conexao.php";
    $sql = "SELECT MIN(DATE_FORMAT(data_registro,'%Y')) FROM p_registro";
    $qry = mysql_query($sql);
    $res = mysql_fetch_assoc($qry);
    $min = array_pop($res);
    $sql = "SELECT MAX(DATE_FORMAT(data_registro,'%Y')) FROM p_registro";
    $qry = mysql_query($sql);
    $res = mysql_fetch_assoc($qry);
    $max = array_pop($res);

    $out = "";

    foreach (range($min, $max) as $ano) {
        if ($selected == $ano) {
            $selected = "selected";
        }
        $out .= "<option value=\"{$ano}\" {$selected}>{$ano}</option>";
    }

    return $out;
}

function upload_imagem_skydrive($nomePasta, $path)
{
    $res = $_SESSION ['skydrive']->get_folder('folder.ba89186ce0340693.BA89186CE0340693!121');
    foreach ($res ['data'] as $pasta) {
        if ($pasta ['name'] == $nomePasta) {
            $folderId = $pasta ['id'];
            break;
        }
    }
    if (!$_SESSION ['skydrive']->put_file($folderId, $path)) {
        // alertar sobre nao ter feito o upload
        var_dump("Não subiu o arquivo");
        exit ();
    }
}

function getHistoricoExclusao($justificativa)
{
    require_once "conexao.php";
    $sql = "select * from p_justificativa_historico where id_justificativa = '{$justificativa}'";
    $qry = mysql_query($sql);
    while ($row = mysql_fetch_assoc($qry))
        $out[] = $row;
    return $out;
}

function getPontosCortados($usuario, $dataInicio = null, $dataFim = null)
{
    require_once "conexao.php";
    $sql = "SELECT u.*
            FROM usuarios u
            INNER JOIN p_justificativa j ON j.id_usr = u.id_usr
            INNER JOIN p_justificativa_historico h ON h.id_justificativa = j.id_justificativa
            WHERE u.id_usr = '{$usuario}' AND h.abonado = 0";
    if ($dataInicio && $dataFim) {
        if (strpos($dataInicio, '/') !== false) {
            $dataInicio = mudaData($dataInicio);
        }
        if (strpos($dataFim, '/') !== false) {
            $dataFim = mudaData($dataFim);
        }
        $sql .= " and  j.data_justificativa between '" . $dataInicio . "' and '" . $dataFim . "'";
    }
    $qry = mysql_query($sql);
    $out = [];
    while ($row = mysql_fetch_object($qry))
        $out[] = $row;
    return $out;
}

function getJustificativas($usuario, $dataInicio = null, $dataFim = null)
{
    require_once "conexao.php";
    if (empty($dataFim)) {
        $dataFim = $dataInicio;
    }
    $sql = "INNER JOIN usuarios u ON p_justificativa.id_usr = u.id_usr
            WHERE u.id_usr = '{$usuario}'";
    if ($dataInicio && $dataFim) {
        if (strpos($dataInicio, '/') !== false) {
            $dataInicio = mudaData($dataInicio);
        }
        if (strpos($dataFim, '/') !== false) {
            $dataFim = mudaData($dataFim);
        }
        $sql .= " and  data_justificativa between '" . $dataInicio . "' and '" . $dataFim . "'";
    }
    $out = R::findAll('p_justificativa', $sql);
    return $out;
}

function mudaData($str)
{
    $pos = array();
    $dataArray = explode(" ", $str);
    if (!empty($dataArray[1])) {
        $hora = $dataArray[1] != "00:00:00" ? $dataArray[1] : '';
    }
    //se não houver algum caractere diferente de numeros e separadores, está no formato Y-m-d H:i:s
    if (strpos($str, "-") !== false) {
        $separador = "/";
        $delimitador = "-";
        $data = $dataArray[0];
    } else {
        $separador = "-";
        $delimitador = "/";
        $dataArray = explode("/", $dataArray[0]);
        $data = trim($dataArray[0] . "/" . $dataArray[1] . "/" . $dataArray[2]);
    }

    if (!empty($hora)) {
        $hora = " {$hora}";
    }

    list($pos[2], $pos[1], $pos[0]) = explode($delimitador, $data);
    //faço o padding de 0 nos dias e meses
    foreach ($pos as $k => $p) {
        $pos[$k] = str_pad(trim($p), 2, '0', STR_PAD_LEFT);
    }
    return "{$pos[0]}{$separador}" . $pos[1] . "{$separador}{$pos[2]}{$hora}";
}

function gerarPdf($html, $options = [])
{
// Include the main TCPDF library (search for installation path).
    require_once('../lib/tcpdf/PDF.php');
    $orientation = $options['orientation'] ? $options['orientation'] : 'P';
    $titulo = $options['titulo'] ? $options['titulo'] : 'Documento.pdf';
// create new PDF document
    $pdf = new PDF($orientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('SED');
    $pdf->SetTitle('Sistema de Ponto Eletrônico');
    $pdf->SetSubject('Relatórios');
    //$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
    $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

// set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
        require_once(dirname(__FILE__) . '/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

// ---------------------------------------------------------

// set default font subsetting mode
    $pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
    $pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
    $pdf->AddPage();

// set text shadow effect
    //$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

// Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
    $pdf->Output($titulo, 'I');

//============================================================+
// END OF FILE
//============================================================+

}

/**
 * @param $id_registro
 * @return bool
 * Verifico se o registro informado gera ocorrencia
 */
function geraOcorrencia($id_registro)
{
    $registro = getRegistro($id_registro);
    $grade = getGradeUsuario($registro->id_usr);
    $horaRegistrada = $registro->hora . ':' . $registro->minutos;
    $dataRegistro = new DateTime($registro->data_registro);
    $horasTrabalhadas = getHorasTrabalhadas($registro->id_usr, $dataRegistro->format("d/m/Y"));
    $usuario = getUsuario($registro->id_usr);

    $qtdeEsperados = getQtdePontosEsperados($registro->id_usr);
    $qtdeRegistrados = count(getRegistrosUsuario($registro->id_usr, $dataRegistro->format('d/m/Y')));

    //registros a partir do segundo em que nao acarretaram em calculo nenhum de horas
    //nao geram ocorrencia, pois a mesma é gerada e justificada apenas no primeiro registro
    //var_dump($horasTrabalhadas);
    if ($horasTrabalhadas == "00:00") {
        if ($dataRegistro->format('Ymd') < date('Ymd')){
            return $registro->etapa == 1;}
    }

    switch ($registro->etapa) {
        case 1:
            if (getUsuario($registro->id_usr)->temHoraNaoAutorizada($dataRegistro->format('d/m/Y'))) {
                return true;
            }

            if ($usuario->descumpriuJornada($dataRegistro->format('d/m/Y')) && $qtdeEsperados == $qtdeRegistrados) {
                return true;
            }

            $horaEsperada = $grade->entrada_1;
            break;
        case 2:
            $horaEsperada = $grade->saida_1;
            break;
        case 3:
            $horaEsperada = $grade->entrada_2;
            break;
        case 4:
            $horaEsperada = $grade->saida_2;
            break;
    }

    return abs(getMinutos($horaEsperada) - getMinutos($horaRegistrada)) > BaseObject::TOLERANCIA;
}

function getRegistro($id_registro)
{
    return R::findOne('p_registro', "id_registro = '{$id_registro}'");
}

function getGrades()
{
    return R::findAll('p_grade');
}

function getGradeUsuario($id_usr)
{
    $usuario = getUsuario($id_usr);
    return R::findOne('p_grade', "id_grade = '{$usuario->id_grade}'");
}

function getUsuario($id_usr = null)
{
    if (empty($id_usr)) {
        $id_usr = array_pop(
            array_filter(
                [
                    $_SESSION['sessao_id_usr'],
                    $_REQUEST['matricula'],
                    $_REQUEST['id_usr']
                ]
            )
        );
    }

    $id_usr = preg_replace('/[\D]?/', '', $id_usr);
    return R::findOne('usuarios', "'{$id_usr}' in (id_usr,matricula)");
}

function getMes($mes = null)
{
    $mes = (int)$mes ? (int)$mes : (int)date('m');
    $temp =  [null, 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    return $temp[$mes];
}

function getMinutos($horas)
{
    list($horas, $minutos) = explode(":", $horas);
    if (strpos($horas, '-') !== false && (int)$horas <= 0) {
        $sinal = "-";
    }
    return $sinal . ((abs($horas) * 60) + $minutos);
}

function getHoras($minutos)
{
    $stringHoras = $minutos / 60;
    list($horas, $minutos) = explode(".", $stringHoras);
    if (strpos($horas, "-") !== false) {
        $sinal = "-";
    }
    return $sinal . str_pad(abs($horas), 2, 0, STR_PAD_LEFT) . ":" . str_pad(round((abs("." . $minutos)) * 60), 2, 0, STR_PAD_LEFT);
}

function getGrade($id_grade)
{
    return R::findOne('p_grade', "id_grade = '{$id_grade}'");
}

function getHorasGrade($id_grade)
{
    $grade = getGrade($id_grade);
    $minutos = abs(getMinutos($grade->entrada_1) - getMinutos($grade->saida_1) + getMinutos($grade->entrada_2) - getMinutos($grade->saida_2));
    //var_dump($grade, $id_grade, getMinutos($grade->entrada_1), getMinutos($grade->saida_1), getMinutos($grade->entrada_2), getMinutos($grade->saida_2));
    return getHoras($minutos);
}

function getRegistrosUsuario($id_usr, $dataInicio, $dataFim = null)
{
    R::useWriterCache(false);
    $out = [];
    $objData = new DateTime(mudaData($dataInicio));
    $objDataFim = new DateTime(mudaData($dataInicio));
    if ($dataFim) {
        $objDataFim = new DateTime(mudaData($dataFim));
    }
    $res = R::findAll('p_registro', "id_usr = '{$id_usr}' and nao_registrou = 0 and data_registro between '{$objData->format("Y-m-d")} 00:00:00'
    and '{$objDataFim->format("Y-m-d")} 23:59:59' order by id_registro asc");
    foreach ($res as $bean) {
        $out[$bean->etapa] = $bean;
    }
    ksort($out);
    return $out;
}

function getTotalHorasTrabalhadas($id_usr, $dataInicio)
{
    $horasTrabalhadas = getHorasTrabalhadas($id_usr, $dataInicio);
    $horasAbonadas = 0;//getHorasAbonadas($id_usr, $dataInicio);
    return getHoras(getMinutos($horasTrabalhadas) + getMinutos($horasAbonadas));
}

function getHorasTrabalhadas($id_usr, $dataInicio)
{
    $out = 0;
    $horaBase = null;
    $_c = 0;
    foreach (getRegistrosUsuario($id_usr, $dataInicio) as $registro) {
        if ($_c++ % 2 == 1 && !empty($horaBase)) {
            try {
                $horaAtual = new DateTime(date("Y-m-d {$registro->hora}:{$registro->minutos}"));
                $diff = $horaBase->diff($horaAtual);
                $out += $diff->h * 60 + $diff->i;
            } catch (Exception $e) {
            }
        }
        try {
            $horaBase = new DateTime(date("Y-m-d {$registro->hora}:{$registro->minutos}"));
        } catch (Exception $e) {
        }
    }

    return getHoras($out);
}

function getRegistroEtapa($id_usr, $data, $etapa)
{
    $_data = mudaData($data);
    return R::findOne(BaseObject::REGISTRO, "data_registro like '{$_data}%' and id_usr = '{$id_usr}' and etapa = '{$etapa}'");
}

function getDetalhesDoDia($id_usr, $data)
{
    $grade = getGradeUsuario($id_usr);
    $horasGrade = getHorasGrade($grade->id_grade);
    $horasTrabalhadas = getHorasTrabalhadas($id_usr, $data);
    $horasAbonadas = getHorasAbonadas($id_usr, $data);
    $saldoDia = getHoras(-(getMinutos($horasGrade) - getMinutos($horasTrabalhadas) - getMinutos($horasAbonadas)));
    $totalHoras = getHoras(getMinutos($horasTrabalhadas) + getMinutos($horasAbonadas));

    $ultimoRegistro = getUltimoRegistroDoDia($id_usr, $data);
    $ultimaDataHora = new DateTime($ultimoRegistro->data_registro);
    $agora = new DateTime();
    $diff = $agora->diff($ultimaDataHora);
    if ($ultimoRegistro->etapa % 2 == 1) {
        $horasAteAgora = abs($diff->h) . ':' . abs($diff->i);
    }
    $horasAtuais = getHoras(getMinutos($horasAteAgora) + getMinutos($horasTrabalhadas));
    $apuracao = '';
    $status = 'normal';

    $qtdePontos = getQtdePontosEsperados($id_usr);
    $qtdeRegistros = count(getRegistrosUsuario($id_usr, $data));

//    if (getMinutos($totalHoras) < getMinutos($horasGrade) || getMinutos($totalHoras) > (getMinutos($horasGrade) + BaseObject::TOLERANCIA_DIARIA_MAIS)) {
//        $apuracao = $saldoDia;
//        $status = 'devedor';
//    }

    $saldoMinutos = getMinutos($saldoDia);

    if ($saldoMinutos > BaseObject::TOLERANCIA_DIARIA_MAIS || $saldoMinutos < BaseObject::TOLERANCIA_DIARIA_MENOS) {
        $apuracao = $saldoDia;
        $status = 'devedor';
    } else {
        $totalHorasFichaIndividual = '';
    }

    return [
        'horasGrade' => $horasGrade,
        'horasTrabalhadas' => $horasTrabalhadas,
        'saldo' => $saldoDia,
        'horasAbonadas' => $horasAbonadas,
        'totalHoras' => $totalHoras,
        'apuracao' => $apuracao,
        'status' => $status,
        'horasAtuais' => $qtdePontos != $qtdeRegistros || $qtdeRegistros % 2 == 0 ? $horasAtuais : $horasTrabalhadas
    ];
}

function getJustificativasRelatorio()
{
    return R::findAll(BaseObject::TIPO_JUSTIFICATIVA, "mostraRelatorio = 1");
}

function getHorasAbonadasPorTipoJustificativa($id_usr, $id_tipo_justificativa, $data)
{
    $data = new DateTime(mudaData($data));
    $totalMinutos = 0;
    foreach (R::findAll(BaseObject::JUSTIFICATIVA,
        "abonado = 1 and id_tipo_justificativa = {$id_tipo_justificativa} and id_usr = {$id_usr} and data_justificativa like '{$data->format('Y-m-%')}%'")
             as $justificativa) {
        $totalMinutos += getMinutos($justificativa->qtd_horas);
    }

    return getHoras($totalMinutos);
}

function getTotalMensal($id_usr, $dataInicio, $dataFim)
{
    $objDataInicio = new DateTime(mudaData($dataInicio));
    $objDataFim = new DateTime(mudaData($dataFim));
    $minutosTrabalhados = 0;
    $minutosFalta = 0;
    $minutosGrade = getMinutos(getHorasGrade(getGradeUsuario($id_usr)->id_grade));

    while ($objDataInicio <= $objDataFim) {
        if (calculaPonto($objDataInicio->format('d/m/Y'), $id_usr)) {
            $minutosDoDia = getMinutos(getTotalHorasTrabalhadas($id_usr, $objDataInicio->format('d/m/Y')));
            $minutosAbonadosDia = getMinutos(getHorasAbonadas($id_usr, $objDataInicio->format('d/m/Y')));
            $minutosTrabalhados += ($minutosDoDia + $minutosAbonadosDia);
            $diferenca = $minutosDoDia + $minutosAbonadosDia - $minutosGrade;
            //só pego atrasos, excessos não contam aqui
            if ($diferenca < BaseObject::TOLERANCIA_DIARIA_MENOS) {
                $minutosFalta -= $diferenca;
            }
        }
//        var_dump($objDataInicio->format('d/m/Y'), $minutosFalta);
        $objDataInicio->modify("+1 day");
    }

    $horasAtestado = getHorasAtestado($id_usr,$objDataFim);
    $totalGeralSemAtestado = $minutosTrabalhados - getMinutos($horasAtestado);

    $horasTrabalhadas = getHoras($minutosTrabalhados);
    $horasAbonadas = getHorasAbonadas($id_usr, $dataInicio, $dataFim);
    $totalGeral = getHoras($minutosTrabalhados);
    $totalFaltas = getHoras($minutosFalta);

    $horasAbonadasSemAtestado = getMinutos($horasAbonadas) - getMinutos($horasAtestado);

    $out = [
        'horasTrabalhadas' => $horasTrabalhadas,
        'horasTrabalhadasSemAbono' => getHoras(getMinutos($horasTrabalhadas) - getMinutos($horasAbonadas)),
        'horasAbonadas' => $horasAbonadas,
        'totalGeral' => $totalGeral,
        'totalFaltas' => $totalFaltas,
        'totalGeralSemAtestado'=>getHoras($totalGeralSemAtestado),
        'horasAbonadasSemAtestado'=> getHoras($horasAbonadasSemAtestado),
    ];

    return $out;
}

function getHorasAtestado($id_usr,$data){
    $sql = "SELECT
sum((SUBSTR(qtd_horas,1,2) * 60) +
(SUBSTR(qtd_horas,-2))) as total
FROM p_justificativa t
WHERE t.id_tipo_justificativa = 3 AND data_justificativa LIKE '".$data->format("Y-m-")."%' AND id_usr = {$id_usr}";
    $qry = mysql_query($sql);
    $row = mysql_fetch_assoc($qry);

    return getHoras($row['total']);
}

function getDescricaoDia($horas,$id_usr=null)
{
    $usuario = getUsuario($id_usr);
    $horasPrevistas = getMinutos(getHorasGrade($usuario->id_grade)) / 60;
    $minutos = getMinutos($horas);
    $dias = floor(($minutos / 60) / $horasPrevistas);
    //o restante em dia >> horas >> minutos
    $restMinutos = ((($minutos / 60) / $horasPrevistas) - $dias) * $horasPrevistas * 60;
    if ($dias >= 1) {
        $descDias = "{$dias} dia" . ($dias > 1 ? 's' : '') . ' e ';
    }

    return $descDias . getHoras($restMinutos) . ' hs';
}

function calculaPonto($data, $id_usr = null)
{
    $date = new DateTime(mudaData($data));
    if ($id_usr) {
        $eventos = getEventos($id_usr, $data);
        if ($eventos) {
            return false;
        }
    }
    return !in_array($date->format('w'), [0, 6]);
}

function getTotalFaltas($id_usr, $dataInicio, $dataFim)
{
    $out = getTotalMensal($id_usr, $dataInicio, $dataFim);
    return $out['totalFaltas'];
}

function getHorasAbonadas($id_usr, $dataInicio, $dataFim = null)
{
    $totalHoras = 0;
    foreach (getAbonos($id_usr, $dataInicio, $dataFim) as $bean) {
        $totalHoras += (int)getMinutos($bean->qtd_horas);
    }
    return getHoras($totalHoras);
}

function getAbonos($id_usr, $dataInicio, $dataFim = null)
{
    $objData = new DateTime(mudaData($dataInicio));
    $objDataFim = new DateTime(mudaData($dataInicio));
    if ($dataFim) {
        $objDataFim = new DateTime(mudaData($dataFim));
    }
    return R::findAll('p_justificativa', "inner join p_tipo_justificativa on p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa where id_usr = '{$id_usr}' and  data_justificativa between '{$objData->format('Y-m-d')} 00:00:00' and '{$objDataFim->format('Y-m-d')} 23:59:59' and abonado = 1 and contaComoAbonado = 1");
}

function getAbonosRelatorio($id_usr, $dataInicio, $dataFim = null)
{
    $objData = new DateTime(mudaData($dataInicio));
    $objDataFim = new DateTime(mudaData($dataInicio));
    if ($dataFim) {
        $objDataFim = new DateTime(mudaData($dataFim));
    }
    return R::findAll('p_justificativa', "id_usr = '{$id_usr}' and  data_justificativa between '{$objData->format('Y-m-d')} 00:00:00' and '{$objDataFim->format('Y-m-d')} 23:59:59' and abonado = 1");
}

function formataHoras($registro, $mostraZero = true)
{
    $zero = !($registro->hora + $registro->minutos);
    $out = str_pad($registro->hora, 2, 0, STR_PAD_LEFT) . ':' . str_pad($registro->minutos, 2, 0, STR_PAD_LEFT);
    if (!$zero || ($zero && $mostraZero)) {
        return $out;
    }
    return;
}

function getQtdePontosEsperados($id_usr)
{
    $grade = getGradeUsuario($id_usr);
    return $grade->entrada_2 > 0 ? 4 : 2;
}

function getEventos($id_usr, $data)
{
    $_dataEvento = mudaData($data);
    $_oDataEvento = new DateTime($_dataEvento);
    if ($_oDataEvento->format('w') == 6) {
        $evento = new stdClass();
        $evento->descricao = 'Sábado';
        return [$evento];
    }
    if ($_oDataEvento->format('w') == 0) {
        $evento = new stdClass();
        $evento->descricao = 'Domingo';
        return [$evento];
    }
    return R::findAll('p_eventos', "inner join p_tipo_justificativa on p_eventos.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa where data_evento = '{$_dataEvento}' and (id_usr = " . $id_usr . " or id_depto = 0)");
}

function podeRegistrar($id_usr, $dataHora)
{
    $dataHora = new DateTime(mudaData($dataHora));
    $qtde = getQtdePontosEsperados($id_usr);
    $registros = getRegistrosUsuario($id_usr, $dataHora->format('d/m/Y'));
    if (count($registros) == $qtde) {
        return false;
    } elseif ($qtde == 4 && count($registros) == 3) {
        if (getMinutos($dataHora->format('H:i')) - getMinutos($registros[3]->hora . ':' . $registros[3]->minutos) < 15) {
            return false;
        }
    }
    return true;
}

function getTipoJustificativa($id_tipo_justificativa)
{
    return R::findOne(BaseObject::TIPO_JUSTIFICATIVA, "id_tipo_justificativa = '{$id_tipo_justificativa}'");
}

/**
 * @param $dataInicio
 * @param $dataFim
 * @return array
 * retorno um array com DateTimes dos dias entre o período informado
 * que são de segunda a sexta
 */
function getDiasUteis($dataInicio, $dataFim)
{
    $out = [];

    $oDataInicio = new DateTime(mudaData($dataInicio));
    $oDataFim = new DateTime(mudaData($dataFim));

    while ($oDataInicio < $oDataFim) {
        if (!in_array($oDataInicio->format('w'), [0, 6])) {
            $out[] = $oDataInicio;
        }
        $dataInicio->modify("+1 day");
    }

    return $out;

}

function registraOcorrencia($id_usr, $dataHora)
{
    $out = getPossivelOcorrencia($id_usr, $dataHora);
    $_dataHora = new DateTime(mudaData($dataHora));
    $horas = $_dataHora->format("H:i");

    if ($out->situacao) {
        $usuario = getUsuario($id_usr);
        $bean = R::dispense(BaseObject::OCORRENCIA);
        $bean->situacao = $out->situacao;
        $bean->id_usr = $id_usr;
        $bean->dataInclusao = date('Y-m-d H:i:s');
        $bean->mensagem = $out->mensagem;
        $bean->etapa = $out->etapa;
        $bean->descricao = "Ocorrência registrada para {$usuario->nome}. Registro de ponto às {$horas} com horario previsto de {$out->horaPrevista}";
        //enviarEmailForaDaGrade($id_usr);
        R::store($bean);
    }
}

/**
 * @param $id_usr
 * @param $dataHora
 * @return stdClass $out
 *
 * -situações que impedem o registro:
 * ---- SITUACAO 1 Menos de 1 hora de almoço
 * ---- SITUACAO 2 Todos os registros da grade registrados
 * ---- SITUACAO 3 Menos de 15 minutos do registro anterior
 *
 * -situações que geram ocorrência
 * ---- SITUACAO 4 Atraso  > 15 minutos do retorno do almoço
 * ---- SITUACAO 5 Atraso ou Adiantado por mais de 30 minutos
 * ---- SITUACAO 6 Carga horária do dia não cumprida
 * ---- SITUACAO 7 Carga horária do dia excedeu a tolerancia do maximo
 *
 */
function getPossivelOcorrencia($id_usr, $dataHora)
{
    $out = new stdClass();
    $out->trava = false;
    $out->enviaEmail = false;
    $out->mensagem = '';
    $out->situacao = 0;
    $out->titulo = "PONTO FORA DA GRADE";

    $oDataHora = new DateTime(mudaData($dataHora));
    $horaAtual = $oDataHora->format('H:i');
    $qtde = getQtdePontosEsperados($id_usr);
    $registros = getRegistrosUsuario($id_usr, $dataHora);
    $gradeUsuario = getGradeUsuario($id_usr);
    $detalhes = getDetalhesDoDia(getUsuario()->id_usr, $oDataHora->format('d/m/Y'));
    $etapasUsuario = getEtapasUsuario($id_usr);
    $proximaEtapa = getProximaEtapa($id_usr, $dataHora);
    $out->etapa = $proximaEtapa;

    $tempoAlmoco = BaseObject::TEMPO_MINIMO_ALMOCO;
    //$tempoAlmoco = getMinutos($gradeUsuario->entrada_2) - getMinutos($gradeUsuario->saida_1) - BaseObject::TOLERANCIA_DIARIA_MENOS;
    //$tempoAlmoco >= 60 ? $tempoAlmoco : 60;
    $out->horaPrevista = $etapasUsuario[$proximaEtapa];

   if (date('Ymd') > '20151110' && strpos(getUsuario($id_usr)->id_depto, '401180104') === false) {
		$out->titulo = "NOVO SISTEMA DE PONTO";
        $out->situacao = 7;
        $out->trava = true;
        $out->mensagem = 'Você precisa bater o ponto no novo sistema!';
    } elseif (count($registros) == $qtde) {
        //SITUACAO 2
        $out->situacao = 2;
        $out->trava = true;
        $out->mensagem = 'Você já fez todos os registros de hoje!';
    } elseif ($qtde == 4 && count($registros) == 2) {
        //SITUAÇÕES DE ROTORNO DO ALMOÇO
        //SITUACAO 1
        if ( (getMinutos($horaAtual) - getMinutos($registros[2]->hora . ':' . $registros[2]->minutos)) < $tempoAlmoco) {
            $out->situacao = 1;
            $out->trava = true;
            $out->mensagem = 'É obrigatório pelo menos 1 hora de almoço!';
            //SITUACAO 4
        } elseif ((getMinutos($horaAtual) - getMinutos($gradeUsuario->entrada_2)) > BaseObject::TOLERANCIA_ALMOCO) {
            $out->situacao = 4;
            $out->mensagem = 'Tolerância para retorno do almoço é de ' . BaseObject::TOLERANCIA_ALMOCO . ' minutos. ';
            $out->mensagem .= "Serão enviados emails para sua caixa postal e de seu gerente. É obrigatório apresentar
                            justificativa   por registro fora da grade.";
            $out->enviaEmail = true;

            //eximindo de solicitar confirmacao
            $out->mensagem = '';

        }
    } else {
        //SITUAÇÕES DE DESCUMPRIMENTO DE GRADE
        $registro = getUltimoRegistroDoDia($id_usr, $dataHora);
        if ((getMinutos($horaAtual) - getMinutosRegistro($registro)) < BaseObject::TEMPO_MINIMO_DIFERENCA_REGISTRO_ANTERIOR) {
            //SITUACAO 3
            $out->situacao = 3;
            $out->trava = true;
            $out->mensagem = 'É preciso esperar pelo menos 15 minutos para registrar outro ponto!';
        } elseif ($etapa = getProximaEtapa($id_usr, $dataHora)) {
            $minutos = getMinutos(getHoraEtapaUsuario($id_usr, $etapa));
            //SITUACAO 5
            if (abs($minutos - getMinutos($horaAtual)) > BaseObject::TOLERANCIA) {
                $out->trava = false;
                $out->situacao = 5;
                $out->mensagem = "Serão enviados emails para sua caixa postal e de seu gerente. É obrigatório apresentar
                            justificativa por registro fora da grade.";
                $out->enviaEmail = true;
                //eximindo de solicitar confirmacao
                $out->mensagem = '';

            } elseif (count($registros) == $qtde - 1) {
                //ÚLTIMO PONTO DO DIA
                //SE NAO CUMPRIU A JORNADA
                if (getMinutos($detalhes['horasAtuais']) < (getMinutos($detalhes['horasGrade']) + BaseObject::TOLERANCIA_DIARIA_MENOS)) {
                    //SITUACAO 6
                    $out->situacao = 6;
                    $out->trava = false;
                    $out->mensagem = "Você ainda não cumpriu sua carga horária de {$detalhes['horasGrade']} horas. Você cumpriu {$detalhes['horasAtuais']} horas.<br />";
                    $out->mensagem .= "Serão enviados emails para sua caixa postal e de seu gerente. É obrigatório apresentar
                            justificativa por registro fora da grade.";
                    $out->enviaEmail = true;
                } elseif (getMinutos($detalhes['horasGrade']) > (getMinutos($detalhes['horasAtuais']) + BaseObject::TOLERANCIA_DIARIA_MAIS)) {
                    //SE PASSOU DA JORNADA POR MAIS QUE O TOLERAVEL
                    //SITUACAO 7
                    $out->situacao = 7;
                    $out->titulo = "CARGA HORÁRIA EXCEDIDA";
                    $out->trava = false;
                    $out->mensagem = "Você excedeu as horas previstas de {$detalhes['horasGrade']} horas. <br />";
                    $out->mensagem .= "Serão enviados emails para sua caixa postal e de seu gerente. É obrigatório apresentar
                            justificativa por registro fora da grade.";
                    $out->enviaEmail = true;
                }
            }
        }
    }

    return $out;
}

function getGerentes()
{
    return R::findAll(BaseObject::USUARIO, "gerente > 0");
}

function temHoraNaoAutorizada($id_usr, $data)
{
    return getUsuario($id_usr)->temHoraNaoAutorizada($data);
}

function getDiferencaGrade($id_registro)
{
    $registro = getRegistro($id_registro);
    $horaEsperada = getHoraEtapaUsuario($registro->id_usr, $registro->etapa);
    $diff = getMinutos($horaEsperada) - getMinutos(formataHoras($registro));
    $data = new DateTime($registro->data_registro);

    //se for a primeira etapa de um dia que teve possiveis horas extras, retorno
    //a diferença entre a hora da grade e a feita
    if ($registro->etapa == 1) {
        $minutosGrade = getMinutos(getHorasGrade(getUsuario($registro->id_usr)->id_grade));
        $minutosTrabalhados = getMinutos(getHorasTrabalhadas($registro->id_usr, $data->format('d/m/Y')));
        $minutosExtra = $minutosTrabalhados - $minutosGrade;
        if (!geraOcorrencia($id_registro) && $minutosExtra > BaseObject::TOLERANCIA_DIARIA_MAIS) {
            $diff += $minutosExtra;
        }
    }

    //para registros inseridos automaticamente nao calculo a inconcistencia
    if ($data->format('H') != '00' && formataHoras($registro) >= 1 || $diff > 0) {
        //return ($diff < 0 ? '-' : '') . getHoras(abs($diff));
        return getHoras(abs($diff));
    }
    return '';
}

function getHoraEtapaUsuario($id_usr, $etapa)
{
    $grade = getGradeUsuario($id_usr);
    switch ($etapa) {
        case 1:
            return $grade->entrada_1;
            break;
        case 2:
            return $grade->saida_1;
            break;
        case 3:
            return $grade->entrada_2;
            break;
        case 4:
            return $grade->saida_2;
            break;
    }
}

function getEtapasUsuario($id_usr)
{
    $out = [];
    $grade = getGradeUsuario($id_usr);
    $out[1] = $grade->entrada_1;
    $out[2] = $grade->saida_1;
    if ($grade->entrada_2) {
        $out[3] = $grade->entrada_2;
        $out[4] = $grade->saida_2;
    }
    return $out;
}

function getUltimoRegistroDoDia($id_usr, $dataHora = null)
{
    if (empty($dataHora)) {
        $dataHora = date('d/m/Y');
    }
    $data = new DateTime(mudaData($dataHora));
    return R::findOne(BaseObject::REGISTRO, "id_usr = '{$id_usr}' and data_registro like '" . $data->format('Y-m-d') . "%' order by id_registro desc");
}

function getProximaEtapa($id_usr, $dataHora = null)
{
    if (empty($dataHora)) {
        $dataHora = date('d/m/Y');
    }
    $ultimo = getUltimoRegistroDoDia($id_usr, $dataHora);
    if ($ultimo->etapa < getQtdePontosEsperados($id_usr)) {
        return ($ultimo->etapa ? $ultimo->etapa + 1 : 1);
    }
    return false;
}

function getHorasRegistro($registro)
{
    return $registro->hora . ':' . $registro->minutos;
}

function getMinutosRegistro($registro)
{
    return getMinutos(getHorasRegistro($registro));
}

function gravarTentativa($id_usr, $data_tentativa, $texto, $numr_ip)
{
    R::exec("insert into p_tentativa (id_usr,data_tentativa,tipo_dispositivo,numr_ip) values (" . $id_usr . ",now(),'" . $texto . "','" . $numr_ip . "')");
}

function getIpsAutorizados()
{
    return R::find('ips_autorizados');
}

function getIpAutorizado($ip)
{
    return R::findOne('ips_autorizados', "ip = '{$ip}'");
}

function podeAcessar()
{
    return R::findOne('ips_autorizados', "ip in ('{$_SERVER['REMOTE_ADDR']}','{$_SERVER['HTTP_X_FORWARDED_FOR']}')");
}

function diaPrecisaJustificativa($id_usr, $data)
{
    $_data = new DateTime(mudaData($data));
    $_hoje = new DateTime();
    $usuario = getUsuario($id_usr);
    $registrosDoDia = getRegistrosUsuario($id_usr, $data);

    if (
        ($_hoje->format('m') > $_data->format('m')) && getWorkingDays($_hoje->format('Y-m-01'), $_hoje->format('Y-m-d'), []) > 110
    ) {
        return false;
    }

    if (getQtdePontosEsperados($id_usr) > count($registrosDoDia)) {
        return true;
    }

    if ($usuario->descumpriuJornada($data)) {

        return true;
    }

    //foreach ($registrosDoDia as $registro) {
    //    if (geraOcorrencia($registro->id_registro)) {
    //       return true;
    //   }
    //}

    return false;
}

function isVPN()
{
    return false;
    $temp2 = (R::findOne('ips_autorizados', "ip = '{$_SERVER['REMOTE_ADDR']}' and local = 'VPN'"));
    return !empty($temp2);
}

function registraDadosConexao($dadosConexao_)
{
    $c = new MCrypt();
    $dadosConexao = $c->decrypt($dadosConexao_);
    $json = str_replace(["/"], ["\/"], $dadosConexao);
    $obj = json_decode($json);
    $registro = R::findOne('dadosconexao', "dados='{$obj->dados}'");
    if (empty($registro)) {
        $registro = R::dispense('dadosconexao');
        $registro->dados = addslashes($obj->dados);
        $registro->dadosAdicionais = json_encode($obj->dadosAdicionais);
        $registro->dataInclusao = date('Y-m-d H:i:s');
        $id = R::store($registro);
    } else {
        $id = $registro->id;
    }
    return $id;
}

function gravarRegistro($usuario, $params = [])
{
    try {
        R::begin();
        $registroBean = R::pdispense(BaseObject::REGISTRO);
        $registroBean->id_usr = $usuario->id_usr;
        $registroBean->data_registro = $params['data_registro'];
        $registroBean->etapa = $params['etapa'];
        $registroBean->numr_ip = $params['numr_ip'];
        $registroBean->hora = $params['hora'];
        $registroBean->minutos = $params['minutos'];
        $registroBean->origem = $params['texto'];
        $registroBean->tsp = $params['tempo'];
        $registroBean->dadosConexaoId = $params['codigoDadosConexao'];
        $registroBean->nao_registrou = $params['nao_registrou'] ? $params['nao_registrou'] : 0;
        $registroBean->id_grade = $usuario->id_grade;
        $ultimo = R::store($registroBean);
        $registroBean->id_registro = $ultimo;
        R::store($registroBean);
        R::commit();
    } catch (Exception $e) {
        R::rollback();
        header("Location: https://pontoeletronico.sed.go.gov.br/noimplo.php?erro_ponto=1");
    }
    return $ultimo;
}

function getDiaSemana($dia = null)
{
    $dia = $dia ? $dia : date('w');
    switch ($dia) {
        case 0 :
            return 'Domingo';
            break;
        case 1 :
            return 'Segunda-feira';
            break;
        case 2 :
            return 'Terça-feira';
            break;
        case 3 :
            return 'Quarta-feira';
            break;
        case 4 :
            return 'Quinta-feira';
            break;
        case 5 :
            return 'Sexta-feira';
            break;
        case 6 :
            return 'Sábado';
            break;
    }
}

function getWorkingDays($startDate, $endDate, $holidays)
{
    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);

    $days = ($endDate - $startDate) / 86400 + 1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    if ($the_first_day_of_week <= $the_last_day_of_week) {
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    } else {
        if ($the_first_day_of_week == 7) {
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                $no_remaining_days--;
            }
        } else {
            $no_remaining_days -= 2;
        }
    }

    $workingDays = $no_full_weeks * 5;
    if ($no_remaining_days > 0) {
        $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    foreach ($holidays as $holiday) {
        $time_stamp = strtotime($holiday);
        if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
            $workingDays--;
    }

    return $workingDays;
}
