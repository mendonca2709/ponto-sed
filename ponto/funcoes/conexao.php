<?php
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', true);
session_start();
//$_SESSION['config'] = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/config/properties.json'));
$__BANCO = array_filter([getenv('MYSQL_DATABASE'), 'ponto']);
$__HOST = array_filter([getenv('MYSQL_HOST'), 'localhost']);
$__PORT = array_filter([getenv('MYSQL_PORT'), '3306']);
$__USER = array_filter([getenv('MYSQL_USER'), 'ponto']);
$__PASSWORD = array_filter([getenv('MYSQL_PASSWORD'), '123456']);

$_SESSION['config'] = json_decode(
    json_encode(
        [
            "conexao" => [
                "uri" => array_shift($__HOST),// . ':' . array_shift($__PORT),
                "banco" => array_shift($__BANCO),
                "usuario" => array_shift($__USER),
                "senha" => array_shift($__PASSWORD)
            ],
            "pastaFotos" => "/var/www/html/ponto/fotos/",
            "pastaFotosUsuarios" => "/var/www/html/ponto/fotos/fotos_usuarios/",
            "pastaAnexosJustificativas" => "/var/www/html/ponto/fotos/anexos_justificativas/"
        ]
    )
);
$_SERVER['DOCUMENT_ROOT'] = '/var/www/html/ponto';
$conexao = mysql_connect($_SESSION['config']->conexao->uri, $_SESSION['config']->conexao->usuario, $_SESSION['config']->conexao->senha) or print (mysql_error());

mysql_set_charset("UTF8");
mysql_select_db($_SESSION['config']->conexao->banco, $conexao) or print(mysql_error());
date_default_timezone_set('America/Sao_Paulo');
require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/BaseObject.php';
R::setup("mysql:host={$_SESSION['config']->conexao->uri};dbname={$_SESSION['config']->conexao->banco}", $_SESSION['config']->conexao->usuario, $_SESSION['config']->conexao->senha); //for both mysql or mariaDB
R::ext('pdispense', function ($type) {
    return R::getRedBean()->dispense($type);
});
require_once "funcoes.php";
$oUsuario = getUsuario();
?>
