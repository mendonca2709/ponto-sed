<?php
session_destroy();
require_once './funcoes/conexao.php';
?>
<?php session_start();

$bt = $_GET["bt"]; ?>
<head><title>Registro de Ponto</title>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <!--    <link rel="stylesheet" type="text/css" media="screen" href="./css/estilos.css">-->
    <link rel="stylesheet" type="text/css" media="screen" href="./js/jquery-ui-1.11.3.custom/jquery-ui.min.css">
    <script language="JavaScript" src="./js/jquery-2.1.3.min.js"></script>
    <script language="JavaScript" src="./js/jquery-ui-1.11.3.custom/jquery-ui.min.js"></script>
    <script language="JavaScript" src="./js/webcamjs/webcam.min.js"></script>
    <script language="JavaScript" src="./funcoes/funcao.js"></script>
    <script language="JavaScript" src="./js/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js"></script>
    <link rel="shortcut icon" href="./login_files/bandeira_de_goias.gif" type="image/x-icon">
    <link rel="stylesheet" href="./css/bootstrap-3.3.2-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="./css/bootstrap-3.3.2-dist/css/bootstrap-theme.min.css"/>
    <script language="JavaScript" src="./css/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
    <style>
        .white {
            color: #ffffff;
        }

        .no-padding {
            padding: 0 !important;
        }

        #grad {
            background: rgb(0, 0, 102); /* Old browsers */
            background: -moz-linear-gradient(left, rgba(0, 0, 102, 1) 0%, rgba(0, 0, 229, 1) 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%, rgba(0, 0, 102, 1)), color-stop(100%, rgba(0, 0, 229, 1))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(left, rgba(0, 0, 102, 1) 0%, rgba(0, 0, 229, 1) 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(left, rgba(0, 0, 102, 1) 0%, rgba(0, 0, 229, 1) 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(left, rgba(0, 0, 102, 1) 0%, rgba(0, 0, 229, 1) 100%); /* IE10+ */
            background: linear-gradient(to right, rgba(0, 0, 102, 1) 0%, rgba(0, 0, 229, 1) 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#000066', endColorstr='#0000e5', GradientType=1); /* IE6-9 */
            -webkit-box-shadow: 0px 5px 5px 0px rgba(196, 194, 196, 1);
            -moz-box-shadow: 0px 5px 5px 0px rgba(196, 194, 196, 1);
            box-shadow: 0px 5px 5px 0px rgba(196, 194, 196, 1);
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <header>
        <div class="row white" style="background-color:#000066;">
            <div class="col-lg-8"><img class="img-responsive" src="./login_files/bandeira_barra_gov.png"></div>
            <div class="col-lg-4 text-right"><h5>SED Net - Portal de Sistemas da SED</h5></div>
        </div>
        <div class="row white" style="background-color: #2a303c;position:relative;">
            <div class="col-lg-3">
                <img style="position:absolute;top:50px;" src="./login_files/sed_logo.png" style=""/>
            </div>
            <div class="col-lg-6">
                <h1 class="text-center">Sistema de Ponto</h1>
            </div>
            <div class="col-lg-3">
                <img class="img-responsive col-lg-3 pull-right" src="./login_files/brasao.png">
            </div>
        </div>
    </header>
    <section>
        <div class="row no-padding">
            <div class="col-lg-1 pull-right" style="background:#648cc4;">&nbsp;</div>
            <div class="col-lg-1 pull-right" style="background:#314f88;">&nbsp;</div>
            <div class="col-lg-1 pull-right" style="background:#34653f;">&nbsp;</div>
            <div class="col-lg-1 pull-right" style="background:#a2be47;">&nbsp;</div>
            <div class="col-lg-1 pull-right" style="background:#dddc34;">&nbsp;</div>
            <div class="col-lg-1 pull-right" style="background:#faed1f;border-radius: 0 0 0 25px">&nbsp;</div>
        </div>
        <div class="row">
            <div class="container" style="padding:5%;">
                <?php
                if (podeAcessar()) {
                    if (!$html) {
                        include './formulario_ponto.php';
                    } else {
                        echo $html;
                    }
                } else {
                    include './nao_permissao.php';
                }
                ?>
            </div>
        </div>
    </section>
    <footer style="position: fixed;bottom:0;width:100%;">
        <div class="row white" id="grad">
            <h4 class="text-center">
                Secretaria de Desenvolvimento Econômico, Científico, Tecnológico e de Agricultura, Pecuária e
                Irrigação
            </h4>
        </div>
    </footer>
</div>

</body>
</html>