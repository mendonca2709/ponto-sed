<?php
/**
 * Created by PhpStorm.
 * User: marcelo-tm
 * Date: 06/04/2015
 * Time: 15:54
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/funcoes/conexao.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/funcoes/funcoes.php';
$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
$mime = finfo_file($finfo, $_SESSION['config']->pastaAnexosJustificativas . $_REQUEST['anexo']);
header("Content-Type: " . $mime);
readfile($_SESSION['config']->pastaAnexosJustificativas . $_REQUEST['anexo']);