﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == '') {
    ?>
    <script language="JavaScript">
        alert("Sua sessão expirou!\n\nLogue no sistema novamente!");
        window.location.href = 'http://<?php echo $_SERVER['HTTP_HOST'];?>../adm/index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}

require_once("../funcoes/conexao.php");


$cont = 0;
$inicio = $_POST["inicio"];
if ($inicio == '') {
    $inicio = 0;
}
$texto_busca = $_POST['texto_busca'];
$final = $inicio + 40;
$enviados = $_POST["enviados"];
if ($enviados == "") {
    $enviados = 0;
}
$final = $_POST["final"];
$texto_busca = $_POST["texto_busca"];

$tipo_ocorrencia = $_POST["tipo_ocorrencia"];
if ($tipo_ocorrencia == "") {
    $tipo_ocorrencia = 0;
}
//echo $enviados." - ".$tipo_ocorrencia;

$id_usr = $_POST["id_usr"];
if ($id_usr == "") {
    $id_usr = '0';
}

$id_depto = $_POST["id_depto"];
if ($id_depto == "") {
    $id_depto = 0;
}
if ($_SESSION["sessao_rh"] == 0 && $_SESSION["usr_gerente"] == 1) {
    $id_depto = $_SESSION["sessao_id_depto"];
}


$txt_data = $_POST["txt_data"];
$DIA = substr($txt_data, 0, 2);
if ($DIA == "") {
    $DIA = date('d');
}


$MES = substr($txt_data, 3, 2);

//$MES 			= $_POST["selMES"];
if ($MES == "") {
    $MES = date('m');
}


//$ANO 			= $_POST["selANO"];
$ANO = substr($txt_data, 6, 4);
if ($ANO == "") {
    $ANO = date('Y');
}


//echo $DIA." - ".$MES." - ".$ANO;


//#################################################################################################################################################
$sql_total_busca = "select count(usuarios.id_usr) as total_busca from p_registro inner join usuarios on p_registro.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where nao_registrou = 0 ";
if ($DIA != 13) {
    $sql_total_busca = $sql_total_busca . " and day(data_registro) = " . $DIA;
}

if ($MES != 13) {
    $sql_total_busca = $sql_total_busca . " and month(data_registro) = " . $MES;
}
if ($ANO != 13) {
    $sql_total_busca = $sql_total_busca . " and year(data_registro) = " . $ANO;
}

if ($id_depto != 0) {
    $sql_total_busca = $sql_total_busca . " and usuarios.id_depto = " . $id_depto;
}
if ($id_usr != 0) {
    $sql_total_busca = $sql_total_busca . " and usuarios.id_usr = " . $id_usr;
}
$dados_total_busca = mysql_query($sql_total_busca, $conexao);
$resultado_total_busca = mysql_fetch_array($dados_total_busca);
$total = $resultado_total_busca[total_busca];

//#################################################################################################################################################
$sql = "select * from p_registro inner join usuarios on p_registro.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where nao_registrou = 0 ";
if ($DIA != 13) {
    $sql = $sql . " and day(data_registro) = " . $DIA;
}
if ($MES != 13) {
    $sql = $sql . " and month(data_registro) = " . $MES;
}
if ($ANO != 13) {
    $sql = $sql . " and year(data_registro) = " . $ANO;
}

if ($id_depto != 0) {
    $sql = $sql . " and usuarios.id_depto = " . $id_depto;
}
if ($id_usr != 0) {
    $sql = $sql . " and usuarios.id_usr = " . $id_usr;
}
$sql = $sql . " order by id_registro desc limit " . $inicio . ",40";
$dados = mysql_query($sql, $conexao);

if ($texto_busca != '') {
    $total = $total_busca;
}


?>


<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>


<head>
    <title>Ocorr&ecirc;ncias</title>

</head>
<script language="JavaScript">
    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }
        else {
            return true;
        }
    }
    function fncMontacombo(id_depto) {
        janela.location.href = 'frmMontafuncionarios.php?id_depto=' + id_depto;
    }
    function fncJustifica(id_usr, mes, ano, id_depto) {
        document.justifica.id_usr.value = id_usr;
        document.justifica.mes.value = mes;
        document.justifica.ano.value = ano;
        document.justifica.id_depto.value = id_depto;
        document.justifica.submit();

    }
    function AbreJustifivativa(id_usr, dia, mes, ano) {

        //document.abre_janela2.id_justificativa.value=id_justificativa;
        document.abre_janela2.id_usr.value = id_usr;
        document.abre_janela2.dia.value = dia;
        document.abre_janela2.mes.value = mes;
        document.abre_janela2.ano.value = ano;
//    window.open("","myNewWin","width=500,height=300,toolbar=0"); 
        window.open("", "myNewWin", "");
        var a = window.setTimeout("document.abre_janela2.submit();", 500);
    }
    function fncCamadas() {

//	setTimeout("document.lista.selANO.value='<?php echo $ANO?>'", 100); 
//	setTimeout("document.lista.selMES.value='<?php echo $MES?>'", 200); 
        if (<?php echo $_SESSION["sessao_rh"];?>==1 || <?php echo $_SESSION["usr_gerente"];?> == 2
    )
        {

            setTimeout("document.lista.id_depto.value='<?php echo $id_depto?>'", 600);
            setTimeout("fncMontacombo('<?php echo $id_depto?>')", 900);

            if (<?php echo $id_usr;?>!=
            0
        )
            {

                setTimeout("fncColoca2()", 1400);
            }
        }

        if (<?php echo $_SESSION["usr_gerente"];?>>=
        1
    )
        {
            setTimeout("document.lista.id_usr.value='<?php echo $id_usr?>'", 1600);
        }
    }
    function fncColoca2() {

        document.lista.id_usr.value = '<?php echo $id_usr?>'
    }
    function sendme(id_justificativa) {
        document.abre_janela.id_justificativa.value = id_justificativa;
//    window.open("","myNewWin","width=500,height=300,toolbar=0"); 
        window.open("", "myNewWin", "");
        var a = window.setTimeout("document.abre_janela.submit();", 500);
    }
    function fncValida() {
        document.lista.submit()
    }
    function fncEnter() {
        if (window.event.keyCode == 13) {
            fncValida(0, document.form_busca.texto_busca.value, 10, -1, 1);
        }
    }
    function fncHistorico(id_justificativa) {
        document.abre_janela2.id_justificativa.value = id_justificativa;
//    window.open("","myNewWin","width=500,height=300,toolbar=0"); 
        window.open("", "myNewWin", "");
        var a = window.setTimeout("document.abre_janela2.submit();", 500);
    }


    function fncVerLista(id_justificativa) {
        window.open("ListaEnviados.php?id_justificativa=" + id_justificativa, "", "toolbar=no,location=no,directories=no,tipo_ocorrencia=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=830,height=800,top=10,left=10");
    }

    function getPosicaoElemento(elemID) {
        var offsetTrail = document.getElementById(elemID);
        var offsetLeft = 0;
        var offsetTop = 0;
        while (offsetTrail) {
            offsetLeft += offsetTrail.offsetLeft;
            offsetTop += offsetTrail.offsetTop;
            offsetTrail = offsetTrail.offsetParent;
        }
        if (navigator.userAgent.indexOf("Mac") != -1 &&
            typeof document.body.leftMargin != "undefined") {
            offsetLeft += document.body.leftMargin;
            offsetTop += document.body.topMargin;
        }
        return {left: offsetLeft, top: offsetTop};

    }
    function Fechar() {
        tipo_ocorrencia_opcoes.style.display = 'none';
    }

    function mapa(cep) {
//window.open("mapa2.php?cep="+cep, "", "toolbar=no,location=no,directories=no,tipo_ocorrencia=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=640,height=480,top=20,left=50");
        window.open("http://www.google.com.br/maps?f=q&source=s_q&hl=pt-BR&geocode=&q=" + cep + "&aq=&sll=-14.239424,-53.186502&sspn=57.994688,79.013672&ie=UTF8&hq=&hnear=" + cep + "&z=17", "", "toolbar=no,location=no,directories=no,tipo_ocorrencia=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=900,height=600,top=20,left=50");
    }

    function Fechar() {
        tipo_ocorrencia_opcoes.style.display = 'none';
        tipo_ocorrencia_opcoes2.style.display = 'none';
    }


</script>

<body onLoad="javascript:fncCamadas()">
<?php require_once("frm_topo.php"); ?>




<br>

<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><BR><strong><font
                                    color="#333333" size="4">
                                </font></strong>

                        <p><strong><font color="#333333" size="4">&nbsp; Lista de Registros
                                    de Ponto - Por ordem de chegada<br>
                                </font><font color="#666666"><strong><font color="#0066FF"> </font></strong></font><font
                                    color="#333333" size="4">
                                </font></strong></p>

                        <form name="lista" action="lista_online.php" method="post">
                            <table width="100%" border="0" cellspacing="0" cellpadding="1">
                                <tr>
                                    <td width="9">&nbsp;</td>
                                    <td width="845" align="right">
                                        <table width="900" border="0" cellpadding="0" cellspacing="1">
                                            <tr>
                                                <td width="428" height="12" align="right"><strong><font color="#666666">Registros</font></strong>
                                                    <strong><font color="#666666"> de:
                                                            <?php if ($_SESSION["sessao_rh"] == 1 || $_SESSION["usr_gerente"] == 2) { ?>
                                                                <select name="id_depto" id="id_depto"
                                                                        style="width:250px"
                                                                        onChange="fncMontacombo(this.value);">
                                                                    <option value="0" selected>Todas Unidades</option>
                                                                    <?php
                                                                    if ($_SESSION["sessao_rh"] == 0) {
                                                                        if ($_SESSION["usr_gerente"] == 1) {
                                                                            $sql_und = "select id_depto,depto,sigla from depto where ativo = 1 and id_depto = " . $_SESSION["sessao_id_depto"] . " order by sigla;";
                                                                        }
                                                                        if ($_SESSION["usr_gerente"] == 2) {
                                                                            $sql_und = "select id_depto,depto,sigla from depto where ativo = 1 and id_superintendencia like concat(trim(TRAILING '0' from '{$_SESSION["sessao_id_superintendencia"]}'),'%') order by sigla;";
                                                                        }

                                                                    } else {
                                                                        $sql_und = "select id_depto,depto,sigla from depto where ativo = 1 order by sigla;";
                                                                    }
                                                                    $dados_und = mysql_query($sql_und, $conexao);
                                                                    while ($resultado_und = mysql_fetch_array($dados_und)) {
                                                                        ?>
                                                                        <option
                                                                            value="<?php echo $resultado_und[id_depto]; ?>"><?php echo $resultado_und[depto]; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            <?php } ?>
                                                        </font></strong></td>
                                                <td width="228" align="center"><strong><font style="font-size:10px"
                                                                                             color="#333333">
                                                            <?php if ($_SESSION["usr_gerente"] >= 1){ ?>
                                                        </font></strong>

                                                    <div id="div_id_usr"><strong><font color="#666666">
                                                                <select name="id_usr" id="id_usr" style="width:200px">
                                                                    <option value="0" selected>Todos funcionários
                                                                    </option>
                                                                    <?php
                                                                    $sql_usuarios = "select id_usr,nome from usuarios where id_depto = " . $_SESSION["sessao_id_depto"] . " order by nome";
                                                                    $dados_usuarios = mysql_query($sql_usuarios, $conexao);
                                                                    while ($resultado_usuarios = mysql_fetch_array($dados_usuarios)) {
                                                                        ?>
                                                                        <option
                                                                            value="<?php echo $resultado_usuarios[id_usr]; ?>"><?php echo $resultado_usuarios[nome]; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </font></strong></div>
                                                    <?php } ?>
                                                </td>
                                                <td width="240"><strong><font color="#666666">em:</font></strong>
                                                    <strong>
                                                        <input name="txt_data" type="text" id="txt_data" size="10"
                                                               maxlength="10"
                                                               onKeyPress="return txtBoxFormat(this, '99/99/9999', event);"
                                                               value="<?php echo $txt_data; ?>">
                                                    </strong> <strong></strong> <strong><font color="#666666">
                                                        </font></strong>
                                                    <label for="op3"></label>
                                                    <strong><font color="#666666"><a href="#"
                                                                                     onClick="fncValida();"><img
                                                                    src="../images/ok_bt.gif" width="27" height="15"
                                                                    border="0" title="Realiza o filtro"></a>
                                                            <input type="text" name="inicio"
                                                                   value="<?php echo $inicio; ?>" style="display:none">
                                                        </font></strong></td>
                                            </tr>
                                        </table>
                                        <strong><font color="#666666"> </font></strong></td>
                                </tr>
                            </table>
                        </form>
                        <br>
                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" valign="top" bgcolor="#FFFFFF"><br>
                        <table width="790" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
                            <tr valign="bottom">
                                <td height="25" class="Titulo_rel">
                                    <table width="790" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td width="322" height="12"><strong><font style="font-size:10px"
                                                                                      color="#333333">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;Usu&aacute;rio</font></strong></td>
                                            <td width="215"><strong><font style="font-size:10px" color="#333333">
                                                    </font></strong></td>
                                            <td width="167"><strong><font style="font-size:10px"
                                                                          color="#333333">Data</font></strong></td>
                                            <td width="76"><strong><font style="font-size:10px"
                                                                         color="#333333">Depto</font></strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="Itens_rel_s_traco">
                                <td height="15">
                                    <table width="790" border="0" cellpadding="2" cellspacing="0">
                                        <?php


                                        while ($resultado = mysql_fetch_array($dados)) {
                                        $cont = $cont + 1;
                                        $par = ($cont % 2); ?>
                                        <tr <?php if ($par == 0) {
                                            echo "class='Itens_sel_rel'";
                                        } else {
                                            echo "class='Itens_normal'";
                                        }
                                        ?>>
                                            <td width="6" height="25" align="center"><strong></strong></td>
                                            <td width="527" valign="middle"><strong><font color="#009933">
                                                        <div style="cursor:pointer;"
                                                             onClick="fncJustifica(<?php echo $resultado[id_usr] ?>,<?php echo substr($resultado[data_registro], 5, 2) ?>,<?php echo substr($resultado[data_registro], 0, 4) ?>,<?php echo $resultado[id_depto] ?>);">
                                                            <strong><font color="#009933"><a href="#"
                                                                                             onClick="fncJustifica(<?php echo $resultado[id_usr] ?>,<?php echo substr($resultado[data_registro], 5, 2) ?>,<?php echo substr($resultado[data_registro], 0, 4) ?>,<?php echo $resultado[id_depto] ?>);">
                                                                        <?php if ($resultado[foto] == 1) { ?>
                                                                            <img
                                                                                src="../fotos/<?php echo $resultado[id_usr] . "_usr.jpg" ?>"
                                                                                width="70" border="0" align="absmiddle">
                                                                        <?php } else {
                                                                            ?>
                                                                            <img src="../images/nouser.jpg"
                                                                                 alt="Funcion&aacute;rio sem foto"
                                                                                 width="70" border="0"
                                                                                 align="absmiddle">
                                                                        <?php } ?>
                                                                    </a></font></strong>&nbsp;&nbsp;&nbsp;<?php echo $resultado[nome]; ?>
                                                        </DIV>
                                                    </font></strong></td>
                                            <td width="167"> <?php echo date('d/m/Y H:i:s', strtotime($resultado[data_registro])); ?>
                                            </td>
                                            <td width="74"><strong><?php echo $resultado[sigla]; ?></strong></td>
                                            <?php } ?>
                                        </tr>
                                    </table>
                                    <BR>
                                    <?php if ($dados == false){ ?>
                                    <BR> <BR> <BR> <BR>
                                    <table width="72%" border="0" align="center" cellpadding="2" cellspacing="0">
                                        <tr>
                                            <td width="6%"><img src="../images/demo.png" width="71" height="71">
                                            </td>
                                            <td width="94%"><strong><font color="#FF0000">&nbsp;N&atilde;o
                                                        foram encontradas ocorr&ecirc;ncias com essa combina&ccedil;&atilde;o
                                                        no sistema!<BR>
                                                    </font><font color="#999999"></font></strong></td>
                                        </tr>
                                    </table>
                                    <p><BR>
                                        <BR>
                                        <BR>
                                        <BR>
                                        <?php } ?>
                                        <BR>
                                        <BR>
                                        <BR>
                                </td>
                            </tr>
                            <tr class="Sub_titulo_rel">
                                <td height="25">&nbsp;
                                    <table width="406" border="0" align="center" cellpadding="2">
                                        <tr align="center">
                                            <td><font color="#333333"><strong>Resultado: <?php echo $inicio + 1 ?>
                                                        até
                                                        <?php
                                                        if (($final) < $total) {
                                                            echo $final;
                                                        } else {
                                                            echo $total;
                                                        } ?>
                                                        - Total de <?php echo $total; ?>. </strong> </font></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <?php if ($inicio != 0){ ?>
                                                <a href="#"
                                                   onClick="document.lista.inicio.value=<?php echo($inicio - 40) ?>;document.lista.submit();"><img
                                                        src="../images/seta_esq.jpg" alt="P&aacute;gina anterior"
                                                        width="15" height="15" border="0">
                                                    <?php } ?>
                                                </a><font color="#FF6600">&nbsp;P&aacute;ginas: <strong>
                                                    </strong></font><font color="#FF6600"><strong>
                                                        <?php
                                                        $total_paginas = floor($total / 40);
                                                        if (($total % 40) > 0) {
                                                            $total_paginas++;
                                                        }
                                                        //echo "-".$total_paginas."-<BR>";]
                                                        $total_paginas_geral = $total_paginas;
                                                        if ($total_paginas > 40) {
                                                            $total_paginas = 40;
                                                        }
                                                        if ($inicio == 0) {
                                                            $pagina_inicial = 1;
                                                        }
                                                        if ($inicio > 0) {
                                                            $pagina_inicial = $inicio / 40;
                                                        }
                                                        if ($total_paginas < 40) {
                                                            $pagina_inicial = 1;
                                                        }
                                                        for ($p = 1; $p <= $total_paginas; $p++) {
                                                            if ($pagina_inicial <= $total_paginas_geral) {
                                                                $indice = $pagina_inicial * 40;
                                                                if ((($pagina_inicial - 1) * 40) != $inicio) {
                                                                    echo "<a href='#' onClick='document.lista.inicio.value=" . (($pagina_inicial - 1) * 40) . ";document.lista.submit();'><font color = '#333333'>" . $pagina_inicial . "</font></a>";
                                                                }
                                                                if ((($pagina_inicial - 1) * 40) == $inicio) {
                                                                    echo "<font color = '#99999'>" . $pagina_inicial . "</font>";
                                                                }
//if($p<15&&$p<$total_paginas&&($pagina_inicial<$total_paginas_geral)){
                                                                if ($pagina_inicial < $total_paginas_geral) {
                                                                    echo "<font color = '#999999'>,</font>";
                                                                }

                                                                if ($pagina_inicial == $total_paginas_geral) {
                                                                    echo "<font color = '#999999'>.</font>";
                                                                }
                                                                if ($p == 40) {
                                                                    echo "<font color = '#999999'>...</font>";
                                                                }
                                                                $pagina_inicial++;
                                                            }
                                                        }

                                                        ?>
                                                    </strong></font>
                                                <?php if (($inicio + 40) <= $total) { ?>
                                                    <a href="#"
                                                       onClick="document.lista.inicio.value=<?php echo($inicio + 40) ?>;document.lista.submit();"><img
                                                            src="../images/seta_dir.jpg"
                                                            alt="Pr&oacute;xima p&aacute;gina" width="15" height="15"
                                                            border="0"></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p><br>
                        </p>
                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="96%">&nbsp;</td>
                                <td width="4%">
                                    <div align="center"><a href="javascript:history.go(-1)"><img
                                                src="../images/reply-16x16.gif" title="Voltar" width="16"
                                                height="16" border="0"></a></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Busca</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <form name="form_busca" method="post" action="frm_Lista_manifestacoes.php">
                                        <font color="#FF6600" size="1"><strong>
                                                <input name="status" type="text" id="status" size="3"
                                                       style="display:none" value="<?php echo $status; ?>">
                                                <input name="selMES" type="text" id="selMES" size="3"
                                                       value="<?php echo $MES; ?>" style="display:none">
                                                <input name="selANO" type="text" id="selANO" size="3"
                                                       value="<?php echo $ANO; ?>" style="display:none">
                                                <input name="local_atual" type="text" id="local_atual" size="3"
                                                       value="<?php echo $local_atual; ?>" style="display:none">
                                                <input name="enviados" type="text" id="enviados" size="3"
                                                       value="<?php echo $enviados; ?>" style="display:none">
                                                <input name="texto" type="text" id="texto" size="3"
                                                       value="<?php echo $texto; ?>" style="display:none">
                                                <input name="inicio" type="text" id="inicio" size="3"
                                                       value="<?php echo $inicio; ?>" style="display:none">
                                                <input name="local_atual_busca" type="text" id="local_atual_busca"
                                                       size="3" value="<?php echo $local_atual_busca; ?>"
                                                       style="display:none">
                                                <br>
                                                Digite texto p/ busca:</strong></font><br>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><input type="text" name="texto_busca2" style="width:130px"
                                                           value="<?php echo $texto_busca; ?>" onKeyPress="fncEnter();">
                                                </td>
                                                <td><a href="#" onClick="fncBusca();"><img src="../images/ok_bt.gif"
                                                                                           width="27" height="15"
                                                                                           border="0"></a></td>
                                            </tr>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <br>
                        &nbsp;
                        <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                            <div align="center"><a href="lista_chamada.php"><img src="../images/lists.png"
                                                                                 width="110" height="110"
                                                                                 border="0"></a><br>
                                &nbsp;&nbsp;&nbsp;Lista Verifica&ccedil;&atilde;o
                            </div>
                        <?php } ?>
                    </td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?=date('Y')?> - SED&reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>

<form name="abre_janela" method="post" action="processo.php" target="myNewWin">
    <input type="hidden" name="id_justificativa">
</form>

<form name="abre_janela2" method="post" action="mostra_justificativa.php" target="myNewWin">
    <input type="hidden" name="id_justificativa">
    <input type="hidden" name="id_usr">
    <input type="hidden" name="dia">
    <input type="hidden" name="mes">
    <input type="hidden" name="ano">
</form>

<form name="justifica" method="post" action="frm_justificativa.php">
    <input type="hidden" name="id_usr">
    <input type="hidden" name="mes">
    <input type="hidden" name="ano">
    <input type="hidden" name="id_depto">
</form>
<iframe name="janela" width="800" height="200" style="display:none"></iframe>
</p>
</body>
</html>
