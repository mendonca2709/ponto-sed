﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == '') {
    ?>
    <script language="JavaScript">
        alert("Sua sessão expirou!\n\nLogue no sistema novamente!");
        window.location.href = 'http://<?php echo $_SERVER['HTTP_HOST'];?>../adm/index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}
?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<html>


<head>
    <title>Sistema de Ponto Eletr&ocirc;nico</title>

</head>

<body>
<script language="JavaScript" type="text/javascript">
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }

        document.form_busca.enviados.value = 20;
        document.form_busca.submit();
//window.location.href="frm_Lista_manifestacoes.php?texto_busca="+document.form_busca.texto_busca.value;

    }
    function fncEnter() {
        if (window.event.keyCode == 13) {
            fncBusca();
        }
    }

    function fncHierarquia() {
        window.open("hierarquia.php", "", "");
    }

</script>
<?php require_once("frm_topo.php"); ?>

<br>
<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><strong><font color="#333333"
                                                                                                       size="4">
                                    &nbsp; Configura&ccedil;&otilde;es<br>
                                    &nbsp;</font><font color="#333333">&nbsp; <font color="#666666">Perfil
                                        de:</font> <span class="copyright"><font color="#FF6600">
                                            <?php
                                            echo $_SESSION["sessao_usuario"]; ?>
                                        </font></span></font></strong></p></td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" valign="top" bgcolor="#FFFFFF"><br>
                        <table width="700" border="0" align="center" cellpadding="3" cellspacing="0">
                            <tr>
                                <td width="179" style="background-color: ">

                                </td>
                                <td width="21">&nbsp;</td>
                                <td width="188">
                                    <div align="center">
                                        <?php if (false && $_SESSION["sessao_rh"] == 1) { ?>
                                            <div align="center"><a href="cad_usuarios.php"> <img
                                                        src="../images/associados.png" width="100" height="72"
                                                        border="0"></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </td>
                                <td width="22">&nbsp;</td>
                                <td width="176">
                                    <?php if (false && $_SESSION["sessao_rh"] == 1) { ?>
                                        <div align="center"><a href="cad_deptos.php"><img
                                                    src="../images/company.png" width="97" height="80"
                                                    border="0"></a></div>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle">
                                </td>
                                <td>&nbsp;</td>
                                <td>
                                    <?php if (false && $_SESSION["sessao_rh"] == 1) { ?>
                                        <div align="center" class="tryit"
                                             onClick="window.location.href='cad_usuarios.php'">
                                            Cadastro de Usu&aacute;rios&nbsp; </div>
                                    <?php } ?>
                                </td>
                                <td>&nbsp;</td>
                                <td>
                                    <?php if (false && $_SESSION["sessao_rh"] == 1) { ?>
                                        <div align="center" class="tryit"
                                             onClick="window.location.href='cad_deptos.php'">Cadastro
                                            de Deptos&nbsp;</div>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr valign="middle">
                                <td>
                                    <div align="center">
                                        <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                                            <div align="center"><a href="frm_cadastro_grades.php"> <img
                                                        src="../images/historytime.png" width="100" height="100"
                                                        border="0"></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </td>
                                <td>
                                    <div align="center"></div>
                                </td>
                                <td>
                                    <div align="center">
                                        <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                                            <div align="center"><a href="cad_eventos.php"> <img
                                                        src="../images/eventos.png" width="110" height="110"
                                                        border="0"></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td align="center">
                                    <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                                        <div align="center"><a href="lista_online.php"><img
                                                    src="../images/icone4.png" width="110" height="110" border="0"></a>
                                        </div>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr valign="middle">
                                <td>
                                    <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                                        <div align="center" class="tryit"
                                             onClick="window.location.href='frm_cadastro_grades.php'">
                                            Cadastro de Grade&nbsp; </div>
                                    <?php } ?>
                                </td>
                                <td></td>
                                <td>
                                    <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                                        <div align="center" class="tryit"
                                             onClick="window.location.href='cad_eventos.php'">
                                            Cadastro de Eventos&nbsp; </div>
                                    <?php } ?>
                                </td>
                                <td>&nbsp;</td>
                                <td>
                                    <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                                        <div align="center" class="tryit"
                                             onClick="window.location.href='lista_online.php'">Lista
                                            On-line
                                        </div>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr valign="middle">
                                <td>
                                    <div align="center"></div>
                                </td>
                                <td>
                                    <div align="center"></div>
                                </td>
                                <td>
                                    <div align="center"></div>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr valign="middle">
                                <td>
                                    <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                                        <div align="center"><a href="lista_chamada.php"><img
                                                    src="../images/lists.png" width="110" height="110"
                                                    border="0"></a></div>
                                    <?php } ?>
                                </td>
                                <td>&nbsp;</td>
                                <td>
                                    <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                                        <div align="center"><a href="frm_cadastro_ocorrencias.php"><img
                                                    src="../images/contro.png" width="110" height="110" border="0"></a>
                                        </div>
                                    <?php } ?>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr valign="middle">
                                <td>
                                    <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                                        <div align="center" class="tryit"
                                             onClick="window.location.href='lista_chamada.php'">Lista
                                            Verifica&ccedil;&atilde;o
                                        </div>
                                    <?php } ?>
                                </td>
                                <td>&nbsp;</td>
                                <td>
                                    <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                                        <div align="center" class="tryit"
                                             onClick="window.location.href='frm_cadastro_ocorrencias.php'">Tipos
                                            de ocorr&ecirc;ncias
                                        </div>
                                    <?php } ?>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p><br>
                        </p>
                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="96%">&nbsp;</td>
                                <td width="4%">
                                    <div align="center"><a href="javascript:history.go(-1)"><img
                                                src="../images/reply-16x16.gif" title="Voltar" width="16"
                                                height="16" border="0"></a></div>
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Busca</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <form name="form_busca" method="post" action="frm_Lista_ocorrencias.php">
                                        <font color="#FF6600" size="1"><strong>
                                                <input name="status" type="text" id="status" size="3"
                                                       style="display:none" value="<?php echo $status; ?>">
                                                <input name="selMES" type="text" id="selMES" size="3"
                                                       value="<?php echo $MES; ?>" style="display:none">
                                                <input name="selANO" type="text" id="selANO" size="3"
                                                       value="<?php echo $ANO; ?>" style="display:none">
                                                <input name="local_atual" type="text" id="local_atual" size="3"
                                                       value="<?php echo $local_atual; ?>" style="display:none">
                                                <input name="enviados" type="text" id="enviados" size="3"
                                                       value="<?php echo $enviados; ?>" style="display:none">
                                                <input name="texto" type="text" id="texto" size="3"
                                                       value="<?php echo $texto; ?>" style="display:none">
                                                <input name="inicio" type="text" id="inicio" size="3"
                                                       value="<?php echo $inicio; ?>" style="display:none">
                                                <input name="local_atual_busca" type="text" id="local_atual_busca"
                                                       size="3" value="<?php echo $local_atual_busca; ?>"
                                                       style="display:none">
                                                <br>
                                                Digite texto p/ busca:</strong></font><br>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><input type="text" name="texto_busca" style="width:130px"
                                                           value="<?php echo $texto_busca; ?>" onKeyPress="fncEnter();">
                                                </td>
                                                <td><a href="#" onClick="fncBusca();"><img src="../images/ok_bt.gif"
                                                                                           width="27" height="15"
                                                                                           border="0"></a></td>
                                            </tr>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <br>

                        <p align="center"><a href="../ajuda/" target="_blank"><img
                                    src="../images/respostas.png" alt="Manual do Sistema"
                                    width="87" height="62" border="0" class="icon"></a></p>

                        <p align="center"><a href="../ajuda/" target="_blank">Manual
                                do Sistema</a></p></td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?=date('Y')?> - SED&reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>&nbsp; </p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<iframe width="801" height="201" name="diaria_grava" frameborder="1" style="display:none"></iframe>

</body>
</html>
