﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == '') {
    ?>
    <script language="JavaScript">
        alert("Sua sessão expirou!\n\nLogue no sistema novamente!");
        window.location.href = 'http://<?php echo $_SERVER['HTTP_HOST'];?>../adm/index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}

require_once("../funcoes/conexao.php");


$cont = 0;
$inicio = $_POST["inicio"];
if ($inicio == '') {
    $inicio = 0;
}
$onde = $_POST["onde"];
if ($onde == '') {
    $onde = 2;
}


$texto_busca = $_POST['texto_busca'];
$final = $inicio + 40;
$enviados = $_POST["enviados"];
if ($enviados == "") {
    $enviados = 0;
}
$final = $_POST["final"];


$tipo_ocorrencia = $_POST["tipo_ocorrencia"];
if ($tipo_ocorrencia == "") {
    $tipo_ocorrencia = 0;
}
//echo $enviados." - ".$tipo_ocorrencia;

$id_usr = $_POST["id_usr"];
if ($id_usr == "") {
    $id_usr = '0';
}
$id_depto = $_POST["id_depto"];
if ($id_depto == "") {
    $id_depto = 0;
}
if ($_SESSION["sessao_rh"] == 0 && $_SESSION["usr_gerente"] == 1) {
    $id_depto = $_SESSION["sessao_id_depto"];
}


$MES = $_POST["selMES"];
if ($MES == "") {
    $MES = date('m');
}


$ANO = $_POST["selANO"];
if ($ANO == "") {
    $ANO = date('Y');
}


//#################################################################################################################################################
$sql_total = "select count(distinct data_justificativa,p_justificativa.id_usr) as total_andamento from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where 1 = 1";
if ($MES != 13) {
    $sql_total = $sql_total . " and month(data_justificativa) = " . $MES;
}
if ($ANO != 13) {
    $sql_total = $sql_total . " and year(data_justificativa) = " . $ANO;
}


if ($_SESSION["usr_gerente"] == 2) {
    $sql_total = $sql_total . " and depto.id_superintendencia = " . $_SESSION["sessao_id_superintendencia"];
}
if ($id_depto != 0) {

    $sql_total = $sql_total . " and usuarios.id_depto = " . $id_depto;

}
$sql_total = $sql_total . " and abonado = 0";//.$tipo_ocorrencia;
if ($id_usr != 0) {
    $sql_total = $sql_total . " and usuarios.id_usr = " . $id_usr;
}
$sql_total = $sql_total . " and registra = 1 ";
$dados_total = mysql_query($sql_total, $conexao);
$resultado_total = mysql_fetch_array($dados_total);
$total_andamento = $resultado_total[total_andamento];
//#################################################################################################################################################
$sql_abonado = "select count(distinct data_justificativa,p_justificativa.id_usr) as total_abonado from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where abonado = 1";
if ($MES != 13) {
    $sql_abonado = $sql_abonado . " and month(data_justificativa) = " . $MES;
}
if ($ANO != 13) {
    $sql_abonado = $sql_abonado . " and year(data_justificativa) = " . $ANO;
}

if ($_SESSION["usr_gerente"] == 2) {
    $sql_abonado = $sql_abonado . " and depto.id_superintendencia = " . $_SESSION["sessao_id_superintendencia"];
}
if ($id_depto != 0) {


    $sql_abonado = $sql_abonado . " and usuarios.id_depto = " . $id_depto;

}
if ($id_usr != 0) {
    $sql_abonado = $sql_abonado . " and usuarios.id_usr = " . $id_usr;
}

$sql_abonado = $sql_abonado . " and registra = 1 ";
$dados_abonado = mysql_query($sql_abonado, $conexao);
$resultado_abonado = mysql_fetch_array($dados_abonado);
$total_abonado = $resultado_abonado[total_abonado];

//#################################################################################################################################################
$sql_nao_abonado = "select count(distinct data_justificativa,p_justificativa.id_usr) as total_nao_abonado from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where (abonado = 2 or abonado = 3)";
if ($MES != 13) {
    $sql_nao_abonado = $sql_nao_abonado . " and month(data_justificativa) = " . $MES;
}
if ($ANO != 13) {
    $sql_nao_abonado = $sql_nao_abonado . " and year(data_justificativa) = " . $ANO;
}
if ($_SESSION["usr_gerente"] == 2) {
    $sql_nao_abonado = $sql_nao_abonado . " and depto.id_superintendencia = " . $_SESSION["sessao_id_superintendencia"];
}
if ($id_depto != 0) {


    $sql_nao_abonado = $sql_nao_abonado . " and usuarios.id_depto = " . $id_depto;

}
if ($id_usr != 0) {
    $sql_nao_abonado = $sql_nao_abonado . " and usuarios.id_usr = " . $id_usr;
}
$sql_nao_abonado = $sql_nao_abonado . " and registra = 1 ";
$dados_nao_abonado = mysql_query($sql_nao_abonado, $conexao);
$resultado_nao_abonado = mysql_fetch_array($dados_nao_abonado);
$total_nao_abonado = $resultado_nao_abonado[total_nao_abonado];

//#################################################################################################################################################
$sql_total_busca = "select count(distinct data_justificativa,p_justificativa.id_usr) as total_nao_abonado from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where 0=0 ";
$sql_total_busca = $sql_total_busca . " and observacoes like '%" . $texto_busca . "%'";
if ($_SESSION["usr_gerente"] == 2) {
    $sql_total_busca = $sql_total_busca . " and depto.id_superintendencia = " . $_SESSION["sessao_id_superintendencia"];
}
if ($id_depto != 0) {


    $sql_total_busca = $sql_total_busca . " and usuarios.id_depto = " . $id_depto;

}
if ($id_usr != 0) {
    $sql_total_busca = $sql_total_busca . " and usuarios.id_usr = " . $id_usr;
}
$dados_total_busca = mysql_query($sql_total_busca, $conexao);
$resultado_total_busca = mysql_fetch_array($dados_total_busca);
$total_busca = $resultado_total_busca[total_busca];
//#################################################################################################################################################
if ($tipo_ocorrencia == 2) {
    $sql = "select * from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where (abonado = 2 or abonado = 3)";
    if ($MES != 13) {
        $sql = $sql . " and month(data_justificativa) = " . $MES;
    }
    if ($ANO != 13) {
        $sql = $sql . " and year(data_justificativa) = " . $ANO;
    }
} else {
    $sql = "select * from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where abonado = " . $tipo_ocorrencia;
    if ($MES != 13) {
        $sql = $sql . " and month(data_justificativa) = " . $MES;
    }
    if ($ANO != 13) {
        $sql = $sql . " and year(data_justificativa) = " . $ANO;
    }
}
if ($_SESSION["usr_gerente"] == 2) {
    $sql = $sql . " and depto.id_superintendencia = " . $_SESSION["sessao_id_superintendencia"];
}
if ($id_depto != 0) {
    $sql = $sql . " and usuarios.id_depto = " . $id_depto;
}
if ($id_usr != 0) {
    $sql = $sql . " and usuarios.id_usr = " . $id_usr;
}
if ($texto_busca != '') {
    $sql = $sql . " and (observacoes like '%" . $texto_busca . "%' or observacoes_chefe like '%" . $texto_busca . "%' or usuarios.nome like '%" . $texto_busca . "%')";
}
$sql = $sql . " and registra = 1 ";
$sql = $sql . " group by data_justificativa,p_justificativa.id_usr order by id_justificativa desc limit " . $inicio . ",40";

$sql = str_replace('*', "*,CONCAT(CAST((SUM(SUBSTRING_INDEX(SUBSTRING_INDEX(qtd_horas, ':', 1), ':', 1)) +
                    (SUM(SUBSTRING_INDEX(SUBSTRING_INDEX(qtd_horas, ':', -1), ':', 1)) / 60)) AS UNSIGNED), ':', lpad(.6 *
                                                                                                                 SUBSTRING_INDEX(
                                                                                                                     SUBSTRING_INDEX(
                                                                                                                         (

                                                                                                                             SUM(
                                                                                                                                 SUBSTRING_INDEX(
                                                                                                                                     SUBSTRING_INDEX(
                                                                                                                                         qtd_horas,
                                                                                                                                         ':',
                                                                                                                                         -1),
                                                                                                                                     ':',
                                                                                                                                     1))
                                                                                                                             /
                                                                                                                             60),
                                                                                                                         '.',
                                                                                                                         -1),
                                                                                                                     '.',
                                                                                                                     1),2,0)) as qtd_horas", $sql);

$dados = mysql_query($sql, $conexao);

if ($texto_busca == '') {
    if ($enviados == 0) {
        $total = $total_andamento;
    }
    if ($enviados == 1) {
        $total = $total_abonado;
    }
    if ($enviados == 2) {
        $total = $total_nao_abonado;
    }
}

if ($texto_busca != '') {
    $total = $total_busca;
}

?>


<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>


<head>
    <title>Ocorr&ecirc;ncias</title>

</head>
<script language="JavaScript">
    function fncMontacombo(id_depto) {
        janela.location.href = 'frmMontafuncionarios.php?id_depto=' + id_depto;
    }
    function fncJustifica(id_usr, mes, ano, id_depto) {
        document.justifica.id_usr.value = id_usr;
        document.justifica.mes.value = mes;
        document.justifica.ano.value = ano;
        document.justifica.id_depto.value = id_depto;
        document.justifica.submit();

    }
    function envia_dados(id_depto, id_usr, selMES, selANO, onde) {

        document.ocorrencias.id_depto.value = id_depto;
        document.ocorrencias.id_usr.value = id_usr;
        document.ocorrencias.selMES.value = selMES;
        document.ocorrencias.selANO.value = selANO;
        document.ocorrencias.onde.value = onde;
        document.ocorrencias.submit();
    }
    function AbreJustifivativa(id_usr, dia, mes, ano, onde, id_depto_s) {

        //document.abre_janela2.id_justificativa.value=id_justificativa;
        document.abre_janela2.id_usr.value = id_usr;
        document.abre_janela2.dia.value = dia;
        document.abre_janela2.mes.value = mes;
        document.abre_janela2.ano.value = ano;

        document.abre_janela2.onde.value = onde;
        document.abre_janela2.id_depto_s.value = id_depto_s;

//    window.open("","myNewWin","width=500,height=300,toolbar=0"); 
        window.open("", "myNewWin", "");
        var a = window.setTimeout("document.abre_janela2.submit();", 500);
    }
    function fncCamadas() {

        setTimeout("document.lista.selANO.value='<?php echo $ANO?>'", 100);
        setTimeout("document.lista.selMES.value='<?php echo $MES?>'", 200);
        if (<?php echo $_SESSION["sessao_rh"];?>==1 || <?php echo $_SESSION["usr_gerente"];?> == 2
    )
        {

            setTimeout("document.lista.id_depto.value='<?php echo $id_depto?>'", 400);
            setTimeout("fncMontacombo('<?php echo $id_depto?>')", 500);

            if (<?php echo $id_usr;?>!=
            0
        )
            {

                setTimeout("fncColoca2()", 800);
            }
        }

        if (<?php echo $_SESSION["usr_gerente"];?>>=
        1
    )
        {
            setTimeout("document.lista.id_usr.value='<?php echo $id_usr?>'", 900);
        }
    }
    function fncColoca2() {

        document.lista.id_usr.value = '<?php echo $id_usr?>'
    }
    function sendme(id_justificativa) {
        document.abre_janela.id_justificativa.value = id_justificativa;
//    window.open("","myNewWin","width=500,height=300,toolbar=0"); 
        window.open("", "myNewWin", "");
        var a = window.setTimeout("document.abre_janela.submit();", 500);
    }
    function fncValida(inicio, texto_busca, enviados, tipo_ocorrencia, busca) {
        document.form_busca.selMES.value = document.lista.selMES.value;
        document.form_busca.selANO.value = document.lista.selANO.value;
        document.form_busca.inicio.value = inicio;
        document.form_busca.texto_busca.value = texto_busca;
        if (<?php echo $_SESSION["sessao_rh"];?>==1 || <?php echo $_SESSION["usr_gerente"];?> == 2
    )
        {

            document.form_busca.id_depto.value = document.lista.id_depto.value;
        }
        document.form_busca.id_usr.value = document.lista.id_usr.value;


        if (tipo_ocorrencia != '-1') {
            document.form_busca.tipo_ocorrencia.value = tipo_ocorrencia;
            document.form_busca.enviados.value = enviados;
        }
        if (busca == 1) {
            document.form_busca.enviados.value = 10;
        }

        document.form_busca.submit()
    }
    function fncEnter() {
        if (window.event.keyCode == 13) {
            fncValida(0, document.form_busca.texto_busca.value, 10, -1, 1);
        }
    }
    function fncHistorico(id_justificativa) {
        document.abre_janela2.id_justificativa.value = id_justificativa;
//    window.open("","myNewWin","width=500,height=300,toolbar=0"); 
        window.open("", "myNewWin", "");
        var a = window.setTimeout("document.abre_janela2.submit();", 500);
    }


    function fncVerLista(id_justificativa) {
        window.open("ListaEnviados.php?id_justificativa=" + id_justificativa, "", "");
    }

    function getPosicaoElemento(elemID) {
        var offsetTrail = document.getElementById(elemID);
        var offsetLeft = 0;
        var offsetTop = 0;
        while (offsetTrail) {
            offsetLeft += offsetTrail.offsetLeft;
            offsetTop += offsetTrail.offsetTop;
            offsetTrail = offsetTrail.offsetParent;
        }
        if (navigator.userAgent.indexOf("Mac") != -1 &&
            typeof document.body.leftMargin != "undefined") {
            offsetLeft += document.body.leftMargin;
            offsetTop += document.body.topMargin;
        }
        return {left: offsetLeft, top: offsetTop};

    }
    function Fechar() {
        tipo_ocorrencia_opcoes.style.display = 'none';
    }

    function mapa(cep) {
//window.open("mapa2.php?cep="+cep, "", "toolbar=no,location=no,directories=no,tipo_ocorrencia=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=640,height=480,top=20,left=50");
        window.open("http://www.google.com.br/maps?f=q&source=s_q&hl=pt-BR&geocode=&q=" + cep + "&aq=&sll=-14.239424,-53.186502&sspn=57.994688,79.013672&ie=UTF8&hq=&hnear=" + cep + "&z=17", "", "toolbar=no,location=no,directories=no,tipo_ocorrencia=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=900,height=600,top=20,left=50");
    }

    function Fechar() {
        tipo_ocorrencia_opcoes.style.display = 'none';
        tipo_ocorrencia_opcoes2.style.display = 'none';
    }


</script>

<body onLoad="javascript:fncCamadas()">
<?php require_once("frm_topo.php"); ?>




<br>

<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><BR><strong><font
                                    color="#333333" size="4">
                                </font></strong>

                        <p><strong><font color="#333333" size="4">&nbsp; Lista de Ocorr&ecirc;ncias<br>
                                </font><font color="#666666"><strong><font color="#0066FF"> &nbsp;&nbsp;
                                            <?php switch ($enviados) {
                                                case 10:
                                                    echo "<font color = 'red'>Resultado da Busca</font>";
                                                    break;
                                                case 0:
                                                    echo "Total";
                                                    break;
                                                case 1:
                                                    echo "Abonadas";
                                                    break;
                                                case 2:
                                                    echo "Não abonadas";
                                                    break;
                                            }

                                            if ($_SESSION["sessao_rh"] == 0 && $_SESSION["usr_gerente"] == 1) {
                                                echo " - " . $_SESSION["sessao_sigla"];
                                            }
                                            ?>
                                        </font></strong></font><font color="#333333" size="4"> </font></strong></p>

                        <form name="lista" action="frm_Lista_ocorrencias.php" method="post">
                            <table width="100%" border="0" cellspacing="0" cellpadding="1">
                                <tr>
                                    <td width="9">&nbsp;</td>
                                    <td width="845" align="right">
                                        <table width="900" border="0" cellpadding="0" cellspacing="1">
                                            <tr>
                                                <td width="428" height="12" align="right"><strong><font
                                                            style="font-size:10px" color="#333333">
                                                        </font><font color="#666666">Ocorr&ecirc;ncias</font></strong>
                                                    <strong><font color="#666666"> de:
                                                            <?php if ($_SESSION["sessao_rh"] == 1 || $_SESSION["usr_gerente"] == 2) { ?>
                                                                <select name="id_depto" id="id_depto"
                                                                        style="width:250px"
                                                                        onChange="fncMontacombo(this.value);">
                                                                    <option value="0" selected>Todas Unidades</option>
                                                                    <?php
                                                                    foreach ($oUsuario->getMeusDepartamentosArray() as $id => $nome) {
                                                                        ?>
                                                                        <option value="<?php echo $id; ?>">
                                                                            <?php echo $nome; ?>
                                                                        </option>
                                                                    <?php } ?>
                                                                </select>
                                                            <?php } ?>
                                                        </font></strong></td>
                                                <td width="228" align="center"><strong><font style="font-size:10px"
                                                                                             color="#333333">
                                                            <?php if ($_SESSION["usr_gerente"] >= 1){ ?>
                                                        </font></strong>

                                                    <div id="div_id_usr"><strong><font color="#666666">
                                                                <select name="id_usr" id="id_usr" style="width:200px">
                                                                    <option value="0" selected>Todos funcionários
                                                                    </option>
                                                                    <?php foreach ($oUsuario->getMeusServidoresArray() as $id => $nome) { ?>
                                                                        <option value="<?php echo $id; ?>">
                                                                            <?php echo $nome; ?>
                                                                        </option>
                                                                    <?php } ?>
                                                                </select>
                                                            </font></strong></div>
                                                    <?php } else { ?>
                                                        <input type="hidden" name="id_usr" id="id_usr" value="0"/>
                                                    <?php } ?>
                                                </td>
                                                <td width="240"><strong><font color="#666666">em:</font></strong>
                                                    <strong>
                                                        <select name="selMES" id="selMES">
                                                            <option value="13">Todos</option>
                                                            <option value="01">Janeiro</option>
                                                            <option value="02">Fevereiro</option>
                                                            <option value="03">Mar&ccedil;o</option>
                                                            <option value="04">Abril</option>
                                                            <option value="05">Maio</option>
                                                            <option value="06">Junho</option>
                                                            <option value="07">Julho</option>
                                                            <option value="08">Agosto</option>
                                                            <option value="09">Setembro</option>
                                                            <option value="10">Outubro</option>
                                                            <option value="11">Novembro</option>
                                                            <option value="12">Dezembro</option>
                                                        </select>
                                                    </strong> <strong><font color="#666666">de:</font></strong>
                                                    <select name="selANO" id="selANO">
                                                        <option value="13">Todos</option>
                                                        <?php echo montarComboAnos(); ?>
                                                    </select> <strong><font color="#666666"> </font></strong>
                                                    <label for="op3"></label> <strong><font color="#666666"><a href="#"
                                                                                                               onClick="fncValida(0,'',0,0,0);"><img
                                                                    src="../images/ok_bt.gif" width="27" height="15"
                                                                    border="0"
                                                                    title="Realiza o filtro"></a></font></strong></td>
                                            </tr>
                                        </table>
                                        <strong><font color="#666666"> </font></strong></td>
                                </tr>
                            </table>
                        </form>
                        <br>
                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="4"></td>
                    <td width="200" height="5" valign="top" background="../images/sidebar.gif">
                        <br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'
                               style="display: none;">
                            <tr>
                                <td class='Titulo_caixa'>Justificativas</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <form name="form_busca" action="frm_Lista_ocorrencias.php" method="post">
                                        <table width="165" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="30"><img src="../images/inbox.jpg" width="25"
                                                                    height="25"></td>
                                                <td width="141" height="40"
                                                    <?php if ($enviados == 1) {
                                                        echo "class='Itens_pressionado'";
                                                    } else {
                                                        echo "class='Itens_normal'";
                                                    } ?>><a href="#" title="Lista de justificativas"
                                                            onClick="fncValida(0,'',0,0,0)"><font size="1">Em
                                                            Aberto</font></a><strong><font color="#666666"
                                                                                           size="1">(<?php echo $total_andamento; ?>
                                                            )</font></strong></td>
                                            </tr>
                                            <tr>
                                                <td height="40"><img src="../images/finalizado.png" width="30"
                                                                     height="30"></td>
                                                <td height="40"
                                                    <?php if ($enviados == 2) {
                                                        echo "class='Itens_pressionado'";
                                                    } else {
                                                        echo "class='Itens_normal'";
                                                    } ?>><a href="#" title="Abonadas pelo chefe imediato"
                                                            onClick="fncValida(0,'',1,1,0)"><font
                                                            size="1">Abonadas</font></a><font color="#666666"
                                                                                              size="1"><strong>
                                                            (<?php echo $total_abonado; ?>)</strong></font></td>
                                            </tr>
                                            <tr>
                                                <td height="40"><img src="../images/Negado.png" width="25"
                                                                     height="25"></td>
                                                <td height="40"
                                                    <?php if ($enviados == 3) {
                                                        echo "class='Itens_pressionado'";
                                                    } else {
                                                        echo "class='Itens_normal'";
                                                    } ?>><a href="#" title="Justificativas não abonadas"
                                                            onClick="fncValida(0,'',2,2,0)"><font size="1">N&atilde;o
                                                            Abonadas </font></a><font color="#666666" size="1"><strong>
                                                            (<?php echo $total_nao_abonado; ?>)</strong></font></td>
                                            </tr>
                                            <tr>
                                                <td height="40" colspan="2"><font color="#FF6600" size="1"><strong><font
                                                                color="#E5E5E5">______________________</font><font
                                                                color="#CCCCCC"><br>
                                                            </font>
                                                            <input name="status" type="text" id="status" size="3"
                                                                   style="display:none" value="<?php echo $status; ?>">
                                                            <input name="selMES" type="text" id="selMES" size="3"
                                                                   value="<?php echo $MES; ?>" style="display:none">
                                                            <input name="selANO" type="text" id="selANO" size="3"
                                                                   value="<?php echo $ANO; ?>" style="display:none">
                                                            <input name="local_atual" type="text" id="local_atual"
                                                                   size="3" value="<?php echo $local_atual; ?>"
                                                                   style="display:none">
                                                            <input name="enviados" type="text" id="enviados" size="3"
                                                                   value="<?php echo $enviados; ?>"
                                                                   style="display:none">
                                                            <input name="texto" type="text" id="texto" size="3"
                                                                   value="<?php echo $texto; ?>" style="display:none">
                                                            <input name="inicio" type="text" id="inicio" size="3"
                                                                   value="<?php echo $inicio; ?>" style="display:none">
                                                            <input name="id_depto" type="text" id="id_depto" size="3"
                                                                   value="<?php echo $id_depto; ?>"
                                                                   style="display:none">
                                                            <input name="id_usr" type="text" id="id_usr" size="3"
                                                                   value="<?php echo $id_usr; ?>" style="display:none">
                                                            <input name="tipo_ocorrencia" type="text"
                                                                   id="tipo_ocorrencia" size="3"
                                                                   value="<?php echo $tipo_ocorrencia; ?>"
                                                                   style="display:none">

                                                            <input name="local_atual_busca" type="text"
                                                                   id="local_atual_busca" size="3"
                                                                   value="<?php echo $local_atual_busca; ?>"
                                                                   style="display:none">
                                                            <br>
                                                            Digite texto p/ busca:</strong></font><br>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td><input type="text" name="texto_busca"
                                                                       style="width:130px"
                                                                       value="<?php echo $texto_busca; ?>"
                                                                       onKeyPress="fncEnter();">
                                                            </td>
                                                            <td><a href="#"
                                                                   onClick="document.form_busca.enviados.value=10;document.form_busca.submit();"><img
                                                                        src="../images/ok_bt.gif" width="27"
                                                                        height="15" border="0"></a></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <br>

                        <p align="center"><a href="../ajuda/" target="_blank"><img
                                    src="../images/respostas.png" alt="Manual do Sistema"
                                    width="87" height="62" border="0" class="icon"></a></p>

                        <p align="center"><a href="../ajuda" target="_blank">Manual
                                do Sistema</a></p>

                        <p align="center">&nbsp; </p></td>
                    <td width="722" align="center" valign="top" bgcolor="#FFFFFF"><br>
                        <table width="790" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
                            <tr class="Itens_rel_s_traco">
                                <td height="15">
                                    <table width="790" border="0" cellpadding="2" cellspacing="0">
                                        <tr>
                                            <th>Unidade Administrativa</th>
                                            <th>Pessoa</th>
                                            <th>Horas Pendentes</th>
                                            <th>Painel</th>
                                        </tr>
                                        <?php
                                        foreach ($oUsuario->getMeusServidores($_REQUEST['id_depto']) as $usuario) {
                                            $res = $usuario->getOcorrenciasPendentes();
                                            if (count($res['ocorrencias'])) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $usuario->getDepartamento()->depto; ?></td>
                                                    <td><?php echo $usuario->nome; ?></td>
                                                    <td style="text-align:center;"><?php echo $res['horas']; ?></td>
                                                    <td style="text-align:center;">
                                                        <a href="frm_justificativa.php?id_depto=<?php echo $usuario->id_depto;?>&id_usr=<?php echo $usuario->id_usr; ?>"
                                                           target="new">
                                                            <img src="../images/painel3.gif" width="25" height="25"
                                                                 border="0">
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                        } ?>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p><br>
                            <br>
                        </p>
                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="96%">&nbsp;</td>
                                <td width="4%">
                                    <div align="center"><a href="javascript:history.go(-1)"><img
                                                src="../images/reply-16x16.gif" title="Voltar" width="16"
                                                height="16" border="0"></a></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="3" valign="top" background="../images/rightside.gif"><br> <br>
                        <br></td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?=date('Y')?> - SED&reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>

<form name="abre_janela" method="post" action="processo.php" target="myNewWin">
    <input type="hidden" name="id_justificativa">
</form>

<form name="abre_janela2" method="post" action="mostra_justificativa.php" target="myNewWin">
    <input type="hidden" name="id_justificativa">
    <input type="hidden" name="id_usr">
    <input type="hidden" name="dia">
    <input type="hidden" name="mes">
    <input type="hidden" name="ano">

    <input type="hidden" name="onde">
    <input type="hidden" name="id_depto_s">

</form>

<form name="justifica" method="post" action="frm_justificativa.php">
    <input type="hidden" name="id_usr">
    <input type="hidden" name="mes">
    <input type="hidden" name="ano">
    <input type="hidden" name="id_depto">
</form>

<form name="ocorrencias" method="post" action="frm_Lista_ocorrencias.php">
    <input type="hidden" name="id_depto">
    <input type="hidden" name="id_usr">
    <input type="hidden" name="selMES">
    <input type="hidden" name="selANO">
    <input type="hidden" name="onde">
</form>


<iframe name="janela" width="800" height="200" style="display:none"></iframe>
</p>
</body>
</html>
