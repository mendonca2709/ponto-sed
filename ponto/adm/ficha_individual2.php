﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.location.href = 'index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}
require_once("../funcoes/conexao.php");

$quantos_justificar = 0;
$cont = 0;

$id_usr = $_POST["id_usr"];
if ($id_usr == '') {
    $id_usr = $_SESSION["sessao_id_usr"];
}
$mes = $_POST["mes"];
if ($mes == '') {
    $mes = date("m");
}
$ano = $_POST["ano"];
if ($ano == '') {
    $ano = date("Y");
}
$id_depto = $_POST["id_depto"];
if ($id_depto == "") {
    $id_depto = $_SESSION["sessao_id_depto"];
}
$mes_p = $mes;
$ano_p = $ano;
$dia = date("d");
$existe_abonado_0 = 0;
//#######################################################################################################################################################
//#######################################################################################################################################################


$sql_horas_aguardando = "select qtd_horas from p_justificativa where id_usr = " . $id_usr . " and month(data_justificativa) = " . $mes_p . " and year(data_justificativa) = " . $ano_p . " and abonado = 0";
$dados_horas_aguardando = mysql_query($sql_horas_aguardando, $conexao);
while ($resultado_horas_aguardando = mysql_fetch_array($dados_horas_aguardando)) {
    $horario_1_banco = $resultado_horas_aguardando[qtd_horas];
    $horario_1_banco = explode(':', $horario_1_banco);
    $total_horas = (60 * 60 * $horario_1_banco[0]) + (60 * $horario_1_banco[1]) + $total_horas;
}
$total_horas = $total_horas / (60 * 60);
$tempo1 = explode('.', $total_horas); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
$hora1 = $tempo1[0];
@$minutos1 = (float)(0) . '.' . $tempo1[1]; // Aqui forçamos a conversão para float, para não ter erro.
$minutos1 = round($minutos1 * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
$minutos1 = explode('.', $minutos1); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
$minutos1 = $minutos1[0];
if ($minutos1 < 10) {
    $minutos1 = "0" . $minutos1;
}
if ($hora1 < 10) {
    $hora1 = "0" . $hora1;
}
$total_1 = $hora1 . ':' . $minutos1;
//echo $total_horas."<BR>";
//echo $total_1."<BR>";
//die;
$sql_horas_nao_abonadas = "select qtd_horas from p_justificativa where id_usr = " . $id_usr . " and month(data_justificativa) = " . $mes_p . " and year(data_justificativa) = " . $ano_p . " and (abonado = 2 or abonado = 3)";

$dados_horas_nao_abonadas = mysql_query($sql_horas_nao_abonadas, $conexao);
while ($resultado_horas_nao_abonadas = mysql_fetch_array($dados_horas_nao_abonadas)) {
    //echo $resultado_horas_aguardando[qtd_horas]."<BR>";
    $horario_2_banco = $resultado_horas_nao_abonadas[qtd_horas];
    $horario_2_banco = explode(':', $horario_2_banco);
    $total_horas2 = (60 * 60 * $horario_2_banco[0]) + (60 * $horario_2_banco[1]) + $total_horas2;
}
$total_horas2 = $total_horas2 / (60 * 60);
$tempo2 = explode('.', $total_horas2); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
$hora2 = $tempo2[0];
@$minutos2 = (float)(0) . '.' . $tempo2[1]; // Aqui forçamos a conversão para float, para não ter erro.
$minutos2 = round($minutos2 * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
$minutos2 = explode('.', $minutos2); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
$minutos2 = $minutos2[0];
if ($minutos2 < 10) {
    $minutos2 = "0" . $minutos2;
}
if ($hora2 < 10) {
    $hora2 = "0" . $hora2;
}
$total_2 = $hora2 . ':' . $minutos2;


$sql_horas_abonadas = "select cota,qtd_horas from p_justificativa inner join p_tipo_justificativa on p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa where p_justificativa.id_usr = " . $id_usr . " and month(data_justificativa) = " . $mes_p . " and year(data_justificativa) = " . $ano_p . " and abonado = 1";

$dados_horas_abonadas = mysql_query($sql_horas_abonadas, $conexao);
while ($resultado_horas_abonadas = mysql_fetch_array($dados_horas_abonadas)) {
    $horario_3_banco = $resultado_horas_abonadas[qtd_horas];
    $horario_3_banco = explode(':', $horario_3_banco);

    if ($resultado_horas_abonadas[cota] == 3) {
        $total_horas4 = (60 * 60 * $horario_3_banco[0]) + (60 * $horario_3_banco[1]) + $total_horas4;
    } else {
        $total_horas3 = (60 * 60 * $horario_3_banco[0]) + (60 * $horario_3_banco[1]) + $total_horas3;
    }
}
$total_horas3 = $total_horas3 / (60 * 60);
$tempo3 = explode('.', $total_horas3); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
$hora3 = $tempo3[0];
@$minutos3 = (float)(0) . '.' . $tempo3[1]; // Aqui forçamos a conversão para float, para não ter erro.
$minutos3 = round($minutos3 * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
$minutos3 = explode('.', $minutos3); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
$minutos3 = $minutos3[0];
if ($minutos3 < 10) {
    $minutos3 = "0" . $minutos3;
}
if ($hora3 < 10) {
    $hora3 = "0" . $hora3;
}
$total_3 = $hora3 . ':' . $minutos3;


$total_horas4 = $total_horas4 / (60 * 60);
$tempo4 = explode('.', $total_horas4); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
$hora4 = $tempo4[0];
@$minutos4 = (float)(0) . '.' . $tempo4[1]; // Aqui forçamos a conversão para float, para não ter erro.
$minutos4 = round($minutos4 * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
$minutos4 = explode('.', $minutos4); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
$minutos4 = $minutos4[0];
if ($minutos4 < 10) {
    $minutos4 = "0" . $minutos4;
}
if ($hora4 < 10) {
    $hora4 = "0" . $hora4;
}
$total_4 = $hora4 . ':' . $minutos4;

//	echo $id_usr;
$numr_ip = $_SERVER[HTTP_X_FORWARDED_FOR] . "-" . $_SERVER[REMOTE_ADDR];
if ($_SESSION["sessao_id_usr"] != $id_usr) {
    $sql3 = "insert into acessos (id_usr,data_acesso,entrada,numr_ip,visao,ho) values (" . $_SESSION["sessao_id_usr"] . ",'" . date("Y/m/d H:i:s") . "','" . date("H:i:s") . "','" . $numr_ip . "',1," . $id_usr . ")";

    $dados3 = mysql_query($sql3, $conexao);
}


$Data = strtotime($mes_p . "/" . $dia . "/" . $ano_p);
$Dia = date('w', strtotime(date('n/\0\1\/Y', $Data)));
$Dias = date('t', $Data);
$dias_ok = 0;

for ($i = 1, $d = 1; $d <= $Dias;) {


    for ($x = 1; $x <= 7 && $d <= $Dias; $x++, $i++) {
        $mostra_botao = 0;

        if ($x == 1 || $x == 7) {
            //$total_acum=0;
        }
        if ($i > $Dia) {
            $destaque = '';


            if ($x == 1) {
                $destaque = $domingo;
            }
            if ($d == $dia && $mes_p == date("m") && $ano_p = date("Y")) {
                $destaque = $hoje;
            }
            if (($x == 1) && ($d == $dia)) {
                $destaque = $hoje;
            }

            $nao_calcula = 0;
            $sql_evento = "select id_evento,titulo from p_eventos inner join p_tipo_justificativa on p_eventos.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa where (day(data_evento) = " . $d . " and month(data_evento) = " . $mes_p . " and year(data_evento) = " . $ano_p . ") and (id_usr = " . $id_usr . " or id_depto = 0)";

            $dados_evento = mysql_query($sql_evento, $conexao);
            $resultado_evento = mysql_fetch_array($dados_evento);
            if ($resultado_evento[id_evento] != "") {
                $nao_calcula = 1;

            }

            // echo "<td ".$destaque." height=95px width=110px valign=top";

            if ($x != 1 && $x != 7) {
                if ($nao_calcula == 1) {
                    //echo " class='tab_evento' ";
                } else {
                    //echo " class='tab_resa' ";
                    $dias_ok++;
                }
            }


            //echo $d;


            $d_busca = $d;
            $d++;


            //echo " ".$d."/".$mes."/".$ano;

            $data_calendario = $ano_p . "/" . $mes_p . "/" . $d;
            $vetor_registro = explode("/", $data_calendario);
            $data = mktime(0, 0, 0, $vetor_registro[1], $vetor_registro[2], $vetor_registro[0]);
            $data_atual = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
            $tdias = ($data - $data_atual) / 86400;
            $tdias = ceil($tdias);
            if ($x != 1 && $x != 7) {
                //echo "<br><table width='100%' border='0' cellspacing='1'> ";

                //for($i=1;$i<5;$i++){


                $sql_agenda = "select id_registro,hora,minutos,etapa from p_registro where day(data_registro) = " . $d_busca . " and month(data_registro) = " . $mes_p . " and year(data_registro) = " . $ano_p . " and id_usr = " . $id_usr . " order by etapa";
                $dados_agenda = mysql_query($sql_agenda, $conexao);

                //$resultado_agenda = mysql_fetch_array($dados_agenda);
                $qtd_registro = 0;
                $diferenca_1 = 0;
                $diferenca_2 = 0;
                $achou_registro = 0;
                //echo $achou_registro;
                $sql_usuarios2 = "select id_grade from usuarios where id_usr = " . $id_usr;

                $dados_usuarios2 = mysql_query($sql_usuarios2, $conexao);
                $resultado_usuarios2 = mysql_fetch_array($dados_usuarios2);

                $sql_consulta_grade = "select * from p_grade where id_grade = " . $resultado_usuarios2[id_grade];

                $dados_consulta_grade = mysql_query($sql_consulta_grade, $conexao);
                $resultado_consulta_grade = mysql_fetch_array($dados_consulta_grade);
                $entrada_1 = $resultado_consulta_grade[entrada_1];
                $saida_1 = $resultado_consulta_grade[saida_1];
                $entrada_2 = $resultado_consulta_grade[entrada_2];
                $saida_2 = $resultado_consulta_grade[saida_2];

                $periodo_banco_1 = $resultado_consulta_grade[entrada_1];
                $periodo_banco_1 = explode(':', $periodo_banco_1);
                $periodo_banco_1_x = (60 * 60 * $periodo_banco_1[0]) + (60 * $periodo_banco_1[1]);

                $periodo_banco_2 = $resultado_consulta_grade[saida_1];
                $periodo_banco_2 = explode(':', $periodo_banco_2);
                $periodo_banco_2_x = (60 * 60 * $periodo_banco_2[0]) + (60 * $periodo_banco_2[1]);

                $periodo_banco_3 = $resultado_consulta_grade[entrada_2];
                $periodo_banco_3 = explode(':', $periodo_banco_3);
                $periodo_banco_3_x = (60 * 60 * $periodo_banco_3[0]) + (60 * $periodo_banco_3[1]);

                $periodo_banco_4 = $resultado_consulta_grade[saida_2];
                $periodo_banco_4 = explode(':', $periodo_banco_4);
                $periodo_banco_4_x = (60 * 60 * $periodo_banco_4[0]) + (60 * $periodo_banco_4[1]);

                $total_horas_periodo_1 = abs(($periodo_banco_2_x - $periodo_banco_1_x) / 60);
                $total_horas_periodo_2 = abs(($periodo_banco_4_x - $periodo_banco_3_x) / 60);


                //echo $resultado_consulta_grade[tipo_grade];
                if ($resultado_consulta_grade[tipo_grade] == 0) {
                    $total_horas_grade = abs(($total_horas_periodo_2 + $total_horas_periodo_1));
                    $qtd_total_registros = 4;
                } else {
                    $total_horas_grade = abs($total_horas_periodo_1);
                    $qtd_total_registros = 2;
                }
                //echo $total_horas_periodo_1." - ".$total_horas_periodo_2." yy".$total_horas_grade;
                $zera_periodo_1 = 0;
                $zera_periodo_2 = 0;
                $preenchimento_1 = 0;
                $preenchimento_2 = 0;
                $preenchimento_3 = 0;
                $preenchimento_4 = 0;
                $abonado_mostra = 0;
                $status_abonado = 0;
                $status_nao_abonado = 0;
                $status_cortado = 0;
                $qtd_justificativas = 0;

                while ($resultado_agenda = mysql_fetch_array($dados_agenda)) {


                    $sql_consulta_registro = "select p_justificativa.id_justificativa,abonado from p_justificativa inner join p_registro on p_registro.id_justificativa = p_justificativa.id_justificativa where day(data_justificativa) = " . $d_busca . " and month(data_justificativa) = " . $mes_p . " and year(data_justificativa) = " . $ano_p . " and p_justificativa.id_usr = " . $id_usr . " and etapa = " . ($qtd_registro + 1) . " limit 0,1";
                    $dados_consulta_registro = mysql_query($sql_consulta_registro, $conexao);
                    $resultado_consulta_registro = mysql_fetch_array($dados_consulta_registro);
                    $qtd_justificativas++;
                    //$achou_registro = ;
                    //echo $sql_consulta_registro." - ";
                    if ($resultado_consulta_registro[id_justificativa] != '') {
                        $achou_registro++;
                    }
                    $abonado = $resultado_consulta_registro[abonado];
                    //echo $abonado."<BR>";
                    $abonado_mostra = $abonado;
                    if ($abonado != '' && $abonado == 0) {
                        //echo "aqui<br>";
                        $existe_abonado_0++;
                    }
                    if ($abonado == 1) {
                        $status_abonado++;
                    }
                    if ($abonado == 3) {
                        $status_cortado++;
                    }
                    if ($abonado == 2) {
                        $status_nao_abonado++;
                    }

                    $justifica = 0;
                    $qtd_registro++;
                    if ($nao_calcula == 0) {
                        if ($qtd_registro == 1) {
                            $horario_1_banco = $entrada_1;
                            $horario_1_banco = explode(':', $horario_1_banco);
                            $hora_1_banco = (60 * 60 * $horario_1_banco[0]) + (60 * $horario_1_banco[1]);

                            $horario_1 = $resultado_agenda[hora] . ":" . $resultado_agenda[minutos];
                            if ($resultado_agenda[hora] != '') {
                                $preenchimento_1 = 1;
                            }
                            $horario_1 = explode(':', $horario_1);
                            $hora_1 = (60 * 60 * $horario_1[0]) + (60 * $horario_1[1]);

                            $diferenca_registro_1 = abs(($hora_1_banco - $hora_1) / 60);

                            if ($diferenca_registro_1 > 30) {
                                $mostra_botao = 1;
                                $cor = "#FF6600";
                                $justifica = 1;
                            } else {
                                $cor = "#666666";
                            }
                            if ($resultado_consulta_registro[abonado] == 1) {
                                $cor = "#009933";
                            }
                            if ($resultado_consulta_registro[abonado] == 3) {
                                $cor = "#FF0000";
                            }

                            //echo "<center><font color = '".$cor."'; size = 2> <strong>".$resultado_agenda[hora].":".$resultado_agenda[minutos]."</strong></font></center>";
                        }
                        if ($qtd_registro == 2) {

                            $horario_2_banco = $saida_1;
                            $horario_2_banco = explode(':', $horario_2_banco);
                            $hora_2_banco = (60 * 60 * $horario_2_banco[0]) + (60 * $horario_2_banco[1]);


                            $horario_2 = $resultado_agenda[hora] . ":" . $resultado_agenda[minutos];
                            $horario_2 = explode(':', $horario_2);
                            $hora_2 = (60 * 60 * $horario_2[0]) + (60 * $horario_2[1]);
                            //echo $hora_1." - ".$hora_2."<BR>";
                            $diferenca_registro_2 = abs(($hora_2_banco - $hora_2) / 60);
                            $diferenca_periodo_1 = abs(($hora_1 - $hora_2) / 60);
                            $total_horas_periodo = $diferenca_periodo_1;

                            //if($resultado_agenda[hora]==''&&$preenchimento_1==1){
                            //$preenchimento_2=0;
                            //$zera_periodo_1=1;
                            //}
                            $diferenca_registro_2 = abs(($hora_2_banco - $hora_2) / 60);
                            //echo $diferenca_registro_2;
                            if ($diferenca_registro_2 > 30) {
                                $mostra_botao = 1;
                                $cor = "#FF6600";
                                $justifica = 1;
                            } else {
                                $cor = "#666666";
                            }
                            if ($resultado_agenda[hora] != '') {
                                $preenchimento_2 = 1;
                            }

                            //echo $diferenca_1;


                            if ($resultado_consulta_registro[abonado] == 1) {
                                $cor = "#009933";
                            }
                            if ($resultado_consulta_registro[abonado] == 3) {
                                $cor = "#FF0000";
                            }
                            //echo "<center><font color = '".$cor."'; size = 2> <strong>".$resultado_agenda[hora].":".$resultado_agenda[minutos]."</strong></font><BR></center>";
                        }
                        if ($qtd_registro == 3 && $resultado_consulta_grade[tipo_grade] == 0) {

                            $horario_3_banco = $entrada_2;
                            $horario_3_banco = explode(':', $horario_3_banco);
                            $hora_3_banco = (60 * 60 * $horario_3_banco[0]) + (60 * $horario_3_banco[1]);


                            $horario_3 = $resultado_agenda[hora] . ":" . $resultado_agenda[minutos];
                            $horario_3 = explode(':', $horario_3);
                            $hora_3 = (60 * 60 * $horario_3[0]) + (60 * $horario_3[1]);

                            $diferenca_registro_3 = abs(($hora_3_banco - $hora_3) / 60);
                            if ($resultado_agenda[hora] != '') {
                                $preenchimento_3 = 1;
                            }
                            //echo $diferenca_3;
                            if ($diferenca_registro_3 > 30) {
                                $cor = "#FF6600";
                                $mostra_botao = 1;
                                $justifica = 1;
                            } else {
                                $cor = "#666666";
                            }
                            if ($resultado_consulta_registro[abonado] == 1) {
                                $cor = "#009933";
                            }
                            if ($resultado_consulta_registro[abonado] == 3) {
                                $cor = "#FF0000";
                            }
                            //echo "<center><font color = '".$cor."'; size = 2> <strong>".$resultado_agenda[hora].":".$resultado_agenda[minutos]."</strong></font><BR></center>";

                        }
                        if ($qtd_registro == 4 && $resultado_consulta_grade[tipo_grade] == 0) {

                            $horario_4_banco = $saida_2;
                            $horario_4_banco = explode(':', $horario_4_banco);
                            $hora_4_banco = (60 * 60 * $horario_4_banco[0]) + (60 * $horario_4_banco[1]);

                            $horario_4 = $resultado_agenda[hora] . ":" . $resultado_agenda[minutos];

                            $horario_4 = explode(':', $horario_4);
                            $hora_4 = (60 * 60 * $horario_4[0]) + (60 * $horario_4[1]);
                            $diferenca_periodo_2 = abs(($hora_3 - $hora_4) / 60);
                            //echo $diferenca_2;

                            $total_horas_periodo = $diferenca_periodo_1 + $diferenca_periodo_2;

                            $diferenca_registro_4 = abs(($hora_4_banco - $hora_4) / 60);
                            if ($diferenca_registro_4 > 30) {
                                $cor = "#FF6600";
                                $mostra_botao = 1;
                                $justifica = 1;
                            } else {
                                $cor = "#666666";
                            }


                            if ($resultado_consulta_registro[abonado] == 1) {
                                $cor = "#009933";
                            }
                            if ($resultado_consulta_registro[abonado] == 3) {
                                $cor = "#FF0000";
                            }
                            //if($resultado_agenda[hora]==''&&$preenchimento_1==1){
                            //	$preenchimento_4=0;
                            //	$zera_periodo_2=1;
                            //}
                            //echo "(".$resultado_agenda[hora].")";
                            if ($resultado_agenda[hora] != '') {
                                $preenchimento_4 = 1;
                            }
                            //echo "<center><font color = '".$cor."'; size = 2> <strong>".$resultado_agenda[hora].":".$resultado_agenda[minutos]."</strong></font><BR></center>";
                        }
                    }//fim calcula

                }
                //echo $abonado;

                //if($qtd_registro>0){


                //}
                $d_par = $d - 1;

                //antes $d++;


                //echo "</tr></table>";
//echo $qtd_registro;
                $qtd_registro_compara = $qtd_registro;

                //echo $qtd_registro."-".$qtd_total_registros."--".$total_horas_periodo."-".$total_horas_grade."-".$justifica."-".$tdias;
                if (($qtd_registro <= $qtd_total_registros || $total_horas_periodo < $total_horas_grade || $justifica == 1) && $tdias <= 0 && $nao_calcula == 0) {
                    //echo $qtd_registro."<br>";
                    if ($resultado_consulta_grade[tipo_grade] == 0 && $qtd_registro < 4) {

                        $mostra_botao = 1;
                    }


                    $qtd_registro_foto = $qtd_registro;
                    if ($resultado_consulta_grade[tipo_grade] == 0) {
                        $qtd_registro = 4;
                    } else {
                        $qtd_registro = 2;
                    }


                    if (($preenchimento_1 + $preenchimento_2 + $preenchimento_3 + $preenchimento_4) == 0) {
                        $zera_periodo_1 = 1;
                        $zera_periodo_2 = 1;
                    }

                    if (($preenchimento_1 + $preenchimento_2 + $preenchimento_3 + $preenchimento_4) == 1) {
                        $zera_periodo_1 = 1;
                        $zera_periodo_2 = 1;
                    }
                    if (($preenchimento_1 + $preenchimento_2 + $preenchimento_3 + $preenchimento_4) == 2) {
                        $zera_periodo_2 = 1;
                    }
                    if (($preenchimento_1 + $preenchimento_2 + $preenchimento_3 + $preenchimento_4) == 3) {
                        $zera_periodo_2 = 1;
                    }
//echo $preenchimento_1." - ".$preenchimento_2." - ".$preenchimento_3." - ".$preenchimento_4."<BR>";
//echo $nao_calcula." - ".$zera_periodo_2;
//if($abonado==2||$abonado==3){
//	$nao_calcula=1;
//	echo "AQUI";
//}
                    if ($nao_calcula == 0) {

                        if ($resultado_consulta_grade[tipo_grade] == 2) {
//	echo $zera_periodo_2;
                            if ($zera_periodo_1 == 1) {
                                $difDeHora = 0;
                            } else {
                                $difDeHora = ($hora_2 - $hora_1);
                            }
                        } else {

                            //echo $zera_periodo_1."-".$zera_periodo_2."-".$difDeHora;


                            if ($resultado_consulta_grade[tipo_grade] == 0) {

                                if ($zera_periodo_1 == 1 && $zera_periodo_2 == 0) {

                                    //	$difDeHora = 0;
                                    //echo "a";
                                    $difDeHora = ($hora_4 - $hora_3);
                                }
                                if ($zera_periodo_1 == 0 && $zera_periodo_2 == 1) {

                                    //	$difDeHora = 0;

                                    $difDeHora = ($hora_2 - $hora_1);
                                    $mostra_botao = 1;
                                }
                                if ($zera_periodo_1 == 1 && $zera_periodo_2 == 1) {
                                    //	echo "c";
                                    $difDeHora = 0;
                                }
                                if ($zera_periodo_1 == 0 && $zera_periodo_2 == 0) {
                                    //	echo "d";
                                    $difDeHora = ($hora_2 - $hora_1) + ($hora_4 - $hora_3);
                                    //echo "bb";
                                }
                            }

                            if ($qtd_registro_compara == 3) {
                                $difDeHora = ($hora_2 - $hora_1);
                            }
                        }
                        if ($qtd_registro_compara <= 1) {
                            $difDeHora = 0;
                        }

                        $tempo = $difDeHora / (60 * 60);
                        //echo 60*$tempo."-".$total_horas_grade;
                        if (60 * $tempo < ($total_horas_grade - 30)) {

                            $mostra_botao = 1;
                        }
                        $qtd_tempo = $tempo;
                        $tempo = explode('.', $tempo); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
                        $hora = $tempo[0];
                        @$minutos = (float)(0) . '.' . $tempo[1]; // Aqui forçamos a conversão para float, para não ter erro.
                        $minutos = round($minutos * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.

                        $minutos = explode('.', $minutos); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos

                        $minutos = $minutos[0];
                        if ($minutos < 10) {
                            $minutos = "0" . $minutos;
                        }

                        $total_dia = $hora . ':' . $minutos;
                        $total_mes = $total_mes + $difDeHora;

                        if ($x > 1 && $x < 7 && $nao_calcula == 0) {
                            if ($resultado_consulta_grade[tipo_grade] == 0) {

                                $total_acum = $difDeHora + $total_acum;
                            } else {
                                $total_acum = $difDeHora / 60 + $total_acum;
                            }
                        }
//echo $total_acum."<BR>";
//." = ".($total_horas_periodo_1+$total_horas_periodo_2)."-".($difDeHora/60);
                        $total_acum3 = $total_acum;
                        $tempo3 = $total_acum3 / (60 * 60);

                        $tempo3 = explode('.', $tempo3); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
                        $hora3 = $tempo3[0];
                        @$minutos3 = (float)(0) . '.' . $tempo3[1]; // Aqui forçamos a conversão para float, para não ter erro.
                        $minutos3 = $minutos3 * 60; // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.

                        $minutos3 = explode('.', $minutos3); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
                        $minutos3 = $minutos3[0];
                        if ($minutos3 < 10) {
                            $minutos3 = "0" . $minutos3;
                        }

                        $total_acum_mostra = $hora3 . ':' . $minutos3;
                        if ($total_dia < 10) {
                            $total_dia = "0" . $total_dia;
                        }

                    }//fim calcula
//echo $hora;
//echo $qtd_total_registros;
                    if ($qtd_total_registros == 2) {
                        $qtd_compara = $total_horas_periodo_1 / 60;
                    } else {
                        $qtd_compara = ($total_horas_periodo_1 + $total_horas_periodo_2) / 60;
                    }
//echo $total_horas_periodo_1." - ".$total_horas_periodo_2;

                    if ($qtd_tempo < $qtd_compara) {
                        $mostra_botao = 1;
                        //echo $qtd_tempo."_".$qtd_compara;
                    }


                    if ($nao_calcula == 0) {
                    }

                    if ($_SESSION["usr_gerente"] == 0 && ($status_abonado + $status_cortado + $status_nao_abonado) == 0 && $achou_registro == 0 && $mostra_botao == 1 && ($_SESSION["sessao_id_usr"] == $id_usr)) {
                        $quantos_justificar++;
                        //echo "<center><strong><font color = '#666666';' size = 1><br><div style='cursor:pointer;' onClick='AbreJanela(".$id_usr.",".$d_par.",".$mes_p.",".$ano_p.")';>Justificar</div></font></strong></center><br>";
                    }
                    if ($_SESSION["usr_gerente"] >= 1 && $id_usr == $_SESSION["sessao_id_usr"] && $status_abonado == 0 && $achou_registro == 0 && $mostra_botao == 1) {
                        //echo "<center><strong><font color = '#666666';' size = 1><br><div style='cursor:pointer;' onClick='AbreJanela(".$id_usr.",".$d_par.",".$mes_p.",".$ano_p.")';>Justificar</div></font></strong></center><br>";
                        $quantos_justificar++;
                    }


                }


            }//sabado e domingo

        }

    }


}


//#######################################################################################################################################################
//#######################################################################################################################################################

$sql_func = "select foto,nome,cargo,depto,tipo_grade,nome_grade,entrada_1,saida_1,entrada_2,saida_2 from usuarios inner join depto on usuarios.id_depto = depto.id_depto inner join p_grade on usuarios.id_grade = p_grade.id_grade where id_usr = " . $id_usr;
$dados_func = mysql_query($sql_func, $conexao);
$resultado_func = mysql_fetch_array($dados_func);
$foto = $resultado_func[foto];
$tipo_grade = $resultado_func[tipo_grade];
$nome = $resultado_func[nome];
$entrada_1 = $resultado_func[entrada_1];
$saida_1 = $resultado_func[saida_1];
$entrada_2 = $resultado_func[entrada_2];
$saida_2 = $resultado_func[saida_2];
//header('Content-type: application/pdf');
// Formato do arquivo (Ex: .xls, .doc, .pdf ...)
//header('Content-Disposition: attachment; filename="ficha.pdf"');


?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<script language="JavaScript" type="text/javascript">

    function fncJustifica(id_usr, mes, ano, id_depto) {
        document.justifica.id_usr.value = id_usr;
        document.justifica.mes.value = mes;
        document.justifica.ano.value = ano;
        document.justifica.id_depto.value = id_depto;
        document.justifica.submit();

    }
    function Imprime() {
        window.print();
    }
    function AbreJustifivativa(id_usr, dia, mes, ano) {

        //document.abre_janela2.id_justificativa.value=id_justificativa;
        document.abre_janela2.id_usr.value = id_usr;
        document.abre_janela2.dia.value = dia;
        document.abre_janela2.mes.value = mes;
        document.abre_janela2.ano.value = ano;
//    window.open("","myNewWin","width=500,height=300,toolbar=0"); 
        window.open("", "myNewWin", "");
        var a = window.setTimeout("document.abre_janela2.submit();", 500);
    }


    function fncRelatorio2(sql, sql_total) {
        document.relatorio.sql.value = sql;
        document.relatorio.sql_total.value = sql_total;
        document.relatorio.submit();
    }
    function fncEnter() {
        if (window.event.keyCode == 13) {
            fncBusca();
        }
    }
    function fncDeleta_favorito(id_favorito) {
        janela.location.href = 'exclui_favorito.php?id_favorito=' + id_favorito + '&pagina=frm_relatorios.php';
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
    }

    function Abre(id_processo) {
        window.open("historico.php?id_processo=" + id_processo + '&tp=1', "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=700,top=10,left=20");
//window.open("processo_auditor.php?id_processo="+id_processo, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=700,top=10,left=20");
    }

    function Abre2(id_processo) {
//window.open("processo.php?id_processo="+id_processo, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=800,top=10,left=20");
        window.showModalDialog("processo.php?id_processo=" + id_processo, "resizable: no", "dialogWidth:1070px; dialogHeight:800px; center:yes");
    }
    function fncFavoritos() {
        div_favorito.style.display = '';
        iframe_favoritos.location.href = 'favorito.php';
    }
</script>
<html>


<head>
    <title>Ficha individual</title>

</head>

<meta name="title" content="SED - SED"/>
<meta name="url" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>"/>
<meta name="description"
      content="SED-GO - A SED de Goiás implementou em um projeto altamente inovador de acompanhamento de Processos totalmente digital."/>
<meta name="keywords"
      content="sistema,ouvidoria,corregedoria,controladoria,processos,protocolo,jedi,workflow,processo eletronico,gerencial,manifestação,acompanhamento,encaminhamento,controle,pesquisa,histórico,governo de goiás,sistema de acompanhamento de processos,sistema gerencial,marcelo roncato"/>

<meta name="autor" content="Marcelo Roncato"/>
<meta name="company" content="SED"/>
<meta name="revisit-after" content="5"/>
<body bgcolor="#FFFFFF">
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100" colspan="2">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2"><img src="../images/brasao_p.jpg" width="60" height="80" align="absmiddle">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <img src="../images/SED_logo.jpg" width="210" height="25"></td>
                    <td align="right"><font style='font-size:14px; font-family:Verdana, Arial'><strong>Ficha de Frequ&ecirc;ncia</strong></font>
                    </td>
                </tr>
                <tr>
                    <td width="58">&nbsp;</td>
                    <td colspan="2" align="center">&nbsp;</td>
                </tr>
                <tr>
                    <td height="20"><strong></strong></td>
                    <td width="520" align="center">&nbsp;</td>
                    <td width="222" rowspan="5" align="right" valign="top">
                        <?php if ($foto == 1){ ?></font></strong>
                        <img src="../fotos/<?php echo $id_usr . "_usr.jpg" ?>" height="100">
                        <?php } ?>
                        <?php if ($foto == 0) { ?>
                            <img src="../images/nouser.jpg" width="100" height="100">
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td height="20"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Nome:</font></strong>
                    </td>
                    <td><font color="#0066FF"
                              style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'><strong><?php echo $resultado_func[nome]; ?></strong></font>
                    </td>
                </tr>
                <tr>
                    <td height="20"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Cargo:</font></strong>
                    </td>
                    <td><font color="#0066FF"
                              style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'><strong><?php echo $resultado_func[cargo]; ?></strong></font>
                    </td>
                </tr>
                <tr>
                    <td height="20"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Lota&ccedil;&atilde;o:</font></strong>
                    </td>
                    <td><font color="#0066FF"
                              style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'><strong><?php echo $resultado_func[depto]; ?></strong></font>
                    </td>
                </tr>
                <tr>
                    <td height="20"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Per&iacute;odo</font><font
                                style="font-size:12px" color="#333333">:&nbsp;</font></strong></td>
                    <td><strong><font color="#0066FF"> <font
                                    style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'>
                                    <?php switch ($mes) {
                                        case 1:
                                            echo "Janeiro";
                                            break;
                                        case 2:
                                            echo "Fevereiro";
                                            break;
                                        case 3:
                                            echo "Março";
                                            break;
                                        case 4:
                                            echo "Abril";
                                            break;
                                        case 5:
                                            echo "Maio";
                                            break;
                                        case 6:
                                            echo "Junho";
                                            break;
                                        case 7:
                                            echo "Julho";
                                            break;
                                        case 8:
                                            echo "Agosto";
                                            break;
                                        case 9:
                                            echo "Setembro";
                                            break;
                                        case 10:
                                            echo "Outubro";
                                            break;
                                        case 11:
                                            echo "Novembro";
                                            break;
                                        case 12:
                                            echo "Dezembro";
                                            break;
                                    }
                                    echo "/" . $ano; ?>
                                </font></font></strong></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php
//echo $existe_abonado_0;
$sql_checa_ponto = "select count(id_justificativa) as total_nao_justificadas from p_justificativa where id_usr = " . $id_usr . " and month(data_justificativa) = " . $mes . " and year(data_justificativa) = " . $ano . " and abonado = 0";
$dados_checa_ponto = mysql_query($sql_checa_ponto, $conexao);
$resultado_checa_ponto = mysql_fetch_array($dados_checa_ponto);
$VARtotal_nao_justificadas = $resultado_checa_ponto[total_nao_justificadas];
if ($VARtotal_nao_justificadas == '') {
    $VARtotal_nao_justificadas = 0;
}
//echo $VARtotal_nao_justificadas;
//if($VARtotal_nao_justificadas>0||$quantos_justificar>0){
if ($existe_abonado_0 > 0) {
    ?>
    <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr align="center">
            <td height="50" colspan="3">&nbsp;</td>
        </tr>
        <tr align="center">
            <td height="50" colspan="3">&nbsp;</td>
        </tr>
        <tr align="center">
            <td width="2400" height="50" colspan="3"><p><font color="#FF0000"
                                                              style='font-size:10px; font-family:Verdana, Arial'><strong><font
                                size="4">N&atilde;o
                                foi poss&iacute;vel gerar essa ficha de frequ&ecirc;ncia.<br>
                                Ainda existem justificativas em aberto para este per&iacute;odo!</font><br>
                            <br>
                            <font color="#000000" size="3">Verifique suas justificativas e solicite
                                ao seu chefe imediato o abono.</font></strong></font></p></td>
        </tr>
    </table>
    <?php
    die;
} ?>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel"
       style='border-bottom: 1px solid #cccccc; border-top: 1px solid #cccccc;border-left: 1px solid #cccccc;border-right: 1px solid #cccccc'>
    <tr valign="bottom">
        <td height="25" bgcolor="#e5e6e8" style='border-bottom: 1px solid #cccccc'>
            <table width="800" border="0" cellpadding="0" cellspacing="0">
                <tr valign="middle">
                    <td width="54" height="22" align="center" bgcolor="#CCCCCC" style='border-right: 1px solid #cccccc'>
                        <strong><font style="font-size:10px; font-family: Verdana, Arial; color=#333333">Dia</font>
                        </strong></td>
                    <td width="55" align="center" style="border-bottom: 1px solid #cccccc"><strong><font
                                style="font-size:10px; font-family: Verdana, Arial; color=#333333">Entrada</font></strong>
                    </td>
                    <td width="55" align="center" style="border-bottom: 1px solid #cccccc"><strong><font
                                style="font-size:10px; font-family: Verdana, Arial; color=#333333">Sa&iacute;da</font></strong>
                    </td>
                    <td width="54" align="center" style="border-bottom: 1px solid #cccccc"><strong><font
                                style="font-size:10px; font-family: Verdana, Arial; color=#333333">Entrada</font></strong>
                    </td>
                    <td width="54" align="center" style="border-bottom: 1px solid #cccccc"><strong><font
                                style="font-size:10px; font-family: Verdana, Arial; color=#333333">Sa&iacute;da</font></strong>
                    </td>
                    <td width="78" align="center"
                        style="border-left: 1px solid #cccccc; padding: 3px;border-bottom: 1px solid #cccccc">
                        <strong><font style="font-size:10px; font-family: Verdana, Arial; color=#333333">Horas<br>
                                trabalhadas</font></strong></td>
                    <td width="70" align="center"
                        style="border-left: 1px solid #cccccc; padding: 3px;border-bottom: 1px solid #cccccc">
                        <strong><font style="font-size:10px; font-family: Verdana, Arial; color=#333333">Horas<br>
                                abonadas </font></strong></td>
                    <td width="56" align="center"
                        style="border-left: 1px solid #cccccc; padding: 3px;border-bottom: 1px solid #cccccc">
                        <strong><font
                                style="font-size:10px; font-family: Verdana, Arial; color=#333333">Total</font></strong>
                    </td>
                    <td width="235" align="center"
                        style="border-left: 1px solid #cccccc; padding: 3px;border-bottom: 1px solid #cccccc">
                        <strong><font style="font-size:10px; font-family: Verdana, Arial; color=#333333">Ocorr&ecirc;ncias</font></strong>
                    </td>
                    <td width="59" align="center"
                        style="border-left: 1px solid #cccccc; padding: 3px;border-bottom: 1px solid #cccccc">
                        <strong><font style="font-size:10px; font-family: Verdana, Arial; color=#333333">Apura&ccedil;&atilde;o</font></strong>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr class="Itens_rel_s_traco">
        <td height="15" valign="top">
            <table width="800" border="0" cellpadding="0" cellspacing="0">
                <?php
                $Data = strtotime($mes . "/01/" . $ano);
                $Dia = date('w', strtotime(date('n/\0\1\/Y', $Data)));
                $Dias = date('t', $Data);

                for ($d = 1; $d <= $Dias; $d++) {
                    $data_dia = strtotime($mes . "/" . $d . "/" . $ano);
                    $data_dias = date("w", $data_dia);
                    $sql_evento = "select id_evento,titulo from p_eventos inner join p_tipo_justificativa on p_eventos.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa where (day(data_evento) = " . $d . " and month(data_evento) = " . $mes . " and year(data_evento) = " . $ano . ") and (id_usr = " . $id_usr . " or id_depto = 0)";
                    $dados_evento = mysql_query($sql_evento, $conexao);
                    $resultado_evento = mysql_fetch_array($dados_evento);

                    ?>
                    <tr valign="middle"  <?php if ($data_dias == 0 || $data_dias == 6) {
                        echo " 'style=color:#C30; bgcolor=#FFDDDD ";
                    }
                    if ($resultado_evento[id_evento] != "") {
                        echo " 'style=color:#C30; bgcolor=#FFCCBF ";
                    }
                    ?>>
                        <td width="50" height="23" align="center" bgcolor="#CCCCCC"
                            style='border-right: 1px solid #cccccc'>
                            <strong> <font style="font-size:10px; font-family: Verdana, Arial; color=#666666">
                                    <?php echo $d; ?> </font></strong></td>
                        <?php

                        if ($data_dias == 0 || $data_dias == 6 || $resultado_evento[id_evento] != "") {
                            echo "<td colspan=9 align=center  style='border-bottom: 1px solid #cccccc'><strong><font color=";
                            if ($resultado_evento[id_evento] != "") {
                                echo "#990000>";
                            } else {
                                echo "#FF0000>";
                            }
                            if ($resultado_evento[id_evento] != "") {
                                echo "<font style='font-size:10px; font-family: Verdana, Arial; color=#333333'>" . $resultado_evento[titulo] . "</font>";
                            } else {
                                if ($data_dias == 0)
                                    echo "<font style='font-size:10px; font-family: Verdana, Arial; color=#333333'>D o m i n g o </font>";
                                if ($data_dias == 6)
                                    echo "<font style='font-size:10px; font-family: Verdana, Arial; color=#333333'>S á b a d o </font>";

                            }
                            echo "</font></strong>";
                        } else {
                            ?>
                            <td width="55" align="center" style="border-bottom: 1px solid #cccccc">
                                <?php $sql1 = "select hora,minutos,etapa from p_registro left join p_justificativa on p_justificativa.id_usr = p_registro.id_usr where p_registro.id_usr = " . $id_usr . " and day(data_registro) = " . $d . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and etapa = 1";
                                $dados1 = mysql_query($sql1, $conexao);
                                $resultado1 = mysql_fetch_array($dados1);
                                if ($resultado1[hora] != '') {
                                    echo "<font style='font-size:10px; font-family:Verdana, Arial'>" . $resultado1[hora] . ":" . $resultado1[minutos] . "</font>";
                                } else {
                                    echo "-";
                                }?>
                            </td>
                            <td width="55" align="center" style="border-bottom: 1px solid #cccccc">
                                <?php $sql2 = "select hora,minutos,etapa from p_registro left join p_justificativa on p_justificativa.id_usr = p_registro.id_usr where p_registro.id_usr = " . $id_usr . " and day(data_registro) = " . $d . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and etapa = 2";
                                $dados2 = mysql_query($sql2, $conexao);
                                $resultado2 = mysql_fetch_array($dados2);
                                if ($resultado2[hora] != '') {
                                    echo "<font style='font-size:10px; font-family: Verdana, Arial'>" . $resultado2[hora] . ":" . $resultado2[minutos] . "</font>";
                                } else {
                                    echo "-";
                                }?>
                            </td>
                            <td width="55" align="center" style="border-bottom: 1px solid #cccccc">
                                <?php $sql3 = "select hora,minutos,etapa from p_registro left join p_justificativa on p_justificativa.id_usr = p_registro.id_usr where p_registro.id_usr = " . $id_usr . " and day(data_registro) = " . $d . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and etapa = 3";
                                $dados3 = mysql_query($sql3, $conexao);
                                $resultado3 = mysql_fetch_array($dados3);
                                if ($resultado3[hora] != '') {
                                    echo "<font style='font-size:10px; font-family:Verdana, Arial'>" . $resultado3[hora] . ":" . $resultado3[minutos] . "</font>";
                                } else {
                                    echo "-";
                                }?>
                            </td>
                            <td width="55" align="center" style="border-bottom: 1px solid #cccccc">
                                <?php $sql4 = "select hora,minutos,etapa from p_registro left join p_justificativa on p_justificativa.id_usr = p_registro.id_usr where p_registro.id_usr = " . $id_usr . " and day(data_registro) = " . $d . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and etapa = 4";
                                $dados4 = mysql_query($sql4, $conexao);
                                $resultado4 = mysql_fetch_array($dados4);
                                if ($resultado4[hora] != '') {
                                    echo "<font style='font-size:10px; font-family:Verdana, Arial'>" . $resultado4[hora] . ":" . $resultado4[minutos] . "</font>";
                                } else {
                                    echo "-";
                                }?>
                            </td>
                            <td width="80"
                                style="border-left: 1px solid #cccccc; padding: 3px;border-bottom: 1px solid #cccccc"
                                align="center"><?php
                                $diferenca_periodo_1 = 0;
                                $diferenca_periodo_2 = 0;
                                $horario_1 = $resultado1[hora] . ":" . $resultado1[minutos];
                                $horario_1 = explode(':', $horario_1);
                                $hora_1 = (60 * 60 * $horario_1[0]) + (60 * $horario_1[1]);

                                $horario_2 = $resultado2[hora] . ":" . $resultado2[minutos];
                                $horario_2 = explode(':', $horario_2);
                                $hora_2 = (60 * 60 * $horario_2[0]) + (60 * $horario_2[1]);

                                $diferenca_periodo_1 = abs(($hora_1 - $hora_2) / 60);
                                if ($resultado1[hora] == '' || $resultado2[hora] == '') {
                                    $diferenca_periodo_1 = 0;
                                }

                                if ($tipo_grade == 0) {
                                    if ($resultado3[hora] != '' && $resultado4[hora] != '') {
                                        $horario_3 = $resultado3[hora] . ":" . $resultado3[minutos];
                                        $horario_3 = explode(':', $horario_3);
                                        $hora_3 = (60 * 60 * $horario_3[0]) + (60 * $horario_3[1]);

                                        $horario_4 = $resultado4[hora] . ":" . $resultado4[minutos];
                                        $horario_4 = explode(':', $horario_4);
                                        $hora_4 = (60 * 60 * $horario_4[0]) + (60 * $horario_4[1]);

                                        $diferenca_periodo_2 = abs(($hora_3 - $hora_4) / 60);
                                    }
                                }


                                $diferenca_periodos = ($diferenca_periodo_1 + $diferenca_periodo_2) / 60;

                                $tempo = explode('.', $diferenca_periodos); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
                                $hora = $tempo[0];

                                //echo $diferenca_periodos." - ".$hora."<BR>";
                                @$minutos = (float)(0) . '.' . $tempo[1]; // Aqui forçamos a conversão para float, para não ter erro.
                                $minutos = round($minutos * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
                                $minutos = explode('.', $minutos); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
                                $minutos = $minutos[0];
                                if ($minutos < 10) {
                                    $minutos = "0" . $minutos;
                                }
                                if ($hora < 10) {
                                    $hora = "0" . $hora;
                                }
                                echo "<font style='font-size:10px; font-family:Verdana, Arial'>" . $hora . ':' . $minutos . "</font>";

                                ?>&nbsp;
                            </td>
                            <td width="70"
                                style="border-left: 1px solid #cccccc; padding: 3px;border-bottom: 1px solid #cccccc"
                                align="center"><?php
                                $sql_abonado = "select qtd_horas,abonado,id_tipo_justificativa from p_justificativa where id_usr = " . $id_usr . " and day(data_justificativa) = " . $d . " and month(data_justificativa) = " . $mes . " and year(data_justificativa) = " . $ano;

                                $dados_abonado = mysql_query($sql_abonado, $conexao);
                                $total_abonado = 0;
                                $total_abonado_nao = 0;
                                while ($resultado_abonado = mysql_fetch_array($dados_abonado)) {
                                    $horario_abonado = explode(':', $resultado_abonado[qtd_horas]);
                                    $hora_abonado = (60 * 60 * $horario_abonado[0]) + (60 * $horario_abonado[1]);
                                    if ($resultado_abonado[id_tipo_justificativa] != 21) {
                                        if ($resultado_abonado[abonado] == 1) {
                                            $total_abonado = $total_abonado + $hora_abonado;
                                        } else {
                                            $total_abonado_nao = $total_abonado_nao + $hora_abonado;
                                        }
                                    }
                                    //echo $resultado_abonado[qtd_horas]."<BR>";
                                }
                                $total_abonado = $total_abonado / 60 / 60;
                                $tempo_abonado = explode('.', $total_abonado); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
                                $hora_abonado = $tempo_abonado[0];

                                //echo $diferenca_periodos." - ".$hora."<BR>";
                                @$minutos_abonado = (float)(0) . '.' . $tempo_abonado[1]; // Aqui forçamos a conversão para float, para não ter erro.
                                $minutos_abonado = round($minutos_abonado * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
                                $minutos_abonado = explode('.', $minutos_abonado); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
                                $minutos_abonado = $minutos_abonado[0];
                                if ($minutos_abonado < 10) {
                                    $minutos_abonado = "0" . $minutos_abonado;
                                }
                                if ($hora_abonado < 10) {
                                    $hora_abonado = "0" . $hora_abonado;
                                }
                                echo "<font style='font-size:10px; font-family:Verdana, Arial'>" . $hora_abonado . ':' . $minutos_abonado . "</font>";

                                ?>&nbsp;</td>
                            <td width="55" align="center"
                                style="border-left: 1px solid #cccccc; padding: 3px;border-bottom: 1px solid #cccccc">
                                <strong>
                                    <?php
                                    $total_geral = ($diferenca_periodos + $total_abonado);
                                    $total_geral_soma = $total_geral_soma + $total_geral;
                                    // echo $total_geral."<BR>";
                                    $tempo_total = explode('.', $total_geral); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
                                    $hora_total = $tempo_total[0];

                                    //echo $diferenca_periodos." - ".$hora."<BR>";
                                    @$minutos_total = (float)(0) . '.' . $tempo_total[1]; // Aqui forçamos a conversão para float, para não ter erro.
                                    $minutos_total = round($minutos_total * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
                                    $minutos_total = explode('.', $minutos_total); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
                                    $minutos_total = $minutos_total[0];
                                    if ($minutos_total < 10) {
                                        $minutos_total = "0" . $minutos_total;
                                    }
                                    if ($hora_total < 10) {
                                        $hora_total = "0" . $hora_total;
                                    }
                                    echo "<font style='font-size:10px; font-family:Verdana, Arial'>" . $hora_total . ':' . $minutos_total . "</font>";
                                    ?>
                                </strong></td>
                            <td width="228"
                                style="border-left: 1px solid #cccccc; padding: 3px;border-bottom: 1px solid #cccccc">
                                <?php //$sql_obs = "select observacoes from p_justificativa where id_usr = ".$id_usr." and day(data_justificativa) = ".$d." and month(data_justificativa) = ".$mes." and year(data_justificativa) = ".$ano;
                                $sql_obs = "select distinct titulo from p_tipo_justificativa inner join p_justificativa on p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa where id_usr = " . $id_usr . " and day(data_justificativa) = " . $d . " and month(data_justificativa) = " . $mes . " and year(data_justificativa) = " . $ano . " and abonado = 1";
                                //echo $sql_obs;
                                $dados_obs = mysql_query($sql_obs, $conexao);
                                $qtd_obs = 0;
                                while ($resultado_obs = mysql_fetch_array($dados_obs)) {
                                    $qtd_obs++;
                                    if ($resultado_obs[titulo] != '') {
                                        echo "<font style='font-size:10px; font-family:Verdana, Arial'>-" . $resultado_obs[titulo] . "<br></font>";


                                    }
                                }
                                ?>
                            </td>
                            <td width="60" align="center"
                                style="border-left: 1px solid #cccccc; padding: 3px;border-bottom: 1px solid #cccccc">
                                <strong>
                                    <?php

                                    $total_aqui = (($diferenca_periodos + $total_abonado) * 60) - ($total_abonado_nao / 60);
                                    //  echo $diferenca_periodos*60;
                                    // echo "<br>".$total_abonado*60;
                                    // echo "<br>".$total_abonado_nao/60;
                                    $total_aqui2 = 0;

                                    $periodo_banco_1 = $entrada_1;
                                    $periodo_banco_1 = explode(':', $periodo_banco_1);
                                    $periodo_banco_1_x = (60 * 60 * $periodo_banco_1[0]) + (60 * $periodo_banco_1[1]);

                                    $periodo_banco_2 = $saida_1;
                                    $periodo_banco_2 = explode(':', $periodo_banco_2);
                                    $periodo_banco_2_x = (60 * 60 * $periodo_banco_2[0]) + (60 * $periodo_banco_2[1]);

                                    $periodo_banco_3 = $entrada_2;
                                    $periodo_banco_3 = explode(':', $periodo_banco_3);
                                    $periodo_banco_3_x = (60 * 60 * $periodo_banco_3[0]) + (60 * $periodo_banco_3[1]);

                                    $periodo_banco_4 = $saida_2;
                                    $periodo_banco_4 = explode(':', $periodo_banco_4);
                                    $periodo_banco_4_x = (60 * 60 * $periodo_banco_4[0]) + (60 * $periodo_banco_4[1]);

                                    $total_horas_periodo_1 = abs(($periodo_banco_2_x - $periodo_banco_1_x) / 60);
                                    $total_horas_periodo_2 = abs(($periodo_banco_4_x - $periodo_banco_3_x) / 60);


                                    if ($tipo_grade == 0) {
                                        $total_horas_grade = abs(($total_horas_periodo_2 + $total_horas_periodo_1));
                                    } else {
                                        $total_horas_grade = abs($total_horas_periodo_1);

                                    }

                                    //echo "<br>".($total_horas_grade*60);
                                    //echo "<br>".$diferenca_periodos*60*60;
                                    //echo "<br>".$total_abonado*60*60;

                                    $total_horas_cumprir = ($total_horas_grade * 60) - (($diferenca_periodos + $total_abonado) * 60 * 60);

                                    //if($total_horas_grade>$total_aqui){
                                    if ($total_horas_grade > ($total_geral * 60)) {
                                        $total_aqui2 = $total_horas_cumprir / 60 / 60;
                                        //echo $total_horas_grade."-".$total_geral*60;
                                        $super_total = $super_total + $total_aqui2;
                                        $tempo_aqui = explode('.', $total_aqui2); // Aqui divide o restante da hora, pois se não for inteiro, retornará um decimal, o minuto, será o valor depois do ponto.
                                        $hora_aqui = $tempo_aqui[0];

                                        //echo $diferenca_periodos." - ".$hora."<BR>";
                                        @$minutos_aqui = (float)(0) . '.' . $tempo_aqui[1]; // Aqui forçamos a conversão para float, para não ter erro.
                                        $minutos_aqui = round($minutos_aqui * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
                                        $minutos_aqui = explode('.', $minutos_aqui); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
                                        $minutos_aqui = $minutos_aqui[0];
                                        if ($minutos_aqui < 10) {
                                            $minutos_aqui = "0" . $minutos_aqui;
                                        }
                                        if ($hora_aqui < 10) {
                                            $hora_aqui = "0" . abs($hora_aqui);
                                        }
                                        echo "<font style='font-size:10px; font-family:Verdana, Arial' color = '#FF0000'>(" . $hora_aqui . ':' . $minutos_aqui . ")</font>";

                                    } else {
                                        echo "<font style='font-size:10px; font-family:Verdana, Arial' color = '#009933'>NORMAL</font>";
                                    }
                                    ?>
                                </strong></td>
                        <?php } ?>
                    </tr>
                <?php }
                ?>
            </table>

        </td>
    </tr>
    <tr class="Sub_titulo_rel">
        <td height="25" align="right">
            <?php
            //  $tempo_total_soma = explode('.', $total_geral_soma);
            $tempo_total_soma = explode('.', $super_total);

            $hora_total_soma = $tempo_total_soma[0];

            //echo $diferenca_periodos." - ".$hora."<BR>";
            @$minutos_total_soma = (float)(0) . '.' . $tempo_total_soma[1]; // Aqui forçamos a conversão para float, para não ter erro.
            $minutos_total_soma = round($minutos_total_soma * 60); // Aqui multiplicamos o valor que sobra que é menor que 1, por 60, assim ele retornará o minuto corretamente, entre 0 á 59 minutos.
            $minutos_total_soma = explode('.', $minutos_total_soma); // Aqui damos explode para retornar somente o valor inteiro do minuto. O que sobra será os segundos
            $minutos_total_soma = $minutos_total_soma[0];
            if ($minutos_total_soma < 10) {
                $minutos_total_soma = "0" . $minutos_total_soma;
            }
            if ($hora_total_soma < 10) {
                $hora_total_soma = "0" . $hora_total_soma;
            }

            ?>
            <?php if ($super_total > 0) {
                echo "<font color='#FF0000' style='font-size:10px; font-family: Verdana, Arial; color=#FF0000'>Total de: <strong>" . $hora_total_soma . ":" . $minutos_total_soma . "</strong> hs ";
            } else {
                echo "Situação:&nbsp;&nbsp;<font color='#009933' style='font-size:10px; font-family: Verdana, Arial; color=#009933'>NORMAL</font>";
            } ?>
            &nbsp;&nbsp; </font> </td>
    </tr>
</table>
<?php mysql_close($conexao); ?>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr align="center">
        <td height="30">&nbsp;</td>
        <td colspan="2" align="right"><strong><font style='font-size:10px; font-family:Verdana, Arial'>Goi&acirc;nia,
                    <?php

                    $hoje = getdate();

                    switch ($hoje['wday']) {
                        case 0:
                            echo "Domingo, ";
                            break;
                        case 1:
                            echo "Segunda-Feira, ";
                            break;
                        case 2:
                            echo "Terça-Feira, ";
                            break;
                        case 3:
                            echo "Quarta-Feira, ";
                            break;
                        case 4:
                            echo "Quinta-Feira, ";
                            break;
                        case 5:
                            echo "Sexta-Feira, ";
                            break;
                        case 6:
                            echo "Sábado, ";
                            break;
                    }

                    echo $hoje['mday'];
                    switch ($hoje['mon']) {
                        case 1:
                            echo " de Janeiro de ";
                            break;
                        case 2:
                            echo " de Fevereiro de ";
                            break;
                        case 3:
                            echo " de Março de ";
                            break;
                        case 4:
                            echo " de Abril de ";
                            break;
                        case 5:
                            echo " de Maio de ";
                            break;
                        case 6:
                            echo " de Junho de ";
                            break;
                        case 7:
                            echo " de Julho de ";
                            break;
                        case 8:
                            echo " de Agosto de ";
                            break;
                        case 9:
                            echo " de Setembro de ";
                            break;
                        case 10:
                            echo " de Outubro de ";
                            break;
                        case 11:
                            echo " de Novembro de ";
                            break;
                        case 12:
                            echo " de Dezembro de ";
                            break;
                    }

                    echo $hoje['year'] . ".";
                    ?>
                </font></strong></td>
    </tr>
    <tr align="center">
        <td height="50" colspan="3"><font style='font-size:10px; font-family:Verdana, Arial'><strong>O
                    presente documento &eacute; de minha responsabilidade, pela qual respondo
                    penal e administrativamente.</strong></font></td>
    </tr>
    <tr align="center">
        <td height="30">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr align="center" valign="top">
        <td width="800" height="30" style="border-top: 1px solid #000000"><strong><font
                    style='font-size:10px; font-family: Verdana, Arial; color=#333333'><?php echo $nome; ?></font></strong>
        </td>
        <td width="800">&nbsp;</td>
        <td width="800" style="border-top: 1px solid #000000"><font
                style='font-size:10px; font-family: Verdana, Arial; color=#333333'><strong>Atesto
                    e carimbo do chefe imediato</strong></font></td>
    </tr>
</table>
</body>
</html>