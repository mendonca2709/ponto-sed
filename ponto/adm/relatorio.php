﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.location.href = 'index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}
require_once("../funcoes/conexao.php");

$cont = 0;

$sql_total = $_POST["sql_total"];
$sql = $_POST["sql"];
if ($sql == "") {
    $condicao_1 = $_POST["condicao_1"];
    $condicao_2 = $_POST["condicao_2"];
    $condicao_3 = $_POST["condicao_3"];
    $condicao_4 = $_POST["condicao_4"];
    $condicao_5 = $_POST["condicao_5"];
    $condicao_6 = $_POST["condicao_6"];
    $condicao_7 = $_POST["condicao_7"];


    $id_tipo_justificativa = $_POST["id_tipo_justificativa"];
    $observacoes = $_POST["observacoes"];
    $protocolo = $_POST["protocolo"];

    $id_depto = $_POST["id_depto"];
    if ($_SESSION["usr_gerente"] == 1 && $_SESSION["sessao_rh"] == 0) {
        $id_depto = $_SESSION["sessao_id_depto"];
    }
    $abonado = $_POST["abonado"];
    $selMES = $_POST["selMES"];
    $selANO = $_POST["selANO"];
    $id_usr = $_POST["id_usr"];


    $inicio = $_POST["inicio"];
    $final = $_POST["final"];
    if ($inicio == '') {
        $inicio = 0;
    }
    $final = $inicio + 30;

    $sql_total = "select count(id_justificativa) as total from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto inner join p_tipo_justificativa on p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa ";
    $sql_total = $sql_total . " where 0=0 ";
    if ($id_tipo_justificativa != 0) {
        $sql_total = $sql_total . " and p_justificativa.id_tipo_justificativa = " . $id_tipo_justificativa . " ";
    }

    if ($observacoes != '') {
        $sql_total = $sql_total . $condicao_1 . " observacoes like '%" . $observacoes . "%'";
    }

    if ($protocolo != '') {
        $sql_total = $sql_total . $condicao_2 . " protocolo like '%" . $protocolo . "'%";
    }

    if ($id_depto != 0) {
        $sql_total = $sql_total . $condicao_3 . " depto.id_depto = " . $id_depto . " ";
    }

    if ($abonado != 0) {
        $sql_total = $sql_total . $condicao_4 . " abonado = " . $abonado . " ";
    }
    if ($selMES != 0) {
        $sql_total = $sql_total . $condicao_5 . " month(p_justificativa.data_justificativa) = '" . $selMES . "' ";
    }
    if ($selANO != 0) {
        $sql_total = $sql_total . $condicao_6 . " year(p_justificativa.data_justificativa) = '" . $selANO . "' ";
    }

    if ($id_usr != 0) {
        $sql_total = $sql_total . $condicao_7 . " p_justificativa.id_usr = " . $id_usr . " ";
    }

    $dados_total = mysql_query($sql_total, $conexao);
    $resultado_total = mysql_fetch_array($dados_total);
    $total = $resultado_total[total];

    $sql = "select * from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto  inner join p_tipo_justificativa on p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa ";
    $sql = $sql . " where 0=0 ";
    if ($id_tipo_justificativa != 0) {
        $sql = $sql . " and p_justificativa.id_tipo_justificativa = " . $id_tipo_justificativa . " ";
    }

    if ($observacoes != '') {
        $sql = $sql . $condicao_1 . " observacoes like '%" . $observacoes . "%'";
    }

    if ($protocolo != '') {
        $sql = $sql . $condicao_2 . " protocolo like '%" . $protocolo . "'%";
    }

    if ($id_depto != 0) {
        $sql = $sql . $condicao_3 . " depto.id_depto = " . $id_depto . " ";
    }

    if ($abonado !== "") {
        $sql = $sql . $condicao_4 . " abonado = " . $abonado . " ";
    }
    if ($selMES != 0) {
        $sql = $sql . $condicao_5 . " month(p_justificativa.data_justificativa) = '" . $selMES . "' ";
    }
    if ($selANO != 0) {
        $sql = $sql . $condicao_6 . " year(p_justificativa.data_justificativa) = '" . $selANO . "' ";
    }

    if ($id_usr != 0) {
        $sql = $sql . $condicao_7 . " p_justificativa.id_usr = " . $id_usr . " ";
    }

    if ($_SESSION["sessao_id_usr"] != 55) {
        $sql = $sql . " and usuarios.id_usr <> 4";
    }
    $sql = $sql . " order by p_justificativa.data_justificativa desc limit " . $inicio . ",30";
    $dados = mysql_query($sql, $conexao);

} else {

    $dados_total = mysql_query($sql_total, $conexao);
    $resultado_total = mysql_fetch_array($dados_total);
    $total = $resultado_total[total];


    $dados = mysql_query($sql, $conexao);

}
?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<script language="JavaScript" type="text/javascript">

    function fncJustifica(id_usr, mes, ano, id_depto) {
        document.justifica.id_usr.value = id_usr;
        document.justifica.mes.value = mes;
        document.justifica.ano.value = ano;
        document.justifica.id_depto.value = id_depto;
        document.justifica.submit();

    }
    function Imprime() {
        window.print();
    }
    function AbreJustifivativa(id_usr, dia, mes, ano) {

        //document.abre_janela2.id_justificativa.value=id_justificativa;
        document.abre_janela2.id_usr.value = id_usr;
        document.abre_janela2.dia.value = dia;
        document.abre_janela2.mes.value = mes;
        document.abre_janela2.ano.value = ano;
//    window.open("","myNewWin","width=500,height=300,toolbar=0"); 
        window.open("", "myNewWin", "");
        var a = window.setTimeout("document.abre_janela2.submit();", 500);
    }


    function fncRelatorio2(sql, sql_total) {
        document.relatorio.sql.value = sql;
        document.relatorio.sql_total.value = sql_total;
        document.relatorio.submit();
    }
    function fncEnter() {
        if (window.event.keyCode == 13) {
            fncBusca();
        }
    }
    function fncDeleta_favorito(id_favorito) {
        janela.location.href = 'exclui_favorito.php?id_favorito=' + id_favorito + '&pagina=frm_relatorios.php';
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
    }

    function Abre(id_processo) {
        window.open("historico.php?id_processo=" + id_processo + '&tp=1', "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=700,top=10,left=20");
//window.open("processo_auditor.php?id_processo="+id_processo, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=700,top=10,left=20");
    }

    function Abre2(id_processo) {
//window.open("processo.php?id_processo="+id_processo, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=800,top=10,left=20");
        window.showModalDialog("processo.php?id_processo=" + id_processo, "resizable: no", "dialogWidth:1070px; dialogHeight:800px; center:yes");
    }
    function fncFavoritos() {
        div_favorito.style.display = '';
        iframe_favoritos.location.href = 'favorito.php';
    }
</script>
<html>


<head>
    <title>Relat&oacute;rio de ocorr&ecirc;ncias</title>

</head>
<meta name="title" content="SED - SED"/>
<meta name="url" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>"/>
<meta name="description"
      content="SED-GO - A SED de Goiás implementou em um projeto altamente inovador de acompanhamento de Processos totalmente digital."/>
<meta name="keywords"
      content="sistema,ouvidoria,corregedoria,controladoria,processos,protocolo,jedi,workflow,processo eletronico,gerencial,manifestação,acompanhamento,encaminhamento,controle,pesquisa,histórico,governo de goiás,sistema de acompanhamento de processos,sistema gerencial,marcelo roncato"/>

<meta name="autor" content="Marcelo Roncato"/>
<meta name="company" content="SED"/>
<meta name="revisit-after" content="5"/>
<body>
<div id="div_favorito"
     style="position:absolute; width:180px; height:99px; z-index:1; border: 1px none #000000; display:none; left: 13px; top: 328px;"
     z-index="1">
    <iframe name="iframe_favoritos" width="820" height="600" scrolling="no" frameborder="0"></iframe>
</div>

<?php require_once("frm_topo.php"); ?>
<br>

<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><BR><strong><font
                                    color="#333333" size="4">
                                </font></strong>

                        <p><strong><font color="#333333" size="4">&nbsp; Relat&oacute;rio<br>
                                </font><font color="#666666"><strong><font color="#0066FF">&nbsp;&nbsp;

                                            <?php

                                            echo "<font color='#FF3300'>Resultado do relatório</font>";

                                            ?>
                                        </font></strong></font><font color="#333333" size="4">
                                </font></strong></p>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td></td>
                            </tr>
                        </table>

                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" valign="top" bgcolor="#FFFFFF">
                        <table width="800" border="0" cellpadding="3" cellspacing="0">
                            <tr>
                                <td>&nbsp;</td>
                                <td align="right">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="14">&nbsp;</td>
                                <td align="right"><?php if ($total > 0) { ?><img src="../images/addfavoritos.png"
                                                                                 width="35" height="35"
                                                                                 align="absmiddle">
                                        <a href="#" onClick="fncFavoritos();">Adicionar Relat&oacute;rio
                                            aos meus Favoritos</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php } ?></td>
                            </tr>
                        </table>
                        <br>
                        <table width="780" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
                            <tr valign="bottom">
                                <td height="25" class="Titulo_rel">
                                    <table width="780" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td width="165" height="12"><strong><font style="font-size:10px"
                                                                                      color="#333333">
                                                        &nbsp;&nbsp;&nbsp;Protocolo</font></strong></td>
                                            <td width="88"><strong><font style="font-size:10px"
                                                                         color="#333333">Data</font></strong></td>
                                            <td width="195"><strong><font style="font-size:10px" color="#333333">Usu&aacute;rio</font></strong>
                                            </td>
                                            <td width="79" align="left"><strong><font style="font-size:10px"
                                                                                      color="#333333">Depto</font></strong>
                                            </td>
                                            <td width="42"><strong><font style="font-size:10px"
                                                                         color="#333333">Horas</font></strong></td>
                                            <td width="157"><strong><font style="font-size:10px"
                                                                          color="#333333">Tipo</font></strong></td>
                                            <td width="38"><strong><font style="font-size:10px"
                                                                         color="#333333">Painel</font></strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="Itens_rel_s_traco">
                                <td height="15" valign="top">
                                    <table width="780" border="0" cellpadding="1" cellspacing="0">
                                        <?php
                                        $total_horas = 0;
                                        while ($resultado = mysql_fetch_array($dados)) {


                                            $cont = $cont + 1;
                                            $par = ($cont % 2); ?>
                                            <tr <?php

                                            if ($par == 0) {
                                                echo "class='Itens_sel_rel'";
                                            } else {
                                                echo "class='Itens_normal'";
                                            }



                                            ?>>
                                                <td width="4" align="center" valign="middle"><strong> <font
                                                            color="#009933"><img src="../images/morearrow_08c.gif"
                                                                                 width="4" height="7">
                                                        </font></strong></td>
                                                <td width="162" height="30" valign="middle"><strong><font
                                                            color="#009933">
                                                            <div style="cursor:pointer;"
                                                                 onClick="AbreJustifivativa('<?php echo $resultado[id_usr] ?>',<?php echo substr($resultado[data_justificativa], 8, 2) ?>,<?php echo substr($resultado[data_justificativa], 5, 2) ?>,<?php echo substr($resultado[data_justificativa], 0, 4) ?>);"
                                                                 id="<?php echo substr($resultado[data_justificativa], 0, 4) ?>"><?php echo $resultado[protocolo]; ?></div>
                                                        </font></strong></td>
                                                <td width="88"
                                                    valign="middle"> <?php echo date('d/m/Y', strtotime($resultado[data_justificativa])); ?>
                                                </td>
                                                <td width="201" valign="middle"> <?php echo $resultado[nome]; ?>
                                                </td>
                                                <td width="77" valign="middle">
                                                    <strong><?php echo $resultado[sigla]; ?></strong></td>
                                                <td width="34" valign="middle" style='padding:0cm 3.5pt 0cm 3.5pt'>
                                                    <strong>
                                                        <?php
                                                        $total_horas = $total_horas + $resultado[qtd_horas];

                                                        echo $resultado[qtd_horas]; ?>
                                                    </strong></td>
                                                <td width="157" valign="middle" style='padding:0cm 3.5pt 0cm 3.5pt'>
                                                    <strong><?php echo $resultado[titulo]; ?></strong></td>
                                                <td width="25" align="center" valign="middle"
                                                    style='padding:0cm 3.5pt 0cm 3.5pt'><a href="#"
                                                                                           onClick="fncJustifica(<?php echo $resultado[id_usr] ?>,<?php echo substr($resultado[data_justificativa], 5, 2) ?>,<?php echo substr($resultado[data_justificativa], 0, 4) ?>,<?php echo $resultado[id_depto] ?>);"><img
                                                            src="../images/painel3.gif" width="25" height="25"
                                                            border="0"></a>

                                                    <div style="cursor:pointer;"
                                                         onClick="Abre(<?php echo $resultado[id_processo]; ?>);"></div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                    <?php if ($dados == false){ ?>
                                    <BR> <BR> <BR> <BR>
                                    <table width="72%" border="0" align="center" cellpadding="2" cellspacing="0">
                                        <tr>
                                            <td width="6%"><img src="../images/demo.png" width="71" height="71">
                                            </td>
                                            <td width="94%"><strong><font color="#FF0000">&nbsp;N&atilde;o
                                                        foram encontrados ocorr&ecirc;ncias com essa combina&ccedil;&atilde;o
                                                        no sistema!</font></strong></td>
                                        </tr>
                                    </table>
                                    <p><BR>
                                        <BR>
                                        <BR>
                                        <BR>
                                        <?php } ?>
                                        <BR><?php if ($cont > 0) {
                                            echo "<font size=2px>&nbsp;Total de horas para esta busca: <strong>" . number_format($total_horas, 0, ",", ".") . " horas.</strong></font>";
                                        } ?>
                                        <BR>

                                    <p><BR>
                                        <font color="#000000">

                                        </font></td>
                            </tr>
                            <tr class="Sub_titulo_rel">
                                <td height="25">&nbsp;
                                    <table width="406" border="0" align="center" cellpadding="2">
                                        <tr align="center">
                                            <td><font color="#333333"><strong>Resultado: <?php echo $inicio + 1 ?>
                                                        até
                                                        <?php
                                                        if (($final) < $total) {
                                                            echo $final;
                                                        } else {
                                                            echo $total;
                                                        } ?>
                                                        - Total de <?php echo $total; ?>. </strong> </font></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <?php if ($inicio != 0){ ?>
                                                <a href="#"
                                                   onClick="document.form_busca.inicio.value=<?php echo($inicio - 30) ?>;document.form_busca.submit();"><img
                                                        src="../images/seta_esq.jpg" alt="P&aacute;gina anterior"
                                                        width="15" height="15" border="0">
                                                    <?php } ?>
                                                </a><font color="#FF6600">&nbsp;P&aacute;ginas: <strong>
                                                    </strong></font><font color="#FF6600"><strong>
                                                        <?php
                                                        $total_paginas = floor($total / 30);
                                                        if (($total % 30) > 0) {
                                                            $total_paginas++;
                                                        }
                                                        //echo "-".$total_paginas."-<BR>";]
                                                        $total_paginas_geral = $total_paginas;
                                                        if ($total_paginas > 30) {
                                                            $total_paginas = 30;
                                                        }
                                                        if ($inicio == 0) {
                                                            $pagina_inicial = 1;
                                                        }
                                                        if ($inicio > 0) {
                                                            $pagina_inicial = $inicio / 30;
                                                        }
                                                        if ($total_paginas < 30) {
                                                            $pagina_inicial = 1;
                                                        }
                                                        for ($p = 1; $p <= $total_paginas; $p++) {
                                                            if ($pagina_inicial <= $total_paginas_geral) {
                                                                $indice = $pagina_inicial * 30;
                                                                if ((($pagina_inicial - 1) * 30) != $inicio) {
                                                                    echo "<a href='#' onClick='document.form_busca.inicio.value=" . (($pagina_inicial - 1) * 30) . ";document.form_busca.submit();'><font color = '#333333'>" . $pagina_inicial . "</font></a>";
                                                                }
                                                                if ((($pagina_inicial - 1) * 30) == $inicio) {
                                                                    echo "<font color = '#99999'>" . $pagina_inicial . "</font>";
                                                                }
//if($p<15&&$p<$total_paginas&&($pagina_inicial<$total_paginas_geral)){
                                                                if ($pagina_inicial < $total_paginas_geral) {
                                                                    echo "<font color = '#999999'>,</font>";
                                                                }

                                                                if ($pagina_inicial == $total_paginas_geral) {
                                                                    echo "<font color = '#999999'>.</font>";
                                                                }
                                                                if ($p == 30) {
                                                                    echo "<font color = '#999999'>...</font>";
                                                                }
                                                                $pagina_inicial++;
                                                            }
                                                        }

                                                        ?>
                                                    </strong></font>
                                                <?php if (($inicio + 30) <= $total) { ?>
                                                    <a href="#"
                                                       onClick="document.form_busca.inicio.value=<?php echo($inicio + 30) ?>;document.form_busca.submit();"><img
                                                            src="../images/seta_dir.jpg"
                                                            alt="Pr&oacute;xima p&aacute;gina" width="15" height="15"
                                                            border="0"></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p></p>
                        <br> <br> <br> <br>

                        <form name="relatorio" method="post" action="relatorio.php">
                            <input type="text" name="sql" value="<?php echo $sql; ?>" style="display:none">
                            <input type="text" name="sql_total" value="<?php echo $sql_total; ?>" style="display:none">
                        </form>
                        <form name="relatorio_teste" method="post" action="teste.php" target="_blank">
                            <input type="text" name="sql" value="<?php echo $sql; ?>" style="display:none">
                            <input type="text" name="sql_total" value="<?php echo $sql_total; ?>" style="display:none">
                        </form>
                        <br>

                        <p><br>
                        </p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>
                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="96%"><a href="frm_menu_relatorios.php">&nbsp;&nbsp;&nbsp;&nbsp;Voltar
                                        para o menu relat&oacute;rio</a></td>
                                <td width="4%">
                                    <div align="center"><a href="javascript:history.go(-1)"><img
                                                src="../images/reply-16x16.gif" title="Voltar" width="16"
                                                height="16" border="0"></a></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Busca</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <form name="form_busca" method="post" action="frm_Lista_ocorrencias.php">
                                        <font color="#FF6600" size="1"><strong>Informe texto de
                                                busca:</strong></font><br>
                                        <font color="#FF6600" size="1"><strong>
                                                <input name="status_atual" type="text" id="status_atual" size="3"
                                                       style="display:none" value="<?php echo $status_atual; ?>">
                                                <input name="selMES2" type="text" id="selMES2" size="3"
                                                       value="<?php echo $MES; ?>" style="display:none">
                                                <input name="selANO2" type="text" id="selANO2" size="3"
                                                       value="<?php echo $ANO; ?>" style="display:none">
                                                <input name="local_atual2" type="text" id="local_atual2" size="3"
                                                       value="<?php echo $local_atual; ?>" style="display:none">
                                                <input name="enviados" type="text" id="enviados" size="3"
                                                       value="<?php echo $enviados; ?>" style="display:none">
                                                <input name="texto" type="text" id="texto" size="3"
                                                       value="<?php echo $texto; ?>" style="display:none">
                                            </strong></font>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><input type="text" name="texto_busca" style="width:130px"
                                                           value="<?php echo $texto_busca; ?>" onKeyPress="fncEnter();">
                                                </td>
                                                <td><a href="#" onClick="fncBusca();"><img src="../images/ok_bt.gif"
                                                                                           width="27" height="15"
                                                                                           border="0"></a></td>
                                            </tr>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Op&ccedil;&otilde;es</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width="160" border="0" cellpadding="1" cellspacing="0">
                                        <tr align="center">
                                            <td><a href="#" onClick="Imprime();"><img src="../images/op_1.jpg"
                                                                                      width="60" height="60" border="0"></a>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td><a href="#" onClick="Imprime();"><font size="1">Imprimir</font></a></td>
                                        </tr>
                                        <tr align="center">
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'><img src="../images/favorito_1.png" width="20" height="20"
                                                              align="absmiddle">
                                    Favoritos
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellpadding='1' cellspacing='0'>
                                        <?php

                                        $sql_favorito = "select p_favoritos.id_favorito,p_favoritos_frame.id_favoritos_frame,titulo_favorito,instrucao_sql,instrucao_sql_total from p_favoritos inner join p_favoritos_frame on p_favoritos.id_favorito = p_favoritos_frame.id_favorito where id_gerencia_favorito = " . $_SESSION["sessao_id_usr"];
                                        $dados_favorito = mysql_query($sql_favorito, $conexao);
                                        while ($resultado_favorito = mysql_fetch_array($dados_favorito)) {

                                            ?>
                                            <tr>
                                                <td width="95%" valign="middle" style="padding: 3px 3px 0;">
                                                    <strong><font color="#666666" size="1">
                                                            <a href="#"
                                                               onClick="fncRelatorio2('<?php echo $resultado_favorito[instrucao_sql]; ?>','<?php echo $resultado_favorito[instrucao_sql_total]; ?>');"><?php echo $resultado_favorito[titulo_favorito]; ?>
                                                            </a></font></strong></td>
                                                <td width="5%" valign="middle" class="Itens_normal"><a href="#"
                                                                                                       onClick="fncDeleta_favorito(<?php echo $resultado_favorito[id_favorito] ?>);"><img
                                                            src="../images/delete2.png" width="16" height="16"
                                                            border="0"></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>

                        <p align="right">&nbsp;</p></td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?=date('Y')?> - SED&reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>&nbsp; </p>

<p>&nbsp;</p>

<form name="abre_janela" method="post" action="processo.php" target="myNewWin">
    <input type="hidden" name="id_justificativa">
</form>

<form name="abre_janela2" method="post" action="mostra_justificativa.php" target="myNewWin">
    <input type="hidden" name="id_justificativa">
    <input type="hidden" name="id_usr">
    <input type="hidden" name="dia">
    <input type="hidden" name="mes">
    <input type="hidden" name="ano">
</form>

<form name="justifica" method="post" action="frm_justificativa.php">
    <input type="hidden" name="id_usr">
    <input type="hidden" name="mes">
    <input type="hidden" name="ano">
    <input type="hidden" name="id_depto">
</form>

<?php
mysql_close($conexao);
?>
<iframe name="janela" width="800" height="200" style="display:none"></iframe>
</body>
</html>
