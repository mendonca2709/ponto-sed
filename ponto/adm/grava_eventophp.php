﻿<?php
session_start();
require_once("../funcoes/conexao.php");

$id_pai = $_POST["id_pai"];
if ($id_pai == '') {
    $id_pai = 0;
}

$data_evento = $_POST["data_evento"];
$dia = substr($data_evento, 0, 2);
$mes = substr($data_evento, 3, 2);
$ano = substr($data_evento, 6, 4);
$data_evento = substr($data_evento, 6, 4) . "/" . substr($data_evento, 3, 2) . "/" . substr($data_evento, 0, 2);

$id_tipo_justificativa = $_POST["id_tipo_justificativa"];
$id_depto = $_POST["id_depto"];
if ($id_depto == '') {
    $id_depto = 0;
}
$id_usr = $_POST["id_usr"];
if ($id_usr == '') {
    $id_usr = 0;
}
$dt_final_periodo = $_POST["dt_final_periodo"];

$observacoes = $_POST["observacoes"];
$observacoes = str_replace("\n", " ", $observacoes);
$observacoes = str_replace(chr(13), "<br>", $observacoes);
$observacoes = str_replace(chr(10), " ", $observacoes);
$observacoes = str_replace("\'", "", $observacoes);
$observacoes = str_replace('\"', "", $observacoes);
$observacoes = str_replace("'", "", $observacoes);
$observacoes = str_replace('"', "", $observacoes);

$dt_final_periodo = substr($dt_final_periodo, 6, 4) . "/" . substr($dt_final_periodo, 3, 2) . "/" . substr($dt_final_periodo, 0, 2);
$vetor_dt_final_periodo = explode("/", $dt_final_periodo);
$data_checa_dt_final_periodo = mktime(0, 0, 0, $vetor_dt_final_periodo[1], $vetor_dt_final_periodo[2], $vetor_dt_final_periodo[0]);

$vetor_data_evento = explode("/", $data_evento);
$data_checa_vetor_data_evento = mktime(0, 0, 0, $vetor_data_evento[1], $vetor_data_evento[2], $vetor_data_evento[0]);

$dias = ceil(($data_checa_dt_final_periodo - $data_checa_vetor_data_evento) / 86400);
//echo $dt_final_periodo." - ".$data_evento."<br>";
//echo $dias;
if ($dias < 0) {
    ?>
    <script language="JavaScript">
        alert("Data final da ocorrência não pode ser inferior a data do evento!");
    </script>
    <?php
    die;
}

$numr_ip = $_SERVER[HTTP_X_FORWARDED_FOR] . "-" . $_SERVER[REMOTE_ADDR];
$data_cadastro = date('Y') . "/" . date('m') . "/" . date('d') . " " . date("H") . ":" . date("i") . ":" . date("s");
if ($id_pai != 0) {
    $sql_del = "delete from p_eventos where id_pai = " . $id_pai;
    $dados_del = mysql_query($sql_del, $conexao);
}

$id_evento = 0;
for ($i = 0; $i <= $dias; $i++) {
    $novo_dia = $dia + $i;
    echo $mes;
    $nextdate = date('d/m/Y', mktime(0, 0, 0, $mes, $novo_dia, $ano));
    $novadata_grava = substr($nextdate, 6, 4) . "/" . substr($nextdate, 3, 2) . "/" . substr($nextdate, 0, 2);
//	$novo_d 	= substr($nextdate,0,2);
//	$novo_m 	= substr($nextdate,3,2);
//	$novo_a 	= substr($nextdate,6,4);


    $sql = "insert into p_eventos (data_evento,id_tipo_justificativa,id_depto,id_usr,descricao,data_cadastro,numr_ip,id_pai) values ('" . $novadata_grava . "'," . $id_tipo_justificativa . "," . $id_depto . "," . $id_usr . ",'" . $descricao . "','" . $data_cadastro . "','" . $numr_ip . "'," . $id_evento . ")";
    $dados = mysql_query($sql, $conexao);
//	echo $sql."<br>"; 
    if ($i == 0) {
        $sql_ultimo = "select id_evento from p_eventos order by id_evento desc limit 0,1";
        $dados_ultimo = mysql_query($sql_ultimo, $conexao);
        $resultado_ultimo = mysql_fetch_array($dados_ultimo);
        $id_evento = $resultado_ultimo[id_evento];

        $sql_ultimo = "update p_eventos set id_pai = " . $id_evento . " where id_evento = " . $id_evento;
        $dados_ultimo = mysql_query($sql_ultimo, $conexao);

    }
//	$sql_atualiza 				= "update p_registro set id_justificativa = ".$resultado_ultimo[id_justificativa]." where day(data_registro) = ".$novo_d." and month(data_registro) = ".$novo_m." and year(data_registro) = ".$novo_a." and id_usr = ".$id_usr;
//	$dados_atualiza				= mysql_query($sql_atualiza, $conexao);
//	echo $sql_atualiza."<br>"; 
}


?>
<script language="JavaScript" type="text/javascript">
    alert("Evento cadastrado com sucesso!");
    window.parent.location.href = 'cad_eventos.php';
</script>
