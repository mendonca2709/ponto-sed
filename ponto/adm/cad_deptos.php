﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 || $_SESSION["sessao_rh"] != 1) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.location.href = 'index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}

require_once("../funcoes/conexao.php");

$id_depto = $_GET["id_depto"];

if ($id_depto == '') {
    $id_depto = 0;
    $depto = '';
    $sigla = '';
    $ativo = 0;
    $ramal = '';
    $id_superintendencia = 0;


}

if ($id_depto != '') {

    $sql = "select * from depto where id_depto = " . $id_depto;

    $dados = mysql_query($sql, $conexao);
    $resultado = mysql_fetch_array($dados);
    $depto = $resultado[depto];
    $sigla = $resultado[sigla];
    $ativo = $resultado[ativo];
    $ramal = $resultado[ramal];
    $id_superintendencia = $resultado[id_superintendencia];

}
?>
<script language="JavaScript" src="../funcoes/funcao.js"></script>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>

<script language="JavaScript" type="text/javascript">

    function fncLista() {
        window.open("lista_deptos.php", "", "");
    }


    function fncValida() {
        if (document.formulario.depto.value == '') {
            alert("Informe o nome deste departamento!");
            document.formulario.depto.focus();
            return false;
        }
        if (document.formulario.sigla.value == '') {
            alert("Informe a sigla deste departamento!");
            document.formulario.sigla.focus();
            return false;
        }

        document.formulario.submit();
    }
    function fncValidaEmail(vEmail) {
        var vEmailIndex = vEmail.indexOf("@");
        var vDiv = vEmail.split("@");
        var vValAft = vEmail.substring(vEmailIndex + 1, vEmail.length);
        var vDiv2 = vValAft.split(".");
        if ((vEmailIndex == -1) || (vDiv.length < 2) || (vDiv2.length < 2)) {
            //fncEmailInvalido();
            alert("Formato do E-mail inválido!");
            return false;

        }
        for (i = 0; i < vDiv.length; i++)
            if (vDiv[i] == "") {
                alert("Formato do E-mail inválido!");
                return false;
            }

        for (i = 0; i < vDiv2.length; i++)
            if (vDiv2[i] == "") {
                alert("Formato do E-mail inválido!");
                return false;
            }
    }
    function fncColoca() {
        if (<?php echo $id_depto?>!=
        0
    )
        {
            setTimeout("document.formulario.id_superintendencia.value='<?php echo $id_superintendencia;?>';", 300);
            if (<?php echo $ativo?>!=
            1
        )
            {
                document.formulario.ativo[0].checked = false;
                document.formulario.ativo[1].checked = true;
            }
        else
            {
                document.formulario.ativo[1].checked = false;
                document.formulario.ativo[0].checked = true;

            }

        }

    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
    }


    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }


        else {
            return true;
        }
    }
</script>

<head>
    <title>Cadastro de Departamentos</title>

</head>

<body onLoad="fncColoca();">
<?php require_once("frm_topo.php"); ?>
<div id="status_opcoes"
     style="position:absolute; width:160px; height:99px; z-index:1; border: 1px none #000000; display:none;"
     z-index="1">
    <iframe name="opcoes" width="800" height="500" scrolling="no" frameborder="0"></iframe>
    <!--<script>exibeFash('../images/mapa.swf?id_manifestacao=<?php echo date("s"); ?>', 800, 500)</script>-->
</div>

<div id="status_opcoes2"
     style="position:absolute; width:160px; height:99px; z-index:1; border: 1px none #000000; display:none;"
     z-index="1">
    <script>exibeFash('../images/acompanhamento.swf?id_manifestacao=<?php echo date("s");?>', 800, 500)</script>
</div>

<br>

<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><BR><strong><font
                                    color="#333333" size="4">
                                </font></strong>

                        <p><strong><font color="#333333" size="4">&nbsp; Cadastro de Departamentos</font></strong></p>
                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" align="center" valign="top" bgcolor="#FFFFFF"><br>
                        <table width="790" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
                            <tr valign="bottom">
                                <td height="25" class="Titulo_rel">
                                    <table width="790" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td width="194"><strong><font style="font-size:10px" color="#333333">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>Dados do
                                                    departamento:</strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="Itens_rel_s_traco">
                                <td height="15">
                                    <form action="grava_depto.php" method="post" name="formulario" id="formulario"
                                          target="janela">
                                        <table width="732" border="0" align="center" cellpadding="4" cellspacing="0">
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td><input name="id_depto" type="text" id="id_depto"
                                                           value="<?php echo $id_depto ?>" style="display:none"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td>Superintend&ecirc;ncia:</td>
                                                <td><select name="id_superintendencia" id="id_superintendencia"
                                                            style="width:440px">
                                                        <option value="0" selected>Selecione uma Superintendencia
                                                        </option>
                                                        <?php

                                                        $sql = "select * from p_superintendencia order by superintendencia;";
                                                        $dados = mysql_query($sql, $conexao);
                                                        while ($resultado_u = mysql_fetch_array($dados)) {
                                                            ?>
                                                            <option
                                                                value="<?php echo $resultado_u[id_superintendencia]; ?>"><?php echo $resultado_u[superintendencia]; ?></option>
                                                        <?php } ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td>Departamento:</td>
                                                <td><input name="depto" type="text" id="depto"
                                                           style="font-family: Arial; font-size: 8 pt; " size="80"
                                                           maxlength="100" value="<?php echo $depto ?>"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Sigla:</td>
                                                <td><input name="sigla" type="text" id="sigla"
                                                           style="font-family: Arial; font-size: 8 pt; " size="25"
                                                           maxlength="25" value="<?php echo $sigla ?>"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Ramal:</td>
                                                <td><strong>
                                                        <input name="ramal" type="text" id="ramal" size="4"
                                                               maxlength="4"
                                                               onKeyPress="return txtBoxFormat(this, '9999', event);"
                                                               value="<?php echo $ramal ?>">
                                                        <font color="#FF6600" size="1">Somente
                                                            n&uacute;meros.</font></strong></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Ativo:</td>
                                                <td><input name="ativo" type="radio" value="1" checked>
                                                    Sim
                                                    <input type="radio" name="ativo" value="0">
                                                    N&atilde;o
                                                </td>
                                            </tr>
                                        </table>
                                        <p>&nbsp;</p>
                                        <table width="313" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr align="center">
                                                <td>
                                                    <table width="87" height="25" border="0" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="fncValida();">
                                                                    <div align="center">Confirma</div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table width="87" height="25" border="0" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="document.location.href='cad_deptos.php';">
                                                                    <div align="center">Limpar</div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>

                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p>
                                    <table width="371" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="56" height="20" align="right" valign="middle"><img
                                                    src="../images/bg_list.gif" width="3" height="5">&nbsp;</td>
                                            <td width="315"><strong><a href="#" onClick="fncLista();">Listar
                                                        Todas Unidades</a></strong></td>
                                        </tr>
                                    </table>
                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p></td>
                            </tr>
                            <tr class="Sub_titulo_rel">
                                <td height="15">&nbsp;</td>
                            </tr>
                        </table>
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br>


                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="96%">&nbsp;</td>
                                <td width="4%">
                                    <div align="center"><a href="javascript:history.go(-1)"><img
                                                src="../images/reply-16x16.gif" title="Voltar" width="16"
                                                height="16" border="0"></a></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Op&ccedil;&otilde;es</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='3'>
                                        <tr>
                                            <td width="4%"><img src="../images/morearrow_08c.gif" width="4"
                                                                height="7" border="0"></td>
                                            <td width="96%"><a href="frm_Config.php">Menu
                                                    Configura&ccedil;&otilde;es</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="cad_usuarios.php">Cadastro de usu&aacute;rios</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="frm_cadastro_grades.php">Cadastro de grades</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="frm_Principal.php">In&iacute;cio</a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                    </td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Planejamento
                        estratégico - SED &reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>&nbsp; </p>

<p>&nbsp;</p>


<iframe name="janela" width="800" height="600" style="display:none"></iframe>
</body>
</html>
