<?php
/**
 * Created by PhpStorm.
 * User: TMP-mmendonca
 * Date: 10/03/2015
 * Time: 09:49
 */
//include 'ficha_individual_original.php';
//exit;
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    header("Location: index.php");
    die;
}
require_once("../funcoes/conexao.php");
$usuario = getUsuario($_REQUEST['id_usr'] ? $_REQUEST['id_usr'] : $_SESSION['sessao_id_usr']);
ob_start();

$dataInicio = new DateTime("{$_REQUEST['ano']}-{$_REQUEST['mes']}-01");
$dataFim = new DateTime("{$_REQUEST['ano']}-{$_REQUEST['mes']}-01");
$dataFim->modify("+1 month");
$dataFim->modify("-1 day");

if ($oUsuario->gerente < 1 && $usuario->temOcorrenciasPendentes($dataInicio->format('d/m/Y'), $dataFim->format('d/m/Y'))->ocorrencias) {
    header("Location: ocorrencias_pendentes.php");
}

?>
    <!doctype html>
    <html lang="pt-br">
    <head>
        <style type="text/css">
            body {
                width: 11in;
                height: 8.5in;
                border: 1px outset black;
            }

            .table td {
                font-size: 7pt !important;
                padding: 5pt;
            }

            .table th {
                font-size: 8pt !important;
                padding: 5pt;
            }

            <?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/css/print.css'); ?>
        </style>
        <meta charset="UTF-8">
        <title>Ficha Individual</title>
    </head>
    <body>
    <?php if (getUsuario()->terceirizado) { ?>
        <h4 class="text-center">Controle de Entrada/Saída</h4>
    <?php }else{ ?>
    <h4 class="text-center">Ficha de Frequência</h4>
	<?php } ?>
    <table class="table">
        <tr>
            <th class="col-2">Nome:</th>
            <td class="col-6"><?php echo $usuario->nome; ?> <b><?php echo $usuario->id_usr; ?></b></td>
            <td class="col-4" rowspan="4" style="text-align:right;">
                <img src="<?php echo $_SESSION['config']->pastaFotosUsuarios . $usuario->matricula; ?>.jpg" width="100"
                     height="100">
            </td>
        </tr>
        <tr>
            <th>Cargo:</th>
            <td><?php echo $usuario->cargo; ?></td>
        </tr>
        <tr>
            <th>Lotação</th>
            <td><?php echo $usuario->getDepartamento()->depto; ?></td>
        </tr>
        <tr>
            <th>Grade</th>
            <td><?php echo $usuario->getDescricaoGrade(); ?></td>
        </tr>
        <tr>
            <th>Período</th>
            <td><?php echo getMes($_REQUEST['mes']); ?> de <?php echo $_REQUEST['ano']; ?></td>
        </tr>
    </table>
    <table class="table border1">
        <tr class="text-center">
            <th class="col-1">Dia</th>
            <th class="col-1">Entrada</th>
            <th class="col-1">Saída</th>
            <th class="col-1">Entrada</th>
            <th class="col-1">Saída</th>
            <th class="col-1">Horas</th>
            <th class="col-1">Abonos</th>
            <th class="col-1">Total</th>
            <th class="col-3">Ocorrências</th>
            <th class="col-1">&nbsp;</th>
        </tr>
        <?php
        while ($dataFim >= $dataInicio) {
            $registros = getRegistrosUsuario($usuario->id_usr, $dataInicio->format("d/m/Y"));
            $detalhes = getDetalhesDoDia($usuario->id_usr, $dataInicio->format("d/m/Y"));
            if ($eventos = getEventos($usuario->id_usr, $dataInicio->format("d/m/Y"))) {
                foreach ($eventos as $evento) {
                    ?>
                    <tr class="alert-box warning text-center">
                        <td><?php echo $dataInicio->format("d"); ?></td>
                        <td colspan="9" class="text-center"><span><?php echo $evento->descricao; ?></span></td>
                    </tr>
                <?php
                }
            } else {
                ?>
                <tr class="text-center <?php echo $_c_++ % 2 == 0 ? 'bg-dark' : ''; ?>">
                    <td><?php echo $dataInicio->format("d"); ?></td>
                    <td><?php echo formataHoras($registros[1]); ?></td>
                    <td><?php echo formataHoras($registros[2]); ?></td>
                    <td><?php echo formataHoras($registros[3]); ?></td>
                    <td><?php echo formataHoras($registros[4]); ?></td>
                    <td><?php echo $detalhes['horasTrabalhadas']; ?></td>
                    <td><?php echo $detalhes['horasAbonadas']; ?></td>
                    <td><?php echo $detalhes['totalHoras']; ?></td>
                    <td><?php
                        $count = 0;
                        $virgula = "";
                        $mostrados = [];
                        //$justificativas = getJustificativas($usuario->id_usr, $dataInicio->format("d/m/Y"));
                        foreach ($usuario->getAbonosDoDia($dataInicio->format('d/m/Y'), ['relatorio']) as $abono) {
                            if ($count) {
                                $virgula = ", ";
                            }
                            if (!$mostrados[$abono->id_tipo_justificativa]) {
                                echo $virgula . $abono->getTipoJustificativa()->titulo;
                                $mostrados[$abono->id_tipo_justificativa] = true;
                            }
                            $count++;
                        }
                        ?>
                    </td>
                    <td class="<?php echo $detalhes['status'] == 'devedor' ? 'alert-box error' : ''; ?>"><?php echo $detalhes['apuracao']; ?></td>
                </tr>
            <?php
            }
            $dataInicio->modify("+1 day");
        }
        //reseto as datas para pegar os totais mensais
        $dataInicio = new DateTime("{$_REQUEST['ano']}-{$_REQUEST['mes']}-01");
        $dataFim = new DateTime("{$_REQUEST['ano']}-{$_REQUEST['mes']}-01");
        $dataFim->modify("+1 month");
        $dataFim->modify("-1 day");
        $totalMensal = getTotalMensal($usuario->id_usr, $dataInicio->format("d/m/Y"), $dataFim->format("d/m/Y"));
        ?>
        <tr class=text-right>
            <td colspan="10"><br/>
                <table class="table">
                    <tr>
                        <td>
                            <table>
							 <?php if (getUsuario()->terceirizado) { ?>
							 <tr>
                                    <td style="font-size:1.3em;font-weight: bold;">Horas
                                        trabalhadas: <?php echo $totalMensal['horasTrabalhadasSemAbono']; ?></td>
                                    <td style="font-size:1.3em;font-weight: bold;">Horas justificadas: <?php echo $totalMensal['horasAbonadasSemAtestado']; ?></td>
									<td style="font-size:1.3em;font-weight: bold;">Horas de Atestado: <?php echo getHorasAtestado($usuario->id_usr,$dataFim); ?></td>
                                    <td style="font-size:1.3em;font-weight: bold;">
                                        Total: <?php echo $totalMensal['totalGeralSemAtestado']; ?></td>
                                </tr>
							 <?php }else{ ?>
							 <tr>
                                    <td style="font-size:1.3em;font-weight: bold;">Horas
                                        trabalhadas: <?php echo $totalMensal['horasTrabalhadasSemAbono']; ?></td>
                                    <td style="font-size:1.3em;font-weight: bold;">
                                        Faltas: <?php echo getDescricaoDia($totalMensal['totalFaltas']); ?></td>
                                    <td style="font-size:1.3em;font-weight: bold;">Horas
                                        abonadas: <?php echo $totalMensal['horasAbonadas']; ?></td>
                                    <td style="font-size:1.3em;font-weight: bold;">
                                        Total: <?php echo $totalMensal['totalGeral']; ?></td>
                                </tr>
							 <?php } ?>
                            </table>
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr><?php foreach ($usuario->getResumoJustificativasRelatorio($dataInicio->format('d/m/Y')) as $tipoOcorrencia => $horas) { ?>
                                        <td style="font-size:1.1em;font-weight: bold;"><?php echo $tipoOcorrencia; ?>
                                            : <?php echo $horas; ?></td>
                                    <?php } ?></tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br/>
            </td>
        </tr>
        <tr class="text-right">
            <td colspan="10">
                <div>&nbsp;</div>
                Goiânia, <?php echo date('d'); ?> de <?php echo getMes(date('m')); ?>
                de <?php echo date('Y'); ?>
                <div>&nbsp;</div>
            </td>
        </tr>
        <tr class="text-center">
            <td colspan="10" style="font-weight:bold;">
                O presente documento é de minha responsabilidade, pela qual respondo penal e
                administrativamente.
                <br/><br/><br/><br/>
            </td>
        </tr>
        <tr>
            <td colspan="10">
                <table class=table>
                    <tr class="text-center">
                        <td class="col-5" style="border-top:1px solid black;"><?php echo $usuario->nome; ?></td>
                        <td class="col-2"></td>
                        <td class="col-5" style="border-top:1px solid black;">Atesto e carimbo do chefe imediato</td>
                    </tr>
                </table>
                <br/><br/>
            </td>
        </tr>
    </table>
    </body>
    </html>
<?php
$html = ob_get_clean();
gerarPdf($html, ['orientation' => 'P']);
?>