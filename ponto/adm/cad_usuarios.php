﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 || $_SESSION["sessao_rh"] != 1) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.location.href = 'index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}

require_once("../funcoes/conexao.php");

$id_usr = $_GET["id_usr"];

if ($id_usr == '') {
    $id_usr = 0;
    $nome = '';
    $nome_reduzido = '';
    $login = '';
    $cargo = '';
    $id_depto = 0;
    $id_grade = 0;
    $foto = 0;
    $sigla = '';

    $login = '';
    $email = '';
    $nascimento = '';
    $matricula = '';
    $gerente = 0;
    $ativo = 0;
    $registra = 1;

}

if ($id_usr != '') {

    $sql = "select * from usuarios where id_usr = " . $id_usr;
    $dados = mysql_query($sql, $conexao);
    $resultado = mysql_fetch_array($dados);
    $nome = $resultado[nome];
    $nome_reduzido = $resultado[nome_reduzido];
    $login = $resultado[login];
    $cargo = $resultado[cargo];
    $id_depto = $resultado[id_depto];
    $id_grade = $resultado[id_grade];
    $sigla = $resultado[sigla];

    $login = $resultado[login];
    $email = $resultado[email];
    $nascimento = $resultado[nascimento];
    $matricula = $resultado[matricula];
    $gerente = $resultado[gerente];
    $foto = $resultado[foto];
    $ativo = $resultado[ativo];
    $registra = $resultado[registra];
}
$sql_depto = "select * from depto order by depto";
$dados_depto = mysql_query($sql_depto, $conexao);

$sql_grade = "select * from p_grade order by nome_grade";
$dados_grade = mysql_query($sql_grade, $conexao);

?>


<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>

<script language="JavaScript" type="text/javascript">

    function fncLista() {
        window.open("lista_usuarios.php", "", "");
    }


    function fncValida() {
        if (document.formulario.nome.value == '') {
            alert("Informe o nome deste usuário!");
            document.formulario.nome.focus();
            return false;
        }
        if (document.formulario.login.value == '') {
            alert("Informe o login deste usuário!");
            document.formulario.login.focus();
            return false;
        }
        if (document.formulario.cargo.value == '') {
            alert("Informe o cargo deste usuário!");
            document.formulario.cargo.focus();
            return false;
        }
        if (document.formulario.id_depto.value == 0) {
            alert("Selecione um depto para este usuário!");
            document.formulario.id_depto.focus();
            return false;
        }

        if (document.formulario.email.value != '') {
            if (fncValidaEmail(document.formulario.email.value) == false) {
                alert("É necessário informar um endereço de e-mail válido!");
                document.formulario.email.focus();
                return false;
            }
        }

        if (<?php echo $id_usr;?>==0
    )
        {
            if (document.formulario.senha.value == '') {
                alert("Informe uma senha para acesso nos sistemas da SEDnet e registro de ponto!");
                document.formulario.senha.focus();
                return false;
            }
        }

        document.formulario.submit();
    }
    function fncValidaEmail(vEmail) {
        var vEmailIndex = vEmail.indexOf("@");
        var vDiv = vEmail.split("@");
        var vValAft = vEmail.substring(vEmailIndex + 1, vEmail.length);
        var vDiv2 = vValAft.split(".");
        if ((vEmailIndex == -1) || (vDiv.length < 2) || (vDiv2.length < 2)) {
            //fncEmailInvalido();
            alert("Formato do E-mail inválido!");
            return false;

        }
        for (i = 0; i < vDiv.length; i++)
            if (vDiv[i] == "") {
                alert("Formato do E-mail inválido!");
                return false;
            }

        for (i = 0; i < vDiv2.length; i++)
            if (vDiv2[i] == "") {
                alert("Formato do E-mail inválido!");
                return false;
            }
    }
    function fncColoca() {
        setTimeout("document.formulario.id_depto.value=<?php echo $id_depto?>", 30);
        setTimeout("document.formulario.id_grade.value=<?php echo $id_grade?>", 130);
        setTimeout("fncCheca()", 230);
    }
    function fncCheca() {
        if (<?php echo $gerente;?>==0
    )
        {
            document.formulario.gerente[0].checked = false;
            document.formulario.gerente[1].checked = false;
            document.formulario.gerente[2].checked = true;
        }
        if (<?php echo $gerente;?>==1
    )
        {

            document.formulario.gerente[0].checked = false;
            document.formulario.gerente[1].checked = true;
            document.formulario.gerente[2].checked = false;
        }
        if (<?php echo $gerente;?>==2
    )
        {
            document.formulario.gerente[0].checked = true;
            document.formulario.gerente[1].checked = false;
            document.formulario.gerente[2].checked = false;
        }

        if (<?php echo $ativo;?>==0
    )
        {
            document.formulario.bt_ativo[0].checked = false;
            document.formulario.bt_ativo[1].checked = true;
        }
        if (<?php echo $ativo;?>==1
    )
        {
            document.formulario.bt_ativo[0].checked = true;
            document.formulario.bt_ativo[1].checked = false;
        }

        if (<?php echo $registra;?>==1
    )
        {
            document.formulario.registra[0].checked = true;
            document.formulario.registra[1].checked = false;
        }
        if (<?php echo $registra;?>==0
    )
        {
            document.formulario.registra[0].checked = false;
            document.formulario.registra[1].checked = true;
        }

    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
    }


    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }


        else {
            return true;
        }
    }
</script>

<head>
    <title>Cadastro de Usu&aacute;rios</title>

</head>

<body onLoad="fncColoca();">
<?php require_once("frm_topo.php"); ?>




<br>

<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><BR><strong><font
                                    color="#333333" size="4">
                                </font></strong>

                        <p><strong><font color="#333333" size="4">&nbsp; Cadastro de Usu&aacute;rios</font></strong></p>
                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" align="center" valign="top" bgcolor="#FFFFFF"><br>
                        <table width="790" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
                            <tr valign="bottom">
                                <td height="25" class="Titulo_rel">
                                    <table width="790" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td width="194"><strong><font style="font-size:10px" color="#333333">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>Dados do
                                                    usu&aacute;rio:</strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="Itens_rel_s_traco">
                                <td height="15">
                                    <form action="grava_usuario.php" method="post" name="formulario" id="formulario"
                                          enctype="multipart/form-data" target="janela">
                                        <table width="732" border="0" align="center" cellpadding="4" cellspacing="0">
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td>
                                                    <?php if ($foto == 1) { ?>
                                                        <img src="../fotos/<?php echo $id_usr; ?>_usr.jpg" width="138"
                                                             height="183">
                                                    <?php } ?>
                                                </td>
                                                <td><input name="id_usr" type="text" id="id_usr"
                                                           value="<?php echo $id_usr ?>" style="display:none"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td>Nome:</td>
                                                <td><input name="nome" type="text" id="nome"
                                                           style="font-family: Arial; font-size: 8 pt; " size="80"
                                                           maxlength="100" value="<?php echo $nome ?>"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Nome reduzido intranet:</td>
                                                <td><input name="nome_reduzido" type="text" id="nome_reduzido"
                                                           style="font-family: Arial; font-size: 8 pt; " size="25"
                                                           maxlength="25" value="<?php echo $nome_reduzido ?>"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Matr&iacute;cula</td>
                                                <td><input name="matricula" type="text" id="matricula"
                                                           style="font-family: Arial; font-size: 8 pt; " size="15"
                                                           maxlength="15" value="<?php echo $matricula ?>"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Login:</td>
                                                <td><input name="login" type="text" id="login"
                                                           style="font-family: Arial; font-size: 8 pt; " size="50"
                                                           maxlength="50" value="<?php echo $login ?>"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Cargo:</td>
                                                <td><input name="cargo" type="text" id="cargo"
                                                           style="font-family: Arial; font-size: 8 pt; " size="80"
                                                           maxlength="100" value="<?php echo $cargo ?>"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Departamento:</td>
                                                <td><select name="id_depto" id="id_depto">
                                                        <option value="0">Selecione</option>
                                                        <?php while ($resultado_depto = mysql_fetch_array($dados_depto)) { ?>
                                                            <option
                                                                value="<?php echo $resultado_depto[id_depto]; ?>"><?php echo $resultado_depto[depto]; ?></option>
                                                        <?php } ?>
                                                    </select> &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">N&iacute;vel:</td>
                                                <td>
                                                    <input name="gerente" type="radio" value="2" id="g2">
                                                    <label for="g2">Superintendente</label>

                                                    <input type="radio" name="gerente" value="1" id="g1">
                                                    <label for="g1">Gerente</label>

                                                    <input name="gerente" type="radio" value="0" checked id="g0">
                                                    <label for="g0">Funcion&aacute;rio</label></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Dia/Mes nasc:</td>
                                                <td><strong>
                                                        <input name="nascimento" type="text" id="nascimento" size="5"
                                                               maxlength="5"
                                                               onKeyPress="return txtBoxFormat(this, '99/99', event);"
                                                               value="<?php echo $nascimento ?>">
                                                        <font color="#FF6600" size="1">(dia e m&ecirc;s) - Formato:
                                                            dd/mm.</font></strong></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Email:</td>
                                                <td><input name="email" type="text" id="email"
                                                           style="font-family: Arial; font-size: 8 pt; " size="80"
                                                           maxlength="100" value="<?php echo $email ?>"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Grade de ponto:</td>
                                                <td><select name="id_grade" id="id_grade">
                                                        <option value="0">Sem grade</option>
                                                        <?php while ($resultado_grade = mysql_fetch_array($dados_grade)) { ?>
                                                            <option
                                                                value="<?php echo $resultado_grade[id_grade]; ?>"><?php echo $resultado_grade[nome_grade]; ?></option>
                                                        <?php } ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Registra Ponto?</td>
                                                <td><input name="registra" type="radio" id="registra_1" value="1"
                                                           checked>
                                                    <label for="registra_1">Sim</label>
                                                    <input name="registra" type="radio" value="0" id="registra_2">
                                                    <label for="registra_2">N&atilde;o</label></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Ativo:</td>
                                                <td><input name="bt_ativo" type="radio" id="bt_ativo1" value="1"
                                                           checked>
                                                    <label for="bt_ativo1">Sim</label> <input name="bt_ativo"
                                                                                              type="radio" value="0"
                                                                                              id="bt_ativo2">
                                                    <label for="bt_ativo2">N&atilde;o</label></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Senha:</td>
                                                <td><strong>
                                                        <input name="senha" type="text" id="senha" size="10"
                                                               maxlength="10">
                                                    </strong></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Foto p/ intranet:</td>
                                                <td><input name="arquivo" type="file" id="arquivo" size="50"></td>
                                            </tr>
                                        </table>
                                        <p>&nbsp;</p>
                                        <table width="313" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr align="center">
                                                <td>
                                                    <table width="87" height="25" border="0" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="fncValida();">
                                                                    <div align="center">Confirma</div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table width="87" height="25" border="0" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="document.location.href='cad_usuarios.php;">
                                                                    <div align="center">Limpar</div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>

                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p>
                                    <table width="371" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="56" height="20" align="right" valign="middle"><img
                                                    src="../images/bg_list.gif" width="3" height="5">&nbsp;</td>
                                            <td width="315"><strong><a href="#" onClick="fncLista();">Listar
                                                        todos Usu&aacute;rios</a></strong></td>
                                        </tr>
                                    </table>
                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p></td>
                            </tr>
                            <tr class="Sub_titulo_rel">
                                <td height="15">&nbsp;</td>
                            </tr>
                        </table>
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br>


                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="96%">&nbsp;</td>
                                <td width="4%">
                                    <div align="center"><a href="javascript:history.go(-1)"><img
                                                src="../images/reply-16x16.gif" title="Voltar" width="16"
                                                height="16" border="0"></a></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Op&ccedil;&otilde;es</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='3'>
                                        <tr>
                                            <td width="4%"><img src="../images/morearrow_08c.gif" width="4"
                                                                height="7" border="0"></td>
                                            <td width="96%"><a href="frm_Config.php">Menu
                                                    Configura&ccedil;&otilde;es</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="cad_deptos.php">Cadastro de Unidades</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="frm_cadastro_grades.php">Cadastro de grades</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="frm_Principal.php">In&iacute;cio</a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                    </td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?=date('Y')?> - SED&reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>&nbsp; </p>

<p>&nbsp;</p>


<iframe name="janela" width="800" height="600" style="display:none"></iframe>
</body>
</html>
