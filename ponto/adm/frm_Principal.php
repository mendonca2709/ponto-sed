﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == '') {
    ?>
    <script language="JavaScript">
        alert("Sua sessão expirou!\n\nLogue no sistema novamente!");
        window.location.href = 'http://<?php echo $_SERVER['HTTP_HOST'];?>/index.php';
    </script>
    <?php
    header("Location: index.php");
    die;
}
require_once("../funcoes/conexao.php");

$mes = date('m');
$ano = date('Y');
$id_usr = $_SESSION["sessao_id_usr"];
$sql_lista = "select count(id_justificativa) as qtd_total_justificativa from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto ";//where month(data_justificativa) = ".$mes." and year(data_justificativa) = ".$ano;
if ($_SESSION["sessao_rh"] == 0) {
    $sql_lista = $sql_lista . " and usuarios.id_depto = " . $_SESSION["sessao_id_depto"];
}
$dados_lista = mysql_query($sql_lista, $conexao);
$resultado_lista = mysql_fetch_array($dados_lista);
$qtd_total_justificativa = $resultado_lista[qtd_total_justificativa];
if(false) {
    $sql_lista1 = "select count(id_justificativa) as qtd_total_justificativa1 from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where abonado = 1";//where month(data_justificativa) = ".$mes." and year(data_justificativa) = ".$ano." and abonado = 1 ";
    if ($_SESSION["sessao_rh"] == 0) {
        $sql_lista1 = $sql_lista1 . " and usuarios.id_depto = " . $_SESSION["sessao_id_depto"];
    }

    $dados_lista1 = mysql_query($sql_lista1, $conexao);
    $resultado_lista1 = mysql_fetch_array($dados_lista1);
    $qtd_total_justificativa1 = $resultado_lista1[qtd_total_justificativa1];

    $sql_lista2 = "select count(id_justificativa) as qtd_total_justificativa2 from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto where abonado = 2 ";
    if ($_SESSION["sessao_rh"] == 0) {
        $sql_lista2 = $sql_lista2 . " and usuarios.id_depto = " . $_SESSION["sessao_id_depto"];
    }

    $dados_lista2 = mysql_query($sql_lista2, $conexao);
    $resultado_lista2 = mysql_fetch_array($dados_lista2);
    $qtd_total_justificativa2 = $resultado_lista2[qtd_total_justificativa2];

    $sql_processos4 = "select depto.id_depto,depto.sigla,(count(id_justificativa)*100)/(select count(id_usr) from usuarios where id_depto = depto.id_depto) as percentual from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on depto.id_depto = usuarios.id_depto group by usuarios.id_depto order by percentual desc limit 0,10";
    $sql_processos4 = "select depto.id_depto,depto.sigla,count(id_justificativa) as total_just,(select count(id_usr) from usuarios where id_depto = depto.id_depto) as percentual from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on depto.id_depto = usuarios.id_depto group by usuarios.id_depto order by percentual desc ";
    $dados_processos4 = mysql_query($sql_processos4, $conexao);


    if ($_SESSION["usr_gerente"] < 2) {
        $sql_10 = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 10 and id_usr = " . $id_usr;
        $dados_10 = mysql_query($sql_10, $conexao);
        $resultado_10 = mysql_fetch_array($dados_10);

        $sql_11 = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 11 and id_usr = " . $id_usr;
        $dados_11 = mysql_query($sql_11, $conexao);
        $resultado_11 = mysql_fetch_array($dados_11);

        $sql_12 = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 12 and id_usr = " . $id_usr;
        $dados_12 = mysql_query($sql_12, $conexao);
        $resultado_12 = mysql_fetch_array($dados_12);


        $sql_10a = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 10 and abonado = 1 and id_usr = " . $id_usr;
        $dados_10a = mysql_query($sql_10a, $conexao);
        $resultado_10a = mysql_fetch_array($dados_10a);

        $sql_11a = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 11 and abonado = 1 and id_usr = " . $id_usr;
        $dados_11a = mysql_query($sql_11a, $conexao);
        $resultado_11a = mysql_fetch_array($dados_11a);

        $sql_12a = "select count(id_justificativa) as qtd_total from p_justificativa where month(data_justificativa) = 12 and abonado = 1 and id_usr = " . $id_usr;
        $dados_12a = mysql_query($sql_12a, $conexao);
        $resultado_12a = mysql_fetch_array($dados_12a);


        $sql_contador0 = "select count(id_justificativa) as qtd_total from p_justificativa where id_usr = " . $id_usr;
        $dados_contador0 = mysql_query($sql_contador0, $conexao);
        $resultado_contador0 = mysql_fetch_array($dados_contador0);

        $sql_contador1 = "select count(id_justificativa) as qtd_total from p_justificativa where abonado = 1 and id_usr = " . $id_usr;
        $dados_contador1 = mysql_query($sql_contador1, $conexao);
        $resultado_contador1 = mysql_fetch_array($dados_contador1);

        $sql_contador2 = "select count(p_justificativa.id_tipo_justificativa) as qtd_total, titulo from p_justificativa inner join p_tipo_justificativa on p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa  where id_usr = " . $id_usr . " group by p_justificativa.id_tipo_justificativa order by qtd_total desc limit 0,1";
        $dados_contador2 = mysql_query($sql_contador2, $conexao);
        $resultado_contador2 = mysql_fetch_array($dados_contador2);


        $sql_tipo_grade = "select tipo_grade from p_grade where id_grade = " . $_SESSION["sessao_id_grade"];
        $dados_tipo_grade = mysql_query($sql_tipo_grade, $conexao);
        $resultado_tipo_grade = mysql_fetch_array($dados_tipo_grade);
        $data_registro = $ano . "/" . $mes . "/" . $dia;

        $sql_consulta = "select * from usuarios inner join depto on usuarios.id_depto = depto.id_depto where usuarios.id_usr = " . $id_usr;

        $dados_consulta = mysql_query($sql_consulta, $conexao);
        $resultado_consulta = mysql_fetch_array($dados_consulta);
        $nome = explode(' ', $resultado_consulta[nome]);
        $foto = $resultado_consulta[foto];
        $id_depto = $resultado_consulta[id_depto];


        $sql_consulta_grade = "select * from p_grade where id_grade = " . $resultado_consulta[id_grade];
        $dados_consulta_grade = mysql_query($sql_consulta_grade, $conexao);
        $resultado_consulta_grade = mysql_fetch_array($dados_consulta_grade);
        $entrada_1 = $resultado_consulta_grade[entrada_1];
        $saida_1 = $resultado_consulta_grade[saida_1];
        $entrada_2 = $resultado_consulta_grade[entrada_2];
        $saida_2 = $resultado_consulta_grade[saida_2];


    }
}

?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
<html>
<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap-3.3.2-dist/css/bootstrap.min.css">
<!--<script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
<script type="text/javascript">
    //    google.load("visualization", "1", {packages: ["corechart"]});
    //    google.setOnLoadCallback(drawChart);
    //    function drawChart() {
    //        var data = google.visualization.arrayToDataTable([
    //            ['Mes', 'Justificativas', 'Abonos'],
    //            ['Outubro', 10, 8],
    //            ['Novembro', 15, 14],
    //            ['Dezembro', 13, 13]
    //
    ////		   ['Outubro',  <?php //echo $resultado_10[qtd_total]?>//, <?php //echo $resultado_10a[qtd_total]?>//],
    ////          ['Novembro',  <?php //echo $resultado_11[qtd_total]?>//, <?php //echo $resultado_11a[qtd_total]?>//],
    ////          ['Dezembro',  <?php //echo $resultado_11[qtd_total]?>//, <?php //echo $resultado_12a[qtd_total]?>//]
    //
    //        ]);
    //
    //        var options = {
    //            title: 'Gráfico de acompanhamento do funcionário',
    //            hAxis: {title: '', titleTextStyle: {color: 'red'}}
    //        };
    //
    //        var chart = new google.visualization.AreaChart(document.getElementById('chart_div2'));
    //        chart.draw(data, options);
    //    }
</script>

<script type="text/javascript">
//    google.load("visualization", "1", {packages: ["corechart"]});
//    google.setOnLoadCallback(drawChart);
//
//    function drawChart() {
//        var data = google.visualization.arrayToDataTable([
//            ['Ocorrências', 'Quantitativo'],
//            <?php //while($resultado_processos4 = mysql_fetch_array($dados_processos4)){
//            echo "['".$resultado_processos4[sigla]."',".number_format($resultado_processos4[total_just],0,",",".")."],";
//            } ?>
//        ]);
//
//        var options = {
//            title: 'Gráfico de Ocorrências (% do quantitativo em relação ao total)',
//            is3D: true
//        };
//
//        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
//        chart.draw(data, options);
//    }

</script>


<script type="text/javascript">
//    google.load("visualization", "1", {packages: ["corechart"]});
//    google.setOnLoadCallback(drawChart);
//    function drawChart() {
//        var data = google.visualization.arrayToDataTable([
//            ['Mes', 'Justificativas', 'Abonos'],
//            ['Outubro', 10, 8],
//            ['Novembro', 15, 14],
//            ['Dezembro', 13, 13]
//
////		   ['Outubro',  <?php //echo $resultado_10[qtd_total]?>//, <?php //echo $resultado_10a[qtd_total]?>//],
////          ['Novembro',  <?php //echo $resultado_11[qtd_total]?>//, <?php //echo $resultado_11a[qtd_total]?>//],
////          ['Dezembro',  <?php //echo $resultado_11[qtd_total]?>//, <?php //echo $resultado_12a[qtd_total]?>//]
//
//        ]);
//
//        var options = {
//            title: 'Gráfico de acompanhamento do funcionário',
//            hAxis: {title: '', titleTextStyle: {color: 'red'}}
//        };
//
//        var chart = new google.visualization.AreaChart(document.getElementById('chart_div3'));
//        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
//    google.load("visualization", "1", {packages: ["corechart"]});
//    google.setOnLoadCallback(drawChart);
//
//    function drawChart() {
//        var data = google.visualization.arrayToDataTable([
//            ['Ocorrências', 'Quantitativo'],
//
//            ['Abonadas', <?php //echo $qtd_total_justificativa1;?>//],
//            ['Aguardando', <?php //echo $qtd_total_justificativa-$qtd_total_justificativa1;?>//],
//        ]);
//
//        var options = {
//            // title: 'Justificativas deste mês',
//            is3D: true
//        };
//
//        var chart = new google.visualization.PieChart(document.getElementById('chart_div3'));
//        chart.draw(data, options);
//    }

</script>


<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_preloadImages() { //v3.0
        var d = document;
        if (d.images) {
            if (!d.MM_p) d.MM_p = new Array();
            var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
            for (i = 0; i < a.length; i++)
                if (a[i].indexOf("#") != 0) {
                    d.MM_p[j] = new Image;
                    d.MM_p[j++].src = a[i];
                }
        }
    }
    function abre_deptos(id_depto) {
        document.abre_lista.id_depto.value = id_depto;
        document.abre_lista.submit();

    }
    function fncPainel(tipo) {
        document.abre_janela.tipo_ocorrencia.value = tipo;
        document.abre_janela.enviados.value = tipo;
        document.abre_janela.submit();
    }
    //-->
</script>
<head>

    <title>Principal</title>
</head>


<body>
<meta name="title" content="SED - SED"/>
<meta name="url" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>"/>
<meta name="description"
      content="SED-GO - A SED de Goiás implementou em um projeto altamente inovador de acompanhamento de Processos totalmente digital."/>
<meta name="keywords"
      content="Sistema de Ponto Eletrônico,sistema,ouvidoria,corregedoria,controladoria,processos,protocolo,jedi,workflow,processo eletronico,gerencial,manifestação,acompanhamento,encaminhamento,controle,pesquisa,histórico,governo de goiás,sistema de acompanhamento de processos,sistema gerencial,marcelo roncato"/>

<meta name="autor" content="Marcelo Roncato"/>
<meta name="company" content="SED"/>
<meta name="revisit-after" content="5"/>
<script language="JavaScript" type="text/javascript">

    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
//window.location.href="frm_Lista_processos.php?texto_busca="+document.form_busca.texto_busca.value;
    }


    function fncFechaMensagem() {
        mensagem.style.display = 'none';
    }

    function fncOpcoes2() {
        status_opcoes.style.display = '';
    }
    function Fechar() {
        status_opcoes.style.display = 'none';
    }
    function Abre2(id_processo) {
        window.open("processo.php?id_processo=" + id_processo, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=1070,height=700,top=10,left=20");
    }
    function fncEnter() {
        if (window.event.keyCode == 13) {
            fncBusca();
        }
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
//document.form_busca.id_gerencia.value = document.lista.id_gerencia.value;
//document.form_busca.selMES.value = document.lista.selMES.value;
//document.form_busca.selANO.value = document.lista.selANO.value;


        document.form_busca.submit();
//window.location.href="frm_Lista_processos.php?texto_busca="+document.form_busca.texto_busca.value;

    }

</script>
<?php require_once("frm_topo.php"); ?>

<br>
<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg">
                        <table width="500" border="0" cellpadding="3" cellspacing="0">
                            <tr>
                                <td height="10" colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="59">
                                    <div align="right"><strong><font size="2"><font color="#333333"><font
                                                        color="#FF6600"><img src="../images/info3.png" width="45"
                                                                             height="45"></font></font></font></strong>
                                    </div>
                                </td>
                                <td width="441"><strong><font color="#666666" size="4">Resumo</font><br>
                                        <font color="#666666" size="1"> </font></strong></td>
                            </tr>
                        </table>
                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" valign="top" bgcolor="#FFFFFF"><BR>
                        <table width="768" border="0" align="center" cellpadding="2" cellspacing="0">
                            <tr>
                                <td width="16">&nbsp;</td>
                                <td width="87">&nbsp;</td>
                                <td width="83">&nbsp;</td>
                                <td width="236">&nbsp;</td>
                                <td width="19">&nbsp;</td>
                                <td width="303">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right"><img src="../images/read-16x16.gif" width="16" height="16"></td>
                                <td><strong><font color="#666666">&Uacute;ltimo login:</font></strong></td>
                                <td colspan="2"><strong><font color="#FF6600">
                                            <?php

                                            $hoje = getdate();

                                            switch ($hoje['wday']) {
                                                case 0:
                                                    echo "Domingo, ";
                                                    break;
                                                case 1:
                                                    echo "Segunda-Feira, ";
                                                    break;
                                                case 2:
                                                    echo "Terça-Feira, ";
                                                    break;
                                                case 3:
                                                    echo "Quarta-Feira, ";
                                                    break;
                                                case 4:
                                                    echo "Quinta-Feira, ";
                                                    break;
                                                case 5:
                                                    echo "Sexta-Feira, ";
                                                    break;
                                                case 6:
                                                    echo "Sábado, ";
                                                    break;
                                            }

                                            echo $hoje['mday'];
                                            switch ($hoje['mon']) {
                                                case 1:
                                                    echo " de Janeiro de ";
                                                    break;
                                                case 2:
                                                    echo " de Fevereiro de ";
                                                    break;
                                                case 3:
                                                    echo " de Março de ";
                                                    break;
                                                case 4:
                                                    echo " de Abril de ";
                                                    break;
                                                case 5:
                                                    echo " de Maio de ";
                                                    break;
                                                case 6:
                                                    echo " de Junho de ";
                                                    break;
                                                case 7:
                                                    echo " de Julho de ";
                                                    break;
                                                case 8:
                                                    echo " de Agosto de ";
                                                    break;
                                                case 9:
                                                    echo " de Setembro de ";
                                                    break;
                                                case 10:
                                                    echo " de Outubro de ";
                                                    break;
                                                case 11:
                                                    echo " de Novembro de ";
                                                    break;
                                                case 12:
                                                    echo " de Dezembro de ";
                                                    break;
                                            }

                                            echo $hoje['year'] . ".";
                                            ?>
                                        </font></strong></td>
                                <td>
                                    <div align="right"></div>
                                </td>
                                <td><font color="#666666">&nbsp;</font></td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td colspan="5" rowspan="3" valign="top">
                                    <?php if (false && $_SESSION["usr_gerente"] >= 1) { ?>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="1%"><strong><img src="../images/bg_list.gif" width="3"
                                                                            height="5"></strong></td>
                                                <td width="27%"><strong><font color="#666666"> Total de
                                                            Justificativas:</font></strong></td>
                                                <td width="23%"><font
                                                        color="#333333"><strong><?php echo number_format($qtd_total_justificativa, 0, ',', '.'); ?></strong></font>
                                                </td>
                                                <?php if ($_SESSION["sessao_tipo"] != 0) { ?>
                                                    <td width="1%"><font color="#666666">&nbsp;</font></td>
                                                    <td width="22%"><font color="#666666">&nbsp;</font></td>
                                                    <td width="26%"><font color="#FF3300">&nbsp;</font></td>
                                                <?php } ?>
                                            </tr>
                                            <tr>
                                                <td><strong><img src="../images/bg_list.gif" width="3"
                                                                 height="5"></strong></td>
                                                <td><strong><font color="#666666"> Justificativas abonadas:
                                                        </font></strong></td>
                                                <td><font color="#FF3300"><strong><a href="#"
                                                                                     onClick="fncPainel(1);"><font
                                                                    color="#0033FF"><?php echo number_format($qtd_total_justificativa1, 0, ',', '.'); ?></font></a></strong></font><font
                                                        color="#0033FF">&nbsp;</font></td>
                                                <?php if ($_SESSION["sessao_tipo"] != 0) { ?>
                                                    <td><font color="#666666">&nbsp;</font></td>
                                                    <td><font color="#666666">&nbsp;</font></td>
                                                    <td><font color="#FF3300">&nbsp;</font></td>
                                                <?php } ?>
                                            </tr>
                                            <tr>
                                                <td><strong><img src="../images/bg_list.gif" width="3"
                                                                 height="5"></strong></td>
                                                <td><strong><font color="#666666">Justificativas aguardando:
                                                        </font></strong></td>
                                                <td><strong><a href="#" onClick="fncPainel(0);"><font
                                                                color="#FF0000"><?php echo number_format($qtd_total_justificativa - $qtd_total_justificativa1, 0, ',', '.'); ?></font></a></strong>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><strong><img src="../images/bg_list.gif" width="3"
                                                                 height="5"></strong></td>
                                                <td><strong><font color="#666666">Justificativas N&atilde;o
                                                            abonadas: </font></strong></td>
                                                <td><font color="#FF3300"><strong><a href="#"
                                                                                     onClick="fncPainel(2);"><?php echo number_format($qtd_total_justificativa2, 0, ',', '.'); ?></a></strong></font>
                                                </td>
                                                <?php if ($_SESSION["sessao_tipo"] != 0) { ?>
                                                    <td><font color="#666666">&nbsp;</font></td>
                                                    <td><font color="#666666">&nbsp;</font></td>
                                                    <td><font color="#FF3300">&nbsp;</font></td>
                                                <?php } ?>
                                            </tr>
                                        </table>
                                        <div id="chart_div3" style="width: 620px; height: 300px;"></div>
                                    <?php } ?>
                                    <strong></strong></td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right" valign="top">&nbsp;</td>
                            </tr>
                        </table>
                        <?php if (true || $_SESSION["usr_gerente"] < 2 && $_SESSION["sessao_rh"] == 0) { ?>
                            <div>
                                <?php
                                $grade = $oUsuario->getGrade();
                                $detalhes = getDetalhesDoDia($oUsuario->id_usr, date('d/m/Y'));
                                ?>
                                <table class="table table-condesned table-striped table-bordered" style="width:100%;">
                                    <tr>
                                        <th>Registro</th>
                                        <th>Hora Prevista</th>
                                        <th>Hora Registrada</th>
                                    </tr>
                                    <?php if ($grade->entrada_1) {
                                        $registro = getRegistroEtapa($oUsuario->id_usr, date('d/m/Y'), 1);
                                        ?>
                                        <tr>
                                            <td>1</td>
                                            <td><?php echo getHoraEtapaUsuario($oUsuario->id_usr, 1); ?></td>
                                            <td><?php echo formataHoras($registro, false); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($grade->saida_1) {
                                        $registro = getRegistroEtapa($oUsuario->id_usr, date('d/m/Y'), 2); ?>
                                        <tr>
                                            <td>2</td>
                                            <td><?php echo getHoraEtapaUsuario($oUsuario->id_usr, 2); ?></td>
                                            <td><?php echo formataHoras($registro, false); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($grade->entrada_2) {
                                        $registro = getRegistroEtapa($oUsuario->id_usr, date('d/m/Y'), 3); ?>
                                        <tr>
                                            <td>3</td>
                                            <td><?php echo getHoraEtapaUsuario($oUsuario->id_usr, 3); ?></td>
                                            <td><?php echo formataHoras($registro, false); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($grade->saida_2) {
                                        $registro = getRegistroEtapa($oUsuario->id_usr, date('d/m/Y'), 4); ?>
                                        <tr>
                                            <td>4</td>
                                            <td><?php echo getHoraEtapaUsuario($oUsuario->id_usr, 4); ?></td>
                                            <td><?php echo formataHoras($registro, false); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="3"><h3 style="text-align: right;">Horas
                                                Trabalhadas: <?php echo $detalhes['horasAtuais']; ?></h3>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <table width="700" border="0" align="center" cellpadding="0" cellspacing="0"
                                   style="display:none;">
                                <tr>
                                    <td width="174" align="center">
                                        <div align="center"><strong><font color="#333333">
                                                    <?php if ($foto == 1){ ?>
                                                </font></strong>

                                            <div align="center"><img src="../fotos/<?php echo $id_usr . "_usr.jpg" ?>"
                                                                     height="160">
                                                <?php } ?>
                                                <?php if ($foto == 0) { ?>
                                                    <img src="../images/nouser.jpg" width="150" height="150"><br>
                                                    <strong>Sem Foto</strong>
                                                <?php } ?>
                                            </div>
                                            <strong><font color="#333333"> </font></strong></div>
                                    </td>
                                    <td width="152" valign="top"><strong><br>
                                        </strong>
                                        <table width="89%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td colspan="2" align="center"><strong><font color="#0066FF" size="2">
                                                            <?php echo $nome[0] . " " . $nome[count($nome) - 1]; ?></font><font
                                                            color="#0066FF"><br>
                                                            <font
                                                                color="#FF6600"><?php echo $resultado_consulta[sigla]; ?>
                                                            </font></font></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2" align="center"><font color="#666666">___________________</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="25" colspan="2" align="center"><strong>Sua Grade:</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center"><strong><font color="#333333">
                                                            <?php
                                                            echo $entrada_1 . "<br>";
                                                            echo $saida_1 . "<br>";
                                                            if ($resultado_consulta_grade[tipo_grade] == 0) {
                                                                echo $entrada_2 . "<br>";
                                                                echo $saida_2 . "<br>";
                                                            }; ?>
                                                        </font></strong></td>
                                            </tr>
                                        </table>
                                        <strong><br>
                                            <font color="#333333"></font></strong></td>
                                    <td width="374" valign="top">
                                        <div align="center">
                                            <table width='314' border='0' align='right' cellpadding='0' cellspacing='0'>
                                                <tr>
                                                    <td>
                                                        <DIV class="commit commit-tease js-details-container"><font
                                                                color="#000033"><strong><font size="2">Resumo
                                                                        anual deste funcion&aacute;rio</font></strong>
                                                                <br>
                                                                <br>
                                                            </font>

                                                            <DIV class="commit-meta">
                                                                <table width='100%' border='0' cellspacing='0'
                                                                       cellpadding='2'>
                                                                    <tr>
                                                                        <td width="4%" align="center"><strong><font
                                                                                    color="#666666">Justificativas:</font></strong>
                                                                        </td>
                                                                        <td width="96%"><strong><font
                                                                                    color="#FF0000"><?php echo $resultado_contador0[qtd_total] ?></font>
                                                                            </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><strong><font color="#666666">Abonos:</font></strong>
                                                                        </td>
                                                                        <td><strong><font
                                                                                    color="#FF0000"><?php echo $resultado_contador1[qtd_total] ?></font></strong>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"><font color="#666666"><strong>Maior
                                                                                    incid&ecirc;ncia: </strong></font>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"><strong><font
                                                                                    color="#FF0000"><?php echo $resultado_contador2[titulo] ?></font></strong>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div id="chart_div2" style="width: 820px; height: 340px;"></div>
                        <?php } ?>

                        <?php if ($_SESSION["sessao_rh"] == 1 || $_SESSION["usr_gerente"] == 2) { ?>
                            <div id="chart_div" style="width: 820px; height: 950px;"></div><?php } ?>

                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Busca</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <form name="form_busca" method="post" action="frm_Lista_ocorrencias.php">
                                        <font color="#FF6600" size="1"><strong>Informe texto de
                                                busca:</strong></font><br>
                                        <font color="#FF6600" size="1"><strong>

                                                <input name="texto" type="text" id="texto" size="3"
                                                       value="<?php echo $texto; ?>" style="display:none">
                                            </strong></font>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><input type="text" name="texto_busca" style="width:130px"
                                                           value="<?php echo $texto_busca; ?>" onKeyPress="fncEnter();">
                                                </td>
                                                <td><a href="#" onClick="fncBusca();"><img src="../images/ok_bt.gif"
                                                                                           width="27" height="15"
                                                                                           border="0"></a></td>
                                            </tr>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                            <br>
                            <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                                <tr>
                                    <td class='Titulo_caixa'> Ranking por propo&ccedil;&atilde;o (%)</td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" class='Corpo_caixa'>
                                        <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                                            <tr>
                                                <td colspan="2"><p><font color="#FF6600"><strong>10
                                                                Primeiros</strong></font></p>
                                                </td>
                                            </tr>
                                            <?php

                                            //$sql_processos2 = "select count(id_usr) as total,sigla,depto.id_depto from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto group by depto.id_depto order by total desc limit 0,10";
                                            //$dados_processos2 = mysql_query($sql_processos2, $conexao);

                                            //$sql_processos2 = "select count(id_justificativa) as total,sigla,depto.id_depto from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on usuarios.id_depto = depto.id_depto group by depto.id_depto order by total desc limit 0,10";
                                            $sql_processos2 = "select depto.id_depto,depto.sigla,depto.depto,(count(id_justificativa)*100)/(select count(id_usr) from usuarios where id_depto = depto.id_depto) as percentual from p_justificativa inner join usuarios on p_justificativa.id_usr = usuarios.id_usr inner join depto on depto.id_depto = usuarios.id_depto group by usuarios.id_depto order by percentual desc limit 0,10";
                                            $dados_processos2 = mysql_query($sql_processos2, $conexao);
                                            while ($resultado_processos2 = mysql_fetch_array($dados_processos2)) {
                                                ?>
                                                <tr>
                                                    <td width="4%" align="center"><img src="../images/check.jpg"
                                                                                       width="15" height="17"></td>
                                                    <td width="96%" class="Itens_normal">
                                                        <div style="cursor:pointer;"
                                                             onClick="abre_deptos(<?php echo $resultado_processos2[id_depto] ?>);"
                                                             id="<?php echo $resultado_processos2[id_depto] ?>"
                                                             title="<?php echo $resultado_processos2[depto] ?>">
                                                            <p><strong><font
                                                                        color="#009933"> <?php echo $resultado_processos2[sigla] . " - " . number_format($resultado_processos2[percentual], 0, ",", ".") . "%"; ?></font></strong>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr align="right">
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        <?php } ?>
                        <br>
                        &nbsp; &nbsp;
                        <?php if ($_SESSION["sessao_rh"] == 1) { ?>
                            <div align="center"><a href="lista_online.php"><img src="../images/icone4.png"
                                                                                width="110" height="110" border="0"></a><br>
                                Lista on-line
                            </div>
                        <?php } ?>
                        &nbsp;
                        <?php if ($_SESSION["usr_gerente"] >= 1) { ?>
                            <div align="center"><a href="lista_chamada.php"><img src="../images/lists.png"
                                                                                 width="110" height="110"
                                                                                 border="0"></a><br>
                                &nbsp;&nbsp;&nbsp;Lista Verifica&ccedil;&atilde;o
                            </div>
                        <?php } ?>
                        <br>
                        <br>

                        <p align="center"><a href="../ajuda/" target="_blank"><img
                                    src="../images/respostas.png" alt="Manual do Sistema"
                                    width="87" height="62" border="0" class="icon"></a></p>

                        <p align="center"><a href="../ajuda" target="_blank">Manual
                                do Sistema</a></p></td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?= date('Y') ?> - SED&reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<form name="abre_janela" method="post" action="frm_Lista_ocorrencias.php">
    <input type="hidden" name="tipo_ocorrencia">
    <input type="hidden" name="enviados">

</form>
<form name="abre_lista" method="post" action="frm_Lista_ocorrencias.php">
    <input type="hidden" name="id_depto">
    <input type="hidden" name="enviados" value="0">
    <input type="hidden" name="tipo_ocorrencia" value="0">


</form>

<?php
mysql_close($conexao);
?>
</body>
</html>
