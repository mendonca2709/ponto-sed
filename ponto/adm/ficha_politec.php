﻿<?php
require_once("../funcoes/conexao.php");

$id_usr = 147;
$mes_atual = 8;
$mes = $mes_atual - 1;
$ano = '2013';
$sql = "select * from p_registro where id_usr = " . $id_usr;
$dados = mysql_query($sql, $conexao);
$resultado = mysql_fetch_array($dados);

//$sql_horas_aguardando	= "select qtd_horas from p_justificativa where id_usr = ".$id_usr." and month(data_justificativa) = ".$mes_p." and year(data_justificativa) = ".$ano_p." and abonado = 0";
//$dados_horas_aguardando = mysql_query($sql_horas_aguardando, $conexao); 
//while($resultado_horas_aguardando = mysql_fetch_array($dados_horas_aguardando)) { 


?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>


<head>
    <title>Ficha individual de presen&ccedil;a</title>

</head>

<meta name="title" content="SED - SED"/>
<meta name="url" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>"/>
<meta name="description"
      content="SED-GO - A SED de Goiás implementou em um projeto altamente inovador de acompanhamento de Processos totalmente digital."/>
<meta name="keywords"
      content="sistema,ouvidoria,corregedoria,controladoria,processos,protocolo,jedi,workflow,processo eletronico,gerencial,manifestação,acompanhamento,encaminhamento,controle,pesquisa,histórico,governo de goiás,sistema de acompanhamento de processos,sistema gerencial,marcelo roncato"/>

<meta name="autor" content="Marcelo Roncato"/>
<meta name="company" content="SED"/>
<meta name="revisit-after" content="5"/>
<body bgcolor="#FFFFFF">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" valign="top"><img src="../images/politec.jpg" width="218" height="107"
                                                      align="absmiddle">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td width="1019" align="center"><font size="5" face="Arial, Helvetica, sans-serif"><strong>FOLHA
                                INDIVIDUAL DE PRESEN&Ccedil;A</strong></font></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td align="right">&nbsp;</td>
                </tr>
            </table>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="20"><strong></strong></td>
                    <td width="26" align="center">&nbsp;</td>
                    <td align="right" valign="top">&nbsp; </td>
                </tr>
                <tr>
                    <td height="20"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Empresa:
                            </font></strong><font
                            style='font-size:12px; font-family: Verdana, Arial; color=#333333'><font
                                style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'>INDRA
                                BRASIL SOLU&Ccedil;&Otilde;ES E SERVI&Ccedil;OS TECNOLOGICOS S/A</font></font></td>
                    <td>&nbsp;</td>
                    <td valign="top"><font
                            style='font-size:12px; font-family: Verdana, Arial; color=#333333'>CNPJ</font>:<font
                            style='font-size:12px; font-family: Verdana, Arial; color=#333333'>
                            016457380000330</font></td>
                </tr>
                <tr>
                    <td height="20"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Endere&ccedil;o:</font></strong><font
                            style='font-size:12px; font-family: Verdana, Arial; color=#333333'>&nbsp;</font><font
                            style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'>AV.
                            REPUBLICA DO LIBANO, 838</font></td>
                    <td>&nbsp;</td>
                    <td valign="top"><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'><strong>Emiss&atilde;o:</strong>
                            <?php echo date('d') . "." . date('m') . "." . date('y') . " - " . date("H") . ":" . date("i") . ":" . date("s"); ?>
                        </font></td>
                </tr>
                <tr>
                    <td height="20"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Cidade:</font></strong><font
                            style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'>
                            GOIANIA&nbsp;&nbsp;&nbsp;<strong>&nbsp;UF:</strong> GO</font></td>
                    <td>&nbsp;</td>
                    <td valign="top"><font style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'><strong>CEP:
                            </strong>74070-040 </font></td>
                </tr>
                <tr>
                    <td height="20"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Nome:</font></strong><font
                            style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'>
                            00010709 MARCELO AUGUSTO CARVALHO RONCATO</font></td>
                    <td>&nbsp;</td>
                    <td valign="top"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Compet&ecirc;ncia:</font></strong><font
                            style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'>
                            <?php switch ($mes_atual) {
                                case 1:
                                    echo "Janeiro";
                                    break;
                                case 2:
                                    echo "Fevereiro";
                                    break;
                                case 3:
                                    echo "Março";
                                    break;
                                case 4:
                                    echo "Abril";
                                    break;
                                case 5:
                                    echo "Maio";
                                    break;
                                case 6:
                                    echo "Junho";
                                    break;
                                case 7:
                                    echo "Julho";
                                    break;
                                case 8:
                                    echo "Agosto";
                                    break;
                                case 9:
                                    echo "Setembro";
                                    break;
                                case 10:
                                    echo "Outubro";
                                    break;
                                case 11:
                                    echo "Novembro";
                                    break;
                                case 12:
                                    echo "Dezembro";
                                    break;
                            }
                            echo "/" . $ano; ?>
                        </font></td>
                </tr>
                <tr>
                    <td height="20"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Admiss&atilde;o:</font></strong><font
                            style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'>
                            02.02.2009 &nbsp;&nbsp;<strong><font
                                    style='font-size:12px; font-family: Verdana, Arial; color=#333333'>&Aacute;rea
                                    de RH:</font></strong><font
                                style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'>
                                GO01 - Goi&acirc;nia</font></font></td>
                    <td>&nbsp;</td>
                    <td valign="top"><font style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'><strong>CTPS
                                N&ordm; S&eacute;rie: </strong>&nbsp;&nbsp;&nbsp;93877 / 0019 - GO</font></td>
                </tr>
                <tr>
                    <td height="20"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>Fun&ccedil;&atilde;o:</font></strong><font
                            style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'>
                            50039770 - ANALISTA DE SISTEMAS</font></td>
                    <td>&nbsp;</td>
                    <td valign="top"><font style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'><strong>Hor&aacute;rio
                                de Trabalho: </strong>H06_GO- 08:00-18:00</font></td>
                </tr>
                <tr>
                    <td height="20"><strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>LOTA&Ccedil;&Atilde;O:
                            </font></strong><font style='font-size:12px; font-family: Verdana, Arial; color=#333333'>33212-CONTROLADORIA
                            GERAL</font></td>
                    <td><strong><font color="#0066FF"> <font
                                    style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'>
                                </font></font></strong></td>
                    <td valign="top"><font style='font-size:12px; font-family: Verdana, Arial; color=#0066FF'><strong>Intervalo:
                            </strong>12:00 - 14:00</font></td>
                </tr>
                <tr>
                    <td height="20">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr align="center">
        <td width="800" rowspan="2"
            style="border-top: 1px solid #000000 ;border-left: 1px solid #000000;;border-right: 1px solid #000000;border-bottom: 1px solid #000000;">
            <font style='font-size:12px; font-family: Verdana, Arial;'>DIA</font></td>
        <td width="800" rowspan="2"
            style="border-top: 1px solid #000000 ;border-right: 1px solid #000000;border-bottom: 1px solid #000000;">
            <font style='font-size:12px; font-family: Verdana, Arial;'>SEM</font></td>
        <td height="30" colspan="4"
            style="border-top: 1px solid #000000 ;border-right: 1px solid #000000;border-bottom: 1px solid #000000;">
            <font style='font-size:12px; font-family: Verdana, Arial;'>HOR&Aacute;RIO
                TRABALHO</font></td>
        <td width="800" rowspan="2"
            style="border-top: 1px solid #000000 ;border-right: 1px solid #000000;border-bottom: 1px solid #000000;">
            <font style='font-size:12px; font-family: Verdana, Arial;'>RUBLICA</font></td>
        <td colspan="4"
            style="border-top: 1px solid #000000 ;border-right: 1px solid #000000;border-bottom: 1px solid #000000;">
            <font style='font-size:12px; font-family: Verdana, Arial;'>HOR&Aacute;RIO
                EXTRAORDINARIO </font></td>
        <td width="800" rowspan="2"
            style="border-top: 1px solid #000000 ;border-right: 1px solid #000000;border-bottom: 1px solid #000000;">
            <font style='font-size:12px; font-family: Verdana, Arial;'>RUBLICA</font></td>
        <td width="800" rowspan="2"
            style="border-top: 1px solid #000000 ;border-right: 1px solid #000000;border-bottom: 1px solid #000000;">
            <font style='font-size:12px; font-family: Verdana, Arial;'>C&Oacute;DIGO<br>
                Aus/Pres </font></td>
    </tr>
    <tr align="center">
        <td width="800" height="30" style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;"><font
                style='font-size:12px; font-family: Verdana, Arial;'>ENTRADA</font></td>
        <td width="800" style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;"><font
                style='font-size:12px; font-family: Verdana, Arial;'>SAIDA</font></td>
        <td width="800" style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;"><font
                style='font-size:12px; font-family: Verdana, Arial;'>ENTRADA</font></td>
        <td width="800" style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;"><font
                style='font-size:12px; font-family: Verdana, Arial;'>SAIDA</font></td>
        <td width="800" style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;"><font
                style='font-size:12px; font-family: Verdana, Arial;'>ENTRADA</font></td>
        <td width="800" style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;"><font
                style='font-size:12px; font-family: Verdana, Arial;'>SAIDA</font></td>
        <td width="800" style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;"><font
                style='font-size:12px; font-family: Verdana, Arial;'>ENTRADA</font></td>
        <td width="800" style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;"><font
                style='font-size:12px; font-family: Verdana, Arial;'>SAIDA</font></td>
    </tr><?php $Data = strtotime($mes . "/01/" . $ano);
    //$Dia  = date('w',strtotime(date('n/\0\1\/Y',$Data)));
    $Dias = date('t', $Data);


    $dia = 15;
    $vez = 0;
    for ($d = 1; $d <= $Dias; $d++) {

        $dia++;
        if (($dia > $Dias) && $vez == 0) {

            $mes++;
            $dia = 1;
            $vez = 1;

        }
        ?>
        <tr align="center">
            <td height="32"
                style="border-left: 1px solid #000000;;border-right: 1px solid #000000;border-bottom: 1px solid #000000;">
                <font style='font-size:12px; font-family: Verdana, Arial;'><?php echo $dia; ?></font></td>
            <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;"><font
                    style='font-size:12px; font-family: Verdana, Arial;'><?php
                    $diasE = date("w", mktime(0, 0, 0, $mes, $dia, $ano));
                    switch ($diasE) {
                        case"0":
                            $diasemana = "DOM";
                            break;
                        case"1":
                            $diasemana = "SEG";
                            break;
                        case"2":
                            $diasemana = "TER";
                            break;
                        case"3":
                            $diasemana = "QUA";
                            break;
                        case"4":
                            $diasemana = "QUI";
                            break;
                        case"5":
                            $diasemana = "SEX";
                            break;
                        case"6":
                            $diasemana = "SAB";
                            break;
                    }

                    echo $diasemana;
                    ?></font></td>
            <?php if ($diasE == 0 || $diasE == 6) { ?>
                <td colspan="2" style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;"><font
                        style='font-size:12px; font-family: Verdana, Arial;'>
                        <?php if ($diasE == 6) {
                            echo "SABADO";
                        } else {
                            echo "DOMINGO";
                        }
                        ?></font>
                </td>
            <?php } else {
                ?><?php $espaco_11 = rand(0, 4);
                $espaco_21 = rand(0, 3);
                $espaco_31 = rand(0, 12);

                $font_tipo = rand(1, 3);
                if ($font_tipo == 1) {
                    $fonte_tipo = "Kristen ITC";
                    $tamanho_font = rand(16, 18);
                }
                if ($font_tipo == 2) {
                    $fonte_tipo = "Burst My Bubble";
                    $tamanho_font = rand(23, 28);
                }
                if ($font_tipo == 3) {
                    $fonte_tipo = "Segoe Script";
                    $tamanho_font = rand(13, 16);
                }
                ?>
                <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000; padding:<?php echo $espaco_11;?>pt 1pt <?php echo $espaco_21;?>pt <?php echo $espaco_31;?>pt">
                    <font color="#4407F8" face="<?php echo $fonte_tipo;?>"
                          style="font-size:<?php echo $tamanho_font;?>px">
                        <?php $sql1 = "select hora,minutos,etapa from p_registro where id_usr = " . $id_usr . " and day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and etapa = 1";
                        $dados1 = mysql_query($sql1, $conexao);
                        $resultado1 = mysql_fetch_array($dados1);
                        if ($resultado1[hora] != '') {
                            echo $resultado1[hora] . ":" . $resultado1[minutos];
                        }
                        ?>
                    </font></td>
                <?php $espaco_12 = rand(0, 4);
                $espaco_22 = rand(0, 3);
                $espaco_32 = rand(0, 15);
                $font_tipo = rand(1, 3);
                if ($font_tipo == 1) {
                    $fonte_tipo = "Kristen ITC";
                    $tamanho_font2 = rand(16, 18);
                }
                if ($font_tipo == 2) {
                    $fonte_tipo = "Burst My Bubble";
                    $tamanho_font2 = rand(23, 28);
                }
                if ($font_tipo == 3) {
                    $fonte_tipo = "Segoe Script";
                    $tamanho_font2 = rand(13, 16);
                }
                ?>
                <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000; padding:<?php echo $espaco_12;?>pt 1pt <?php echo $espaco_22;?>pt <?php echo $espaco_32;?>pt">
                    <font color="#4407F8" face="<?php echo $fonte_tipo;?>"
                          style="font-size:<?php echo $tamanho_font2;?>px">
                        <?php $sql2 = "select hora,minutos,etapa from p_registro where id_usr = " . $id_usr . " and day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and etapa = 2";
                        $dados2 = mysql_query($sql2, $conexao);
                        $resultado2 = mysql_fetch_array($dados2);
                        if ($resultado2[hora] != '') {
                            echo $resultado2[hora] . ":" . $resultado2[minutos];
                        }
                        ?>
                    </font></td>
            <?php } ?>

            <?php $espaco_13 = rand(0, 4);
            $espaco_23 = rand(0, 3);
            $espaco_33 = rand(0, 18);
            $font_tipo = rand(1, 3);
            if ($font_tipo == 1) {
                $fonte_tipo = "Kristen ITC";
                $tamanho_font3 = rand(16, 18);
            }
            if ($font_tipo == 2) {
                $fonte_tipo = "Burst My Bubble";
                $tamanho_font3 = rand(23, 28);
            }
            if ($font_tipo == 3) {
                $fonte_tipo = "Segoe Script";
                $tamanho_font3 = rand(13, 16);
            }
            ?>
            <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000; padding:<?php echo $espaco_13; ?>pt 1pt <?php echo $espaco_23; ?>pt <?php echo $espaco_33; ?>pt">
                <font color="#4407F8" face="<?php echo $fonte_tipo; ?>"
                      style="font-size:<?php echo $tamanho_font3; ?>px"> <?php $sql3 = "select hora,minutos,etapa from p_registro where id_usr = " . $id_usr . " and day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and etapa = 3";
                    $dados3 = mysql_query($sql3, $conexao);
                    $resultado3 = mysql_fetch_array($dados3);
                    if ($resultado3[hora] != '') {
                        echo $resultado3[hora] . ":" . $resultado3[minutos];
                    }
                    ?></font></td>
            <?php $espaco_14 = rand(0, 4);
            $espaco_24 = rand(0, 3);
            $espaco_34 = rand(0, 15);
            $font_tipo = rand(1, 3);
            if ($font_tipo == 1) {
                //	$fonte_tipo = "Bradley Hand ITC";
                // $tamanho_font4 = rand(16,20);
                $fonte_tipo = "Kristen ITC";
                $tamanho_font4 = rand(16, 18);
            }
            if ($font_tipo == 2) {
                $fonte_tipo = "Burst My Bubble";
                $tamanho_font4 = rand(23, 28);
            }
            if ($font_tipo == 3) {
                $fonte_tipo = "Segoe Script";
                $tamanho_font4 = rand(13, 16);
            }
            ?>
            <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000; padding:<?php echo $espaco_14; ?>pt 1pt <?php echo $espaco_24; ?>pt <?php echo $espaco_34; ?>pt">
                <font color="#4407F8" face="<?php echo $fonte_tipo; ?>"
                      style="font-size:<?php echo $tamanho_font4; ?>px"> <?php $sql4 = "select hora,minutos,etapa from p_registro where id_usr = " . $id_usr . " and day(data_registro) = " . $dia . " and month(data_registro) = " . $mes . " and year(data_registro) = " . $ano . " and etapa = 4";
                    $dados4 = mysql_query($sql4, $conexao);
                    $resultado4 = mysql_fetch_array($dados4);
                    if ($resultado4[hora] != '') {
                        echo $resultado4[hora] . ":" . $resultado4[minutos];
                    }
                    ?></font></td>
            <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;">&nbsp;</td>
            <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;">&nbsp;</td>
            <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;">&nbsp;</td>
            <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;">&nbsp;</td>
            <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;">&nbsp;</td>
            <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;">&nbsp;</td>
            <td style="border-right: 1px solid #000000;border-bottom: 1px solid #000000;">&nbsp;</td>

        </tr>
    <?php } ?>
</table>
<p>&nbsp;</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="50" colspan="3"><p><font style='font-size:10px; font-family:Verdana, Arial'><strong>Horas
                        Extras:_____% = ____________&nbsp;&nbsp; ________% = _____________&nbsp;&nbsp;
                        _______% = _________&nbsp;</strong></font></p>

            <p><font style='font-size:10px; font-family:Verdana, Arial'><strong> Adicional
                        Noturno: __________ Horas Bip:_______ Faltas: ____________</strong></font></p>

            <p>&nbsp;</p></td>
    </tr>
    <tr>
        <td height="30" align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr valign="top">
        <td width="800" height="30" align="center" style="border-top: 1px solid #000000"><strong><font
                    style='font-size:10px; font-family: Verdana, Arial; color=#333333'>Assinatura
                    Colaborador </font></strong></td>
        <td width="800" align="center">&nbsp;</td>
        <td width="800" align="center" style="border-top: 1px solid #000000"><font
                style='font-size:10px; font-family: Verdana, Arial; color=#333333'><strong>Assinatura
                    Respons&aacute;vel</strong></font></td>
    </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
       style="border-top: 1px solid #000000 ;border-left: 1px solid #000000;;border-right: 1px solid #000000;border-bottom: 1px solid #000000;">
    <tr>
        <td height="30" valign="bottom"><font style='font-size:12px; font-family: Verdana, Arial;'>ESPA&Ccedil;O
                DESTINADO A OBSERVA&Ccedil;&Otilde;ES/JUSTIFICATIVAS</font></td>
    </tr>
    <tr valign="top">
        <td width="800" height="30" align="center" style="border-top: 1px solid #000000"><strong></strong></td>
    </tr>
</table>
<p>&nbsp;</p>
</body>
</html>