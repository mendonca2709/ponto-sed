﻿<?php session_start(); ?>
<head><title>Sistema de Ponto Eletrônico - Login</title>
    <?php
    require_once '../funcoes/conexao.php';
    $numr_ip = $_SERVER[HTTP_X_FORWARDED_FOR];
    $ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
    $iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
    $android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
    $palmpre = strpos($_SERVER['HTTP_USER_AGENT'], "webOS");
    $berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
    $ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");

    $texto = "";
    if ($ipad == true) {
        $texto = " para iPad";
    }
    if ($iphone == true) {
        $texto = " para iPhone";
    }
    if ($android == true) {
        $texto = " para Android";
    }
    if ($palmpre == true) {
        $texto = " para Palmpre";
    }
    if ($berry == true) {
        $texto = " para Berry";
    }
    if ($ipod == true) {
        $texto = " para iPod";
    }
    ?>

    <meta name="title" content="SED - SED"/>
    <meta name="url" content="http://<?php echo $_SERVER['HTTP_HOST']; ?>"/>
    <meta name="description"
          content="SED-GO - A SED de Goiás implementou em um projeto altamente inovador de acompanhamento de Processos totalmente digital."/>
    <meta name="keywords"
          content="Sistema de ponto eletronico,sistema,ouvidoria,corregedoria,controladoria,processos,protocolo,jedi,workflow,processo eletronico,gerencial,manifestação,acompanhamento,encaminhamento,controle,pesquisa,histórico,governo de goiás,sistema de acompanhamento de processos,sistema gerencial,marcelo roncato"/>

    <meta name="autor" content="Marcelo Roncato"/>
    <meta name="company" content="SED"/>
    <meta name="revisit-after" content="5"/>

    <link rel="shortcut icon" href="../images/goias.ico" type="image/x-icon"/>


    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <script language="JavaScript" src="../js/jquery-2.1.3.min.js"></script>
    <script language="JavaScript" src="../js/jquery-ui-1.11.3.custom/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../js/jquery-ui-1.11.3.custom/jquery-ui.min.css">
    <script type="text/javascript">
        <!--document.getElementById('var_login_name').focus();-->
    </script>
    <script>
//        $(document).ready(function () {
//            $('#Usuario').autocomplete({
//                source: "../funcoes/autocomplete.php"
//            });
//        });
    </script>

    <script language="javascript" type="text/javascript">
        function fncEnter() {
            if (window.event.keyCode == 13) {
                fncValida();
            }
        }
        function setFocus() {
            document.loginForm.Usuario.select();
            document.loginForm.Usuario.focus();
        }
        $(document).ready(function () {
            $('#Senha').keyup(function (e) {
                if (e.which == 13) {
                    fncValida();
                }
            });
        });
        function fncValida() {
            if (document.loginForm.Usuario.value == '') {
                alert("Informe o seu login de acesso!");
                document.loginForm.Usuario.focus();
                return false;
            }
            if (document.loginForm.Senha.value == '') {
                alert("Informe sua senha!");
                document.loginForm.Senha.focus();
                return false;
            }
            document.loginForm.submit();
        }
        function fncReenvia() {
            if (document.loginForm.Usuario.value == '') {
                alert("Para reenviarmos sua senha para seu email, informe o seu login de acesso!");
                document.loginForm.Usuario.focus();
                return false;
            }
            document.formulario_envia.Usuario.value = document.loginForm.Usuario.value;
            document.formulario_envia.submit()
        }
    </script>
</head>


<body onload="javascript:setFocus()">


<BR> <BR> <BR>
</p>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="top">
            <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="680" height="80" background="../images/header.jpg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="93%"><strong><font color="#333333" size="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login
                                            de Acesso</font></strong></td>
                                <td width="7%" rowspan="2" valign="bottom">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="bottom">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><font color="#666666"
                                                                                                      size="1">&nbsp;
                                            <?php

                                            $hoje = getdate();

                                            switch ($hoje['wday']) {
                                                case 0:
                                                    echo "Domingo, ";
                                                    break;
                                                case 1:
                                                    echo "Segunda-Feira, ";
                                                    break;
                                                case 2:
                                                    echo "Terça-Feira, ";
                                                    break;
                                                case 3:
                                                    echo "Quarta-Feira, ";
                                                    break;
                                                case 4:
                                                    echo "Quinta-Feira, ";
                                                    break;
                                                case 5:
                                                    echo "Sexta-Feira, ";
                                                    break;
                                                case 6:
                                                    echo "Sábado, ";
                                                    break;
                                            }

                                            echo $hoje['mday'];
                                            switch ($hoje['mon']) {
                                                case 1:
                                                    echo " de Janeiro de ";
                                                    break;
                                                case 2:
                                                    echo " de Fevereiro de ";
                                                    break;
                                                case 3:
                                                    echo " de Março de ";
                                                    break;
                                                case 4:
                                                    echo " de Abril de ";
                                                    break;
                                                case 5:
                                                    echo " de Maio de ";
                                                    break;
                                                case 6:
                                                    echo " de Junho de ";
                                                    break;
                                                case 7:
                                                    echo " de Julho de ";
                                                    break;
                                                case 8:
                                                    echo " de Agosto de ";
                                                    break;
                                                case 9:
                                                    echo " de Setembro de ";
                                                    break;
                                                case 10:
                                                    echo " de Outubro de ";
                                                    break;
                                                case 11:
                                                    echo " de Novembro de ";
                                                    break;
                                                case 12:
                                                    echo " de Dezembro de ";
                                                    break;
                                            }

                                            echo $hoje['year'] . ".";
                                            ?>
                                        </font></strong></td>
                            </tr>
                        </table>

                    </td>
                    <td width="20" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="700" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="3" height="5"></td>
                    <td width="4" height="4" background="../images/rightside.gif"></td>
                    <td width="696" valign="top" bgcolor="#FFFFFF"><BR>

                        <p>&nbsp; </p>
                        <table width="587" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
                            <tr valign="bottom">
                                <td height="25" class="Titulo_rel">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                        <tr>
                                            <td width="90" align="center"><img src="../images/2.png" width="128"
                                                                               height="128" border="0">
                                            </td>
                                            <td width="531" valign="middle"><strong><font color="#666666" size="4">
                                                        Sistema de Ponto Eletrônico
                                                        <?php echo $texto; ?>
                                                    </font></strong> <br>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="1">
                                                    <tr>
                                                        <td align="right">-</td>
                                                        <td><font color="#999999"><strong>Gestão de
                                                                    Ponto</strong></font></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">-</td>
                                                        <td><font color="#999999"><strong>Relatórios</strong></font>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">-</td>
                                                        <td><font color="#999999"><strong>Configurações</strong></font>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <strong></strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr valign="bottom">
                                <td height="25" class="Sub_titulo_rel">&nbsp;&nbsp;Autenticação
                                </td>
                            </tr>
                            <tr class="Itens_rel_s_traco">
                                <td height="2">
                                    <form action="frm_Verifica_usuario.php" method="post" name="loginForm"
                                          id="loginForm" target="Verifica" onsubmit="return fncValida();">
                                        <br>
                                        <table width="220" border="0" align="center" cellpadding="2" cellspacing="0">
                                            <tr>
                                                <td width="1" rowspan="4" valign="middle"><strong><font size="2"><font
                                                                color="#333333"></font></font></strong></td>
                                                <td width="48">&nbsp;</td>
                                                <td width="82">&nbsp;</td>
                                                <td width="73">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><font color="#666666">Usu&aacute;rio:</font></td>
                                                <td><input name="Usuario" id="Usuario" size="15" type="text">
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 0px 0px 3px 0px;"><font color="#666666">
                                                        Senha: </font></td>
                                                <td style="padding: 0px 0px 3px 0px;"><input name="Senha" id="Senha"
                                                                                             size="15" type="password">
                                                </td>
                                                <td valign="baseline">
                                                    <div id="resultado1" class="tryit" onClick="fncValida();">Entrar
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><font color="#333333">
                                                        <div style="cursor:pointer;" onClick="fncReenvia()"
                                                             title="Clique aqui para reenviar sua senha para seu email cadastrado.">
                                                            <img src="../images/ajuda.png"
                                                                 alt="Clique aqui para reenviar sua senha para seu email cadastrado."
                                                                 width="16" height="16" align="absmiddle">
                                                            Esqueci minha senha
                                                        </div>
                                                    </font></td>
                                            </tr>
                                        </table>
                                        <input type="text" name="numr_ip" value="<?php echo $numr_ip; ?>"
                                               style="display:none">
                                        <br>
                                        <br>
                                    </form>
                                </td>
                            </tr>
                            <tr class="Sub_titulo_rel">
                                <td height="15">
                                    <form name="formulario_envia" method="post" action="reenvia_senha.php"
                                          target="Verifica">
                                        <input type="text" name="Usuario" style="display:none">
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <p align="center"><font color="#990000">* Recomendável a utilização
                                do <strong>Navegador Mozilla Firefox</strong></font></p>

                        <p>&nbsp;</p></td>
                    <td width="2" valign="top" background="../images/sidebar.gif"><br>
                    </td>
                </tr>
            </table>
            <table width="700" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="596" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sistema
                        de Ponto Eletrônico <?=date('Y')?> - SED&reg;</td>
                    <td width="2" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">&nbsp;</td>
    </tr>
</table>

<BR>
<iframe width="801" height="801" name="Verifica" style="display:none"></iframe>
</body>

</html>