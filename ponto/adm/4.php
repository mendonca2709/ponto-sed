﻿<?php

require_once("../funcoes/conexao.php");


$id_pai = $_GET["id_pai"];

if ($id_pai != '') {
    $sql_qtd = "select count(id_evento) as acrescimo from p_eventos where id_pai = " . $id_pai;
    $dados_qtd = mysql_query($sql_qtd, $conexao);
    $resultado_qtd = mysql_fetch_array($dados_qtd);
    $acrescimo = $resultado_qtd[acrescimo] - 1;

    $sql = "select * from p_eventos where id_evento = " . $id_pai;
    $dados = mysql_query($sql, $conexao);
    $resultado = mysql_fetch_array($dados);
    $data_evento = date('d/m/Y', strtotime($resultado[data_evento]));
    $descricao = $resultado[descricao];
    $id_tipo_justificativa = $resultado[id_tipo_justificativa];
    $id_depto = $resultado[id_depto];
    $id_usr = $resultado[id_usr];

    $mes = date('m', strtotime($resultado[data_evento]));
    $novo_dia = date('d', strtotime($resultado[data_evento])) + $acrescimo;
    $ano = date('Y', strtotime($resultado[data_evento]));


    $nextdate = date('d/m/Y', mktime(0, 0, 0, $mes, $novo_dia, $ano));
    $novadata_grava = substr($nextdate, 6, 4) . "/" . substr($nextdate, 3, 2) . "/" . substr($nextdate, 0, 2);
    $dt_final_periodo = date('d/m/Y', strtotime($novadata_grava));
} else {
    $id_pai = 0;
}

$sql_tipo = "select * from  p_tipo_justificativa where lancamento = 3 order by titulo";
$dados_tipo = mysql_query($sql_tipo, $conexao);

?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>

<script language="JavaScript" type="text/javascript">


    limite = 5000;
    function soma() {
        var mais_um = eval(document.formulario.descricao.value.length - 1);
        mais_um++;
        if (document.formulario.descricao.value.length > limite) {
            document.formulario.descricao.value = '';
            document.formulario.descricao.value = valor_limite;
            alert("Limite de " + limite + " caracteres excedido!");
        } else {
//document.frmCOF.exibe.value=''; 
//document.frmCOF.exibe.value=eval(mais_um); 
            valor_limite = document.formulario.descricao.value;
            document.formulario.exibe.value = '';
            document.formulario.exibe.value = (limite - mais_um);
        }
        document.formulario.descricao.focus();
    }
    function fncAtualizadata(data_digitada) {
        document.formulario.dt_final_periodo.value = data_digitada;
    }
    function fncLista() {
        window.open("lista_eventos.php", "", "");
    }

    function fncMontacombo(id_depto) {
        janela.location.href = 'frmMontafuncionarios.php?id_depto=' + id_depto;
    }

    function fncValida() {
        if (document.formulario.data_evento.value == '') {
            alert("Informe a data para este(s) evento(s)!");
            document.formulario.data_evento.focus();
            return false;
        }
        if (document.formulario.id_tipo_justificativa.value == 0) {
            alert("Selecione o tipo de justificativa para este evento!");
            document.formulario.id_tipo_justificativa.focus();
            return false;
        }

        if (document.formulario.id_depto.value != 0) {
            if (document.formulario.id_usr.value == 0) {
                alert("Selecione um funcionário para este departamento!");
                document.formulario.id_usr.focus();
                return false;
            }
        }

        if (document.formulario.descricao.value == '') {
            alert("Informe uma descrição para este evento!");
            document.formulario.descricao.focus();
            return false;
        }


        document.formulario.submit();
    }
    function fncValidaEmail(vEmail) {
        var vEmailIndex = vEmail.indexOf("@");
        var vDiv = vEmail.split("@");
        var vValAft = vEmail.substring(vEmailIndex + 1, vEmail.length);
        var vDiv2 = vValAft.split(".");
        if ((vEmailIndex == -1) || (vDiv.length < 2) || (vDiv2.length < 2)) {
            //fncEmailInvalido();
            alert("Formato do E-mail inválido!");
            return false;

        }
        for (i = 0; i < vDiv.length; i++)
            if (vDiv[i] == "") {
                alert("Formato do E-mail inválido!");
                return false;
            }

        for (i = 0; i < vDiv2.length; i++)
            if (vDiv2[i] == "") {
                alert("Formato do E-mail inválido!");
                return false;
            }
    }
    function fncColoca() {
        if (<?php echo $id_pai;?>!=
        0
    )
        {
            setTimeout("document.formulario.id_tipo_justificativa.value=<?php echo $id_tipo_justificativa;?>;", 500);
            setTimeout("document.formulario.id_depto.value=<?php echo $id_depto;?>;", 800);
            setTimeout("janela.location.href='frmMontafuncionarios.php?id_depto='+<?php echo $id_depto;?>;", 1200);
            setTimeout("document.formulario.id_usr.value=<?php echo $id_usr;?>;", 2000);
        }
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
    }


    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }


        else {
            return true;
        }
    }
</script>

<head>
    <title>Cadastro de Eventos</title>

</head>

<body onLoad="fncColoca();">
<?php require_once("frm_topo.php"); ?>



<br>

<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><BR><strong><font
                                    color="#333333" size="4">
                                </font></strong>

                        <p><strong><font color="#333333" size="4">&nbsp; Cadastro de Eventos</font></strong></p>
                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" align="center" valign="top" bgcolor="#FFFFFF"><br>
                        <table width="790" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
                            <tr valign="bottom">
                                <td height="25" class="Titulo_rel">
                                    <table width="790" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td width="194"><strong><font style="font-size:10px" color="#333333">
                                                        &nbsp;</font><font color="#FF0000"><img
                                                            src="../images/dollar_cash_money.png" width="100"
                                                            height="100" align="absmiddle"></font><font
                                                        style="font-size:10px" color="#333333">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;</font><font color="#660000" size="3">Cadastro
                                                        de Adiantamentos</font></strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="Itens_rel_s_traco">
                                <td height="15">
                                    <form action="grava_evento.php" method="post" name="formulario" id="formulario"
                                          target="janela">
                                        <table border="0" align="center" cellpadding="4" cellspacing="0">
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td><input name="id_pai" type="text" id="id_pai"
                                                           value="<?php echo $id_pai ?>" style="display:none">
                                                    <input name="dia" type="text" id="dia" value="<?php echo $dia ?>"
                                                           style="display:none">
                                                    <input name="mes" type="text" id="mes" value="<?php echo $mes ?>"
                                                           style="display:none">
                                                    <input name="ano" type="text" id="ano" value="<?php echo $ano ?>"
                                                           style="display:none"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td>Data:</td>
                                                <td><input name="data_evento" type="text" id="data_evento"
                                                           style="font-family: Arial; font-size: 8 pt; " size="10"
                                                           maxlength="10"
                                                           onKeyPress="return txtBoxFormat(this, '99/99/9999', event);"
                                                           onBlur="fncAtualizadata(this.value)"
                                                           value="<?php echo $data_evento; ?>"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td>Tipo:</td>
                                                <td><select name="id_tipo_justificativa" id="select">
                                                        <option value="0">Selecione</option>
                                                        <?php while ($resultado_tipo = mysql_fetch_assoc($dados_tipo)) { ?>
                                                            <option
                                                                value="<?php echo $resultado_tipo[id_tipo_justificativa]; ?>"><?php echo $resultado_tipo[titulo]; ?></option>
                                                        <?php } ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Valor:</td>
                                                <td><strong><font color="#666666">
                                                            <input name="data_evento2" type="text" id="data_evento2"
                                                                   style="font-family: Arial; font-size: 8 pt; "
                                                                   size="10" maxlength="10"
                                                                   onKeyPress="return txtBoxFormat(this, '99/99/9999', event);"
                                                                   onBlur="fncAtualizadata(this.value)"
                                                                   value="<?php echo $data_evento; ?>">
                                                        </font></strong></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Parcelado:</td>
                                                <td>
                                                    <div id="div_id_usr">
                                                        <select name="id_usr" id="id_usr" style="width:250px">
                                                            <option value="0">Todos funcionários</option>
                                                            <?php while ($resultado_usuarios = mysql_fetch_array($dados_usuarios)) { ?>
                                                                <option
                                                                    value="<?php echo $resultado_usuarios[id_usr]; ?>"><?php echo $resultado_usuarios[nome]; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <strong><font color="#666666"></font></strong></div>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Descri&ccedil;&atilde;o:</td>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td><textarea name="descricao" cols="120" rows="5"
                                                                          id="textarea"
                                                                          style="font-family: Arial; font-size: 8 pt; "
                                                                          onKeyPress="soma(this.value)"
                                                                          onKeyUp="soma(this.value)"><?php echo $descricao ?></textarea>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right"><font size="1">Caracteres restantes</font>
                                                                <input type="text" name="exibe" size="3" maxlength="3"
                                                                       readonly="yes">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Recibo (opcional):</td>
                                                <td><input name="foto" type="file" id="foto2" size="50"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td colspan="2" valign="top"><strong>Emitir recibo?</strong>
                                                    <input name="radiobutton" type="radio" value="radiobutton" checked>
                                                    Sim
                                                    <input type="radio" name="radiobutton" value="radiobutton">
                                                    N&atilde;o
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td colspan="2" valign="top">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td colspan="2" valign="top"><img src="../images/Auto2.gif" width="35"
                                                                                  height="35" align="absmiddle"><strong><font
                                                            color="#FF0000">Emitr
                                                            extrato </font></strong></td>
                                            </tr>
                                        </table>
                                        <p>&nbsp;</p>
                                        <table width="383" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr align="center">
                                                <td>
                                                    <table width="110" height="25" border="0" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="fncValida();">Cadastrar
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table width="110" height="25" border="0" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="window.location.href='cad_eventos.php';">
                                                                    Limpar
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <p>&nbsp;</p>

                                        <p>&nbsp;</p>
                                    </form>

                                    <table width="371" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="56" height="20" align="right" valign="middle"><img
                                                    src="../images/bg_list.gif" width="3" height="5">&nbsp;</td>
                                            <td width="315"><strong><a href="#" onClick="fncLista();">Mostrar
                                                        todos Adiantamentos</a></strong></td>
                                        </tr>
                                    </table>
                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p></td>
                            </tr>
                            <tr class="Sub_titulo_rel">
                                <td height="15">&nbsp;</td>
                            </tr>
                        </table>
                        <br><br><br><br><br><br><br>
                        <br>
                        <br><br>


                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="96%">&nbsp;</td>
                                <td width="4%">
                                    <div align="center"><a href="javascript:history.go(-1)"><img
                                                src="../images/reply-16x16.gif" title="Voltar" width="16"
                                                height="16" border="0"></a></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Op&ccedil;&otilde;es</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='3'>
                                        <tr>
                                            <td width="4%"><img src="../images/morearrow_08c.gif" width="4"
                                                                height="7" border="0"></td>
                                            <td width="96%"><a href="frm_Config.php">Menu
                                                    Configura&ccedil;&otilde;es</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="cad_usuarios.php">Cadastro de usu&aacute;rios</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="frm_cadastro_grades.php">Cadastro de grades</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="frm_Principal.php">In&iacute;cio</a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                    </td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Planejamento
                        estratégico - SED &reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>&nbsp; </p>

<p>&nbsp;</p>


<iframe name="janela" width="800" height="600" style="display:none"></iframe>
</body>
</html>
