﻿<?php
session_start();
if ($_SESSION["sessao_id_usr"] == 0 && $_SESSION["sessao_id_usr"] == 0) {
    ?>
    <script language="JavaScript">
        alert("Você não tem permissão para acessar este módulo!");
        window.close();
    </script>
    <?php

    die;
}

require_once("../funcoes/conexao.php");


$id_depto = $_POST["id_depto"];
if ($id_depto == '') {
    $id_depto = 0;
}


$sql_depto0 = "select * from depto where id_depto = " . $id_depto;
$dados_depto0 = mysql_query($sql_depto0, $conexao);
$resultado_depto0 = mysql_fetch_array($dados_depto0);

$sql_depto1 = "select nome from usuarios inner join depto on depto.id_depto = usuarios.id_depto where depto.id_depto = " . $id_depto . " and gerente = 1";;
$dados_depto1 = mysql_query($sql_depto1, $conexao);
$resultado_depto1 = mysql_fetch_array($dados_depto1);

$sql_depto2 = "select count(id_usr) as total from usuarios where id_depto = " . $id_depto;
$dados_depto2 = mysql_query($sql_depto2, $conexao);
$resultado_depto2 = mysql_fetch_array($dados_depto2);

$sql_depto3 = "select count(usuarios.id_usr) as total,usuarios.id_usr,nome from p_justificativa inner join usuarios on usuarios.id_usr = p_justificativa.id_usr inner join depto on depto.id_depto = usuarios.id_depto where depto.id_depto = " . $id_depto . " group by usuarios.id_usr order by total desc limit 0,1";
$dados_depto3 = mysql_query($sql_depto3, $conexao);
$resultado_depto3 = mysql_fetch_array($dados_depto3);


$sql_contador0 = "select count(id_justificativa) as qtd_total from p_justificativa inner join depto on depto.id_depto = id_depto where abonado = 0 and id_depto = " . $id_depto;
$dados_contador0 = mysql_query($sql_contador0, $conexao);
$resultado_contador0 = mysql_fetch_array($dados_contador0);

$sql_contador1 = "select count(id_justificativa) as qtd_total from p_justificativa  inner join depto on depto.id_depto = id_depto where abonado = 1 and id_depto = " . $id_depto;
$dados_contador1 = mysql_query($sql_contador1, $conexao);
$resultado_contador1 = mysql_fetch_array($dados_contador1);

$sql_contador2 = "select count(p_justificativa.id_tipo_justificativa) as qtd_total, titulo from p_justificativa inner join p_tipo_justificativa on p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa  inner join depto on depto.id_depto = id_depto  where id_depto = " . $id_depto . " group by p_justificativa.id_tipo_justificativa order by qtd_total desc limit 0,1";
$dados_contador2 = mysql_query($sql_contador2, $conexao);
$resultado_contador2 = mysql_fetch_array($dados_contador2);


$sql_contador0 = "select count(id_justificativa) as qtd_total,nome,(select count(id_justificativa) from p_justificativa where id_usr = p.id_usr and abonado = 1) as abonados  from p_justificativa p inner join usuarios on p.id_usr = usuarios.id_usr where id_depto = " . $id_depto . " group by p.id_usr";
$dados_contador0 = mysql_query($sql_contador0, $conexao);
//$resultado_depto	= mysql_fetch_array($dados_contador0);

?>

<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Ocorrências', 'Quantitativo'],
            <?php while($resultado_depto = mysql_fetch_array($dados_contador0)){
            echo "['".$resultado_depto[nome]."',".number_format($resultado_depto[qtd_total],0,",",".")."],";
            } ?>
        ]);

        var options = {
            title: 'Gráfico de Ocorrências deste departamento',
            is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }

</script>


<script language="JavaScript" type="text/javascript">

    function fncLista() {
        window.open("lista_observacoess.php", "", "");
    }
    function fncValida2(abonado) {
        document.formulario.abonado.value = abonado;
        if (document.formulario.observacoes.value == 0) {
            alert("Informe uma justificativa para este abono!");
            document.formulario.observacoes.focus();
            return false;
        }
        document.formulario.submit();
    }
    function fncValida(abonado) {
        document.formulario.abonado.value = abonado;

        if (document.formulario.horario_1.value == '') {
            alert("Informe o horário da entrada pela manhã!");
            document.formulario.horario_1.focus();
            return false;
        }
        if (document.formulario.etapa_1.value == 0) {
            alert("Selecione a etapa para o primeiro horário!");
            document.formulario.etapa_1.focus();
            return false;
        }
        if (document.formulario.horario_2.value == '') {
            alert("Informe o horário da saída pela manhã!");
            document.formulario.horario_2.focus();
            return false;
        }
        if (document.formulario.etapa_2.value == 0) {
            alert("Selecione a etapa para o segundo horário!");
            document.formulario.etapa_2.focus();
            return false;
        }
        if (document.formulario.horario_3.value == '') {
            alert("Informe o horário da entrada pela tarde!");
            document.formulario.horario_3.focus();
            return false;
        }
        if (document.formulario.etapa_3.value == 0) {
            alert("Selecione a etapa para o terceiro horário!");
            document.formulario.etapa_3.focus();
            return false;
        }
        if (document.formulario.horario_4.value == '') {
            alert("Informe o horário da saída pela tarde!");
            document.formulario.horario_4.focus();
            return false;
        }


        if (document.formulario.etapa_4.value == 0) {
            alert("Selecione a etapa para o quarto horário!");
            document.formulario.etapa_4.focus();
            return false;
        }
        if (document.formulario.observacoes.value == 0) {
            alert("Informe uma justificativa para este abono!");
            document.formulario.observacoes.focus();
            return false;
        }
        document.formulario.submit();
    }
    function fncBusca() {
        if (document.form_busca.texto_busca.value == '') {
            alert("Informe um texto para busca!");
            document.form_busca.texto_busca.focus();
            return false;
        }
        document.form_busca.submit();
    }

    limite = 5000;
    function soma() {
        var mais_um = eval(document.formulario.observacoes.value.length - 1);
        mais_um++;
        if (document.formulario.observacoes.value.length > limite) {
            document.formulario.observacoes.value = '';
            document.formulario.observacoes.value = valor_limite;
            alert("Limite de " + limite + " caracteres excedido!");
        } else {
//document.frmCOF.exibe.value=''; 
//document.frmCOF.exibe.value=eval(mais_um); 
            valor_limite = document.formulario.observacoes.value;
            document.formulario.exibe.value = '';
            document.formulario.exibe.value = (limite - mais_um);
        }
        document.formulario.observacoes.focus();
    }
    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }


        else {
            return true;
        }
    }
</script>

<head>
    <title>Estat&iacute;stica de desempenho do funcion&aacute;rio</title>

</head>

<body>
<table width="790" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
    <tr valign="bottom">
        <td height="25" class="Titulo_rel">
            <table width="790" border="0" cellpadding="0" cellspacing="2">
                <tr>
                    <td width="194"><strong><font style="font-size:10px" color="#333333">
                                &nbsp;&nbsp;&nbsp;&nbsp;<font size="3">Estat&iacute;stica da
                                    Ger&ecirc;ncia</font></font></strong></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="Itens_rel_s_traco">
        <td height="312">
            <table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="311" align="center">
                        <div align="center">
                            <table width='314' border='0' align='right' cellpadding='0' cellspacing='0'>
                                <tr>
                                    <td>
                                        <DIV class="commit commit-tease js-details-container"><font
                                                size="2"><strong><font
                                                        color="#0033FF"><?php echo $resultado_depto0[sigla]; ?></font></strong><font
                                                    color="#0033FF"><br>
                                                    <?php echo $resultado_depto0[depto]; ?> </font><br>
                                                <br>
                                            </font>

                                            <DIV class="commit-meta">
                                                <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                                                    <tr>
                                                        <td width="31%" align="center">
                                                            <div align="left"><strong><font
                                                                        color="#666666">Gerente:</font></strong></div>
                                                        </td>
                                                        <td width="69%">
                                                            <div align="left"><strong><font
                                                                        color="#009933"><?php echo $resultado_depto1[nome] ?></font>
                                                                </strong></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div align="left"><strong><font color="#666666">Servidores:</font></strong>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div align="left"><strong><font
                                                                        color="#009933"><?php echo $resultado_depto2[total] ?></font></strong>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <div align="left"><font color="#666666"><strong>Funcion&aacute;rio
                                                                        com maior indid&ecirc;ncia: </strong></font>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"><strong><font
                                                                    color="#009933"><?php echo $resultado_depto3[nome] ?></font></strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <strong><font color="#333333"> </font></strong></div>
                    </td>
                    <td width="15" valign="top">&nbsp;</td>
                    <td width="374" valign="top">
                        <div align="center">
                            <table width='314' border='0' align='right' cellpadding='0' cellspacing='0'>
                                <tr>
                                    <td>
                                        <DIV class="commit commit-tease js-details-container"><font
                                                color="#000033"><strong><font size="2">Resumo
                                                        anual deste departamento</font></strong> <br>
                                                <br>
                                            </font>

                                            <DIV class="commit-meta">
                                                <table width='100%' border='0' cellspacing='0' cellpadding='2'>
                                                    <tr>
                                                        <td width="4%" align="center"><strong><font color="#666666">Justificativas:</font></strong>
                                                        </td>
                                                        <td width="96%"><strong><font
                                                                    color="#FF0000"><?php echo $resultado_contador1[qtd_total] ?></font>
                                                            </strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong><font color="#666666">Abonos:</font></strong></td>
                                                        <td><strong><font
                                                                    color="#FF0000"><?php echo $resultado_contador1[qtd_total] ?></font></strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"><font color="#666666"><strong>Maior indid&ecirc;ncia:
                                                                </strong></font></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"><strong><font
                                                                    color="#FF0000"><?php echo $resultado_contador2[titulo] ?></font></strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>


                    </td>
                </tr>
            </table>
            <div id="chart_div" style="width: 765px; height: 400px;"></div>

            <table width="124" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center">
                        <div id="resultado1" class="tryit" onClick="window.close();">
                            Fechar
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="Sub_titulo_rel">
        <td height="15">&nbsp;</td>
    </tr>
</table>


<form name="volta" method="post" action="mostra_justificativa.php">
    <input type="hidden" name="id_usr" value="<?php echo $id_usr; ?>">
    <input type="hidden" name="dia" value="<?php echo $dia; ?>">
    <input type="hidden" name="mes" value="<?php echo $mes; ?>">
    <input type="hidden" name="ano" value="<?php echo $ano; ?>">
    <input type="hidden" name="id_depto" value="<?php echo $id_depto; ?>">
    <input type="hidden" name="id_justificativa" value="<?php echo $id_justificativa; ?>">
</form>

<form name="depto" method="post" action="estatistica_depto.php">
    <input type="hidden" name="id_usr" value="<?php echo $id_usr; ?>">
    <input type="hidden" name="dia" value="<?php echo $dia; ?>">
    <input type="hidden" name="mes" value="<?php echo $mes; ?>">
    <input type="hidden" name="ano" value="<?php echo $ano; ?>">
    <input type="hidden" name="id_depto" value="<?php echo $id_depto; ?>">
    <input type="hidden" name="id_justificativa" value="<?php echo $id_justificativa; ?>">
</form>


<iframe name="janela" width="800" height="200" style="display:none"></iframe>
</body>
</html>
