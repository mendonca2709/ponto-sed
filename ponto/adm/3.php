﻿<?php

require_once("../funcoes/conexao.php");

$id_grade = $_GET["id_grade"];

if ($id_grade == '') {
    $id_grade = 0;
    $nome_grade = '';
    $entrada_1 = '';
    $saida_1 = '';
    $entrada_2 = '';
    $saida_2 = '';
    $tipo_grade = 0;

} else {

    $sql = "select * from p_grade where id_grade = " . $id_grade;
    $dados = mysql_query($sql, $conexao);
    $resultado = mysql_fetch_array($dados);
    $nome_grade = $resultado[nome_grade];
    $tipo_grade = $resultado[tipo_grade];
    $entrada_1 = $resultado[entrada_1];
    $saida_1 = $resultado[saida_1];
    $entrada_2 = $resultado[entrada_2];
    $saida_2 = $resultado[saida_2];

}

?>
<link rel="stylesheet" type="text/css" media="screen" href="../css/estilos.css">

<html>

<script language="JavaScript" type="text/javascript">
    function fncMostraGrade(tipo) {
        if (tipo == 2) {
            m1.style.display = 'none';
            m2.style.display = 'none';
            m3.style.display = 'none';
            m4.style.display = 'none';
            m5.style.display = 'none';
        }
        if (tipo == 4) {
            m1.style.display = '';
            m2.style.display = '';
            m3.style.display = '';
            m4.style.display = '';
            m5.style.display = '';
        }

    }
    function fncLista() {
        window.open("lista_grades.php", "", "");
    }
    function fncValida() {

        if (document.formulario.nome_grade.value == '') {
            alert("Informe o título desta grade!");
            document.formulario.nome_grade.focus();
            return false;
        }
        if (document.formulario.entrada_1.value == '') {
            alert("Informe o horário de entrada de primeiro período!");
            document.formulario.entrada_1.focus();
            return false;
        }
        if (document.formulario.saida_1.value == '') {
            alert("Informe o horário de saída do primeiro periodo!");
            document.formulario.saida_1.focus();
            return false;
        }
        if (document.formulario.tipo_grade[1].checked == true) {
            if (document.formulario.entrada_2.value == '') {
                alert("Informe o horário de entrada do segundo período!");
                document.formulario.entrada_2.focus();
                return false;
            }

            if (document.formulario.saida_2.value == '') {
                alert("Informe o horário de saída do segundo período!");
                document.formulario.saida_2.focus();
                return false;
            }
        }
        document.formulario.submit();
    }

    function fncAtualiza() {
        if (<?php echo $id_grade;?>!=
        0
    )
        {
            if (<?php echo $tipo_grade;?>!=
            0
        )
            {
                document.formulario.tipo_grade[1].checked = true;
                document.formulario.tipo_grade[0].checked = true;
                fncMostraGrade(2);
            }
            if (<?php echo $tipo_grade;?>==0
        )
            {
                document.formulario.tipo_grade[0].checked = true;
                document.formulario.tipo_grade[1].checked = true;
            }

        }
    }
    function txtBoxFormat(objeto, sMask, evtKeyPress) {
        var i, nCount, sValue, fldLen, mskLen, bolMask, sCod, nTecla;


        if (document.all) { // Internet Explorer
            nTecla = evtKeyPress.keyCode;
        } else if (document.layers) { // Nestcape
            nTecla = evtKeyPress.which;
        } else {
            nTecla = evtKeyPress.which;
            if (nTecla == 8) {
                return true;
            }
        }

        sValue = objeto.value;

        // Limpa todos os caracteres de formatação que
        // já estiverem no campo.
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace("-", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace(".", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace("/", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace(":", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace("(", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(")", "");
        sValue = sValue.toString().replace(" ", "");
        sValue = sValue.toString().replace(" ", "");
        fldLen = sValue.length;
        mskLen = sMask.length;

        i = 0;
        nCount = 0;
        sCod = "";
        mskLen = fldLen;

        while (i <= mskLen) {
            bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/") || (sMask.charAt(i) == ":"))
            bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

            if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++;
            }
            else {
                sCod += sValue.charAt(nCount);
                nCount++;
            }

            i++;
        }

        objeto.value = sCod;

        if (nTecla != 8) { // backspace
            if (sMask.charAt(i - 1) == "9") { // apenas números...
                return ((nTecla > 47) && (nTecla < 58));
            }
            else { // qualquer caracter...
                return true;
            }
        }


        else {
            return true;
        }
    }
</script>

<head>
    <title>Cadastro de Grades</title>

</head>

<body onLoad="fncAtualiza();">
<?php require_once("frm_topo.php"); ?>
<br>

<table width="1155" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="990" valign="top">
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="980" height="80" background="../images/header.jpg"><p><BR><strong><font
                                    color="#333333" size="4">
                                </font></strong>

                        <p><strong><font color="#333333" size="4">&nbsp; Cadastro de Grades</font></strong></p>
                    </td>
                    <td width="19" background="../images/header_rightcap.jpg">&nbsp;</td>
                </tr>
            </table>
            <table width="1030" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="880" background="../images/contentshadow.gif" height="2"></td>
                </tr>
            </table>
            <table width="1030" height="13" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="8" height="5" background="../images/leftside.gif"></td>
                    <td width="821" align="center" valign="top" bgcolor="#FFFFFF"><br>
                        <table width="790" border="0" align="center" cellpadding="0" cellspacing="0" class="Tabela_rel">
                            <tr valign="bottom">
                                <td height="25" class="Titulo_rel">
                                    <table width="790" border="0" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td width="194"><strong><font style="font-size:10px" color="#333333">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;</font><font color="#333333"
                                                                                             size="4"><img
                                                            src="../images/contro.png" width="100" height="100"
                                                            align="absmiddle"></font><font style="font-size:10px"
                                                                                           color="#333333"><font
                                                            size="3">&nbsp;</font></font><font color="#660000" size="3">Cadastro
                                                        de Grade de hor&aacute;rio</font></strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="Itens_rel_s_traco">
                                <td height="15">
                                    <form action="grava_grade.php" method="post" name="formulario" id="formulario"
                                          target="janela">
                                        <table width="800" border="0" align="center" cellpadding="4" cellspacing="0">
                                            <tr>
                                                <td width="3" align="right">&nbsp;</td>
                                                <td width="36">&nbsp;</td>
                                                <td width="737">
                                                    <input name="id_grade" type="text" id="id_grade"
                                                           value="<?php echo $id_grade ?>" style="display:none"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td>T&iacute;tulo:</td>
                                                <td><input name="nome_grade" type="text" id="nome_grade"
                                                           style="font-family: Arial; font-size: 8 pt; " size="65"
                                                           maxlength="100" value="<?php echo $nome_grade ?>"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">Tipo:</td>
                                                <td><input type="radio" name="tipo_grade" value="2"
                                                           onClick="fncMostraGrade(2)" id="op1">
                                                    <label for="op1"><strong>2 Registros</strong></label>
                                                    <input name="tipo_grade" type="radio" value="0" checked
                                                           onClick="fncMostraGrade(4)" id="op2">
                                                    <label for="op2"><strong>4 Registros
                                                            <input name="tipo_grade" type="radio" value="0"
                                                                   onClick="fncMostraGrade(4)" id="radio">
                                                            <strong>Indefinido</strong></strong></label></td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td valign="top">&nbsp;</td>
                                                <td valign="top"><br>
                                                    <table width="351" border="0" cellpadding="0" cellspacing="0"
                                                           class="tablefill">
                                                        <tr align="center">
                                                            <td colspan="2"><strong><font size="2">Per&iacute;odo
                                                                        1 </font></strong></td>
                                                            <td width="83">&nbsp;</td>
                                                            <td colspan="2">
                                                                <div id="m1"><strong><font size="2">Per&iacute;odo
                                                                            2 </font></strong></div>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td width="70">Entrada</td>
                                                            <td width="65">Sa&iacute;da</td>
                                                            <td>&nbsp;</td>
                                                            <td width="70">
                                                                <div id="m2">Entrada</div>
                                                            </td>
                                                            <td width="63">
                                                                <div id="m3">Sa&iacute;da</div>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td><strong>
                                                                    <input name="entrada_1" type="text" id="entrada_1"
                                                                           size="5" maxlength="5"
                                                                           onKeyPress="return txtBoxFormat(this, '99:99', event);"
                                                                           value="<?php echo $entrada_1 ?>">
                                                                </strong></td>
                                                            <td><strong>
                                                                    <input name="saida_1" type="text" id="saida_1"
                                                                           size="5" maxlength="5"
                                                                           onKeyPress="return txtBoxFormat(this, '99:99', event);"
                                                                           value="<?php echo $saida_1 ?>">
                                                                </strong></td>
                                                            <td>&nbsp;</td>
                                                            <td>
                                                                <div id="m4">
                                                                    <input name="entrada_2" type="text" id="entrada_2"
                                                                           size="5" maxlength="5"
                                                                           onKeyPress="return txtBoxFormat(this, '99:99', event);"
                                                                           value="<?php echo $entrada_2 ?>"></div>
                                                            </td>
                                                            <td>
                                                                <div id="m5">
                                                                    <input name="saida_2" type="text" id="saida_2"
                                                                           size="5" maxlength="5"
                                                                           onKeyPress="return txtBoxFormat(this, '99:99', event);"
                                                                           value="<?php echo $saida_2 ?>">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                    <img src="../images/49.gif" width="15" height="15">
                                                    <strong>Adicionar outros per&iacute;odos</strong></td>
                                            </tr>
                                        </table>
                                        <p>&nbsp;</p>
                                        <table width="291" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr align="center">
                                                <td>
                                                    <table width="87" height="25" border="0" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="fncValida();">
                                                                    <div align="center">Enviar</div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table width="87" height="25" border="0" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="document.location.href='frm_cadastro_grades.php';">
                                                                    <div align="center">Limpar</div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <p>&nbsp;</p></form>

                                    <table width="371" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="56" height="20" align="right" valign="middle"><img
                                                    src="../images/bg_list.gif" width="3" height="5">&nbsp;</td>
                                            <td width="315"><strong><a href="#" onClick="fncLista();">Mostrar
                                                        todas Grades</a></strong></td>
                                        </tr>
                                    </table>
                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p>

                                    <p>&nbsp;</p></td>
                            </tr>
                            <tr class="Sub_titulo_rel">
                                <td height="15">&nbsp;</td>
                            </tr>
                        </table>
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br>


                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td width="96%">&nbsp;</td>
                                <td width="4%">
                                    <div align="center"><a href="javascript:history.go(-1)"><img
                                                src="../images/reply-16x16.gif" title="Voltar" width="16"
                                                height="16" border="0"></a></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="201" valign="top" background="../images/sidebar.gif"><br>
                        <table width='180' border='0' align='center' cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class='Titulo_caixa'> Op&ccedil;&otilde;es</td>
                            </tr>
                            <tr>
                                <td valign="top" class='Corpo_caixa'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='3'>
                                        <tr>
                                            <td width="4%"><img src="../images/morearrow_08c.gif" width="4"
                                                                height="7" border="0"></td>
                                            <td width="96%"><a href="frm_Config.php">Menu
                                                    Configura&ccedil;&otilde;es</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="cad_usuarios.php">Cadastro de usu&aacute;rios</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="cad_deptos.php">Cadastro de Unidades</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="../images/morearrow_08c.gif" width="4" height="7"
                                                     border="0"></td>
                                            <td><a href="frm_Principal.php">In&iacute;cio</a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                    </td>
                </tr>
            </table>
            <table width="1030" height="59" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="1" colspan="2"></td>
                </tr>
                <tr>
                    <td width="996" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Planejamento
                        estratégico - SED &reg;</td>
                    <td width="4" class="Fundo_caixa_canto_jpg"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>&nbsp; </p>

<p>&nbsp;</p>


<iframe name="janela" width="800" height="200" style="ddisplay:none"></iframe>
</body>
</html>
