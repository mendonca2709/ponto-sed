<?php
/**
 * Created by PhpStorm.
 * User: marcelo-tm
 * Date: 27/03/2015
 * Time: 16:16
 */
require_once './funcoes/conexao.php';
require_once './funcoes/funcoes.php';

$usuario = getUsuario($_REQUEST['id_usr']);
$grade = getGrade($usuario->id_grade);
$out = ['title' => $usuario->nome, 'html' => ''];
$detalhes = getDetalhesDoDia($usuario->id_usr, date('d/m/Y'));
ob_start();
?>

<?php if ($_REQUEST['id_usr']) { ?>
    <table class="table table-condesned table-striped table-bordered">
        <tr>
            <th>Registro</th>
            <th>Hora Prevista</th>
            <th>Hora Registrada</th>
        </tr>
        <?php if ($grade->entrada_1) {
            $registro = getRegistroEtapa($usuario->id_usr, date('d/m/Y'), 1);
            ?>
            <tr>
                <td>1</td>
                <td><?php echo getHoraEtapaUsuario($usuario->id_usr, 1); ?></td>
                <td><?php echo formataHoras($registro,false); ?></td>
            </tr>
        <?php } ?>
        <?php if ($grade->saida_1) {
            $registro = getRegistroEtapa($usuario->id_usr, date('d/m/Y'), 2); ?>
            <tr>
                <td>2</td>
                <td><?php echo getHoraEtapaUsuario($usuario->id_usr, 2); ?></td>
                <td><?php echo formataHoras($registro,false); ?></td>
            </tr>
        <?php } ?>
        <?php if ($grade->entrada_2) {
            $registro = getRegistroEtapa($usuario->id_usr, date('d/m/Y'), 3); ?>
            <tr>
                <td>3</td>
                <td><?php echo getHoraEtapaUsuario($usuario->id_usr, 3); ?></td>
                <td><?php echo formataHoras($registro,false); ?></td>
            </tr>
        <?php } ?>
        <?php if ($grade->saida_2) {
            $registro = getRegistroEtapa($usuario->id_usr, date('d/m/Y'), 4); ?>
            <tr>
                <td>4</td>
                <td><?php echo getHoraEtapaUsuario($usuario->id_usr, 4); ?></td>
                <td><?php echo formataHoras($registro,false); ?></td>
            </tr>
        <?php } ?>
        <tr>
            <td colspan="3"><h3>Horas Trabalhadas: <?php echo $detalhes['horasAtuais']; ?></h3></td>
        </tr>
    </table>
<?php } else {
    ?>
    <div class="alert alert-danger">
        <h1 class="text-center">Por favor informe seu CPF!</h1>
    </div>
<?php
}
?>
<?php
$html = ob_get_clean();
$out['html'] = $html;
echo json_encode($out);