INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 2', '2', '07:00', '11:00', '12:00', '16:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 3', '2', '07:00', '12:00', '13:00', '17:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 4', '2', '07:00', '12:00', '14:00', '17:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 5', '0', '07:00', '13:00', '', '');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 6', '2', '07:30', '11:30', '13:30', '17:30');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 7', '2', '07:30', '12:00', '13:30', '17:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 8', '2', '07:30', '12:00', '14:00', '17:30');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 9', '2', '07:30', '12:30', '14:00', '17:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 10', '2', '08:00', '11:00', '12:00', '17:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 11', '2', '08:00', '11:00', '13:00', '18:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 12', '2', '08:00', '11:30', '12:30', '17:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 13', '2', '08:00', '11:30', '13:30', '18:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 14', '2', '08:00', '12:00', '13:00', '17:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 15', '2', '08:00', '12:00', '13:30', '17:30');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 16', '2', '08:00', '12:30', '14:30', '18:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 17', '2', '08:00', '13:00', '14:00', '17:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 18', '0', '08:00', '14:00', '', '');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 19', '2', '08:20', '12:00', '13:00', '17:20');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 20', '2', '08:30', '12:00', '13:00', '17:30');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 21', '2', '08:30', '12:00', '13:30', '18:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 22', '2', '08:30', '12:30', '13:30', '17:30');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 23', '2', '08:30', '13:00', '14:00', '17:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 24', '0', '08:30', '14:30', '', '');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 25', '2', '09:00', '11:00', '12:00', '18:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 26', '2', '09:00', '12:00', '13:00', '18:00');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 27', '0', '09:00', '15:00', '', '');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 28', '2', '09:30', '13:30', '14:30', '17:30');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 29', '0', '10:30', '16:30', '', '');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 30', '0', '12:00', '18:00', '', '');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 31', '0', '12:30', '18:30', '', '');
INSERT INTO p_grade (nome_grade, tipo_grade, entrada_1, saida_1, entrada_2, saida_2)
VALUES ('Grade 32', '0', '13:00', '19:00', '', '');

UPDATE p_grade
SET nome_grade = CONCAT(entrada_1, ' - ', saida_1, IF(entrada_2 <> '', CONCAT(', ', entrada_2, ' - ', saida_2), ''));

UPDATE rh_servidor
SET srv_ponto_grade_id = 7
WHERE srv_id IN (21919267115, 82825610100, 53349229115);
UPDATE rh_servidor
SET srv_ponto_grade_id = 8
WHERE srv_id IN (41872002153, 43613730197, 47218312187, 21809518172);
UPDATE rh_servidor
SET srv_ponto_grade_id = 9
WHERE srv_id IN (41322126100, 92612342168, 89167520197, 36952443134, 32425392149, 16630300149);
UPDATE rh_servidor
SET srv_ponto_grade_id = 10
WHERE srv_id IN (27001326100, 32349050106);
UPDATE rh_servidor
SET srv_ponto_grade_id = 11
WHERE srv_id IN (78878802115);
UPDATE rh_servidor
SET srv_ponto_grade_id = 12
WHERE srv_id IN (72791993134, 27799581115, 98004824153, 12407020197, 13666355153, 01931011818);
UPDATE rh_servidor
SET srv_ponto_grade_id = 13
WHERE srv_id IN (05606764172, 49427679115);
UPDATE rh_servidor
SET srv_ponto_grade_id = 14
WHERE srv_id IN (32284080197);
UPDATE rh_servidor
SET srv_ponto_grade_id = 15
WHERE srv_id IN (28148460191, 07089849910);
UPDATE rh_servidor
SET srv_ponto_grade_id = 16
WHERE srv_id IN (13659170100, 13573071104, 36334839187);
UPDATE rh_servidor
SET srv_ponto_grade_id = 17
WHERE srv_id IN (86549375134, 77471067187);
UPDATE rh_servidor
SET srv_ponto_grade_id = 18
WHERE srv_id IN (00359705103);
UPDATE rh_servidor
SET srv_ponto_grade_id = 19
WHERE srv_id IN
      (38078384149, 23179872187, 97161110106, 00497907143, 14496690187, 52712257120, 23431202187, 41253752168, 30762820187, 63448564168, 23507756153, 22922660168, 24353680110, 28271289187, 70735786100, 47674148149, 00995713154, 14787130110, 37029444191, 06083684153, 10045350159, 00269443169, 17008891172, 30707552168, 31108730876, 19643381153, 16635914153, 60364793104);
UPDATE rh_servidor
SET srv_ponto_grade_id = 20
WHERE srv_id IN (12237418187, 43078796191);
UPDATE rh_servidor
SET srv_ponto_grade_id = 21
WHERE srv_id IN (45517827187);
UPDATE rh_servidor
SET srv_ponto_grade_id = 22
WHERE srv_id IN (09577319149);
UPDATE rh_servidor
SET srv_ponto_grade_id = 23
WHERE srv_id IN
      (05109098174, 84691743120, 45417687120, 36380563172, 95054960104, 73464953149, 36995754115, 97478571115, 76109194115, 29533759100, 93342101172, 92962262104, 94807159100, 78652642168, 18547753168, 84989017153, 02483774180, 05436850120, 40209903104, 22924396115, 96189134149, 86537822149, 01099811112, 05825296115, 30894280910, 34818154172, 36353426100, 25117602153, 41252233191, 15433161153, 75461200125, 58705155134, 31085890163, 02925203108, 99503573149, 02325816141, 01069741140, 05418234188, 13688480104, 98102150106, 44216858368, 03224126175, 75449072104, 23637684187, 57549281572, 31984614134, 32098707134, 24214418115, 42238412115, 57562954100, 99427109149, 36985210163, 69219311100, 04183361133, 81739796187, 35461284191, 71000500144, 47623683187, 43601154153, 00036316130, 36017035134, 45705593104, 03995577172, 03920217144, 27960013268, 04369679109, 06679684153, 99162679104, 00350094110, 33218358191, 50862790115, 34924914134, 01636514189, 97395323149, 33335940130, 58964100182, 69202010110, 57569312104);
UPDATE rh_servidor
SET srv_ponto_grade_id = 24
WHERE srv_id IN (02883457115);
UPDATE rh_servidor
SET srv_ponto_grade_id = 25
WHERE srv_id IN (01714519104, 02319922113);
UPDATE rh_servidor
SET srv_ponto_grade_id = 26
WHERE srv_id IN (42226465120);
UPDATE rh_servidor
SET srv_ponto_grade_id = 27
WHERE srv_id IN (08174520104);
UPDATE rh_servidor
SET srv_ponto_grade_id = 28
WHERE srv_id IN (16651278134);
UPDATE rh_servidor
SET srv_ponto_grade_id = 29
WHERE srv_id IN (03320831100);
UPDATE rh_servidor
SET srv_ponto_grade_id = 30
WHERE srv_id IN (99005972149, 02126557103, 99369486100, 02324242176);
UPDATE rh_servidor
SET srv_ponto_grade_id = 31
WHERE srv_id IN (15504441153, 16103041104);
UPDATE rh_servidor
SET srv_ponto_grade_id = 32
WHERE srv_id IN (01210099128);
UPDATE rh_servidor
SET srv_ponto_grade_id = 33
WHERE srv_id IN (13512110568);
UPDATE rh_servidor
SET srv_ponto_grade_id = 34
WHERE srv_id IN (05211873114, 15396290153);
UPDATE rh_servidor
SET srv_ponto_grade_id = 35
WHERE srv_id IN (34891692120);
UPDATE rh_servidor
SET srv_ponto_grade_id = 36
WHERE srv_id IN (46761268168, 03992083136);
UPDATE rh_servidor
SET srv_ponto_grade_id = 37
WHERE srv_id IN (03999015152, 01913543170);