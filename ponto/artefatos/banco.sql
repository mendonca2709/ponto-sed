-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.6.17 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE IF NOT EXISTS `ponto` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ponto`;

-- Copiando estrutura do banco de dados para banco

-- Copiando estrutura para tabela banco.acessos
CREATE TABLE IF NOT EXISTS `acessos` (
  `id_acesso`   BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `data_acesso` DATETIME     NULL     DEFAULT NULL,
  `entrada`     VARCHAR(20)  NULL     DEFAULT NULL,
  `saida`       VARCHAR(20)  NULL     DEFAULT NULL,
  `id_usr`      BIGINT(20)   NULL     DEFAULT NULL,
  `numr_ip`     VARCHAR(200) NULL     DEFAULT NULL,
  `visao`       BIGINT(20)   NOT NULL DEFAULT '0',
  `ho`          BIGINT(20)   NULL     DEFAULT NULL,
  `pma`         BIGINT(20)   NULL     DEFAULT NULL,
  PRIMARY KEY (`id_acesso`),
  KEY `id_acesso` (`id_acesso`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =84597
  DEFAULT CHARSET =utf8;

-- Copiando estrutura para tabela banco.depto
CREATE TABLE IF NOT EXISTS `depto_` (
  `id_depto`            BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `id_superintendencia` BIGINT(20)   NULL     DEFAULT NULL,
  `depto`               VARCHAR(200) NULL     DEFAULT NULL,
  `sigla`               VARCHAR(30)  NULL     DEFAULT NULL,
  `ativo`               BIGINT(20)   NULL     DEFAULT NULL,
  `ramal`               VARCHAR(10)  NULL     DEFAULT NULL,
  PRIMARY KEY (`id_depto`),
  KEY `id_depto` (`id_depto`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =37
  DEFAULT CHARSET =utf8;

-- Copiando estrutura para tabela banco.p_anexos
CREATE TABLE IF NOT EXISTS `p_anexos` (
  `id_anexo`         BIGINT(20) NOT NULL AUTO_INCREMENT,
  `id_usr`           BIGINT(20) NULL     DEFAULT NULL,
  `id_justificativa` BIGINT(20) NULL     DEFAULT NULL,
  `data_anexo`       DATETIME   NULL     DEFAULT NULL,
  `nivel_just`       BIGINT(20) NULL     DEFAULT NULL,
  PRIMARY KEY (`id_anexo`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando dados para a tabela banco.p_anexos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `p_anexos` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_anexos` ENABLE KEYS */;


-- Copiando estrutura para tabela banco.p_eventos
CREATE TABLE IF NOT EXISTS `p_eventos` (
  `id_evento`             BIGINT(20)    NOT NULL AUTO_INCREMENT,
  `data_evento`           DATETIME      NULL     DEFAULT NULL,
  `descricao`             VARCHAR(5000) NULL     DEFAULT NULL,
  `id_depto`              BIGINT(20)    NULL     DEFAULT NULL,
  `id_usr`                BIGINT(20)    NULL     DEFAULT NULL,
  `id_tipo_justificativa` BIGINT(20)    NULL     DEFAULT NULL,
  `data_cadastro`         DATETIME      NULL     DEFAULT NULL,
  `numr_ip`               VARCHAR(200)  NULL     DEFAULT NULL,
  `id_pai`                BIGINT(20)    NULL     DEFAULT NULL,
  PRIMARY KEY (`id_evento`),
  KEY `id_usr` (`id_usr`),
  KEY `data_evento` (`data_evento`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando dados para a tabela banco.p_eventos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `p_eventos` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_eventos` ENABLE KEYS */;


-- Copiando estrutura para tabela banco.p_favoritos
CREATE TABLE IF NOT EXISTS `p_favoritos` (
  `id_favorito`         BIGINT(20)    NOT NULL AUTO_INCREMENT,
  `id_gerencia_dono`    BIGINT(20)    NULL     DEFAULT NULL,
  `titulo_favorito`     VARCHAR(100)  NULL     DEFAULT NULL,
  `id_trabalho`         BIGINT(20)    NULL     DEFAULT NULL,
  `data_favorito`       DATETIME      NULL     DEFAULT NULL,
  `instrucao_sql`       VARCHAR(3000) NULL     DEFAULT NULL,
  `instrucao_sql_total` VARCHAR(3000) NULL     DEFAULT NULL,
  PRIMARY KEY (`id_favorito`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando dados para a tabela banco.p_favoritos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `p_favoritos` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_favoritos` ENABLE KEYS */;


-- Copiando estrutura para tabela banco.p_favoritos_frame
CREATE TABLE IF NOT EXISTS `p_favoritos_frame` (
  `id_favoritos_frame`   BIGINT(20) NOT NULL AUTO_INCREMENT,
  `id_gerencia_dono`     BIGINT(20) NULL     DEFAULT NULL,
  `id_gerencia_favorito` BIGINT(20) NULL     DEFAULT NULL,
  `id_trabalho`          BIGINT(20) NULL     DEFAULT NULL,
  `id_favorito`          BIGINT(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_favoritos_frame`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando dados para a tabela banco.p_favoritos_frame: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `p_favoritos_frame` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_favoritos_frame` ENABLE KEYS */;


-- Copiando estrutura para tabela banco.p_grade
CREATE TABLE IF NOT EXISTS `p_grade` (
  `id_grade`          BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `nome_grade`        VARCHAR(200) NULL     DEFAULT NULL,
  `tipo_grade`        BIGINT(20)   NULL     DEFAULT NULL,
  `data_grade`        DATETIME     NULL     DEFAULT NULL,
  `id_usr_resp_grade` BIGINT(20)   NULL     DEFAULT NULL,
  `entrada_1`         VARCHAR(5)   NULL     DEFAULT NULL,
  `saida_1`           VARCHAR(5)   NULL     DEFAULT NULL,
  `entrada_2`         VARCHAR(5)   NULL     DEFAULT NULL,
  `saida_2`           VARCHAR(5)   NULL     DEFAULT NULL,
  `ativo`             BIGINT(20)   NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_grade`),
  KEY `id_usr_resp_grade` (`id_usr_resp_grade`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando estrutura para tabela banco.p_historico_justificativa
CREATE TABLE IF NOT EXISTS `p_historico_justificativa` (
  `id_justificativa`      BIGINT(20)    NOT NULL AUTO_INCREMENT,
  `id_usr`                BIGINT(20)    NULL     DEFAULT NULL,
  `id_tipo_justificativa` BIGINT(20)    NULL     DEFAULT NULL,
  `data_justificativa`    DATETIME      NULL     DEFAULT NULL,
  `observacoes`           VARCHAR(4000) NULL     DEFAULT NULL,
  `observacoes_chefe`     VARCHAR(4000) NULL     DEFAULT NULL,
  `data_cadastro_just`    DATETIME      NULL     DEFAULT NULL,
  `abonado`               BIGINT(20)    NULL     DEFAULT NULL,
  `id_chefe`              BIGINT(20)    NULL     DEFAULT NULL,
  `data_abono`            DATETIME      NULL     DEFAULT NULL,
  `qtd_horas`             VARCHAR(5)    NULL     DEFAULT NULL,
  `protocolo`             VARCHAR(100)  NULL     DEFAULT NULL,
  `anexo`                 BIGINT(20)    NOT NULL DEFAULT '0',
  `nome_arquivo`          VARCHAR(100)  NULL     DEFAULT NULL,
  `extensao`              VARCHAR(10)   NULL     DEFAULT NULL,
  `who_r`                 BIGINT(20)    NULL     DEFAULT NULL,
  `ordem_justificativa`   BIGINT(20)    NULL     DEFAULT NULL,
  PRIMARY KEY (`id_justificativa`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando dados para a tabela banco.p_historico_justificativa: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `p_historico_justificativa` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_historico_justificativa` ENABLE KEYS */;


-- Copiando estrutura para tabela banco.p_justificativa
CREATE TABLE IF NOT EXISTS `p_justificativa` (
  `id_justificativa`      BIGINT(20)    NOT NULL AUTO_INCREMENT,
  `id_usr`                BIGINT(20)    NULL     DEFAULT NULL,
  `id_tipo_justificativa` BIGINT(20)    NULL     DEFAULT NULL,
  `data_justificativa`    DATETIME      NULL     DEFAULT NULL,
  `observacoes`           VARCHAR(4000) NULL     DEFAULT NULL,
  `observacoes_chefe`     VARCHAR(4000) NULL     DEFAULT NULL,
  `data_cadastro_just`    DATETIME      NULL     DEFAULT NULL,
  `abonado`               BIGINT(20)    NULL     DEFAULT 0,
  `id_chefe`              BIGINT(20)    NULL     DEFAULT NULL,
  `data_abono`            DATETIME      NULL     DEFAULT NULL,
  `qtd_horas`             VARCHAR(10)   NULL     DEFAULT NULL,
  `protocolo`             VARCHAR(100)  NULL     DEFAULT NULL,
  `anexo`                 BIGINT(20)    NOT NULL DEFAULT '0',
  `nome_arquivo`          VARCHAR(100)  NULL     DEFAULT NULL,
  `extensao`              VARCHAR(10)   NULL     DEFAULT NULL,
  `who_r`                 BIGINT(20)    NULL     DEFAULT NULL,
  `ordem_justificativa`   BIGINT(20)    NULL     DEFAULT NULL,
  `decisao`               MEDIUMTEXT    NULL     DEFAULT NULL,
  `data_decisao`          DATETIME      NULL     DEFAULT NULL,
  `id_decisao`            BIGINT(20)    NULL     DEFAULT NULL,
  PRIMARY KEY (`id_justificativa`),
  KEY `data_justificativa` (`data_justificativa`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando dados para a tabela banco.p_justificativa: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `p_justificativa` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_justificativa` ENABLE KEYS */;


-- Copiando estrutura para tabela banco.p_justificativa_historico
CREATE TABLE IF NOT EXISTS `p_justificativa_historico` (
  `id_justificativa`      BIGINT(20)    NOT NULL AUTO_INCREMENT,
  `id_usr`                BIGINT(20)    NULL     DEFAULT NULL,
  `id_tipo_justificativa` BIGINT(20)    NULL     DEFAULT NULL,
  `data_justificativa`    DATETIME      NULL     DEFAULT NULL,
  `observacoes`           VARCHAR(4000) NULL     DEFAULT NULL,
  `observacoes_chefe`     VARCHAR(4000) NULL     DEFAULT NULL,
  `data_cadastro_just`    DATETIME      NULL     DEFAULT NULL,
  `abonado`               BIGINT(20)    NULL     DEFAULT NULL,
  `id_chefe`              BIGINT(20)    NULL     DEFAULT NULL,
  `data_abono`            DATETIME      NULL     DEFAULT NULL,
  `qtd_horas`             VARCHAR(10)   NULL     DEFAULT NULL,
  `protocolo`             VARCHAR(100)  NULL     DEFAULT NULL,
  `anexo`                 BIGINT(20)    NOT NULL DEFAULT '0',
  `nome_arquivo`          VARCHAR(100)  NULL     DEFAULT NULL,
  `extensao`              VARCHAR(10)   NULL     DEFAULT NULL,
  `who_r`                 BIGINT(20)    NULL     DEFAULT NULL,
  `ordem_justificativa`   BIGINT(20)    NULL     DEFAULT NULL,
  `decisao`               VARCHAR(5000) NULL     DEFAULT NULL,
  `data_decisao`          DATETIME      NULL     DEFAULT NULL,
  `id_decisao`            BIGINT(20)    NULL     DEFAULT NULL,
  PRIMARY KEY (`id_justificativa`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando dados para a tabela banco.p_justificativa_historico: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `p_justificativa_historico` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_justificativa_historico` ENABLE KEYS */;


-- Copiando estrutura para tabela banco.p_reenvio
CREATE TABLE IF NOT EXISTS `p_reenvio` (
  `id_reenvio`  BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `email_envio` VARCHAR(200) NULL     DEFAULT NULL,
  `id_gerencia` BIGINT(20)   NULL     DEFAULT NULL,
  `data_envio`  DATETIME     NULL     DEFAULT NULL,
  `id_trabalho` BIGINT(20)   NULL     DEFAULT NULL,
  PRIMARY KEY (`id_reenvio`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando dados para a tabela banco.p_reenvio: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `p_reenvio` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_reenvio` ENABLE KEYS */;


-- Copiando estrutura para tabela banco.p_registro
CREATE TABLE IF NOT EXISTS `p_registro` (
  `id_registro`        BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `id_usr`             BIGINT(20)   NULL     DEFAULT NULL,
  `data_registro`      DATETIME     NULL     DEFAULT NULL,
  `etapa`              BIGINT(20)   NULL     DEFAULT NULL,
  `hora`               VARCHAR(5)   NULL     DEFAULT NULL,
  `minutos`            VARCHAR(5)   NULL     DEFAULT NULL,
  `numr_ip`            VARCHAR(150) NULL     DEFAULT NULL,
  `id_justificativa`   BIGINT(20)   NULL     DEFAULT NULL,
  `nao_registrou`      BIGINT(20)   NOT NULL DEFAULT '0',
  `origem`             VARCHAR(100) NULL     DEFAULT NULL,
  `id_registrou_chefe` BIGINT(20)   NULL     DEFAULT NULL,
  `tsp`                VARCHAR(50)  NULL     DEFAULT NULL,
  PRIMARY KEY (`id_registro`),
  KEY `id_usr` (`id_usr`),
  KEY `id_usr_data_registro` (`id_usr`, `data_registro`),
  KEY `data_registro` (`data_registro`),
  KEY `id_justificativa` (`id_justificativa`),
  KEY `id_registrou_chefe` (`id_registrou_chefe`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando estrutura para tabela banco.p_superintendencia
CREATE TABLE IF NOT EXISTS `p_superintendencia` (
  `id_superintendencia` BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `superintendencia`    VARCHAR(200) NULL     DEFAULT NULL,
  PRIMARY KEY (`id_superintendencia`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando estrutura para tabela banco.p_tentativa
CREATE TABLE IF NOT EXISTS `p_tentativa` (
  `id_tentativa`     BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `id_usr`           BIGINT(20)   NULL     DEFAULT NULL,
  `data_tentativa`   DATETIME     NULL     DEFAULT NULL,
  `tipo_dispositivo` VARCHAR(500) NULL     DEFAULT NULL,
  `numr_ip`          VARCHAR(300) NULL     DEFAULT NULL,
  PRIMARY KEY (`id_tentativa`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando dados para a tabela banco.p_tentativa: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `p_tentativa` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_tentativa` ENABLE KEYS */;


-- Copiando estrutura para tabela banco.p_tipo_justificativa
CREATE TABLE IF NOT EXISTS `p_tipo_justificativa` (
  `id_tipo_justificativa` BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `titulo`                VARCHAR(200) NULL     DEFAULT NULL,
  `cota`                  BIGINT(20)   NULL     DEFAULT NULL,
  `lancamento`            BIGINT(20)   NULL     DEFAULT NULL,
  `homologa`              BIGINT(20)   NULL     DEFAULT NULL,
  `exibe`                 BIGINT(20)   NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_tipo_justificativa`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando dados para a tabela banco.p_tipo_justificativa: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `p_tipo_justificativa` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_tipo_justificativa` ENABLE KEYS */;


-- Copiando estrutura para tabela banco.temporario
CREATE TABLE IF NOT EXISTS `temporario` (
  `id_temp`        BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `nome_arquivo`   VARCHAR(100) NULL     DEFAULT NULL,
  `data_foto`      DATETIME     NULL     DEFAULT NULL,
  `matricula_temp` VARCHAR(200) NULL     DEFAULT NULL,
  PRIMARY KEY (`id_temp`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =148864
  DEFAULT CHARSET =utf8;


-- Copiando estrutura para tabela banco.unidades
CREATE TABLE IF NOT EXISTS `unidades` (
  `id_unidade`    BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `sigla`         VARCHAR(40)  NULL     DEFAULT NULL,
  `nome`          VARCHAR(200) NULL     DEFAULT NULL,
  `titular`       VARCHAR(100) NULL     DEFAULT NULL,
  `ddd`           VARCHAR(5)   NULL     DEFAULT NULL,
  `fone`          VARCHAR(15)  NULL     DEFAULT NULL,
  `email`         VARCHAR(100) NULL     DEFAULT NULL,
  `tipo`          BIGINT(20)   NULL     DEFAULT NULL,
  `id_supervisor` BIGINT(20)   NULL     DEFAULT NULL,
  PRIMARY KEY (`id_unidade`),
  KEY `id_unidade` (`id_unidade`),
  KEY `id_unidade_2` (`id_unidade`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- Copiando dados para a tabela banco.unidades: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;

DROP TABLE IF EXISTS `depto`;
CREATE ALGORITHM = UNDEFINED
  DEFINER =`sicnet`@`%` VIEW `depto` AS
  SELECT
    lot_id                          AS `id_depto`,
    lot_lotacao_superior_lotacao_id AS `id_superintendencia`,
    lot_nome                        AS `depto`,
    lot_nome                        AS `sigla`,
    1                               AS `ativo`,
    '0000'                          AS `ramal`

  FROM `sic_net_loc`.`rh_lotacao`
    INNER JOIN `ponto`.`usuarios` ON `ponto`.`usuarios`.`id_depto` = `sic_net_loc`.`rh_lotacao`.`lot_id`
  WHERE lot_id > 0;


-- Copiando estrutura para view ponto.usuarios
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `usuarios`;
CREATE ALGORITHM = UNDEFINED
  DEFINER =`sicnet`@`%` VIEW `usuarios` AS
  SELECT
    1                                                          AS `empresa_codigo`,
    `sic_net`.`rh_servidor`.`srv_id`                     AS `id_usr`,
    `sic_net`.`rh_servidor`.`srv_matricula_funcional`    AS `matricula`,
    `sic_net`.`com_pessoa`.`p_nome`                      AS `nome`,
    `sic_net`.`rh_cargo`.`cg_nome`                       AS `cargo`,
    `sic_net`.`rh_servidor`.`srv_id`                     AS `srv_id`,
    `sic_net`.`rh_servidor`.`srv_servidor_situacao_id` =
    'AT'                                                       AS `ativo`,
    ''                                                         AS `foto`,
    coalesce(`sic_net`.`rh_servidor`.`srv_ponto_grade_id`,
             1)                                                AS `id_grade`,
    1                                                          AS `registra`,
    coalesce((SELECT `sic_net`.`seg_grupo_usuario`.gu_gr_id = 67
              FROM `sic_net`.`seg_grupo_usuario`
              WHERE `sic_net`.`seg_grupo_usuario`.gu_us_id = rh_servidor.srv_id AND gu_gr_id = 67),
             0)                                                AS `rh`,

    `sic_net`.`seg_usuario`.`us_login_ad`                AS `login`,
    `sic_net`.`seg_usuario`.`us_senha`                   AS `hd_user_passwd`,
#`sic_net`.`seg_usuario`.`us_senha`                            AS `senha`,
    md5(
        123456)                                                AS `senha`,
    `sic_net`.`seg_usuario`.`us_servico_de_autenticacao` AS `us_servico_de_autenticacao`,

#`sic_net`.`rh_lotacao`.`lot_sigla`                            AS `sigla`,
    `sic_net`.`com_pessoa`.`p_email`                     AS email,
    `sic_net`.`rh_lotacao`.`lot_id`                      AS `id_depto`,
    if(
        `sic_net`.`rh_lotacao`.`lot_tipo_estrutura` = 'basica' AND
        `sic_net`.`rh_lotacao`.`lot_responsavel_servidor_id` = `sic_net`.`rh_servidor`.`srv_id`,
        2,
        if(
            `sic_net`.`rh_lotacao`.`lot_responsavel_servidor_id` = `sic_net`.`rh_servidor`.`srv_id`,
            1,
            0
        )
    )                                                          AS `gerente`

  FROM ((`sic_net`.`rh_servidor`
    JOIN `sic_net`.`com_pessoa`
      ON ((`sic_net`.`rh_servidor`.`srv_id` = `sic_net`.`com_pessoa`.`p_id`)))
    JOIN `sic_net`.`rh_lotacao`
      ON ((`sic_net`.`rh_servidor`.`srv_lotacao_id` = `sic_net`.`rh_lotacao`.`lot_id`))
    INNER JOIN `sic_net`.`rh_cargo`
      ON `sic_net`.`rh_cargo`.`cg_id` = `sic_net`.`rh_servidor`.`srv_cargo_id`
#INNER JOIN `banco_dev`.`usuarios_` ON `usuarios_`.`matricula` = `sic_net`.`rh_servidor`.`srv_matricula_funcional`
    INNER JOIN `sic_net`.`seg_usuario`
      ON `sic_net`.`seg_usuario`.`us_id` = `sic_net`.`rh_servidor`.`srv_id`
  )
  WHERE ((`sic_net`.`rh_servidor`.`srv_categoria_id` IS NOT NULL) AND
         (`sic_net`.`rh_servidor`.`srv_escala_id` IS NOT NULL) AND
         `sic_net`.`rh_servidor`.`srv_servidor_situacao_id` IN (
           SELECT `sic_net`.`rh_servidor_situacao`.`sst_id`
           FROM `sic_net`.`rh_servidor_situacao`
           WHERE (`sic_net`.`rh_servidor_situacao`.`sst_em_atividade` <> 0)) AND
         (`sic_net`.`rh_servidor`.`srv_pis_pasep_numero` <> 0) AND
         (`sic_net`.`rh_servidor`.`srv_pis_pasep_numero` IS NOT NULL)
#and (`sic_net`.`rh_servidor`.`tmp_integrar_com_ponto` = 1)
  )
  ORDER BY nome;

INSERT INTO `p_grade` (`id_grade`, `nome_grade`, `tipo_grade`, `data_grade`, `id_usr_resp_grade`, `entrada_1`, `saida_1`, `entrada_2`, `saida_2`, `ativo`)
VALUES (1, 'Grade 1', 0, '2015-01-28 15:41:21', 95567968191, '08:00', '12:00', '14:00', '18:00', 1),
  (2, 'Grade 2', 0, '2015-01-28 15:43:37', 95567968191, '07:00', '11:00', '13:00', '17:00', 1),
  (3, 'Grade 3', 2, '2015-01-28 15:44:00', 95567968191, '08:00', '12:00', '', '', 1),
  (4, 'Grade 4', 2, '2015-01-28 15:44:08', 95567968191, '12:00', '18:00', '', '', 1),
  (5, 'Grade 5', 2, '2015-01-28 15:44:23', 95567968191, '07:00', '13:00', '', '', 1),
  (6, 'Grade 6', 2, '2015-01-28 15:44:32', 95567968191, '14:00', '18:00', '', '', 1);

INSERT INTO p_tipo_justificativa (titulo, homologa, exibe, lancamento)
VALUES
  ('Abono: Falta', 1, 1, 1),
  ('Abono: Atraso/Saída Antecipada', 1, 1, 1),
  ('Atestado Médico', 1, 1, 1),
  ('Corte', 1, 1, 1),
  ('Curso/Treinamento', 1, 1, 1),
  ('Descumprimento da grade de trabalho', 1, 1, 1),
  ('Dispensa coletiva', 1, 1, 1),
  ('Dispensa para estudo', 1, 1, 1),
  ('Dispensa tratamento - CLT', 1, 1, 1),
  ('Doação de sangue', 1, 1, 1),
  ('Folga TRE', 1, 1, 1),
  ('Juri e Serviço Obrigatório', 1, 1, 1),
  ('Problemas Técnicos', 1, 1, 1),
  ('Trabalho Externo', 1, 1, 1),
  ('Viagem a trabalho', 1, 1, 1),
  ('Aposentadoria', 3, 1, 1),
  ('Atestado Médico', 3, 1, 1),
  ('Atestado Médico - Estágio', 3, 1, 1),
  ('Convocação da Escola do Governo', 3, 1, 1),
  ('Curso/Treinamento', 3, 1, 1),
  ('Desligamento', 3, 1, 1),
  ('Dias não trabalhados (Novo Servidor)', 3, 1, 1),
  ('Dispensa', 3, 1, 1),
  ('Dispanesa Artigo 268', 3, 1, 1),
  ('Dispensa Artigo 326', 3, 1, 1),
  ('Dispensa Coletiva', 3, 1, 1),
  ('Dispensa Tratamento - CLT', 3, 1, 1),
  ('Disposição', 3, 1, 1),
  ('Disposição (Quando sai da folha)', 3, 1, 1),
  ('Disposição (Colocação com ônus)', 3, 1, 1),
  ('Disposição do Gabinete', 3, 1, 1),
  ('Exoneração', 3, 1, 1),
  ('Falta', 3, 1, 1),
  ('Feriado', 3, 1, 1),
  ('Férias', 3, 1, 1),
  ('Júri e Outros Serviços Obrigatórios (Artigo 35)', 3, 1, 1),
  ('Licença', 3, 1, 1),
  ('Perícia Médica Agendada - Aguardando resultado', 3, 1, 1),
  ('Ponto Cortado', 3, 1, 1),
  ('Ponto Facultativo', 3, 1, 1),
  ('Recesso', 3, 1, 1),
  ('Recisão Contratual', 3, 1, 1),
  ('Servidor com Exercício em Outro Órgão', 3, 1, 1),
  ('Servidor não Cadastrado - Aguardando ato formal de entrada', 3, 1, 1),
  ('Superintendente em Exercício', 3, 1, 1);
