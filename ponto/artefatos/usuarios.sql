SELECT
  `sic_net`.`rh_servidor`.`srv_id`                                                                   AS `id_usr`,
  1                                                                                                  AS `empresa_codigo`,
  `sic_net`.`seg_usuario`.`us_login`                                                                 AS `matricula`,
  `sic_net`.`com_pessoa`.`p_nome`                                                                    AS `nome`,
  `sic_net`.`rh_cargo`.`cg_nome`                                                                     AS `cargo`,
  `sic_net`.`rh_servidor`.`srv_id`                                                                   AS `srv_id`,
  (`sic_net`.`rh_servidor`.`srv_servidor_situacao_id` = 'AT')                                        AS `ativo`,
  concat(`sic_net`.`seg_usuario`.`us_login`, '.jpg')                                                 AS `foto`,
  COALESCE(`sic_net`.`rh_servidor`.`srv_ponto_grade_id`, 1)                                          AS `id_grade`,
  `sic_net`.`rh_servidor`.`srv_registra_ponto_web`                                                   AS `registra`,
  COALESCE((SELECT `sic_net`.`seg_grupo_usuario`.gu_gr_id = 67
            FROM `sic_net`.`seg_grupo_usuario`
            WHERE `sic_net`.`seg_grupo_usuario`.gu_us_id = rh_servidor.srv_id AND gu_gr_id = 67), 0) AS `rh`,
  `sic_net`.`seg_usuario`.`us_login_ad`                                                              AS `login`,
  `sic_net`.`seg_usuario`.`us_senha`                                                                 AS `hd_user_passwd`,
  MD5(123456)                                                                                        AS `senha`,
  `sic_net`.`seg_usuario`.`us_servico_de_autenticacao`                                               AS `us_servico_de_autenticacao`,
  `sic_net`.`com_pessoa`.`p_email`                                                                   AS email,
  `sic_net`.`rh_lotacao`.`lot_id`                                                                    AS `id_depto`,
  IF(`sic_net`.`rh_lotacao`.`lot_tipo_estrutura` = 'basica' AND
     `sic_net`.`rh_lotacao`.`lot_responsavel_servidor_id` = `sic_net`.`rh_servidor`.`srv_id`, 2,
     IF(`sic_net`.`rh_lotacao`.`lot_responsavel_servidor_id` = `sic_net`.`rh_servidor`.`srv_id`,
        1,
        0
     )
  )                                                                                                  AS `gerente`,
  IF(ISNULL(`sic_net`.`rh_lotacao`.`lot_responsavel_servidor_id`) OR `sic_net`.`rh_servidor`.srv_id = `sic_net`.`rh_lotacao`.`lot_responsavel_servidor_id`,
	  CASE ISNULL(lot_superior.`lot_responsavel_servidor_id`)
		WHEN true THEN (SELECT lot_acimadDaSuperior.`lot_responsavel_servidor_id` FROM  `sic_net`.`rh_lotacao` AS lot_acimadDaSuperior WHERE lot_acimadDaSuperior.`lot_id`= lot_superior.`lot_lotacao_superior_lotacao_id` LIMIT 1)
		ELSE lot_superior.`lot_responsavel_servidor_id`
	  END
	, `sic_net`.`rh_lotacao`.`lot_responsavel_servidor_id`)     									AS id_chefe
FROM `sic_net`.`rh_servidor` INNER JOIN `sic_net`.`com_pessoa`  				ON `sic_net`.`rh_servidor`.`srv_id` = `sic_net`.`com_pessoa`.`p_id`
							 INNER JOIN `sic_net`.`rh_lotacao`  				ON `sic_net`.`rh_servidor`.`srv_lotacao_id` = `sic_net`.`rh_lotacao`.`lot_id`
							 INNER JOIN `sic_net`.`rh_cargo`    				ON `sic_net`.`rh_cargo`.`cg_id` = `sic_net`.`rh_servidor`.`srv_cargo_id`
							 INNER JOIN `sic_net`.`seg_usuario` 				ON `sic_net`.`seg_usuario`.`us_id` = `sic_net`.`rh_servidor`.`srv_id`
                             INNER JOIN `sic_net`.`rh_servidor_situacao`        ON `sic_net`.`rh_servidor_situacao`.`sst_id` = `sic_net`.`rh_servidor`.`srv_servidor_situacao_id`
							 LEFT  JOIN `sic_net`.`rh_lotacao` AS lot_superior  ON lot_superior.`lot_id`= `sic_net`.`rh_lotacao`.`lot_lotacao_superior_lotacao_id`
WHERE
  `sic_net`.`rh_lotacao`.`lot_orgao_id` = 21652711000110 AND
  `sic_net`.`rh_servidor`.`srv_registra_ponto_web` = true AND
  `sic_net`.`rh_servidor_situacao`.`sst_em_atividade` <> 0
ORDER BY `sic_net`.`com_pessoa`.`p_nome`