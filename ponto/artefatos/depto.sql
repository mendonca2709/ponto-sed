SELECT DISTINCT
  lot_id                          AS `id_depto`,
  lot_lotacao_superior_lotacao_id AS `id_superintendencia`,
  lot_nome                        AS `depto`,
  lot_nome                        AS `sigla`,
  1                               AS `ativo`,
  '0000'                          AS `ramal`

FROM `sic_net`.`rh_lotacao`
  INNER JOIN `ponto`.`usuarios` ON `ponto`.`usuarios`.`id_depto` = `sic_net`.`rh_lotacao`.`lot_id`
WHERE lot_id > 0 AND lot_data_inativacao IS NULL