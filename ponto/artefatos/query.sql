SELECT CONCAT(CAST((SUM(SUBSTRING_INDEX(SUBSTRING_INDEX(qtd_horas, ':', 1), ':', 1)) +
                    (SUM(SUBSTRING_INDEX(SUBSTRING_INDEX(qtd_horas, ':', -1), ':', 1)) / 60)) AS UNSIGNED), ':', .6 *
                                                                                                                 SUBSTRING_INDEX(
                                                                                                                     SUBSTRING_INDEX(
                                                                                                                         (

                                                                                                                           SUM(
                                                                                                                               SUBSTRING_INDEX(
                                                                                                                                   SUBSTRING_INDEX(
                                                                                                                                       qtd_horas,
                                                                                                                                       ':',
                                                                                                                                       -1),
                                                                                                                                   ':',
                                                                                                                                   1))
                                                                                                                           /
                                                                                                                           60),
                                                                                                                         '.',
                                                                                                                         -1),
                                                                                                                     '.',
                                                                                                                     1))
FROM p_justificativa
  INNER JOIN usuarios ON p_justificativa.id_usr = usuarios.id_usr
  INNER JOIN depto ON usuarios.id_depto = depto.id_depto
WHERE abonado = 0 AND MONTH(data_justificativa) = 01 AND YEAR(data_justificativa) = 2015 AND registra = 1
GROUP BY data_justificativa, p_justificativa.id_usr
ORDER BY id_justificativa DESC
LIMIT 0, 40