INSERT INTO p_tipo_justificativa (titulo, homologa, exibe, lancamento)
VALUES
  ('Abono: Falta', 1, 1, 1),
  ('Abono: Atraso/Saída Antecipada', 1, 1, 1),
  ('Atestado Médico', 1, 1, 1),
  ('Corte', 1, 1, 1),
  ('Curso/Treinamento', 1, 1, 1),
  ('Descumprimento da grade de trabalho', 1, 1, 1),
  ('Dispensa coletiva', 1, 1, 1),
  ('Dispensa para estudo', 1, 1, 1),
  ('Dispensa tratamento - CLT', 1, 1, 1),
  ('Doação de sangue', 1, 1, 1),
  ('Folga TRE', 1, 1, 1),
  ('Juri e Serviço Obrigatório', 1, 1, 1),
  ('Problemas Técnicos', 1, 1, 1),
  ('Trabalho Externo', 1, 1, 1),
  ('Viagem a trabalho', 1, 1, 1)