<?php

/**
 * Created by PhpStorm.
 * User: TMP-mmendonca
 * Date: 11/03/2015
 * Time: 10:49
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/funcoes/funcoes.php';

class BaseObject
{
    const JUSTIFICATIVA = 'p_justificativa';
    const REGISTRO = 'p_registro';
    const USUARIO = 'usuarios';
    const TIPO_JUSTIFICATIVA = 'p_tipo_justificativa';
    const DEPARTAMENTO = 'depto';
    const OCORRENCIA = 'ocorrencias';
    const IPS_AUTORIZADOS = 'ips_autorizados';

    //tempos limite do ponto em minutos
    const TEMPO_MINIMO_ALMOCO = 60;
    const TEMPO_MINIMO_DIFERENCA_REGISTRO_ANTERIOR = 15;
    const TOLERANCIA = 30;
    const TOLERANCIA_ALMOCO = 30;
    const TOLERANCIA_DIARIA_MAIS = 30;
    const TOLERANCIA_DIARIA_MENOS = -15;
    const COTA_ABONOS_HORAS = 24;

    //ids dos tipos de justificativas usados
    const ID_DESCUMPRIMENTO_DE_GRADE = 6;
    const ID_HORAS_NAO_AUTORIZADAS = 65;

    public function getRegistro()
    {
        switch ($this->__info['type']) {
            case self::JUSTIFICATIVA:
                return R::findOne(self::REGISTRO, "id_justificativa = '{$this->id_justificativa}'");
        }
    }

    public function getUsuario()
    {
        return getUsuario($this->id_usr);
    }

    public function getGrade()
    {
        return getGradeUsuario($this->id_usr);
    }
	
	public function getDescricaoGrade(){

        $grade = $this->getGrade();
        return $grade->nome_grade;

    }

    public function getChefe()
    {
        return R::findOne(BaseObject::USUARIO, "id_depto = '{$this->id_depto}'");
    }

    public function getResumoJustificativasRelatorio($data)
    {
        $out = [];
        foreach (getJustificativasRelatorio() as $justificativa) {
            $horas = getHorasAbonadasPorTipoJustificativa($this->id_usr, $justificativa->id_tipo_justificativa, $data);
            if ($horas >= 1)
                $out[$justificativa->titulo] = $horas;
        }
        return $out;
    }

    public function getDepartamento()
    {
        return R::findOne(self::DEPARTAMENTO, "id_depto = {$this->id_depto}");
    }

    public function getAbonosDoDia($data, $options = [])
    {
        if (in_array('relatorio', $options)) {
            return getAbonosRelatorio($this->id_usr, $data);
        }
        return getAbonos($this->id_usr, $data);
    }

    public function getTipoJustificativa()
    {
        return getTipoJustificativa($this->id_tipo_justificativa);
    }

    /**
     * @param $dataInicio
     * @param null $dataFim
     * @return bool
     *
     * verifico se para o período informado existe hora extra ( horas trabalhadas > horas grade )
     * ou justificativas nao abonadas
     *
     * @edit: Nao existem recursos para solucionar problemas de hora extra. nao faço essa validação - marcelo-tm 25/03/2015
     *
     */
    public function temOcorrenciasPendentes($dataInicio = null, $dataFim = null)
    {
        $res = $this->getOcorrenciasPendentes($dataInicio, $dataFim);

        return !empty($res);
    }

    public function getOcorrenciasPendentes($dataInicio = null, $dataFim = null)
    {
        $out = ['ocorrencias' => [], 'horas' => 0];
        $totalMinutos = 0;
        $where = "true ";

        if (empty($dataFim) && $dataInicio) {
            $dataFim = $dataInicio;
        }

        if ($dataInicio) {
            $oDataInicio = new DateTime(mudaData($dataInicio));
            $oDataFim = new DateTime(mudaData($dataFim));
            $where .= "and data_justificativa between
            '{$oDataInicio->format("Y-m-d")} 00:00:00' and '{$oDataFim->format("Y-m-d")} 23:59:59'";
        }

        $res = R::findAll(self::JUSTIFICATIVA, "{$where} and id_usr = '{$this->id_usr}' and abonado = 0");

        foreach ($res as $justificativa) {
            $out['ocorrencias'][] = $justificativa;
            $totalMinutos += getMinutos($justificativa->qtd_horas);
        }
        $out['horas'] = getHoras($totalMinutos);
        return $out;
    }

    public function getMeusDepartamentosArray()
    {
        $out = [];
        foreach ($this->getMeusDepartamentos() as $bean) {
            $out[$bean->id_depto] = $bean->depto;
        }
        return $out;
    }

    public function getMeusDepartamentos()
    {
        $where = "true ";
        if (!$this->rh) {
            $where .= " and usuarios.id_chefe = {$this->id_usr}";
        }
        $res = R::findAll(self::DEPARTAMENTO, "INNER JOIN
                usuarios ON usuarios.id_depto = depto.id_depto
                WHERE {$where} order by depto.depto");
        return $res;
    }

    public function getMeusServidoresArray($id_depto = null, $bypassRh = false)
    {
        $out = [];
        foreach ($this->getMeusServidores($id_depto, $bypassRh) as $bean) {
            $out[$bean->id_usr] = $bean->nome;
        }
        return $out;
    }

    public function getMeusServidores($id_depto = null, $bypassRh = false)
    {
        $where = 'true';
        if ($id_depto) {
            $where .= " and id_depto = '{$id_depto}'";
        }
        if (!$this->rh || $bypassRh) {
            $where .= " and id_chefe = {$this->id_usr}";
        }
        $res = R::findAll(self::USUARIO, "({$where}) or id_usr = '{$this->id_usr}'  order by nome");
        return $res;
    }

    public function descumpriuJornada($data)
    {
        $minutosGrade = getMinutos(getHorasGrade($this->id_grade));
        $minutosTrabalhados = getMinutos(getHorasTrabalhadas($this->id_usr, $data));
        $diff = $minutosTrabalhados - $minutosGrade;
        if ($diff < BaseObject::TOLERANCIA_DIARIA_MENOS || $diff > BaseObject::TOLERANCIA_DIARIA_MAIS) {
            return true;
        }
    }

    public function temHoraNaoAutorizada($data)
    {
        $_data = new DateTime(mudaData($data));
        $minutosGrade = getMinutos(getHorasGrade($this->id_grade));
        $minutosTrabalhados = getMinutos(getHorasTrabalhadas($this->id_usr, $_data->format('d/m/Y')));
        $minutosExtra = $minutosTrabalhados - $minutosGrade;
        return ($minutosExtra > BaseObject::TOLERANCIA_DIARIA_MAIS);
    }

    public function getHorasAbonosComCota($data)
    {
        $_data = new DateTime(mudaData($data));
        $res = R::findAll(self::JUSTIFICATIVA, "INNER JOIN p_tipo_justificativa ON p_justificativa.id_tipo_justificativa = p_tipo_justificativa.id_tipo_justificativa
        WHERE p_tipo_justificativa.cota = 1 AND abonado = 1 AND data_justificativa LIKE '{$_data->format("Y-m-d")}%' AND p_justificativa.id_usr = '{$this->id_usr}'");
        $minutos = 0;
        foreach ($res as $bean) {
            $minutos += getMinutos($bean->qtd_horas);
        }
        return getHoras($minutos);
    }
}