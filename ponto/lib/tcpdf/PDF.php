<?php
require_once 'tcpdf.php';

/**
 * Created by PhpStorm.
 * User: TMP-mmendonca
 * Date: 26/02/2015
 * Time: 09:42
 */
class PDF extends TCPDF
{
    public function Output($name, $dest)
    {
        $this->setTextShadow(['enabled' => false]);
        parent::Output($name, $dest);
    }

//Page header
    public function Header()
    {
        // Logo
        $image_file = dirname(__FILE__) . '/../../images/brasao-goias_p.png';
        $table = <<<EOT
<table>
<tr>
<td style="width:10%"><img src="{$image_file}" style="height:80" /></td>
<th style="width:90%"><br /><h3>Secretaria de Desenvolvimento Econômico, Científico e Tecnológico, de Agricultura, Pecuária e Irrigação</h3></th>
</tr>
</table>
EOT;

        $this->WriteHTML($table);
    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }

}

/**
 * $bMargin = $this->getBreakMargin();
 * $auto_page_break = $this->AutoPageBreak;
 * $this->SetAutoPageBreak(false, 0);
 * $image_file = dirname(__FILE__) . "/../../images/fundo_{$this->CurOrientation}.JPG";
 * $this->Image($image_file, 0, 0, 297, 210, 'JPG', '', '', false, 300, '', false, false, 0);
 * $this->SetAutoPageBreak($auto_page_break, $bMargin);
 * $this->setPageMark();
 */