<meta charset="utf-8"/>
<meta http-equiv="Content-Script-Type" content="text/javascript">
<!--    <link rel="stylesheet" type="text/css" media="screen" href="./css/estilos.css">-->
<link rel="stylesheet" type="text/css" media="screen" href="./js/jquery-ui-1.11.3.custom/jquery-ui.min.css">
<script language="JavaScript" src="./js/jquery-2.1.3.min.js"></script>
<script language="JavaScript" src="./js/webcamjs/webcam.min.js"></script>
<script language="JavaScript" src="./funcoes/funcao.js"></script>
<link rel="shortcut icon" href="./login_files/bandeira_de_goias.gif" type="image/x-icon">
<link rel="stylesheet" href="./css/bootstrap-3.3.2-dist/css/bootstrap.min.css"/>
<link rel="stylesheet" href="./css/bootstrap-3.3.2-dist/css/bootstrap-theme.min.css"/>

<!doctype html>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>WebcamJS Test Page</title>
    <style type="text/css">
        body {
            font-family: Helvetica, sans-serif;
        }

        h2, h3 {
            margin-top: 0;
        }

        form {
            margin-top: 15px;
        }

        form > input {
            margin-right: 15px;
        }

        #results {
            float: right;
            margin: 20px;
            padding: 20px;
            border: 1px solid;
            background: #ccc;
        }
    </style>
</head>
<body>
<div id="results">Your captured image will appear here...</div>

<h1>WebcamJS Test Page</h1>

<h3>Demonstrates 240x240 cropped capture &amp; display</h3>

<div class="container">
    <div id="my_camera"></div>
</div>
<!-- First, include the Webcam.js JavaScript Library -->

<!-- Configure a few settings and attach camera -->
<script language="JavaScript">
    Webcam.set({
        // live preview size
        width: 640,
        height: 480,

        // device capture size
        dest_width: 640,
        dest_height: 480,

        // final cropped size
        crop_width: 240,
        crop_height: 240,

        // format and quality
        image_format: 'jpeg',
        jpeg_quality: 90
    });

    Webcam.attach('#my_camera');
</script>

<!-- A button for taking snaps -->
<form>
    <input type=button value="Take Snapshot" onClick="take_snapshot()">
</form>

<!-- Code to handle taking the snapshot and displaying it locally -->
<script language="JavaScript">
    function take_snapshot() {
        // take snapshot and get image data
        Webcam.snap(function (data_uri) {
            // display results in page
            document.getElementById('results').innerHTML =
                '<h2>Here is your image:</h2>' +
                '<img src="' + data_uri + '"/>';
        });
    }
</script>

</body>
</html>
