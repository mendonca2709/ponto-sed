﻿<?php session_start(); ?>
<head><title>Registro de Ponto</title>


    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/estilos.css">


</head>
<body onLoad="fncAtualiza();">


<script language="JavaScript" type="text/javascript">

    function fncAtualiza() {
        document.registro_ponto.tecla.focus();
        setTimeout("fncChama()", 6000);
    }
    function fncChama() {
        window.location.href = 'index.php';
    }

    function fncEnter() {
        window.location.href = 'index.php';
    }

</script>
<BR> <BR> <BR>

<form action="frm_registra_ponto.php" method="post" name="registro_ponto" id="loginForm" target="Verifica"
      onsubmit="return fncValida();">
    <table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="680" height="80" background="./images/header.jpg">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="93%"><strong><font color="#333333" size="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Registro
                                                de Ponto - SED</font></strong></td>
                                    <td width="7%" rowspan="2" valign="bottom">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="bottom">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><font
                                                color="#666666" size="1">&nbsp;
                                                <?php

                                                $hoje = getdate();

                                                switch ($hoje['wday']) {
                                                    case 0:
                                                        echo "Domingo, ";
                                                        break;
                                                    case 1:
                                                        echo "Segunda-Feira, ";
                                                        break;
                                                    case 2:
                                                        echo "Terça-Feira, ";
                                                        break;
                                                    case 3:
                                                        echo "Quarta-Feira, ";
                                                        break;
                                                    case 4:
                                                        echo "Quinta-Feira, ";
                                                        break;
                                                    case 5:
                                                        echo "Sexta-Feira, ";
                                                        break;
                                                    case 6:
                                                        echo "Sábado, ";
                                                        break;
                                                }

                                                echo $hoje['mday'];
                                                switch ($hoje['mon']) {
                                                    case 1:
                                                        echo " de Janeiro de ";
                                                        break;
                                                    case 2:
                                                        echo " de Fevereiro de ";
                                                        break;
                                                    case 3:
                                                        echo " de Março de ";
                                                        break;
                                                    case 4:
                                                        echo " de Abril de ";
                                                        break;
                                                    case 5:
                                                        echo " de Maio de ";
                                                        break;
                                                    case 6:
                                                        echo " de Junho de ";
                                                        break;
                                                    case 7:
                                                        echo " de Julho de ";
                                                        break;
                                                    case 8:
                                                        echo " de Agosto de ";
                                                        break;
                                                    case 9:
                                                        echo " de Setembro de ";
                                                        break;
                                                    case 10:
                                                        echo " de Outubro de ";
                                                        break;
                                                    case 11:
                                                        echo "de Novembro de ";
                                                        break;
                                                    case 12:
                                                        echo " de Dezembro de ";
                                                        break;
                                                }

                                                echo $hoje['year'] . ".";
                                                ?>
                                            </font></strong></td>
                                </tr>
                            </table>

                        </td>
                        <td width="20" background="./images/header_rightcap.jpg">&nbsp;</td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="880" background="./images/contentshadow.gif" height="2"></td>
                    </tr>
                </table>
                <table width="699" height="13" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="3" height="5"></td>
                        <td width="12" height="4" background="./images/rightside.gif"></td>
                        <td width="890" valign="top" bgcolor="#FFFFFF"><BR>

                            <p>&nbsp;</p>

                            <p>&nbsp; </p>

                            <table width="593" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="165"><img src="./images/Warning.jpg" width="165" height="150"></td>
                                    <td width="428" valign="bottom">
                                        <table width="398" border="0" align="center" cellpadding="0" cellspacing="0"
                                               class="Tabela_rel">
                                            <tr valign="bottom">
                                                <td height="25" class="Sub_titulo_rel">&nbsp;&nbsp;Autenticação
                                                </td>
                                            </tr>
                                            <tr class="Itens_rel_s_traco">
                                                <td height="150"><br>
                                                    <table width="95%" border="0" align="center" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td width="71%" height="30"><strong><font color="#FF3300"
                                                                                                      size="4">Horário
                                                                        não permitido!</font><font color="#FF3300"
                                                                                                   size="3"><br>
                                                                        <font color="#000000" size="2">Tentativa de
                                                                            registro
                                                                            em horário fora da sua grade.</font></font></strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="30">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                    <table width="34%" border="0" align="center" cellpadding="0"
                                                           cellspacing="0">
                                                        <tr>
                                                            <td width="71%" valign="top">
                                                                <div id="resultado1" class="tryit"
                                                                     onClick="window.location.href='index.php';">
                                                                    <div align="center">Voltar</div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                </td>
                                            </tr>
                                            <tr class="Sub_titulo_rel">
                                                <td height="15">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <p>&nbsp;</p>

                            <p>&nbsp;</p>

                            <p>&nbsp;</p></td>
                        <td width="4" valign="top" background="./images/sidebar.gif"><br>
                        </td>
                    </tr>
                </table>
                <table width="700" height="59" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="1" colspan="2"></td>
                    </tr>
                    <tr>
                        <td width="596" height="58" class="Fundo_caixa_jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SEDNET
                            2012 - SED&reg;</td>
                        <td width="2" class="Fundo_caixa_canto_jpg"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">&nbsp;</td>
        </tr>
    </table>
    <p>&nbsp;</p>

    <p>
        <input name="tecla" type="text" onKeyPress="fncEnter();" size="1" maxlength="1" style="width:1px"
               readonly="yes">
    </p>
</form>
<BR>
<iframe width="500" height="500" name="Verifica" style="display:none"></iframe>
</body>

</html>